#!/usr/bin/env bash

# domain=$1
domain=eidoom.duckdns.org

echo Syncing to remote $domain
echo

mv config.toml orig
echo baseurl = \"https://computing-blog.$domain/\" > config.toml
tail -n +2 orig >> config.toml

hugo

rsync --archive --human-readable --compress --partial --info=progress2 --delete public/ $domain:/var/www/computing-blog

mv orig config.toml

{
  description = "A Hugo environment for Linux";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
  };

  outputs = { nixpkgs, ... }: let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in {
    devShells."${system}".default = with pkgs; mkShell {
      packages = [
        hugo
      ];
    };
  };
}

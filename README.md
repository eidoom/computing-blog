# [computing-blog](https://gitlab.com/eidoom/computing-blog)

[Live](https://eidoom.gitlab.io/computing-blog/)

## Installing dependencies for building locally

Download with

```shell
cd ~/git
git clone --recurse-submodules git@gitlab.com:eidoom/computing-blog.git
```

then

```shell
sudo dnf install npm
cd ~/git/computing-blog
npm install
mv node_modules static
```

Build with `hugo`, or run development server (including drafts and accessible over LAN) with

```shell
hugo server -D --bind=<host ip> --baseURL=http://<host ip>:1313
```

## TODO

-   Add search bar: <https://gist.github.com/sebz/efddfc8fdcb6b480f567>
    -   see dev branch
-   Make titles smaller in lists?
-   Add very short description metadata to use in next post buttons?
-   Improve styling
    -   make fancy terminal input/output code blocks
-   Use `<kbd>...</kbd>` + appropriate CSS for key strokes
-   Should have use LFS for media files
-   Remove Disqus

## Post ideas

-   deb2rpm with `alien`: `sudo alien -r PKG.deb` then `sudo dnf install PKG.rpm`

## Deployment

### GitLab Pages

See `.gitlab-ci.yml`.

### rsync

On remote, 

```shell
sudo usermod -aG <user> www-data
cd /var/www
sudo mkdir computing-blog
sudo chown www-data:www-data computing-blog
sudo chmod g+w computing-blog
```

On local,

```shell
cd ~/git/computing-blog
./deploy.bash <remote>
```

The reported failure to chgrp on the root directory is fine, we don't want to anyway, but I don't know how to tell rsync not to try.

Then to serve with nginx on a subdomain, set the CNAME record, make `/etc/nginx/sites-available/computing-blog` (eg non-TLS setup):

```nginx
server {
    listen 80;

    server_name computing-blog.<remote>;

    root /var/www/computing-blog;

    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}
```

and

```shell
cd /etc/nginx/sites-enabled
ln -s ../sites-available/computing-blog .
sudo systemctl restart nginx
```

then browse `http://computing-blog.<domain>/`.

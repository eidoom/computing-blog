#!/usr/bin/env bash

if [ $# -eq 0 ]; then
	message="^content"
else
	message=$1
fi

git pull
git add .
git commit -m "$message"
git push

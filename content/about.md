+++
title = "About"
date = "2019-12-20"
edit = 2020-01-16
author = "Ryan Moodie"
layout = "single"
type = "about"
+++

## Who

Hello, I'm Ryan. [Here](https://eidoom.gitlab.io) is my landing page.

## What

This a blog detailing some stuff I've done with computers.

The content is under the licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

## How

See [this post]({{< ref "post/meta" >}}) for information about how I build this site.

The cover images were made as discussed in [this post]({{< ref "post/stable-diffusion" >}}) and [this post]({{< ref "post/sd2" >}}) and are in the public domain ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)).

## Why

I decided to start recording some of the various tasks and projects I do because of the great number of such things I have done in the past and forgotten.
I've recently found it frustrating when 

* figuring out a problem (i.e. mainly searching the Internet for a solution) in the knowledge that I've done very similar things before, but I don't remember how
* maintaining some aspect of my computer ecosystem would be so much easier if I could remember how I had set it, or some conflict, up in the first place

I also think that writing up a project such that someone else can read it is an excellent way for me to test and reinforce my learning.

I already had some text files in a `gist` folder in my `$HOME` and some scripts lying around to automate tasks, all part of a `git` repository with a remote that I could access online in the likely case that I lost local access somehow, but I wanted something more aesthetic.
I'm certainly no expert in most of the topics touched on in this blog, but as a proponent of open knowledge and liking the idea of sharing my experiences, this website seemed the obvious solution.


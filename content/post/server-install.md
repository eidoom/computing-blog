+++
title = "A fresh home server I"
date = 2020-06-05T12:16:07+01:00
categories = ["home","operating-system","software"]
tags = ["debian","server","parted","firmware","flash","uefi","bios","install","docker","virtualisation","apt","mergerfs","fstab","drivers","ssh","ethernet","union-filesystem","fuse","debian-testing","sudo","lan","linux","update","systemd","environment","container","microcode","automate","lvm","cli","terminal","bash","shell","alias","home-server","debian-buster","debian-10","dd","debian-backports"]
description = "Setting up my home server again"
toc = true
cover = "img/covers/7.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

DEPRECATED: see [new setup]({{< ref "cloud" >}}).

*This post is split into two entries: this, part one, is about setting up the server's operating system.
[Part two]({{< ref server-install-containers >}}) is about using containers to run software on the server.*

After four years and four months of all-hours service, the boot drive in my home server bit the dust.
It was a 64GB mSATA mini SSD, all of 30mm in height, mounted vertically in a mPCIe slot.
I had a spare SATA SSD lying around that I could use in lieu of the old drive, so it was no big disaster.
In fact, it provided the perfect excuse to rebuild and improve my home server stack.

The old system was quite traditional Linux: [Debian](https://en.wikipedia.org/wiki/Debian) for the distro, [systemd](https://en.wikipedia.org/wiki/Systemd) for service management, most packages looked after by [apt](https://en.wikipedia.org/wiki/APT_(software)), the rest installed from Git([Hub](https://en.wikipedia.org/wiki/GitHub)/[Lab](https://en.wikipedia.org/wiki/GitLab)) releases.
This time, I've decided to employ [containers](https://en.wikipedia.org/wiki/OS-level_virtualization).
This is more to explore and learn the technology rather than for any particular benefit.
I'll use [Docker](https://www.docker.com/) {{< wiki "docker_(software)" >}} {{< arch "Docker" >}} as the {{< wl "Linux container" List_of_Linux_containers >}} engine, [Docker Compose](https://docs.docker.com/compose/) as the local [container orchestrator](https://www.redhat.com/en/topics/containers/what-is-container-orchestration) {{< wiki "Orchestration_(computing)" >}}, and [Docker Hub](https://hub.docker.com/) as the remote repository for {{< wl "OCI images" "Open_Container_Initiative" >}}.

I considered using a [container-based distro](https://distrowatch.com/search.php?category=Docker#simple) like [RancherOS](https://rancher.com/rancher-os/) or [Fedora CoreOS](https://getfedora.org/coreos?stream=stable), but ended up deciding on beloved Debian so I can work from a familiar base.
I will use containers for packages providing a service over the home network and manage them with Docker Compose; more on that ~~[later]({{< ref "#containers" >}})~~ in [part two]({{< ref server-install-containers >}}).

Another new (for me) technology I was excited to include this time was a [union filesystem](https://en.wikipedia.org/wiki/Union_mount) so I'd no longer have to think about which disk a file was on. I opted for [MergerFS](http://spawn.link/mergerfs/). I set that up [here]({{< ref "#union-filesystem" >}}).

I've also arranged this post in increasing order of abstraction for your pleasure.

## Hardware
The current list of hardware is:

Component|Quantity|Product|Purchase date
---|---|---|---
Motherboard|1| [ASRock H170M-ITX/DL](https://www.asrock.com/mb/Intel/H170M-ITXDL/)|20/1/2016
CPU|1| [Intel Pentium G4400](https://ark.intel.com/content/www/us/en/ark/products/88179/intel-pentium-processor-g4400-3m-cache-3-30-ghz.html)|20/1/2016
RAM|1|[Crucial 8GB DDR4-2133 CL15 CT8G4DFD8213](https://uk.pcpartpicker.com/product/xvLypg/crucial-memory-ct8g4dfd82103)|20/1/2016
PSU|1|[Silverstone ST30SF Strider SFX 300W 80+ Bronze](https://www.silverstonetek.com/product.php?pid=458)|20/1/2016
Chassis|1|[Lian Li PC-Q08B Black](https://pcpartpicker.com/product/kZyFf7/lian-li-case-pcq08b)|20/1/16
Boot disk|1|[SanDisk SSD PLUS 120GB 2.5" SATA3](https://kb.sandisk.com/app/answers/detail/a_id/6053#ssdplus)|2/3/2016
Data disk|1|[Toshiba 7200rpm 3TB 3.5" SATA3 DT01ACA300](https://toshiba.semicon-storage.com/us/storage/product/internal-specialty/pc/articles/dt01aca-series.html)|2/11/2014
Data disk|1|2TB unknown|Gifted, second hand
Data disk|2|Samsung 7200rpm 1TB 3.5" SATA3|~2010

It's safe to say, I don't keep any un-backed-up important data on any of these ancient data disks.

> {{< cal 2020-10-07 >}}
>
> The 2TB disk has now failed.

> {{< cal 2022-03-12 >}}
>
> The SSD disk has now also failed.
> RIP this server setup.

The details of the motherboard are:

Part|Type
---|---
Socket| [LGA 1151 r1](https://en.wikipedia.org/wiki/LGA_1151)
Form factor| [Mini-ITX](https://en.wikipedia.org/wiki/Mini-ITX)
Chipset| [H170](https://en.wikipedia.org/wiki/LGA_1151#Skylake_chipsets_(100_series))
Expansion bus|1x [PCIe 3.0 x16](https://en.wikipedia.org/wiki/PCI_Express)

## Firmware
> Although this is a [UEFI](https://en.wikipedia.org/wiki/Unified_Extensible_Firmware_Interface) motherboard, it seems we're all still in the habit of referring to both the firmware *and* its setup interface as the [BIOS](https://en.wikipedia.org/wiki/BIOS). I'll attempt to refrain from this common practice in the following.

### Updating the motherboard firmware
On boot, the motherboard's setup interface can be accessed by mashing `DEL` or `F2`.
The boot menu is accessed by `F11`.

I opened up the setup interface, using Advanced Mode with `F6`, to find the motherboard's firmware version was 1.50, which is out of date. 
It is displayed as P1.50 for some reason. 
I still find the mouse support unnerving.
I was very impressed to see that the interface has an automatic firmware update tool under `Tool > UEFI Update Utility > Internet Flash`.
I tried using this with the `Internet Setting` as `DHCP (Auto IP)` and `UEFI Download Server` as `Europe` under `Tool > UEFI Update Utility > Network Configuration`.
It got online and found the correct latest firmware, but for some reason failed with a network error when it tried to download the file.

Installing the firmware update manually was a success, thankfully.
The up-to-date firmware is available [here](https://www.asrock.com/mb/Intel/H170M-ITXDL/#BIOS).
The newest version as of today is 7.10.
I opted for the Instant Flash method.
I set up a USB flash drive using [parted](https://wiki.archlinux.org/index.php/Parted) and [flashed](https://wiki.archlinux.org/index.php/File_systems#Create_a_file_system) it as [FAT](https://en.wikipedia.org/wiki/File_Allocation_Table) then popped the new firmware on there
```shell
lsblk # the flash drive is /dev/sdb
sudo parted /dev/sdb
(parted) mktable msdos
(parted) mkpart primary fat32 0% 100%
(parted) quit
sudo mkfs.fat /dev/sdb1
mkdir mnt
sudo mount /dev/sdb1 mnt
cd mnt
wget https://download.asrock.com/BIOS/1151/H170M-ITXDL(7.10)
sudo dnf install unzip # I'm working on Fedora here
unzip H170M-ITXDL(7.10)ROM.zip
ls # The firmware file is called `h17mxdl7.10` and should be visible
cd ..
sudo umount mnt
```
To install this on the server motherboard, I entered the setup interface and then plugged in the USB flash drive to the machine.
I selected `Tool > UEFI Update Utility > Instant Flash` and chose the file `h17mxdl7.10`.
The computer restarted several times and successfully updated the firmware.

### Motherboard configuration
I was mostly happy with the default UEFI configuration.
I made a few changes (in Advanced Mode):
* `Advanced > UEFI Configuration > UEFI Setup Style` to `Advance Mode`
* `Advanced > Chipset Configuration > Good Night LED` to `Enabled`
* `Advanced > Storage Configuration > SATA Aggressive Link Power Management` to `Enabled`

The motherboard has a single chassis fan header.
I have connected two chassis fans via a splitter to this header.
One is pulling air in at the front, the other pushes air out at the top,
They both use the three-pin interface, so the revolution rate is voltage controlled.
I set the chassis fan speed to 35% using the `H/W Monitor > FAN-Tastic Tuning > Chassis Fan 1` setting.
The manual setting was necessary as the `Silent Mode` did not provide sufficient voltage to spin the larger front fan.
I also set `H/W Monitor > CPU Fan 1 Setting` to `Silent Mode`.
I chose these slow fan settings as the machine will not be under significant thermal load, and for quieter operation.

I also turned off the chassis LEDs since I find the bright harsh blue light of the power button annoying.

## Operating system
### Preparing the install medium
I downloaded [Debian 10](https://www.debian.org/releases/buster/) for `amd64` platforms through the [online installer](https://www.debian.org/distrib/netinst#smallcd).
This way, the base system is included in the image and anything else will have to be selected and downloaded in the installer.

Like [before]({{< ref "install-fedora#preparing-the-flash-install-medium" >}}), I flashed a USB flash drive with the image using [`dd`](https://wiki.archlinux.org/index.php/Dd)
```shell
wget https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.4.0-amd64-netinst.iso
lsblk # the flash drive is /dev/sda
sudo dd bs=4M if=debian-10.4.0-amd64-netinst.iso of=/dev/sda status=progress oflag=sync
```

Before starting the machine, I detached the data disks, only leaving attached the disk which the operating system (OS) would be installed on.
To boot the server from the USB flash drive in UEFI mode, I had to fiddle with the boot order priority.
This setting is under `Boot > Boot Option Priorities`.
I changed `Boot Option #1` to be `UEFI: General UDisk 5.00, Partition 1`, although there were two options with this same name.
One is for `BIOS` mode, which is for legacy hardware, while the other is for the modern `UEFI` mode.
I selected the correct one by trial and error.
The Debian install screen has subtitle `Debian GNU/Linux UEFI Installer menu` when correctly configured. 

### Installation
I selected `Graphical install` and set the appropriate locales.

> On `Detect and mount CD-ROM`, if the installer fails to detect the install medium, just [remove and reinsert the USB flash drive]({{< ref "boot-media-problem" >}}) and ask the installer to retry. 

The installer complained about a missing firmware file, `rtl_nic/rtl8168h-2.fw`, due to it being non-free.
I ignored this for now.

I had an ethernet cable plugged into the lower ethernet port (nearer the sound outputs), which is the [Intel I219V NIC](https://www.intel.com/content/www/us/en/products/network-io/ethernet/controllers/connection-i219-v.html), so I selected this interface in the network configuration. The other ethernet port is a [Realtek RTL8111H NIC](https://www.realtek.com/en/products/communications-network-ics/item/rtl8111h-s-cg), which you may recognise from the missing firmware warning.

I set up the hostname, the root password, and the default user.

In disk partitioning, I selected to force UEFI partitioning and chose `Guided - use entire disk and set up LVM` with partitioning scheme `Separate /home, /var, and /tmp partitions` over the whole boot disk.
I'll configure this further [later]({{< ref "#configuring-logical-volumes" >}}).
This also generates a logical volume for swap, and uses `ext4` as the file system.

I configured the package manager with the default archive mirror, `deb.debian.org`, without a proxy.

I deselected everything in the software selection screen.
I'll install only what I need later.

The installation was then complete.
I rebooted into my new system.

### Mounting data disks
I identified [disk UUIDs](https://wiki.archlinux.org/index.php/Persistent_block_device_naming#by-uuid) with
```shell
lsblk -f
```
then opened up  [`fstab`](https://wiki.archlinux.org/index.php/Fstab)
```shell
sudo vi /etc/fstab
```
and appended [entries](https://wiki.archlinux.org/index.php/Fstab#Usage) for data disks
```vim
UUID=5cce4601-6c4b-4ba0-9719-c3ef4c5c759c /media/disk-2a ext4   defaults,nofail,x-systemd.device-timeout=1s  0      2
UUID=8e9889ba-49c3-4d3a-92ae-8f2115fa6cb5 /media/disk-1a ext4   defaults,nofail,x-systemd.device-timeout=1s  0      2
UUID=19914b36-94eb-4618-a94a-18cfdec86214 /media/disk-3a ext4   defaults,nofail,x-systemd.device-timeout=1s  0      2
UUID=1807a50c-9e90-44dc-aaf3-c8bfd2da9b98 /media/disk-1b ext4   defaults,nofail,x-systemd.device-timeout=1s  0      2
```
These entries in `fstab` will ensure disks are mounted on boot.
I used the [`nofail` option](https://wiki.archlinux.org/index.php/Fstab#External_devices) to suppress errors if these disks are not present on boot (or `sudo mount -a`) and reduced the timeout.
This is very useful in the case of a failed disk on boot because it means the system will still start, saving moving the machine from its headless location to a desk with a monitor and keyboard to boot from a USB flash drive just to edit `fstab` in order to get a functional system again.

I make the mount points
```shell
sudo mkdir /media/disk-2a
sudo mkdir /media/disk-1a
sudo mkdir /media/disk-1b
sudo mkdir /media/disk-3a
```
and mounted the disks immediately with `sudo mount -a`.

> It's important for `fstab` to be error free after edits, otherwise you'll end up with a broken system and have to do exactly as above on the next boot. Always test the configuration with 
> ```shell
> sudo umount -av
> sudo mount -av
> ```
I did not alter the default behaviour of having these disks spinning all the time in the hope that it would aid longevity.

### Bonding ethernet interfaces
I disabled the default network configuration from installation with
```shell
sudo mv /etc/network/interfaces /etc/network/interfaces.backup
```
then followed [this previous post]({{< ref "bonding-ethernet-interfaces-on-debian-stretch" >}}) to bond the two ethernet interfaces.

> #### `networking`
>
> If we don't care about bonding, we can use the `networking` service (Debian's default for... networking) to configure a network interface, say [`enp0s0`](https://unix.stackexchange.com/a/311844/156381), to get its IP address from the network DHCP server.
>
> `/etc/network/interfaces`
> ```vim
> # This file describes the network interfaces available on your system
> # and how to activate them. For more information, see interfaces(5).
> 
> source /etc/network/interfaces.d/*
> 
> # The loopback network interface
> auto lo
> iface lo inet loopback
> ```
>
> `/etc/network/interfaces.d/primary`
> ```vim
> # The primary network interface
> allow-hotplug enp0s0
> iface enp0s0 inet dhcp  # IPv4
> iface enp0s0 inet6 dhcp  # IPv6
> ```
> Apply with [`ifdown`/`ifup`](https://serverfault.com/a/997214) {{< docs "https://wiki.debian.org/NetworkConfiguration#Starting_and_Stopping_Interfaces" >}} or `reboot`.

### Configuring logical volumes
{{< cal 2020-06-26 >}}

Recall that I chose to set up disk partitioning with [Logical Volume Manager (LVM)](https://wiki.debian.org/LVM) during the [OS installation]({{< ref "#installation" >}}).
The default size of logical volume given to `var` by the Debian installer was too small for my [future use of `docker`]({{< ref "#containers" >}}); I needed to extend the volume, otherwise pulling `docker` images would fail due to lack of space on the volume.
#### Checking configuration
I checked the sizes of [block devices](https://wiki.archlinux.org/index.php/Device_file#Block_devices)
```shell
lsblk
```
```shell
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                         8:0    0 111.8G  0 disk 
├─sda1                      8:1    0   512M  0 part /boot/efi
├─sda2                      8:2    0   244M  0 part /boot
└─sda3                      8:3    0 111.1G  0 part 
  ├─ryanserver--vg-root   254:0    0  19.6G  0 lvm  /
  ├─ryanserver--vg-swap_1 254:1    0   7.7G  0 lvm  [SWAP]
  ├─ryanserver--vg-var    254:2    0  16.5G  0 lvm  /var
  ├─ryanserver--vg-tmp    254:3    0   1.2G  0 lvm  /tmp
  └─ryanserver--vg-home   254:4    0    66G  0 lvm  /home
sdb                         8:16   0   1.8T  0 disk 
└─sdb1                      8:17   0   1.8T  0 part /media/disk-2a
sdc                         8:32   0 931.5G  0 disk 
└─sdc1                      8:33   0 931.5G  0 part /media/disk-1a
sdd                         8:48   0   2.7T  0 disk 
└─sdd1                      8:49   0   2.7T  0 part /media/disk-3a
sde                         8:64   1 931.5G  0 disk 
└─sde1                      8:65   1 931.5G  0 part /media/disk-1b
```
and checked the current space allocation of logical volumes
```shell
sudo lvs
```
```shell
  LV     VG            Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home   ryanserver-vg -wi-ao----  75.66g                                                    
  root   ryanserver-vg -wi-ao---- <19.60g                                                    
  swap_1 ryanserver-vg -wi-ao----  <7.68g                                                    
  tmp    ryanserver-vg -wi-ao----   1.22g                                                    
  var    ryanserver-vg -wi-ao----   6.84g
```
I decided to give `var` about 16 [GiB]({{< ref "jargon#binary" >}}).
Note that the sizes here are not in [GB](https://en.wikipedia.org/wiki/Gigabyte) (multiples of 1000), but in [GiB](https://en.wikipedia.org/wiki/Gibibyte) (multiples of 1024).
#### Reducing a volume
First I made some space by reducing `home`.
This required stopping [services using `home`]({{< ref "#containers" >}}), not being in it, and unmounting it (be aware that this won't work from within `tmux`)
```shell
sudo systemctl stop docker
cd /
sudo umount /home
```
Then I [checked](https://unix.stackexchange.com/a/213417/156381) the disk and shrank the filesystem
```shell
sudo e2fsck -f /dev/ryanserver-vg/home
sudo resize2fs /dev/ryanserver-vg/home 66g
```
To calculate the exact logical volume size on `ext4`, I followed [ArchWiki](https://wiki.archlinux.org/index.php/LVM#Resizing_the_logical_volume_and_file_system_separately).
It gives the formula: `LVM_EXTENTS = FS_BLOCK_COUNT * FS_BLOCK_SIZE / LVM_EXTENTS_SIZE`.

I found the block values with
```shell
sudo tune2fs -l /dev/ryanserver-vg/home | grep Block | head -2
```
```shell
Block count:              17301504
Block size:               4096
```
and the LVM extent size with
```shell
sudo vgdisplay ryanserver-vg | grep "PE Size"
```
```shell
  PE Size               4.00 MiB
```
which is `4*(1024)^2=4194304` bytes.

Plugging in the numbers with consistent units of bytes, `17301504*4096/4194304=16896`, so I shrank the volume with
```shell
sudo lvreduce -l 16896 /dev/ryanserver-vg/home
```

> After unmounting `home`, we can do all the above steps in one line with
> ```shell
> sudo lvreduce -L 66g --resizefs /dev/ryanserver-vg/home
> ```

Having finished the resize, I remounted `home` and restarted services
```shell
sudo mount /home
sudo systemctl start docker
cd
```
Note that it's important to shrink the filesystem before shrinking the logical volume.
#### Extending a volume
I grew the `var` logical volume to fill the freed space
```shell
sudo lvextend -L +9.67g /dev/ryanserver-vg/var
```
Then resized the filesystem;
since `var` was being extended, it could be resized live (without unmounting)
```shell
sudo resize2fs /dev/ryanserver-vg/var
```
This resizes the file system to the maximum size available.
> These last two steps can actually [be done together](https://wiki.archlinux.org/index.php/LVM#Resizing_the_logical_volume_and_file_system_in_one_go) with
> ```shell
> sudo lvextend -L +9.67g --resizefs /dev/ryanserver-vg/var
> ```
#### Checking changes
Finally, I checked
```shell
lsblk
```
```shell
NAME                      MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
...
  ├─ryanserver--vg-var    254:2    0  16.5G  0 lvm  /var
  └─ryanserver--vg-home   254:4    0    66G  0 lvm  /home
...
```
and
```shell
sudo lvs
```
```shell
  LV     VG            Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  home   ryanserver-vg -wi-ao----  66.00g                                                    
...
  var    ryanserver-vg -wi-ao---- <16.52g  
```
#### Resize swap
{{< cal 2020-12-05 >}}

I reduced the swap logical volume from 7.7GiB to 4GiB to top up the root logical volume with some extra space.
I stopped swap
```shell
sudo swapoff -v /dev/mapper/ryanserver--vg-swap_1
```
then reduced the volume size
```shell
sudo lvresize -L -3.7G /dev/mapper/ryanserver--vg-swap_1
```
then made a new swap
```shell
sudo mkswap /dev/mapper/ryanserver--vg-swap_1
```
and enabled it.
```shell
sudo swapon -v /dev/mapper/ryanserver--vg-swap_1
```
Then I could extend root
```shell
sudo lvextend -L +3.7G --resizefs /dev/ryanserver-vg/root
```
### Maintaining boot
{{< cal 2020-12-06 >}}

A recent kernel update failed because `/boot` was full.
To [fix](https://askubuntu.com/a/345611/827359) this, I checked which kernel was currently in use
```shell
uname -r
```
and the list of all installed kernels
```shell
dpkg --list 'linux-image*' | grep ^ii
```
and uninstalled the oldest one (after checking it wasn't the one in use),
```shell
sudo apt remove linux-image-VERSION
sudo apt autoremove
sudo update-grub
```
Then the update has sufficient available disk space to proceed
```shell
sudo apt update && sudo apt upgrade -y
```

## Software packages
### Sudo
I logged into the system with the default user account.
I opened a root shell with
```shell
su -
```
and installed `sudo`
```shell
apt install sudo
```
I added my user to the `sudo` group with
```shell
usermod -aG sudo <user>
```
and opened the `sudo` settings with
```shell
EDITOR=vi visudo
```
to change the line 
```vim
%sudo     ALL=(ALL:ALL) ALL
```
to
```vim
%sudo     ALL=(ALL:ALL) NOPASSWD: ALL
```
to allow `sudo` usage without password entry each time.
Then I `exit`ed the `root` shell.

I logged out and in again to apply the changes.
### Enable contrib, non-free, and backports
Open up the sources
```shell
sudo vi /etc/apt/sources.list
```
and add [`contrib non-free`](https://www.debian.org/distrib/packages#note) after each occurrence of `main`.
The effected lines are
```vim
deb http://security.debian.org/ buster/updates main contrib non-free
deb-src http://security.debian.org/ buster/updates main contrib non-free

deb  http://deb.debian.org/debian buster main contrib non-free
deb-src  http://deb.debian.org/debian buster main contrib non-free

deb http://deb.debian.org/debian buster-updates main contrib non-free
deb-src http://deb.debian.org/debian buster-updates main contrib non-free
```
Also add [backports](https://backports.debian.org/)
```vim
deb http://deb.debian.org/debian buster-backports main contrib non-free
deb-src http://deb.debian.org/debian buster-backports main contrib non-free
```
Then
```shell
sudo apt update
```

> {{< cal "2024-03-02" >}}
>
> Note that from Debian 12/bookworm, firmware in `non-free` is [moved](https://www.debian.org/releases/bookworm/amd64/release-notes/ch-information.en.html#non-free-split) to its own component `non-free-firmware`.

### Optimise Debian mirrors
I used [netselect-apt](https://manpages.debian.org/buster/netselect-apt/netselect-apt.1.en.html) to find the fastest mirror for the [software repositories](https://wiki.debian.org/SourcesList)
```shell
sudo apt install netselect-apt
sudo netselect-apt
```
I could have replaced the default mirror with the winner
```shell
sudo sed -i "s/deb.debian.org/mirror.sov.uk.goscomb.net/g" /etc/apt/sources.list
```
However, it might be preferable to stick to `deb.debian.org` since then `https` can be used.
I ended up going for this in the end for the added security.
```shell
sudo sed -i "s/http:\/\/d/https:\/\/d/g" /etc/apt/sources.list
```
Note that none of these changes affect `security.security.org`.
### Installing selectively from testing
For some packages, I wanted the bleeding edge version from `testing`.
This included
* [`intel-microcode`]({{< ref "#cpu-microcode-updates" >}})
* [`mergerfs`]({{< ref "#installation-1" >}})
* [`docker`]({{< ref "server-install-containers#installation" >}})
* [`docker-compose`]({{< ref "server-install-containers#installation" >}})

Creating a FrakenDebian like this is generally a [bad idea](https://wiki.debian.org/DontBreakDebian), so watch what dependencies get dragged into the upgrades.

I followed [this guide](https://serverfault.com/a/382101) and the [docs](https://manpages.debian.org/stable/apt/apt_preferences.5.en.html) to set this up.

I [set](https://wiki.debian.org/AptConfiguration#apt_preferences_.28APT_pinning.29) `stable` packages as preferred
```shell
sudo vi /etc/apt/preferences.d/stable.pref
```
```vim
# 500 <= P < 990: causes a version to be installed unless there is a
# version available belonging to the target release or the installed
# version is more recent

Package: *
Pin: release a=stable
Pin-Priority: 900
```
and `testing` packages as second-choice
```shell
sudo vi /etc/apt/preferences.d/testing.pref
```
```vim
# 100 <= P < 500: causes a version to be installed unless there is a
# version available belonging to some other distribution or the installed
# version is more recent

Package: *
Pin: release a=testing
Pin-Priority: 400
```
Then 
```shell
sudo mv /etc/apt/sources.list /etc/apt/sources.list.d/stable.list
```
and added the `testing` repos
```shell
sudo vi /etc/apt/sources.list.d/testing.list
```
```vim
deb https://deb.debian.org/debian/ testing main contrib non-free
deb-src https://deb.debian.org/debian/ testing main contrib non-free

deb http://security.debian.org testing-security main contrib non-free
deb-src http://security.debian.org testing-security main contrib non-free

deb https://deb.debian.org/debian/ testing-updates main contrib non-free
deb-src https://deb.debian.org/debian/ testing-updates main contrib non-free
```
<!-- ```shell -->
<!-- sudo cp /etc/apt/sources.list.d/stable.list /etc/apt/sources.list.d/testing.list -->
<!-- sudo sed -i "s/buster/testing/g" /etc/apt/sources.list.d/testing.list -->
<!-- ``` -->
Now I could install packages from `testing` with
```shell
sudo apt install -t testing <package>
```
### CPU microcode updates
I installed the [`intel-microcode`](https://wiki.debian.org/Microcode) package [from `testing`](https://packages.debian.org/testing/intel-microcode) to ensure it was up-to-date.
The package applies any microcode updates on each boot.
```shell
sudo apt install -t testing intel-microcode
sudo reboot
```
### Drivers
With the non-free repo, I could now install that missing firmware from [earlier]({{< ref "#install" >}}).
I used the [`backports` repo](https://packages.debian.org/buster-backports/firmware-realtek)
```shell
sudo apt install -t buster-backports firmware-realtek
```
### SSH
Next I set up SSH access so that the server can be run headless.
```shell
sudo apt install openssh-server
```
On the clients, [copy over the public key]({{< ref "utilities#copy-public-key-to-remote" >}})
```shell
copy-ssh-id <user>@<hostname>
```
Then [enforce access by public key](https://wiki.archlinux.org/index.php/OpenSSH#Force_public_key_authentication) (like [before]({{< ref "xps-install#moving-files-over" >}}))
```shell
sudo vi /etc/ssh/sshd_config
```
and set
```vim
PasswordAuthentication no
```
then restart the SSH server
```shell
sudo systemctl restart ssh.service
```

To give access to further client machines in the future, their public key will have to be [manually appended](https://wiki.archlinux.org/index.php/SSH_keys#Manual_method) to `~/.ssh/authorized_keys` on the server.
Transferring the public key is most easily done via a USB stick.

### Union filesystem
#### MergerFS
I decided to use [MergerFS](https://github.com/trapexit/mergerfs) to allow my data disks to be accessed as a single pool of data, but such that a disk failure would only affect that disk and not the others.
Its operation is described [here](https://github.com/trapexit/mergerfs#how-it-works).

I came across this while reading [this blog](https://blog.linuxserver.io/2016/02/02/the-perfect-media-server-2016/).

#### Installation
The [version in the `stable` repo](https://packages.debian.org/stable/mergerfs) (Debian Buster) was `2.24.2`.
I wanted a more up-to-date version, so decided to the [package in `testing`](https://packages.debian.org/testing/mergerfs) instead.
```shell
sudo apt install -t testing mergerfs
```
#### Set up
I organised the directories of [the data disks]({{< ref "#mounting-data-disks" >}}) with a common structure, using
```shell
media
└── disk-*
    └── data
        ├── cloud
        ├── docs
        ├── film
        ├── games-bkp
        ├── games-dl
        ├── games-extra
        ├── games-saves
        ├── music
        ├── pictures
        ├── scratch
        ├── series
        ├── sounds
        ├── torrents
        │   ├── downloads
        │   └── watch
        └── videos
```

I mostly used the [recommended basic settings](https://github.com/trapexit/mergerfs#basic-setup) for `mergerfs`.
I also added a name, and `noforget` for [NFS compatibility](https://github.com/trapexit/mergerfs/blob/master/README.md#can-mergerfs-mounts-be-exported-over-nfs).
I did this by appending `/etc/fstab/` with
```vim
/media/disk-* /media/pool fuse.mergerfs use_ino,cache.files=off,dropcacheonclose=true,allow_other,category.create=mfs,fsname=mergerfs_pool 0 0
```
I started up the `mergerfs` pool with
```shell
sudo mount -a
```
It is accessed at `/media/pool`.
<!-- I checked success with --> 
<!-- ```shell -->
<!-- df -h -->
<!-- ``` -->
<!-- and saw at the bottom -->
<!-- ```shell -->
<!-- mergerfs_pool                    5.4T  4.3T  834G  85% /media/pool -->
<!-- ``` -->
<!-- It worked! -->

I bypassed `pool` in favour of `disk-3a` for ebooks and music; since these libraries are relatively small in disk space usage, they can live on the most reliable data disk.
### Other software
I also installed
```shell
sudo apt install git htop tree rsync tmux firmware-misc-nonfree smartmontools libsensors5
```
The firmware is for the Intel integrated GPU.
`libsensors5` is for CPU temperature in `htop`.
<!-- ### Review -->
<!-- Did I manage to keep it lightweight? -->
<!-- `free` reports 102 MiB in use. -->
## Containers
I moved this section into [its own post]({{< ref server-install-containers >}}) since this has ended up as quite a large entry!
## Outlook
In the future, I'm interested in
* setting up [TLS](https://en.wikipedia.org/wiki/Transport_Layer_Security) via [Let's Encrypt](https://letsencrypt.org/) {{< wiki "Let's_Encrypt" >}} on the reverse proxy server ~~, maybe using [Caddy](https://caddyserver.com/) {{< wiki "Caddy_(web_server)" >}}~~ .
    * [Done]({{< ref "caldav#tls" >}})!
* switching `docker` out for [`podman`](https://podman.io/) {{< arch "podman" >}}, particularly since it now [supports](https://www.redhat.com/sysadmin/podman-docker-compose) `docker-compose`.
* upgrading the hardware and trying out a {{< wl hypervisor >}} like [Proxmox](https://www.proxmox.com/en/) {{< wiki Proxmox_Virtual_Environment >}} (again, for the pedagogy).
* moving `pihole` onto dedicated hardware like the [Raspberry Pi 1 Model B+](https://www.raspberrypi.org/products/raspberry-pi-1-model-b-plus/) to reduce downtime, like when I restart the server on a kernel update. 
    * [Done]({{< ref "rpi-dns" >}})!
* maybe getting a router like the [TekLager APU2E0](https://teklager.se/en/products/routers/APU2E0-open-source-router) and trying out [OPNsense](https://opnsense.org/) {{< wiki opnsense >}} if using a separate wireless access point, or using [OpenWrt](https://openwrt.org/) otherwise.
* seeing how [AdGuard Home](https://adguard.com/en/adguard-home/overview.html) {{< ghub AdguardTeam AdGuardHome >}} develops versus `pihole`.
* hosting my own DNS resolver.
  * [Done]({{< ref unbound >}})!
* centrally manage local DNS, perhaps with [dnsmasq](https://dnsmasq.org/).
* switching out `linux` for `bsd`, likely FreeBSD, for that ultimate stability (OK, for the nerd prestige).
* using `ZFS` with a new set of hard disks.
* keeping Debian up to date.
  * [I upgraded to Debian 11 when it was released]({{< ref buster2bullseye >}})

+++
title = "Outlook email in Evolution"
date = 2021-01-21T10:18:03Z
edit = 2021-02-01
categories = ["software"]
tags = ["email","communication","fedora","linux","desktop","laptop","microsoft","outlook","office-365"]
description = "Access a Microsoft Outlook email account from GNOME Evolution"
toc = true
cover = "img/covers/14.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

My [Durham University email account](https://www.dur.ac.uk/cis/email/) is cloud hosted on Office 365, or [however](https://www.microsoft.com/en-gb/microsoft-365/exchange/email) Microsoft does it these days.
I opted to use the [GNOME Evolution](https://wiki.gnome.org/Apps/Evolution) mail client {{< wiki "GNOME_Evolution" >}} {{< arch "GNOME/Evolution" >}} to access it from the [GNOME desktop](https://en.wikipedia.org/wiki/GNOME) on Linux.

Evolution is installed on Fedora 33/34 with
```shell
sudo dnf install evolution
```

Email can be access by [IMAP](https://en.wikipedia.org/wiki/Internet_Message_Access_Protocol) or [Exchange ActiveSync](https://en.wikipedia.org/wiki/Exchange_ActiveSync) protocol (also referred to by Exchange Web Services or EWS).

This seems to bypass the university's mandatory [MFA](https://en.wikipedia.org/wiki/Multi-factor_authentication) policy.

> The university has now revoked access by IMAP.
> [This guide](https://help.ippp.dur.ac.uk/knowledge-base/cis-university-email/) shows how to set up the only supported connection authentication method (EWS with OAuth).

## IMAP
To add an IMAP/SMTP account, first open the Evolution Mail Configuration Assistant at `Edit > Accounts > Add > Mail Account`.
Configure as:
* Under `Identity`, 
    * add your `E-mail Address`,
    * but uncheck `Look up mail server details based on the entered e-mail address`.
    To get going with the standard protocol, automatic setup won't work, but don't worry because manual setup is described in the following.
* Under `Receiving E-mail`, 
    * set the `Server Type` to `IMAP`;
    * for the `Configuration`, 
        * set the `Server` to `outlook.office365.com`, 
        * the `Port` to `993`, 
        * and your `Username` (which may be different to your email address!);
    * for `Security`,
        * set the `Encryption Method` to `TLS on a dedicated port`;
    * and for `Authentication`, 
        * choose `Password`.
* Under `Sending E-mail`,
    * choose `SMTP` for the `Server Type`;
    * for the `Configuration`, 
        * set the `Server` to `smtp.office365.com`, 
        * the `Port` to `587`,
        * check `Server requires authentication`;
    * for `Security`,
        * set the `Encryption Method` to `STARTTLS after connecting`;
    * and for `Authentication`, 
        * choose `Login`,
        * and set your `Username`.
You'll have to enter your password in a pop-up dialogue after.

## Exchange ActiveSync
To get access to university contacts and calendar, we have to use Microsoft's proprietary protocol <i class="fas fa-thumbs-down"></i>.
Install
```shell
sudo dnf install evolution-ews
```
To configure receiving email, choose type `Exchange Web Services`, set your username, and use use the automated `Fetch URL` to set the addresses.

I elected to use this method, on one of my laptops at least, in order to access the global university contacts.
When I first tried to do so, I had to enter my username and password again.
This is probably because my email is different from my username on this account for some unknown reason, so it likely failed authentication the first time with the wrong username.

> On later trying to set this up on my other laptop, I couldn't pass authentication.
> I'm not sure if this is because I tried it away from campus and it can only be set up locally on the university network, or if they've changed the security settings since I set up the first laptop.
> No matter, IMAP allows me to send and receive my emails, albeit without access to the university contact registry.

## General settings

I clicked the Date header to order by descending received time.

I chose `Edit > Preferences > Mail Preferences > HTML Messages > Plain Text Mode > HTML Mode > Show plain text if present`.

I changed the location to save sent email in from locally to the remote server at `Edit > Preferences > Mail Accounts > (my account) > Edit > Defaults > Special Folders > Sent Messages Folder`.
I also set the archive folder here to the remote one.
Then everything is on the remote and nothing is local.

## Android

I also used these settings to configure this email account [on Android using FairEmail]({{< ref "fp3#f-droid-apps" >}}).

+++
title = "My pages"
date = 2021-04-05T09:44:23+01:00
categories = ["web"]
tags = ["html","css","markdown","pandoc","yaml","gitlab-ci","grid","flex","gitlab-pages","gitlab-ci"]
description = "A list of my web apps"
toc = true
draft = false
cover = "img/covers/53.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I used [my GitLab user page](https://eidoom.gitlab.io) to display a list of the various things I've made which are accessible by the web.
The source is at {{< gl eidoom.gitlab.io >}}.
The build system has gone through a couple of iterations.

## Pandoc/Markdown

I originally used `pandoc` to [compile](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/f9d666e1515499d2452b2ae86475bedde7a926e5/build.sh) a static web page from a [template](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/f9d666e1515499d2452b2ae86475bedde7a926e5/template.html) and [Markdown content](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/f9d666e1515499d2452b2ae86475bedde7a926e5/index.md).
The [styling](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/f9d666e1515499d2452b2ae86475bedde7a926e5/index.css) uses [`flex`](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Flexbox) and [`grid`](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids) for layout.
It's [deployed](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/f9d666e1515499d2452b2ae86475bedde7a926e5/.gitlab-ci.yml) with GitLab CI.

## Python/Jinja2/YAML

I [changed](https://gitlab.com/eidoom/eidoom.gitlab.io/-/commit/c9fd0b3b9c6c7c5ac55045ff71e33d4b594e9d16) all that to a [Python script](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/master/build.py) that parses a [YAML file](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/master/content.yml) and populates a [Jinja2 template](https://gitlab.com/eidoom/eidoom.gitlab.io/-/blob/master/index.html.jinja2) to generate the webpage.
I find this setup simpler and easier to use.

+++
title = "Web feeds"
date = "2019-11-12T12:47:12Z"
tags = [""]
categories = [""]
description = ""
draft = true
toc = true
+++

## Reviving RSS

https://en.wikipedia.org/wiki/RSS
https://en.wikipedia.org/wiki/Atom_(web_standard)

[read](https://play.google.com/store/apps/details?id=com.read.app&hl=en_US)

[newsboat](https://newsboat.org/)
[tips](https://wiki.archlinux.org/index.php/Newsboat)

## JSON Feed

https://www.jsonfeed.org/
https://en.wikipedia.org/wiki/JSON_Feed

+++
title = "NAT"
date = 2022-07-04T16:49:06+02:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
+++

Rocky Linux 8.6

NAT router
https://en.wikipedia.org/wiki/Network_address_translation

IP masquerade
https://wiki.archlinux.org/title/Internet_sharing

https://linuxguideandhints.com/el/nat.html
https://wiki.archlinux.org/title/Firewalld#NAT_masquerade
https://www.server-world.info/en/note?os=Rocky_Linux_8&p=firewalld&f=2
https://stackoverflow.com/questions/58816770/firewalld-forwarding-traffic-received-on-eth10-to-different-ip-than-eth1
https://doc.opensuse.org/documentation/leap/archive/15.0/security/html/book.security/cha.security.firewall.html

```shell
firewall-cmd --zone=internal --add-interface=<internal interface> --permanent
firewall-cmd --zone=external --add-interface=<external interface> --permanent
firewall-cmd --zone=external --add-masquerade --permanent # should be set already, just to make sure
firewall-cmd --complete-reload
```

+++
title = "Online security"
date = 2020-06-19T12:38:17+01:00
categories = ["environment-extra"]
tags = ["password-manager","2fa","twilio","authy","bitwarden","authenticator-app","firefox","chromium","google","firefox-addon","mozilla","chrome-extension","linux","fedora","desktop","laptop","mobile","google-play","snap","multi-factor-authentication","passwords","security","browser","install","android","apk","flatpak"]
description = "Managing website login authentication across devices as a user"
toc = true
cover = "img/covers/34.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Password manager

I use [BitWarden](https://bitwarden.com/) {{< wiki "Bitwarden" >}} as my password manager.
I use the [Firefox addon](https://addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager/) (and infrequently the [Chrome extension](https://chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb) when I'm on [Chromium](https://admin.rpmfusion.org/pkgdb/package/free/chromium-freeworld/)) to access my passwords in the browser.
On mobile, I use the [Android app](https://play.google.com/store/apps/details?id=com.x8bit.bitwarden).
On the desktop, I use the application from [`flatpak`]({{< ref "software-deploy#flatpak" >}})
```shell
sudo flatpak install flathub bitwarden
```

I had previously used Google Password Manager with Chromium, but decided to migrate to a potentially more secure dedicated password manager.
This was at the same time that I [started using Firefox](https://www.digitaltrends.com/computing/mozilla-firefox-chrome-review-comparison-2020/) as my primary browser, so the discrete password manager gave the immediate benefit of being browser-agnostic.
I liked the look of [pass](https://www.passwordstore.org/), but decided to go with the more familiar service offered by [BitWarden](https://bitwarden.com/) for now.
Importing my passwords from Chromium was [simple](https://bitwarden.com/help/article/import-from-chrome/).


## MFA

For all accounts that support it, I have [multi-factor authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication) (MFA) enabled.
I use [Authy](https://authy.com/) {{< wiki "Twilio" >}} to manage authentication via their [Android app](https://play.google.com/store/apps/details?id=com.authy.authy).

### On desktop

I've recently also started using the [Authy desktop application](https://support.authy.com/hc/en-us/articles/1260803052049-Authy-App-for-Linux-System-Requirements), which is [now out](https://authy.com/blog/announcing-general-availability-authy-desktop-linux/) of [beta](https://authy.com/blog/authy-desktop-now-available-in-beta-for-linux/).
It uses a [classic]({{< ref "software-deploy#snap" >}}) `snap` [package](https://snapcraft.io/authy), installed with
```shell
sudo snap install authy
```

> {{< cal "2024-03-27" >}}
>
> Since Authy is closed-source, consider alternatives, eg [Aegis](https://getaegis.app/).

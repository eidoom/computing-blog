+++
title = "Transparencies"
date = 2022-10-22T16:06:45+02:00
categories = ["web"]
tags = ["mini-project","python3","python","slideshow","static-website","website","webpages","slides","presentation","yaml","markdown","jinja2","css","html","js","vanilla.js","javascript","venv","virtual-environment","pip","transparencies","static-site-generator","ssg"]
description = "A simple web slides solution"
toc = true
draft = false
cover = "img/covers/115.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I made a slides generator for the web at {{< gl transparencies >}} ([demo](https://eidoom.gitlab.io/transparencies/)).
I used it to create a [presentation](https://eidoom.gitlab.io/acat2021/) for a talk I gave, which has a slightly more fleshed-out source at {{< gl acat2021 >}}.
My friend forked the project and has made a more featureful version at {{< gl transparencies transparencies >}}

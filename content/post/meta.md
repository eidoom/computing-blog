+++
title = "Meta"
date = 2019-12-15T14:43:09Z
edit = 2021-01-13
categories = ["web"]
tags = ["meta","git","static-website","hugo","netlify","continuous-deployment","front-end","markdown"]
description = "A record of how I set up this website"
draft = false
toc = true
cover = "img/covers/101.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## The stack

I used

* [Hugo](https://gohugo.io/) as my static site generator 
* [Netlify](https://www.netlify.com/) to deploy and host the site
* Hugo theme [hello friend](https://github.com/panr/hugo-theme-hello-friend/)
* [Git](https://git-scm.com/) as my version control system
* [GitLab](https://about.gitlab.com/what-is-gitlab/) to host my remote git repository

Many thanks to all the creators of these systems.

## The environment

To give further credit where it's due, I used

* [Neovim](https://neovim.io/) as my text editor
* [tmux](https://github.com/tmux/tmux/wiki) as my terminal multiplexer
* [Chromium](https://www.chromium.org/Home) as my web browser
* [Fedora](https://getfedora.org/) as my operating system

Many thanks also to the creators of these systems.

## Let's get started

I made a [new remote repository on GitLab](https://gitlab.com/eidoom/computing-blog).

Locally, I used `hugo` ([installation]({{< ref "install-fedora#hugo" >}})) to [generate a new website folder](https://gohugo.io/getting-started/quick-start/), initialised it with `git`, and install the `hello-friend` theme:

```shell
cd ~/git
hugo new site computing-blog
cd computing-blog
git init
vi .gitignore # Add *.swp, then write and quit
git submodule add https://github.com/panr/hugo-theme-hello-friend.git themes/hello-friend # Make sure to use https, not ssh, to support Netlify
cp -r themes/hello-friend/exampleSite/* .
rm content/post/hello.md static/img/hello.jpg
```

I customised the example configuration by

* changing `theme` to `"hello-friend"` 
* setting `baseurl` to `"https://computing-blog.netlify.com/"` 

in `config.toml`. See the current configuration [here](https://gitlab.com/eidoom/computing-blog/blob/master/config.toml).

In a separate `tmux` pane, I ran `hugo server -D` and opened in the browser <http://localhost:1313/> to keep an eye on what the site looked like with realtime changes.

I set up the post template: 

```shell
vi archetypes/default.md
```
```markdown
---
title: "{{ .Name | humanize }}"
date: {{ .Date }}
categories: [""]
tags: [""]
description: ""
draft: true
---
```

I created this page:

```shell
hugo new post/meta.md
vi content/post/meta.md
```

See the source [here](https://gitlab.com/eidoom/computing-blog/blob/master/content/post/meta.md).
Once I was happy with it, I changed the `draft` metadata field to `false`.

I [told Netlify which version of Hugo to use](https://gohugo.io/hosting-and-deployment/hosting-on-netlify/#configure-hugo-version-in-netlify):

```shell
vi netlify.toml
```
```vim
[context.production.environment]
  HUGO_VERSION = "0.59.1"
```

I pushed everything to the remote:

```shell
git remote add origin git@gitlab.com:eidoom/computing-blog.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

Finally, I [deployed the site](https://gohugo.io/hosting-and-deployment/hosting-on-netlify/#create-a-new-site-with-continuous-deployment) on [Netlify](https://app.netlify.com/).

> {{< cal "2023-01-10" >}}
>
> The site is now hosted on GitLab Pages instead.

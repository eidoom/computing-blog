+++
title = "Installing Go"
date = 2020-01-02T16:32:12Z
edit = 2020-01-10
categories = ["environment-extra"]
tags = ["environment","go","install","fedora","desktop","laptop","server","linux","debian"]
description = "How I installed Go"
toc = true
cover = "img/covers/93.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

The first time I installed Go, it was a manual process.
I recently had to install it on another machine and found that package managers have caught up, thankfully.

I installed [Go](https://golang.org/) on my desktop and laptop, both running [Fedora](https://developer.fedoraproject.org/tech/languages/go/go-install.html), with 
```shell
sudo dnf install golang
```

On my server running Debian, it was the same except to substitute `dnf` for `apt`

```shell
sudo apt install golang
```

Then I configured the systems with

```shell
mkdir ~/go
echo "export GOPATH=$HOME/go" >> ~/.zshrc
echo "export PATH=$GOPATH/bin:$PATH" >> ~/.zshrc
exec $SHELL
```

choosing the canonical `GOPATH`.

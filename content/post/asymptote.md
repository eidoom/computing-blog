+++
title = "Drawing Feynman diagrams with Asymptote"
date = 2022-10-22T15:59:11+02:00
categories = ["typesetting"]
tags = ["asymptote","2d-graphics","graphics","images","image","diagrams","feynman-diagrams","vector","vector-graphics","svg","pdf"]
description = "Trying a new vector graphics language"
toc = true
draft = false
cover = "img/covers/106.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

There are many options for drawing Feynman diagrams.
I've tried
* Graphics tablet, [example](https://eidoom.gitlab.io/ytf20/#/aim-1)
* TikZ, [recall]({{< ref "pandoc-physics#the-build-process" >}}), [example](https://gitlab.com/eidoom/feynman-diagram-template) (I've used a lot in the past)
* [feynmp](https://www.ctan.org/pkg/feynmf), [example](https://gitlab.com/eidoom/trying-feynmp)

More recently, I've used [Asymptote](https://asymptote.sourceforge.io/) and quite liked it, see {{< gl asymptote-diagrams >}}.

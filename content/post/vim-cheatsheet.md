+++
title = "Vim cheatsheet"
date = 2020-01-17T10:46:57Z
edit = 2021-06-05
categories = ["cheatsheet"]
tags = ["linux","vim","terminal","text-editor","laptop","desktop","server","environment","neovim","nvim","vi"]
description = "A reminder of day-to-day Vim commands"
toc = true
cover = "img/covers/17.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I described my Vim configuration in a [previous post]({{< ref "neovim" >}}) and the up-to-date configuration files are in [this repo](https://gitlab.com/eidoom/dotfiles-nvim).
Here, I record the commands I frequently use in Vim for posterity (because I'll almost certainly forget them in the future).

Note that `<leader>` (i.e. `mapleader`) is set to `,`.
Non-standard commands are marked `!custom`; all current mappings can be shown with `:map`.
Remember, you can always ask Vim for help with `:h <command>`.

Keyboard input is shown with `Key`, where 
* `Key1`+`Key2` means hold the first key while pressing the second
* `Key1`,`Key2` or `Key1Key2` means press the keys sequentially
* `Up`,`Down`,`Left`,`Right` are the arrow keys

## Starting Vim

To open a file `<filename>` with Vim (really `nvim`), with environment as described [here]({{< ref "shell" >}}),
```shell
vi <filename>
```
or at a specific line `<line>`
```shell
vi <filename> +<line>
```
or using `fasd` {{< ghub clvv fasd >}}
```shell
v <fuzzy filename>
```
or without all the customisations and plugins
```shell
vi -u NONE <filename>
```

## Modes
* `Esc` normal mode
* `i` insert mode on current character/cursor position
    * `s` as above, but also delete current character
    * `a` insert mode after current character
    * `I` insert mode at start of line
    * `A` insert mode at end of line
    * `o` new line underneath and insert mode
    * `O` new line above and insert mode
* `v<motion>` visual mode, selecting region by [`<motion>`](https://vimdoc.sourceforge.net/htmldoc/motion.html)
    * `V` visual line mode
    * `Ctrl`+`v` character visual mode
* `R` replace mode (same as insert mode, except replace characters rather than inserting them)
* `:<command>`,`Enter` command-line mode
    * `:`,`Up` to access previous commands

Further examples of command-line mode will be written without the `Enter` on the understanding that it is implicit.

## File manipulation
* `:w` write
* `:q` quit current window
* `:qa` quit all
* `:wq` write and quit
* `ZZ` write and quit
* `:e` reload current file, discarding local changes

## Buffers

* `:e <filename>` open new or existing file `<filename>` in new buffer
* `Ctrl`+`6` switch to last buffer
* `:bn` next buffer
* `:bp` previous buffer
* `:bd` close buffer
 
## Navigation
Note that basic movement keys are remapped.
Note that "current word" refers to the [word](http://vimdoc.sourceforge.net/htmldoc/motion.html#word) which the cursor is in.
* `j` left `!custom`
* `k` down `!custom`
* `l` up `!custom`
* `;` right `!custom`
* `^` home 
* `$` end
* `Ctrl`+`e` scroll down without moving cursor
* `Ctrl`+`y` scroll up without moving cursor
* `gg` move cursor to start of document
* `G` move cursor to end of document
* `b` move cursor to first character of current word
    * `B` as above, including characters 
* `w` move cursor to first character of next word
    * `W` as above, including characters
* `e` move cursor to last character of current word
    * `E` as above, including characters
* `:<number>` move cursor to line `<number>`
* `)` move cursor to first character of next sentence
* `(` move cursor to first character of previous sentence
* `f<character>` move cursor to the next instance of `<character>` in the current line
* `%` jump to matching brace (and also comment designator and preprocessor conditionals in C(++))
* `f<character>` jump to next `<character>` in line
* `F<character>` jump to next `<character>` in line, searching backwards
* `t<character>` jump to character before next `<character>` in line
* `''` move cursor back to previous position

## Deleting
* `x` delete character
* `d<motion>` delete characters selected by `<motion>`
* `dd` delete line
* `D` delete to end of line
* `J` delete new line at end of line

## Changing
* `r` replace: delete character and enter insert mode for a single character entry only
* `c<motion>` change: delete characters selected by `<motion>` and enter insert mode
* `cc`  change line
* `C` change to end of line
* `~` toggle case
* `gu<motion>` to switch to lowercase
* `gU<motion>` to switch to uppercase

## Search and replace
* `:s/<search>/<replace>/g` [substitute](https://vim.fandom.com/wiki/Search_and_replace) every occurrence of `<search>` with `<replace>` in the current line
* `:<startLine>,<endLine>s/<search>/<replace>/g` substitute every occurrence of `<search>` with `<replace>` in the specified lines
* `:%s/<search>/<replace>/g` substitute every occurrence of `<search>` with `<replace>` in the whole file
* `:%s/\c<search>/<replace>/g` substitute every case-insensitive occurrence of `<search>` with `<replace>` in the whole file
* `:%s/\<[search]\>/[replace]/g` substitute every occurrence of word (so not counting substrings of words) `[search]` with `[replace]` in the whole file
* `:%s/\(<search>\)/\L\1/` [substitute](https://vim.fandom.com/wiki/Changing_case_with_regular_expressions) every occurrence of `<search>` with its lowercase version in the whole file. Similarly,
    * `\l` for first letter lowercase
    * `\U` for all letters uppercase
    * `\u` for first letter uppercase
    * `\e` to stop the substitute command
* We can use Vim regex syntax, including
    * `.*` greedy match all
    * `.\{-}` lazy match all
    * `[...]` groups with un-escaped square brackets
    * `\(...\)` capture groups with escaped round brackets, backreferenced by `\1` etc
    * `\(...\)\@=` lookahead
    * `\(...\)\@<` lookbehind
    * postfix `!` to the look-ahead/behind to make it negative

## Find
* `/<term>` search forwards for `<term>`
    * Note that `\r` is the newline character
    * `/\c<term>` case insensitive forwards search for `<term>`
    * `/\C<term>` case sensitive forwards search for `<term>`
* `?<term>` search backwards for `<term>`
* `*` search forwards for other instances of current word
* `#` search backwards for other instances of current word
* `n` jump to next matched expression
* `gg`,`n` jump to first match
* `G`,`N` jump to last match
* `Shift`+`n` jump to previous matched expression
* `:noh` disable highlighting of current search results
* `:g<term>` list lines containing `<term>`

## Copy and paste
* `y`   yank: copy
* `yy`  yank line
* `p`   paste
* `P`   paste behind cursor

## Undo and redo
* `u`   undo
* `Ctrl`+`r`  redo
 
## Repeat
* `.`   repeat last command

## Indent
* `>[motion]` increase indentation of lines selected by `[motion]`
* `<[motion]` decrease indentation of lines selected by `[motion]`
* `>>` increase indentation of line
* `<<` decrease indentation of line

## Macros
* `q<letter><command sequence>q` record macro `<command sequence>` to macro register `<letter>`
* `@<letter>` execute macro at register `<letter>`
* `@@` execute last executed macro again
* `<num>@<letter>` for macro at register `<letter>`, execute `<num>` times

## Plugin commands
These commands are provided by plugins
### vim-yankstack
* [Source](https://github.com/maxbrunsfeld/vim-yankstack)
* `Alt`+`p` cycle yankstack `!custom`
* `Shift`+`Alt`+`p` reverse cycles `!custom`
### vim-commentary
* [Source](https://github.com/tpope/vim-commentary)
* `gc<motion>` comment out characters selected by `<motion>` `!custom`
* `gcc` comment out line `!custom`
* `gc` works on a selected section in visual mode too
### vim-autoformat
* [Source](https://github.com/Chiel92/vim-autoformat)
* `:Au` autoformat file `!custom`
* `<leader>af` does the same `!custom`
* It works on a selected section in visual mode too
### JuliaFormatter.vim
* [Source](https://github.com/kdheepak/JuliaFormatter.vim)
* `<leader>jf` autoformat Julia file

## Visual mode
Visual mode can be entered as in [Modes]({{< ref "#modes" >}}) or with the mouse:
select a region of text by clicking and dragging or a word by double clicking on it.
When in visual mode, the following commands can be useful
* `:'<,'>sort`
* `:'<,'>!tac` reverse order of lines (`!` filters though an external program, `tac` in this case)
* `Ctrl`+`A` increment selected numbers (first on each line)
* `Ctrl`+`X` decrement selected numbers (first on each line)
* `g`,`Ctrl`+`A` cumulatively increment selected numbers
* `g`,`Ctrl`+`X` cumulatively decrement selected numbers
* [`c`]({{< ref "#changing" >}})
* [`d`]({{< ref "#changing" >}})
* [`y` and `p`]({{< ref "#copy-and-paste" >}})
* [`>` and `<`]({{< ref "#indent" >}})
* [`~`, `u`, and `U`]({{< ref "#changing" >}})

## Counting
Most of the above commands take a count.
That is to say, they can be prefaced by a number to repeat their behaviour.
For example, `2x` deletes two characters.

## Motion
Some examples of `<motion>` selection using the delete command `d`:
* `dap` delete a paragraph: delete entire current paragraph (including following newline)
* `dip` delete inner paragraph: delete entire current paragraph (excluding following newline)
* `daw` delete a word
* `dip` delete inner word
* `dw` delete word: delete from current character to end of current word (including following space)
* `de` delete to word end: delete from current character to end of current word (excluding following space)
* `dt)` delete to: delete from current character to the character before the next `)` on the line
* `dfe` delete find: delete from current character to the next `e` on the line, inclusive
* `d$`delete to end of line
* `da(` delete a `(...)` block (including brackets)
    * also `di)` for inner to exclude brackets  (`)` is alias of `(`)
    * similarly for `]` and `>`
    * similarly `t` for XML/HTML tags

## Completion
* `Ctrl`+`n` insert next matching word
* `Ctrl`+`p` insert previous matching word

## Spell checking
* `:se spell` to enable spell checking
* `:se nospell` to disable spell checking (default)
* `]s` jump to next spelling mistake
* `[s` jump to previous spelling mistake
* `z=` list correction suggestions when cursor on word that is highlighted as incorrect
* `zg` add word under cursor to custom dictionary at `~/.config/nvim/spell/en.utf-8.add`
* `zw` mark word under cursor as incorrect

## Split windows
* `:sp` split current window horizontally and open current file in new window
* `:sp <filename>` split current window horizontally and open `<filename>` in new window
* `:vs` split current window vertically and open current file in new window
* `:vs <filename>` split current window vertically and open `<filename>` in new window
<!-- * `:clo` close current window but keep buffer open -->
* `:on` close all windows except the current
* `:winc <movement>` or `Ctrl`+`w`+`<movement>` moves the cursor between windows, where `<movement>` can be `hjkl` for vanilla movement or `p` for previous window
    * `Ctrl`+`<movement>` shortcuts the above for `hjkl` movement `!custom`
    * Why not `jkl;`? Unfortunately, Vim doesn't support `Ctrl`+`;` mappings
* `Ctrl`+`w`,`=` resize all windows to equal dimensions

## Nip into the terminal

Suspend Vim with `Ctrl`+`z` for terminal access, then restore session with `fg`.

## Code folding

* `zf<motion>` fold an area of code
    * `zfip` fold a paragraph
    * `zf%` fold to matching bracket
* `zo` open a fold
* `zc` close a fold
* `za` toggle fold
* `zR` expand all folds
* `zM` close all folds
* `zE` erase all folding brackets

## Line breaking

* Disable line breaking with `:set textwidth=0 wrapmargin=0`

## Filetypes

* `set ft?` show current filetype

## Registers

* see `:help registers`
* `:reg` show all currently defined registers
* `"+p` paste from system clipboard on Linux
* `"*p` paste text currently highlighted by mouse on Linux
* `"ayy` yank line into register `a`
* `"Ayy` append line to register `a`
* `"ap" paste from register `a`
* `"` is the unnamed and default register, so `yy == ""yy`
* `_` is the discard or black hole register (don't store yanked text in any register)
* `0` is the register for last yanked text (with no explicit register) (ie. `y` only)
* `1` is similarly for "long" `d` or `c`, with `-` for "short", and stacks into subsequent numbered registers `2`, `3`, etc.

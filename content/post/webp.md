+++
title = "Converting images to WebP"
date = 2021-04-17T21:20:09+01:00
categories = ["web"]
tags = ["image","webp","jpg","jpeg","png","lossy-compression","lossless-compression","compression","fedora","fedora-33","linux","photograph","picture","image"]
description = "So I can provide smaller images here"
toc = true
draft = false
cover = "img/covers/27.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

While [the jury's still up on whether `webp` really beats `jpg`]({{< ref "exporting-photographs-for-web-with-gimp#format" >}}), converting source images---whether `png` screenshots or `jpg` photographs---to `webp` couldn't be easier.
On Fedora, install the [tool set](https://developers.google.com/speed/webp/download) with
```shell
sudo dnf install libwebp-tools
```
then use the [compression tool](https://developers.google.com/speed/webp/docs/cwebp) with multithreading enabled as
```shell
cwebp -mt -o <output> <input>
```
The default (lossy) quality setting seems sensible and there are [plenty of options](https://developers.google.com/speed/webp/docs/cwebp#lossy_options) to play with if desired.
You can also scale the image with fixed aspect ratio using the flag `-resize <width> 0`.

For the best lossless compression, ideal for screenshots, use
```shell
cwebp -mt -z 9 -o <output> <input>
```
I used this to reduce the size of `png` screenshot images on this website.
In the best cases, this provided images that were a quarter of their original size and in the worst a half.

`webpinfo` is useful for printing the format (lossy/lossless) and resolution.

To view `webp` images in `eog`, you also need `webp-pixbuf-loader`.
Alternatively, they can be viewed in a modern browser like `firefox`.


+++
title = "What's the deal with SD cards?"
date = 2021-08-26T08:45:06+01:00
categories = ["hardware"]
tags = ["sd-card","memory-card","data-storage","dd"]
description = "Translating the propaganda"
toc = true
draft = false
cover = "img/covers/31.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[Secure Digital (SD)](https://www.sdcard.org/) {{< wiki SD_card >}} is the industry standard format for non-volatile memory cards; it is {{< wl "proprietary" "SD_card#Openness_of_specification" >}}.

I've been playing around with [Raspberry Pis (RPis)]({{< ref rpi >}}) a bit of late.
They use micro SD cards (except the Pi 1 non-plus models, which use a standard SD card) as the main disk.
My [new phone]({{< ref "fp3" >}}) also accepts a micro SD card to expand data storage.
Thus, I familiarised myself with their specifications.

## Standards

Here I cover the meanings of the pertinent little symbols found on SD cards.
I use {{< wl Wikipedia SD_card >}} as source material.

### Physical form factors
In decreasing size: `Standard`, `MiniSD`, `MicroSD`.

### Capacity
There's a {{< wl "capacity standard" "SD_card#Comparison" >}}, but this seems obsolete when you know the size anyway.
These are, in increasing capacity, the `SD`, `SDHC`, `SDXC`, and `SDUC` logos.

Standard|Maximum capacity
---|---
`SD`|2 GB
`SDHC`|32 GB
`SDXC`|2 TB
`SDUC`|128 TB

### Bus speed
The {{< wl "bus speed standards" "SD_card#Bus" >}} give an idea of potential performance.
The Ultra High Speed (UHS) bus classes are represented by the `I`,`II`,`III` logos on the cards.
More `I`s mean more bandwidth.
This seems superfluous when we can just look at the read/write speed ratings (next).

### Sequential write speed
There are a few (redundant) sequential write {{< wl "speed classes" "SD_card#Class" >}}.
These provide an easy indicator of the card's performance.

Minimum sequential write (MB/s)|Speed Class|UHS Speed Class|Video Speed Class
---|---|---|---
2|`C2`||
4|`C4`||
6|`C6`||`V6`
10|`C10`|`U1`|`V10`
30||`U3`|`V30`
60|||`V60`
90|||`V90`

### Random IOPS
The Application Performance Classes `A1` and `A2` indicate minimum random input/output operations per second (IOPS).
This is a useful measure to have in addition to the sequential write speed, particularly for using in RPis.
They also mandate a minimum sequential write speed of 10 MB/s (=`C10`=`U1`=`V10`)

Class|Minimum random read IOPS|Minimum random write IOPS
---|---|---
`A1`|1500|500
`A2`|4000|2000

## Benchmarks
### Unmarked
[One of my Pis]({{< ref "rpi-dns#hardware" >}}) came with an unmarked 64GB SD card.
I did a quick [`dd` benchmark](https://wiki.archlinux.org/title/Benchmarking#dd) of it on the Pi
```shell
$ dd if=/dev/zero of=tempfile bs=1M count=1024 conv=fdatasync,notrunc status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 64.4409 s, 16.7 MB/s
$ echo "echo 3 > /proc/sys/vm/drop_caches" | sudo sh
$ dd if=tempfile of=/dev/null bs=1M count=1024 status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 46.4494 s, 23.1 MB/s
$ dd if=tempfile of=/dev/null bs=1M count=1024 status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 42.6395 s, 25.2 MB/s
# duplicate buffer-cache read test
$ dd if=tempfile of=/dev/null bs=1M count=1024 status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 42.9618 s, 25.0 MB/s
```

Type|MB/s|MiB/s|Class
---|---|---|---
Write|16.7|15.9|`C10`
Read|23.1|22.0|
Buffer-cache read (mean)|25.1|23.9|

### SanDisk Ultra
I got a new SD card for the other Pi, a 16GB SanDisk Ultra. 
On my laptop, I [formatted the partition](https://wiki.archlinux.org/title/File_systems#Create_a_file_system)
```shell
$ lsblk
...
sda                               8:0    1  14.8G  0 disk
└─sda1                            8:1    1  14.8G  0 part /run/media/ryan/3636-3735
...
```
Note that 14.8 [GiB]({{< ref "jargon#binary" >}}) is 15.9 [GB]({{< ref "jargon#denary" >}}).
```shell
$ sudo umount /dev/sda1
$ sudo mkfs.ext4 /dev/sda1
mke2fs 1.45.6 (20-Mar-2020)
/dev/sda1 contains a vfat file system
Proceed anyway? (y,N) y
Creating filesystem with 3888512 4k blocks and 972944 inodes
Filesystem UUID: 2b8f0012-fde4-4b98-affb-b78e1cc189d6
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208

Allocating group tables: done
Writing inode tables: done
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
$ mkdir ~/tmp-mnt
$ sudo mount /dev/sda1 ~/tmp-mnt
$ sudo chown ryan ~/tmp-mnt
$ cd ~/tmp-mnt
```
Then I preceded with the test
```shell
$ dd if=/dev/zero of=tempfile bs=1M count=1024 conv=fdatasync,notrunc status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 58.2769 s, 18.4 MB/s
$ echo 3 | sudo tee /proc/sys/vm/drop_caches
$ dd if=tempfile of=/dev/null bs=1M count=1024 status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 27.5672 s, 38.9 MB/s
$ dd if=tempfile of=/dev/null bs=1M count=1024 status=progress
1024+0 records in
1024+0 records out
1073741824 bytes (1.1 GB, 1.0 GiB) copied, 0.153346 s, 7.0 GB/s
```

Type|Denary prefix|Binary prefix|Class
---|---|---|---
Write|18.4 MB/s|17.6 MiB/s|`C10`
Read|38.9 MB/s|37.1 MiB/s|
Buffer-cache read|7.0 GB/s|6.5 GiB/s|

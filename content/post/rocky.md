+++
title = "Rocky Linux"
date = 2025-03-10T13:20:49Z
categories = ["operating-system"]
tags = ["rocky-linux","rocky-linux-9.5","rocky-linux-9","rocky-linux-blue-onyx","blue-onyx","server"]
description = "Configuring a Rocky server"
toc = true
draft = true
#cover = "img/covers/?.avif"
#coveralt = "?"
#clip = 25
+++

Back to [Rocky Linux]({{< ref "rocky-linux" >}}) for running enterprise servers.

I did a completely default installation for target "Server with GUI".
Below is a log of the configurations I made after installtion.

## EPEL

The Extra Packages for Enterprise Linux repository contains useful... extra packages like `htop`.

```shell
sudo dnf install epel-release
```

## Persistent journal

By default, the journal is volatile. It's better to write to disk so we can check the logs from previous boots, eg. `journalctl -b -1`.

```shell
sudo vim /etc/systemd/journald.conf.d/persistent.conf
```
```vim
[Journal]
Storage=persistent
```
```shell
sudo systemctl restart systemd-journald.service
```

## Podman

We run OCI images using `podman` and coordinate their deployment with `quadlet`.

+++
title = "Air quality monitor"
date = 2021-10-22T08:43:54+01:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
+++

MCU
ESP32, ESP8226

Particulate matter
PM1, PM2.5, PM10 (micrometers)
PMS5003

CO


CO2
MH-Z19

Ozone
MQ-131

Volative organic compounds VOC
MP503

Temperature and humidity
DHT22

MQ-2, MQ-5, MQ-9, MQ-6, M-306A, AQ-3

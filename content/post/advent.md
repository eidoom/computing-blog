+++
title = "Advent of Code 2019"
date = 2020-01-25T21:24:31Z
categories = ["advent-of-code"]
tags = ["python","cpp","advent-of-code-2019","c++","aoc","aoc19"]
description = "Twenty-five festive programming puzzles"
toc = true
cover = "img/covers/37.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I was recently introduced to [Advent of Code](https://adventofcode.com/about) by a friend. 
It's a festive annual coding challenge, reminiscent of [Project Euler](https://projecteuler.net/) but less mathsy and more compsciy.
I [started](https://gitlab.com/eidoom/advent-of-code-2019/commit/dc69a93f0a978d244cd3fd6db447fe0c419cb9a5) the [2019 challenges](https://adventofcode.com/2019) while travelling on the train in the Christmas break and have been [working through them](https://gitlab.com/eidoom/advent-of-code-2019/commits/master) when I get the chance over the last month.
I've only [completed](https://gitlab.com/eidoom/advent-of-code-2019/tree/d9562e5a198d274c48ff407275d5e0a0948d3491) the first fifteen days so far.

I've really enjoyed these coding problems and feel that I'm learning a lot in doing them.
I do as much as possible offline, then research computer science techniques when I'm stuck.
After completion, I also check out how others' solutions on the [subreddit](https://www.reddit.com/r/adventofcode/).

My solutions are on [GitLab](https://gitlab.com/eidoom/advent-of-code-2019), mainly using Python.

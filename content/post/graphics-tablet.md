+++
title = "Huion Inspiroy WH1409 V2 graphics tablet on Linux"
date = 2021-02-19T20:02:52Z
edit = 2021-04-05
categories = ["driver"]
tags = ["graphics-tablet","linux","fedora","fedora-33","pen-tablet","stylus","krita","digimend","input-wacom","huion"]
description = "Getting going with a new graphics tablet on Fedora 33"
toc = true
draft = false
cover = "img/covers/12.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Initial impressions

The [Huion Inspiroy WH1409 V2](https://huion.uk/collections/pen-tablets/products/huion-inspiroy-wh1409-v2) is a new piece of nonstandard hardware with [no official Linux support](https://digimend.github.io/support/misc/vocabulary/), so I didn't expect much of it on Linux.
I opted for it because work gave me a hard price limit on what they'd pay for and you get a much bigger working area for the same price going with this Huion versus the standard Wacom Intuos Pro options, at the compromise of inferior drivers (for all operating systems).
I found the experience on Windows with the official drivers to be a bit pants as it kept not recognising pen input, requiring the device to be disconnected and reconnected.
This was annoying and prompted me to try Linux, which is where I'd rather work anyway.

## Linux Wacom

I first tried plugging it in to my desktop without installing any drivers.
Surprisingly, it worked including all important pressure sensitivity.
`GNOME Settings > Wacom Tablet > Tablet` reported it as a different model, the [Huion H610PRO](https://www.huion.com/pen_tablet/Huion/H610PRO.html), even providing a nice interface to map the press keys, albeit in the wrong layout, of course.
I'll use the keyboard for shortcuts anyway, so I don't mind missing out on press key mapping.
Sadly, the stylus wasn't reported in `GNOME Settings > Wacom Tablet > Stylus`, so I couldn't easily customise the thumb keys on it, which would be useful.

Since I'm on X11 anyway for the [Nvidia graphics card drivers]({{< ref "nvidia-drivers-on-fedora" >}}), I believe the tablet was using the [Linux Wacom Project](https://linuxwacom.github.io/) driver `input-wacom` {{< arch "Wacom_tablet" >}}, which seems to be the main project providing tablet support - for any manufacturer - to the Linux kernel.
[This driver](https://github.com/linuxwacom/input-wacom) is already in the kernel, and the [X11 driver](https://github.com/linuxwacom/xf86-input-wacom) (which was already on my system for some reason) and [tablet library](https://github.com/linuxwacom/libwacom) can be installed with
```shell
sudo dnf install xorg-x11-drv-wacom libwacom
```

## huion-linux-drivers

I also looked into dedicated drivers.
I [found](https://support.huion.com/en/support/discussions/topics/44001010998) that there was a [project](https://github.com/joseluis/huion-linux-drivers) for Huion Linux userspace drivers, but support has been dropped and it's in (potentially unperformant) Python, so I passed that one by.

## DIGImend

No, not [Digimon](https://en.wikipedia.org/wiki/Digimon).

The [DIGImend project](https://digimend.github.io/) [their website seems out of date] {{< ghub "DIGImend" "digimend-kernel-drivers" >}} provides kernel drivers for new tablets, although it doesn't advertise support for this particular model.
I installed it with
```shell
wget https://github.com/DIGImend/digimend-kernel-drivers/releases/download/v10/digimend-kernel-drivers-10.tar.gz
tar xf digimend-kernel-drivers-10.tar.gz
cd digimend-kernel-drivers-10
sudo dnf install -y "kernel-devel-uname-r == $(uname -r)" dkms
sudo make dkms_install
```
and found it worked just the same as with `input-wacom`.

Strangely, when I uninstalled the DIGImend drivers, the tablet was no longer recognised like it was on first plug in, so I settled on using DIGImend.
I don't know why this happened.
I also don't know what DIGImend currently offers beyond the latest Linux kernel, which it upstreams to, but it seems like a good way to get bleeding edge support.
I was additionally surprised that it didn't require a standard X11 driver like `xorg-x11-drv-wacom` {{< ghub "linuxwacom" "xf86-input-wacom" >}} or `xorg-x11-drv-evdev` {{< ghub "freedesktop" "xorg-xf86-input-evdev" >}}.

The main issue is that I can't easily remap the stylus thumb buttons, but there's [a way](https://digimend.github.io/support/howto/drivers/evdev/) to customise this if it becomes critical.
The default setting is fine in Krita, at least.

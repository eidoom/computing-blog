+++
title = "Reconfiguring Neovim"
date = 2021-06-05T08:49:21+01:00
categories = ["environment-core"]
tags = ["vi","vim","nvim","neovim","linux","environment","vim-plug","terminal","text-editor","configure","customise"]
description = "A clean slate for my text editor"
toc = true
draft = false
cover = "img/covers/50.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

> {{< cal "2024-08-12" >}}
>
> DEPRECATED by {{< gl dotfiles-nvim-lua >}}.

In search of a lighter, more personalised experience, I decided to start afresh with my `vi` configuration.
The new files are in the repository {{< gl dotfiles-nvim-2 >}}, including installation instructions.
The old setup is described [here]({{< ref "neovim" >}}), including some details on installation of plugins.

I continued to use the plugin manager {{< gh junegunn vim-plug >}}.
I loaded the plugins
* {{< gh maxbrunsfeld vim-yankstack >}}
* {{< gh tpope vim-commentary >}}
* {{< gh bling vim-bufferline >}}
* {{< gh sheerun vim-polyglot >}}
* {{< gh Chiel92 vim-autoformat >}}
* {{< gh iCyMind NeoSolarized >}}

and conditionally for language support
* {{< gh bakpakin janet.vim >}}
* {{< gh BeneCollyridam futhark-vim >}}

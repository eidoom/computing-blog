+++
title = "Satellite MPD on a Raspberry Pi"
date = 2021-08-23T10:36:27+01:00
categories = ["audio"]
tags = ["home","raspberry-pi","rpi","raspi","mpd","headless","server","arm","audio","media","music","malp","mpc","nix","git","ncmpcpp","pms","vimpc","malp","android","linux","f-droid","desktop","laptop","fedora","fedora-34","raspian","raspios","flash","install","dd","static-ip","dhcpcd","systemd","service","socket","daemon","nfs","ssh","alsa"]
description = "A convenient way to play music at home"
toc = true
draft = false
cover = "img/covers/6.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

In yet another reconfiguration of how I play the digital music from my fileserver on my living room speakers, I decided to employ another [Raspberry Pi]({{< ref rpi >}}) (another [2B]({{< ref "rpi-dns#hardware" >}})).
I'll use [MPD](https://www.musicpd.org/) {{< docs "https://mpd.readthedocs.io/en/stable/user.html" >}} {{< ghub MusicPlayerDaemon MPD >}} {{< arch "Music_Player_Daemon" >}}, with a *server* instance on the server to manage the database and a *satellite* instance on the Pi to manage playback.
This setup is described in the [docs](https://mpd.readthedocs.io/en/stable/user.html#satellite-setup) and in this [blog post](https://mathieu-requillart.medium.com/my-ultimate-guide-to-the-raspberry-pi-audio-server-i-wanted-mpd-7ec04aabf91f) I found.

## Server

I installed the daemon [`mpd`](https://packages.debian.org/testing/mpd) (from [testing]({{< ref "server-install#installing-selectively-from-testing" >}})) and interface [`mpc`](https://packages.debian.org/stable/mpc) {{< docs "https://musicpd.org/doc/mpc/html/" >}} {{< ghub MusicPlayerDaemon mpc >}}
```shell
sudo apt install -t testing mpd
sudo apt install mpc
```
and configured `mpd`
```shell
sudo vi /etc/mpd.conf
```
```vim
music_directory         "/media/disk-3a/data/music"
playlist_directory      "/var/lib/mpd/playlists"

db_file                 "/var/lib/mpd/tag_cache"
log_file                "syslog"
state_file              "/var/lib/mpd/state"
sticker_file            "/var/lib/mpd/sticker.sql"

user                    "mpd"

auto_update             "yes"

filesystem_charset      "UTF-8"

audio_output {
        type            "null"
        name            "Silence"
}
```
The default port is 6600.

The `music_directory` is on an [NFS share]({{< ref "nfs" >}}).

Rather than running `mpd` as a [`service`](https://www.freedesktop.org/software/systemd/man/systemd.service.html), we'll use a [`socket`](https://www.freedesktop.org/software/systemd/man/systemd.socket.html), so [the daemon will run on demand](https://mpd.readthedocs.io/en/stable/user.html#systemd-socket-activation)
```shell
sudo systemctl disable mpd.service
sudo systemctl stop mpd.service
sudo systemctl enable mpd.socket
sudo systemctl start mpd.socket
```

I started the library scan
```shell
mpc update
```

In the hope that the satellite will have no problems with Linux file permissions on the NFS share,
```shell
sudo chown -R ryan:ryan music/
chmod -R o+r music/
```

## Satellite

### Setup the RPi

> {{< cal "2024-10-10" >}}
>
> I got a second RPi 2B to have another satellite.
> I downloaded the latest version of [32-bit Raspberry Pi OS Lite](https://www.raspberrypi.com/software/operating-systems/#raspberry-pi-os-32-bit) using the [torrent]({{< ref "bittorrent" >}}) download to be kind to their servers.
> This provides a compressed image, which I uncompressed with
> ```shell
> unxz -k 2024-07-04-raspios-bookworm-armhf-lite.img.xz
> ```
> I got access to the [SD card]({{< ref "sd-card" >}}) by inserting it into an SD card slot on a USB hub attached to my laptop.
> Ensure none of the partitions of the SD card is mounted (`lsblk` and `umount`) before proceeding.
> Now we can continue with the original post.

Check name of SD card with `lsblk`, then flash with RasPiOS using [`dd`]({{< ref "utilities#dd" >}}),
```shell
$ sudo dd if=2021-05-07-raspios-buster-armhf-lite.img \
    of=/dev/sda bs=64k oflag=dsync status=progress
28608+0 records in
28608+0 records out
1874853888 bytes (1.9 GB, 1.7 GiB) copied, 86.1886 s, 21.8 MB/s
```

On Pi, check the IP address and enable SSH access (should have done this on the laptop, but I didn't realise `ssh` was disabled by default).
Note that the default user is `pi` with password `raspberry`.
```shell
ip a  # note <ip address> 
sudo raspi-config  # 3 Interface Options > P2 SSH > Yes
reboot
```

> To do this from the laptop (Fedora on `x86-64`) right after flashing, we [can](https://unix.stackexchange.com/a/222981/156381) `chroot` and emulate `arm` with `qemu`,
> ```shell
> sudo dnf install qemu-user-static
> mount /dev/sda2 ~/mnt
> sudo cp /usr/bin/qemu-arm-static ~/mnt/usr/bin
> sudo chroot ~/mnt qemu-arm-static /bin/bash
> ```
> then use `raspi-config` as above.
> 
> [Since Bullseye](https://www.raspberrypi.com/news/raspberry-pi-bullseye-update-april-2022/), there is also no longer a default `pi` user.
> To setup the user, mount the boot disk, eg `/dev/sda1`, and create a file `userconf` in its root,
> ```shell
> sudo dnf install openssl
> echo -n '<user>:' > userconf
> echo '<password>' | openssl passwd -6 -stdin >> userconf
> ```

On laptop,
```shell
ssh pi@<satellite address>
```
and set a static IP address (nb RaspPiOS uses `dhcpcd` not [`networking`]({{< ref "rpi-dns#network" >}}) like Debian by default), for example,
```shell
sudo vi /etc/dhcpcd.conf
```
```vim
interface eth0
static ip_address=192.168.1.4/24
static routers=192.168.1.1
static domain_name_servers=192.168.1.1
```

> Or, even better, set up a [static route]({{< ref "ipv6#static-lease" >}}) on a DHCP server for this.
> Then `dhcpcd.conf` need not be edited (the default `auto eth0` is correct).

and set hostname,
```shell
sudo hostnamectl set-hostname <hostname>
```
and update `/etc/hosts`.

Finally,
```shell
sudo apt update
sudo apt -y upgrade
```
and install some useful utilities like [`vim`]({{< ref "vim" >}}) and [`htop`]({{< ref "utilities#processes" >}}),
```shell
sudo apt install vim htop libsensors5
```

then
```shell
sudo reboot
```
### And mpd
Get back in
```shell
ssh pi@<satellite address>
```
and install the good stuff
```shell
sudo apt install mpd mpc
sudo systemctl enable mpd
```
Configure as
```shell
sudo vi /etc/mpd.conf
```
```vim
music_directory         "nfs://<server address>/media/disk-3a/data/music"
playlist_directory      "/var/lib/mpd/playlists"  # this could also have been shared

log_file                "syslog"
state_file              "/var/lib/mpd/state"
sticker_file            "/var/lib/mpd/sticker.sql"

user                    "mpd"

database {
    plugin              "proxy"
    host                "<server address>"
}

audio_output {
        type            "alsa"
        name            "ALSA"
        mixer_type      "software"
}

filesystem_charset      "UTF-8"
```

Notice that compared to Debian's default configuration, we don't `bind_to_address` or use a `db_file`.

The ALSA audio settings automatically configure, but this sometimes fails, hence why I manually set the `audio_output`.

[Note that NFS can get upset about the ports]({{< ref "#nfs-permissions" >}}).

After
```shell
sudo systemctl start mpd
```
it worked!

Set and save the volume with
```shell
alsamixer
sudo alsactl store
```

## Clients

There are lists of clients [here](https://www.musicpd.org/clients/) and [here](https://wiki.archlinux.org/title/Music_Player_Daemon#Clients).

We have `mpc` on the server and satellite for management and debugging purposes.

### Mobile

On my [Android phone]({{< ref fp3 >}}), I use [M.A.L.P.](https://gitlab.com/gateship-one/malp) from [f-droid](https://f-droid.org/en/packages/org.gateshipone.malp/) as `mpd` client.

### Desktop

#### ncmpcpp
On my laptop, I rather like [`ncmpcpp`](https://rybczak.net/ncmpcpp/) {{< ghubn "ncmpcpp/ncmpcpp" >}}.

Install on Fedora 34 with
```shell
sudo dnf install ncmpcpp
```

Invoke with
```shell
ncmpcpp --host 192.168.1.4
```
or, after [setting up names]({{< ref "new-dns#nsd" >}}),
```shell
ncmpcpp --host rpi-mpd.lan
```
or, even better, set the host in the config,
```shell
cp /usr/share/doc/ncmpcpp/config .config/ncmpcpp/config  # the template
echo "mpd_host = <hostname or ip address>" >> .config/ncmpcpp/config
```

The only problem is that the album library view (press `4` twice) can be slow to show with large libraries.

#### pms
[Practical Music Search](https://ambientsound.github.io/pms/) {{< ghubn "ambientsound/pms" >}} {{< docs "https://github.com/ambientsound/pms/tree/master/doc" >}} isn't in the Fedora repos.
With [Go installed]({{< ref "go" >}}), install `pms` from source with
```
git clone https://github.com/ambientsound/pms
cd pms
go get ./...
make install
```
and run with
```shell
pms --host=rpi-mpd.lan
```

#### vimpc
[`vimpc`](https://github.com/boysetsfrog/vimpc) {{< docs "https://raw.githubusercontent.com/boysetsfrog/vimpc/master/doc/help.txt" >}} is also not in the Fedora repos, but is available through [Nix]({{< ref "nix" >}})
```shell
nix-env -iA nixpkgs.vimpc
vimpc --host rpi-mpd.lan
```

## Outlook

I could also [use PulseAudio to stream system audio from devices to be played on the Pi](https://wiki.archlinux.org/title/PulseAudio/Examples#PulseAudio_over_network).

## Appendices

### Audio on vanilla Debian

I tried to get this working on the [Debian image]({{< ref "rpi-dns#operating-system" >}}), but couldn't get audio to work.
I set up the [kernel modules](https://archlinuxarm.org/wiki/Raspberry_Pi) and then tried [`pulseaudio`](https://packages.debian.org/stable/pulseaudio) {{< ext "https://www.freedesktop.org/wiki/Software/PulseAudio/" >}} {{< ext "https://wiki.debian.org/PulseAudio" >}} for managing audio devices on the Pi, with `/etc/mpd.conf` including
```vim
audio_output {
    type "pulse"
    name "pulse audio"
    server "127.0.0.1"
}
```

but [maybe that doesn't work](https://wiki.debian.org/RaspberryPi4#Pulse_Audio_and_Sound_don.27t_work_as_of_04.2F07.2F2021)?

I also tried [ALSA](https://alsa-project.org/wiki/Main_Page) {{< ext "https://wiki.debian.org/ALSA" >}} with [`libasound2`](https://packages.debian.org/stable/libasound2) and [`alsa-utils`](https://packages.debian.org/stable/alsa-utils)
```shell
sudo apt install libasound2 alsa-utils
sudo adduser ryan audio  # and relogin
sudo modprobe snd_bcm2835
aplay -l
speaker-test
```
but it was in vain: no analogue audio device detected.

Hence why I used RasPiOS.

I wonder if [this](https://kevinboone.me/pi_audio.html) holds some clues?

### NFS permissions

I had some issues with access permissions on the NFS share.
I tried
```vim
user                    "mpd"
group                   "ryan"
```
where the `gid` of `ryan` matches on the server and satellite, but got
```shell
$ mpc play
$ mpc
...
ERROR: Failed to decode nfs://ryanserver.lan/media/disk-3a/data/music/<song path>; nfs_mount_async() failed: RPC error: Mount failed with error MNT3ERR_ACCES(13) Permission denied(13)
```
so set user to `root` for now to temporarily fix it.

> {{< cal "2023-11-22" >}}
>
> Then I realised it was a [port problem](https://bbs.archlinux.org/viewtopic.php?id=234863).
> I used a less severe hack to make the problem go away: running NFS in `insecure` mode (all ports are go) by adding the flag in `/etc/exports` to the appropriate share.

### Network discovery

You can use [Avahi](https://packages.debian.org/stable/avahi-daemon) for [network discovery](https://wiki.debian.org/ZeroConf)
```shell
sudo apt install avahi-daemon
```
although I haven't tried it.

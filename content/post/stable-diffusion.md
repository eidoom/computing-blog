+++
title = "Creating images from text with Stable Diffusion and Real-ESRGAN"
date = 2022-10-09T20:43:08+02:00
categories = ["machine-learning"]
tags = ["ai-art","art","stable-diffusion","gpu","conda","server","python","miniconda","image","picture","pytorch","numpy","cuda","nvidia","ml","ai","real-esrgan","upscaling","machine-learning","artificial-intelligence","diffusion-model","desktop","linux","torch","data","a100-80gb","tesla-v100-32gb","git","github","huggingface","venv","virtual-environment"]
description = "An art project"
toc = true
draft = false
cover = "img/covers/33.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

I [generated images using Stable Diffusion]({{< ref "#image-generation" >}}) and [upscaled them with Real-ESRGAN]({{< ref "#upscaling" >}}) for fun.

One server machine I used had an [Nvidia A100 80GB GPU](https://www.nvidia.com/en-gb/data-center/a100/).
The other machines had with a pair of [Nvidia Tesla V100 32GB or 16GB GPUs](https://www.nvidia.com/en-gb/data-center/tesla-v100/).
Here, I [controlled which card I used](https://stackoverflow.com/a/39661999) with the environment variable `CUDA_VISIBLE_DEVICES=#` with `#` as `0` or `1`.

## Image generation

I [set](https://medium.com/geekculture/run-stable-diffusion-in-your-local-computer-heres-a-step-by-step-guide-af128397d424) [up](https://www.assemblyai.com/blog/how-to-run-stable-diffusion-locally-to-generate-images/) the [Stable Diffusion model](https://ommer-lab.com/research/latent-diffusion-models/) {{< wiki "Stable Diffusion" >}} {{< ghub "CompVis" "stable-diffusion" >}} {{< git "https://huggingface.co/CompVis/stable-diffusion" >}} {{< pl "https://arxiv.org/abs/2112.10752" >}} {{< pl "https://stability.ai/blog/stable-diffusion-public-release" >}} on Linux.

### Installation

I installed [Miniconda](https://docs.conda.io/en/latest/miniconda.html) at `INSTALL_LOCATION` to handle the Python environment
```shell
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
sh Miniconda3-latest-Linux-x86_64.sh
```
I cloned the Stable Diffusion repository, which provides reference tools to use the model
```shell
cd ~/git
git clone https://github.com/CompVis/stable-diffusion.git
```
I downloaded the [stable-diffusion-v1-4](https://huggingface.co/CompVis/stable-diffusion-v-1-4-original) model by cloning its repo, which requires signing up to Hugging Face and accepting the conditions for model access
```shell
cd ~/git
git clone https://huggingface.co/CompVis/stable-diffusion-v-1-4-original  # requires user credentials and Git LFS
```
I symlinked the model file to the tools
```shell
mkdir -p ~/git/stable-diffusion/models/ldm/stable-diffusion-v1/
ln -s ~/git/stable-diffusion-v-1-4-original/sd-v1-4.ckpt ~/git/stable-diffusion/models/ldm/stable-diffusion-v1/model.ckpt 
```
I set up the Python environment
```shell
eval "$(/INSTALL_LOCATION/miniconda3/bin/conda shell.bash hook)"  # only necessary if conda not in PATH
cd ~/git/stable-diffusion
conda env create -f environment.yaml
conda activate ldm
```

### Usage

I generated some images using
```shell
cd ~/git/stable-diffusion
python3 scripts/txt2img.py \
    --plms \
    --outdir "/full/path/to/destination" \
    --n_iter 1 \
    --n_sample 4 \
    --n_rows 2 \
    --W 640 \
    --H 640 \
    --ddim_steps 100 \
    --prompt "PROMPT" \
    --seed 42
```

Comments:
* I used the fancy [PLMS](https://arxiv.org/abs/2202.09778) sampler.
* Note `outdir` doesn't resolve `~`.
* Single `n_iter` above because I'm trying different prompts.
* With more than 4 `n_samples` at `--W 640 --H 640` (0.39 [MiP]({{< ref "jargon#binary" >}})), I started getting strange buggy output images.
* The image height `H` and width `W` must be multiples of 64.
* I found square images came out too chaotic at higher resolutions than 640x640.
* I found `--W 1024 --H 512` (0.5 MiP) worked well for 2:1 aspect ratio landscape images (`n_sample 2` is the limit for 30GB VRAM, while `n_sample 1` just fits on 16GB VRAM).
* It was hard to tell if increasing `ddim_steps` beyond the default of 50 improved things, although I think it made more difference at higher resolutions. It crashed if I pushed beyond 250.
* `--n_sample 9 --W 512 --H 512` is the batch limit for 80GB VRAM on the default resolution of 512x512 (0.25 MiP).
* We can use `$RANDOM` to quickly get random numbers for the `seed`.

### Results

I used [AVIF]({{< ref "avif" >}}) for these images.
In terms of licencing, the images are in the public domain ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/)).

#### Sci-fi

For example, with the prompt
```txt
scifi concept art, highly detailed colorful crisp digital painting, high dynamic range
```

<div class="sq-img">
<img src="/img/ai/sf/00274.avif">
<img src="/img/ai/sf/00275.avif">
<img src="/img/ai/sf/00276.avif">
<img src="/img/ai/sf/00277.avif">
<img src="/img/ai/sf/00365-edit.avif">
<img src="/img/ai/sf/00366.avif">
<img src="/img/ai/sf/00367.avif">
<img src="/img/ai/sf/00368.avif">
<img src="/img/ai/sf/00369.avif">
<img src="/img/ai/sf/00370.avif">
<img src="/img/ai/sf/00371.avif">
<img src="/img/ai/sf/00374.avif">
<img src="/img/ai/sf/00375.avif">
<img src="/img/ai/sf/00376.avif">
<img src="/img/ai/sf/00377.avif">
</div>

#### Cyperpunk

Or prepending `cyberpunk` themed phrases to the previous prompt

<div class="sq-img">
<img src="/img/ai/cp/00110.avif">
<img src="/img/ai/cp/00111.avif">
<img src="/img/ai/cp/00112.avif">
<img src="/img/ai/cp/00113.avif">
<img src="/img/ai/cp/00114.avif">
<img src="/img/ai/cp/00115.avif">
<img src="/img/ai/cp/00402.avif">
<img src="/img/ai/cp/00403.avif">
<img src="/img/ai/cp/00406.avif">
<img src="/img/ai/cp/00407.avif">
<img src="/img/ai/cp/00408.avif">
<img src="/img/ai/cp/00409.avif">
</div>

#### Fantasy landscape

##### Variant one

<div class="sq-img">
<img src="/img/ai/hf1/00270.avif">
<img src="/img/ai/hf1/00271.avif">
<img src="/img/ai/hf1/00272.avif">
<img src="/img/ai/hf1/00273.avif">
<img src="/img/ai/hf1/00390.avif">
<img src="/img/ai/hf1/00391.avif">
<img src="/img/ai/hf1/00392.avif">
<img src="/img/ai/hf1/00393.avif">
</div>

##### Variant two

<div class="sq-img">
<img src="/img/ai/hf2/00386.avif">
<img src="/img/ai/hf2/00387.avif">
<img src="/img/ai/hf2/00388.avif">
<img src="/img/ai/hf2/00389.avif">
<img src="/img/ai/hf2/00398.avif">
<img src="/img/ai/hf2/00399.avif">
<img src="/img/ai/hf2/00400.avif">
<img src="/img/ai/hf2/00401.avif">
</div>

## Upscaling

I tried [traditional methods]({{< ref "exporting-photographs-for-web-with-gimp#imagemagick" >}}), but they didn't really cut it.
Instead, I used another machine learning solution, [Real-ESRGAN](https://github.com/xinntao/Real-ESRGAN), to upscale.

### Installation

I [installed](https://github.com/xinntao/Real-ESRGAN#installation) it with
```shell
git clone https://github.com/xinntao/Real-ESRGAN.git
cd Real-ESRGAN
# if using conda
conda create -n realesrgan python
conda activate realesrgan
# elif using venv
python3 -m venv .venv
. .venv/bin/activate
# endif
python3 -m pip install -r requirements.txt  # slow
python3 setup.py develop
```

### Usage

[Usage](https://github.com/xinntao/Real-ESRGAN#python-script) is simple, with the model files downloading automatically,
```shell
python3 inference_realesrgan.py \
    -n RealESRGAN_x4plus \
    -i <input> \
    -o <output>
```

### Results

I used the upscaled images as cover images for my posts on this site.
I [downscaled again with traditional methods]({{< ref "exporting-photographs-for-web-with-gimp#imagemagick" >}}) to [manage filesize]({{< ref "avif#responsive-images" >}}),
```shell
magick <input>.png \
    -quality 50 \
    -colorspace RGB \
    -resize 1720 \
    -colorspace sRGB \
    -unsharp 0x0.75+0.75+0.008 \
    <output>.avif
```
Note that the viewport is 860 CSS pixels at the widest media query setting, so I use width 1720 pixels to allow for 2x HiDPI screens.

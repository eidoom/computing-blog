+++
title = "Boot media problem"
date = 2020-01-02T17:21:07Z
categories = ["maintenance"]
tags = ["linux","install","solution","boot","server","desktop","laptop"]
description = "Old trick for pernickety boot media"
toc = true
cover = "img/covers/92.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I have had problems booting Linux off USB sticks on a few devices during install of the operating system.
It usually goes like, appear that everything is working, but then hang, show a blank screen, or give some error message about not being able to find boot media.

The odd solution that has always solved this old issue for me is to **immediately remove the USB stick from its port on the computer and then reinsert it**.
After this, the boot media is detected and the install proceeds are normal.

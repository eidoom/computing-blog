+++
title = "Wireguard VPN"
date = 2022-07-30T16:12:17+02:00
categories = ["self-host"]
tags = ["vpn","server","debian","android","f-droid","fedora","debian-11","fedora-36","dnf","choco","chocolatey","client","laptop","mobile","phone","desktop","wireguard","privacy","debian-bullseye","dns","unbound","dns-over-tls","dot","network","virtual-private-network"]
description = "Self-hosted VPN for privacy"
toc = true
draft = false
cover = "img/covers/22.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I set up my [cloud server]({{< ref cloud >}}) to host a {{< wl "virtual private network" >}} (VPN) server with the [WireGuard](https://www.wireguard.com/) {{< wiki WireGuard >}} {{< git "https://git.zx2c4.com/wireguard-linux" >}} {{< pl "https://packages.debian.org/stable/wireguard" >}} protocol and software.
WireGuard is a new alternative to the popular [OpenVPN](https://openvpn.net/).

While VPNs are mostly thought of these days as a service you connect your device to in order to hide your traffic, that's not the whole story.
A VPN can span numerous devices over a {{< wl "wide area network" >}} (WAN) to create a virtual {{< wl "local area network" >}} (LAN) between them.
Traffic between peers can be encrypted for security, and *all* traffic on a peer can be routed through another peer to provide privacy against the "client" peer's {{< wl "internet service provider" >}} (ISP).
I will add my phone and laptop to the VPN as client peers and route all their traffic through a server peer, which will be my cloud server.
Of course, the company operating the server farm can still see my traffic, so this is only a good idea if I trust them more than my home, mobile, and work ISPs.

## Server

This is on Debian 11.

### VPN

My server configuration is automated using [`cdist`]({{< ref "cloud#software-configuration-management-tool" >}}): I made a [`__wireguard` type](https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__wireguard).
To summarise, I

* install `wireguard` and `resolvconf`,
* set up the public/private key pair with `wg`,
* configure WireGuard at `/etc/wireguard/wg0.conf` including `firewalld` rules,
    ```toml
    [Interface]
    PrivateKey = $server_private_key
    Address    = $internal_server_ip_address/24
    ListenPort = $port
    PostUp     = firewall-cmd --zone=public --add-port $port/udp && firewall-cmd --zone=public --add-masquerade
    PostDown   = firewall-cmd --zone=public --remove-port $port/udp && firewall-cmd --zone=public --remove-masquerade
    ```
* enable IP forwarding with `sysctl`,
* and enable and start the `wg-quick@wg0` service.

`$internal` refers to an address on the VPN.
For each client, I then manually add a `Peer` to the end of `/etc/wireguard/wg0.conf`:
```toml
# name
[Peer]
PublicKey = $client_public_key
AllowedIPs = $internal_client_ip_address/32
```
Note the different netmask.
Then
```shell
sudo systemctl reload wg-quick@wg0
```

### DNS

I also want each of my clients to make [domain name system]({{< ref "/tags/dns" >}}) (DNS) calls through the VPN tunnel, otherwise [the URLs I visit would not be private]({{< ref "dns-unbound#upstream-provider" >}}).
To facilitate this, I'll run my own recursive nameserver with [`unbound`]({{< ref "unbound" >}}) on the cloud server.
The configuration is `cdist` automated at [`__unbound`](https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__unbound).
Be sure to [allow access to the nameserver through the firewall over the VPN](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__wireguard/gencode-remote).

> {{< cal "2023-05-12" >}}
>
> I edited this configuration to allow the use of the DNS server over the Internet.
> To `/etc/unbound/unbound.conf.d/main.conf`, I added the Internet-facing interface with another `interface` line and some new [`tls-service-*`](https://unbound.docs.nlnetlabs.nl/en/latest/manpages/unbound.conf.html#unbound-conf-tls-service-key) options (namely pointing at [my TLS keys]({{< ref "cloud#__tls_wildcard_duckdns" >}})) to set up [DNS over TLS (DoT)]({{< ref "dns-over-tls" >}}),
> ```yaml
>   ...
>   interface: <address>
>   ...
>   tls-service-pem: /etc/letsencrypt/live/<name>/fullchain.pem  # public key
>   tls-service-key: /etc/letsencrypt/live/<name>/privkey.pem  # private key
>
>   tls-port: 853  # this is the default
>   ...
> ```
> while giving `unbound` {{< wl AppArmor >}} permissions to access the TLS keys [by](https://serverfault.com/a/1011037) appending `/etc/apparmor.d/local/usr.sbin.unbound` with
> ```vim
> /etc/letsencrypt/archive/** r,
> /etc/letsencrypt/live/** r,
> ```
> and activating
> ```shell
> sudo apparmor_parser --replace /etc/apparmor.d/usr.sbin.unbound
> ```
> then opened ports 53 (DNS) and 853 (DoT) on the firewall with
> ```shell
> firewall-cmd --permanent --zone=public --add-service=dns
> firewall-cmd --permanent --new-service=dot
> firewall-cmd --permanent --service=dot --add-port=853/tcp
> firewall-cmd --permanent --service=dot --add-port=853/udp
> firewall-cmd --permanent --zone=public --add-service=dot
> firewall-cmd --reload
> ```
> before restarting `unbound` (reloading is insufficient)
> ```shell
> sudo systemctl restart unbound
> ```

## Clients

### Linux

On Fedora 36, install `wireguard-tools`,
```shell
sudo dnf install wireguard-tools
```
create a key pair,
```shell
wg genkey | tee privatekey | wg pubkey > publickey
```
create `/etc/wireguard/wg0.conf` as
```toml
[Interface]
Address = $internal_client_ip_address/32
PrivateKey = $client_private_key
DNS = $internal_server_ip_address
[Peer]
PublicKey = $server_public_key
AllowedIPs = 0.0.0.0/0
Endpoint = $server_url:$port
```
then start (and `stop`) with
```shell
sudo systemctl start wg-quick@wg0
```
Setting `AllowedIPs = 0.0.0.0/0` is what tells the host (client peer) to route all traffic through the server peer.

> {{< cal "2023-05-13" >}}
>
> On Fedora 38, we can [control the VPN connection through NetworkManager](https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/).
> Import the VPN profile with
> ```shell
> sudo nmcli connection import type wireguard file /etc/wireguard/wg0.conf
> ```
> then connect and disconnect through the GUI.

### Android

I installed the [WireGuard app from F-Droid](https://f-droid.org/en/packages/com.wireguard.android/).
Key pair creation is automatic.
Manual setup is much like for [Linux]({{< ref "#linux" >}}) but in the app.

### Windows

I installed the WireGuard client using [`choco`]({{< ref "home-cinema/#chocolatey" >}}),
```shell
choco install wireguard
```
Key pair creation is automatic.
Manual setup is just like for [Linux]({{< ref "#linux" >}}) but in the app.

## Further reading

For more information, see

* [WireGuard quick start](https://www.wireguard.com/quickstart/)
* [ArchWiki](https://wiki.archlinux.org/title/WireGuard)
* [Debian wiki](https://wiki.debian.org/WireGuard)
* [Jacob’s WireGuard VPN Guide](https://wireguard.how/)
* Fedora magazine: [2019](https://fedoramagazine.org/build-a-virtual-private-network-with-wireguard/) [2021](https://fedoramagazine.org/configure-wireguard-vpns-with-networkmanager/)

+++
title = "Downloading audio from Youtube"
date = 2020-03-14T18:01:34Z
updated = 2021-05-08
categories = ["software"]
tags = ["youtube","opus","media","audio","music","fedora","ffmpeg"]
description = "How to make an offline copy of a Youtube video's audio"
toc = true
cover = "img/covers/84.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Downloading audio from Youtube is a shameful topic for me to write about as someone who appreciates high-quality digital sound (although not to the extent of unscientific audiophilism).
However, it can be useful under certain circumstances.

This can be easily accomplished from the command line with the tool `youtube-dl`.
Install on Fedora including dependencies with
```shell
sudo dnf install youtube-dl ffmpeg
```
then download with
```shell
youtube-dl -x --audio-format opus --audio-quality 0 --write-thumbnail <url>
```
where `<url>` can be a single video or a playlist.
This downloads only the audio, encoding it as highest quality [VBR](https://en.wikipedia.org/wiki/Variable_bitrate) in [`opus`](https://opus-codec.org/) audio format.
`--write-thumbnail` creates a `jpg` of the thumbnail; embedding thumbnails in `opus` files is not supported.

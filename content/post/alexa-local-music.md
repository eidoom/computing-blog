+++
title = "Alexa, ask Plex to play some music"
date = 2019-12-21T12:35:57Z
edit = 2021-04-28
categories = ["audio"]
tags = ["linux","plex","alexa","apt","music","media","vui","install","server","debian","automate","lan","network","debian-buster","debian-10","home-server","sound","audio"]
description = "Voice controlling playback of my local music collection"
toc = true
cover = "img/covers/38.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I was recently kindly gifted an [Echo Dot (2nd gen)](https://www.amazon.co.uk/Amazon-Echo-Dot-2nd-Gen/dp/B01DFKBL68).
I wanted to use it to voice control music playback from the music library on my home server, which is running Debian and has hostname `ryanserver`.
I set this up using [Plex media server](https://www.plex.tv/en-gb/).

## Plex

### Installation

I installed Plex media server by getting the link for the .deb file from [here](https://www.plex.tv/en-gb/media-server-downloads/#plex-media-server) then

```shell
cd ~/tmp
wget https://downloads.plex.tv/plex-media-server-new/1.18.3.2156-349e9837e/debian/plexmediaserver_1.18.2.2058-e67a4e892_amd64.deb
sudo apt install ./plexmediaserver_1.18.2.2058-e67a4e892_amd64.deb
```

That was it.
I checked it had worked (it had) with

```shell
sudo systemctl status plexmediaserver
```

### Configuration

The server can be configured via the web UI at <http://ryanserver:32400/web/index.html>.
Setting up a music library was self-explanatory.
The only extra point required for Alexa compatibility was to ensure that remote access was enabled under `SETTINGS > Remote Access`.

### Updating

The web interface will warn about new version availability. I just copy the link from there, or use

```shell
cd ~/tmp
wget 'https://plex.tv/downloads/latest/5?channel=16&build=linux-x86_64&distro=debian' -O plex.deb
sudo apt upgrade ./plex.deb
```

## Alexa

Plex has [thorough instructions](https://support.plex.tv/articles/115000320808-getting-started-with-alexa-voice-control/) for connecting to Alexa. In short, enable the `Plex skill` on the [Alexa web app](https://alexa.amazon.co.uk/spa/index.html#skills/dp/B01NBB1INY/?ref-suffix=dag_gw) and follow the prompt to link your Plex account. Since I have only one Plex media server and wish playback to occur on the Echo Dot, that was all the set up necessary.

To play specific music I say 

```shell
Alexa, ask Plex to play TRACK/ALBUM/ARTIST
```

Or to play a random shuffle

```shell
Alexa, ask Plex to play some music
```

Also, `Alexa, stop` and `Alexa, continue` work as you'd expect.

+++
title = "Economical APs"
date = 2023-11-02T08:24:52Z
categories = ["network"]
tags = ["network","lan","modem","router","switch","wireless","ethernet","access-point","ieee-802.11","ieee-802.3","wifi","mimo","ap","qam","bandwidth","openwrt","ssid","wi-fi","signal","dhcp","ieee","isp","ee","orange","bt","sky","huawei","netgear","d-link","sagem","twisted-pair"]
description = "Reusing old routers as wireless access points"
toc = true
draft = false
cover = "img/covers/star-network.avif"
#coveralt = "?"
+++

## Preamble

It used to be standard that the customer got to keep the "free" router received as part of their package from UK ISPs.
This is bad from the point of view of waste, but beneficial if they are actually put to use.
I took an inventory of old wireless routers in my possession and used the better ones as {{< wl "wireless access points" >}} (APs) to extend wi-fi coverage at home.
Generally this just involves turning off the DHCP (so it's controlled centrally by a root node), setting the (static) IP address of the router to something free on the subnet (eg. AP: 192.168.0.2, main/gateway/primary/root (pun intended) router: 192.168.0.1), and plugging it into the LAN by ethernet.
I also set the wireless network SSID and password to be the same on all APs so clients can seamlessly migrate between APs[^1].
The OS can be replaced by something like [OpenWrt]({{< ref "openwrt-services" >}}) if compatible (and I'm not feeling lazy) too.

## Jargon

### Wireless

WI = wireless interface.

#### Protocol standard

{{< wl "Wi-fi" >}} is specified by {{< wl "IEEE 802.11" >}}.
I'll mention the following standards.

Generation|Standard|Released|Max. bandwidth (MHz)|Max. MIMO Streams|{{< wl QAM >}}|Frequency (GHz)
---|---|---|---|---|---|---
---|{{< wl "802.11b" "IEEE_802.11b-1999" >}}|1999|20|---|---|2.4
---|{{< wl "802.11a" "IEEE_802.11a-1999" >}}|1999|20|---|---|5
---|{{< wl "802.11g" "IEEE_802.11g-2003" >}}|2003|20|---|---|2.4
Wi-Fi 4|{{< wl "802.11n" "IEEE_802.11n-2009" >}}|2008|40|4|64|2.4/5
Wi-Fi 5|{{< wl "802.11ac" "IEEE_802.11ac-2013" >}}|2013|160|8|256|5
Wi-Fi 6|{{< wl "802.11ax" "IEEE_802.11ax" >}}|2021|160|8|1024|2.4/5

#### Antennas

I'll classify the [MIMO](https://wikidevi.wi-cat.ru/MIMO) ([recall]({{< ref "fixed-wireless-broadband#fn:1" >}})) configuration of the antenna array as `TxR:S`, where the letters stand for the number of:

-   `T`: transmit {{< wl "radio chains" "RF_chain" >}}
-   `R`: receive radio chains
-   `S`: spatial {{< wl "data streams" "Data_stream" >}}

### Ethernet

When I say ethernet, I mean over twisted pair copper wire.
Unless stated otherwise, when I talk about ethernet sockets, I mean {{< wl "8P8C" "Modular_connector#8P8C" >}}.

I'll refer to two speeds:

* 1G: {{< wl "1 Gb/s" "Gigabit_Ethernet" >}} (1000BASE-T/IEEE 802.3ab)
* 100M: {{< wl "100 Mb/s" "Fast_Ethernet" >}} (100BASE-TX/IEEE 802.3u)

## Inventory

A list of the devices with some details.

We can do some [data-rate tests]({{< ref "fixed-wireless-broadband#bandwidth" >}}) too, networking two machines with the device and running:
* on the server
    ```shell
    iperf3 -s
    ```
* on the client
    ```shell
    iperf3 -c <server address>
    ```
    and reading the average sender bitrate (using the default configuration).
For wireless tests, we connect the server via ethernet and the client by wi-fi with a spacial dislocation from the router of around one metre.
We'll use gigabit interfaces on wired machines with at least Cat 5E cables.
Retr = re-transmitted TCP packets.

### Non-ISP equipment

This is some privately acquired networking equipment, including a cellular router and some switches as well as a wi-fi router.

#### Huawei B525s-23a LTE router

I [used]({{< ref "fixed-wireless-broadband#modem" >}}) this one as an LTE/4G modem with the wi-fi disabled since it's in the loft.
When we got a fibre connection, the cellular connection was retired and this router was repurposed as an access point.

The fourth ethernet socket LAN4 is supposed to double as some sort of a WAN port; however, if I connect it to an {{< wl ONT "Network_interface_device#Optical_network_terminals" >}} (optical network terminal), it doesn't seem to work.
Note that to connect as an AP over LAN4, `Network Settings` > `Ethernet` > `Ethernet Settings` > `Connection mode` must be set to `LAN only`.

If the online interface won't accept the correct password to log in, try using the https address.

Specs (excluding cellular):

* 2016
* WI1: 802.11 b/g/n/ac, 2.4GHz
* WI2: 802.11 a/n/ac, 5GHz
* LAN: 4x 1G

Data-rate measurement (wireless) [2.4 and 5 GHz channels sharing same SSID]:

* 186 Mb/s (Retr 0)
* 160 Mb/s (Retr 0)
* 259 Mb/s (Retr 0)

#### Netgear WNDR3700v2 router

I [use]({{< ref "openwrt-services#router-hardware" >}}) this as the primary node.
I run OpenWrt on it.

Links:

* {{< pl "https://wikidevi.wi-cat.ru/Netgear_WNDR3700v2" >}}
* {{< pl "https://openwrt.org/toh/netgear/wndr3700" >}}

Specs:

* 2010
* WI1: 802.11 a/b/g/n, 2x2:2, 2.4+5GHz
* WI2: 802.11 b/g/n, 2x2:2, 2.4GHz
* LAN: 4x 1G
* WAN: 1x 1G

Data-rate measurement (wireless):

* 173 Mb/s (Retr 0) [5 GHz only]

Comments:

* Ensure {{< wl "WiFi Multimedia" "Wireless_Multimedia_Extensions" >}} (WMM) is enabled under `Network` > `Wireless` > `Wireless Overview` > `<wireless network>` > `Edit` > `Interface Configuration` > `General Setup` > `WMM Mode` otherwise bandwidth can be limited.

#### TP-Link TL-SG1005D switch

A 5-port gigabit unmanaged desktop switch.

Data-rate measurement: 943 Mb/s (Retr 0)

#### D-Link DGS-1008D switch

An 8-port gigabit unmanaged desktop switch.

Data-rate measurement: 943 Mb/s (Retr 0)

#### Edimax CV-7428nS N300 Wi-Fi bridge

A wireless {{< wl bridge Network_bridge >}} supporting 802.11n (requires a good signal) with 5 100M ethernet ports.
Useful to connect wired-only devices where cables can't reach.
Currently unused.

### ISP equipment

Here are the old ISP-provided routers.

#### Adtran 834-v6

Comes with a crippled version of OpenWrt.

Specs:

* 2021
* WI1: 802.11 b/g/n/ax, 2.4GHz
* WI2: 802.11 n/ac/ax, 5GHz
* LAN: 4x 1G
* WAN: 1x 1G

Data-rate measurement (wireless): 897 Mb/s (Retr 0)

#### BT Home Hub 5.0 Type A

It's branded as a Plusnet Hub One.
OpenWrt supported but I use the crapware.

Links:

* {{< wl "BT Smart Hub" >}}
* {{< pl "https://openwrt.org/toh/bt/homehub_v5a" >}}
* {{< pl "https://wikidevi.wi-cat.ru/BT_Home_Hub_5A" >}}

Specs:

* 2013
* WI1: 802.11 b/g/n, 2x2:2, 2.4GHz
* WI2: 802.11 a/n/ac, 3x3:3, 5GHz
* LAN: 4x 1G
* WAN: 1x 1G

Data-rate measurement (wireless):

* 258 Mb/s (Retr 2)
* 274 Mb/s (Retr 2)

#### BT Home Hub 4.0 Type A

I have two in use.
OpenWrt not supported.

* {{< pl "https://openwrt.org/toh/bt/homehub_v4a" >}}
* {{< pl "https://wikidevi.wi-cat.ru/BT_Home_Hub_4A" >}}

Specs:

* 2013
* WI1: 802.11 b/g/n, 2x2:2, 2.4GHz
* WI2: 802.11 a/n, 3x3:2, 5GHz
* LAN: 1x 1G, 3x 100M
* WAN: 1x 1G

Data-rate measurement (wireless): 137 Mb/s (Retr 0)

#### BT Home Hub 1.0

Not in use.
To be recycled.

Links:

* {{< pl "https://wikidevi.wi-cat.ru/BT_Home_Hub_1.0" >}}
* {{< pl "https://openwrt.org/toh/bt/homehub_v1" >}} partial support

Specs:

* 2004
* 802.11 b/g, 2.4GHz
* Ethernet: 2x 100M

#### EE Brightbox 1 (2016 standing version)

I have the 2016 revision of this model (different internals):

* {{< pl "https://wikidevi.wi-cat.ru/Arcadyan_AR7516AAW22" >}}
* {{< pl "https://openwrt.org/toh/arcadyan/ar7516" >}}

Links:

* {{< pl "https://forum.openwrt.org/t/installing-openwrt-on-bright-box-r-wireless-router-ee/76311" >}} unofficial support for OpenWrt

Specs:

* 2016
* WI1: 802.11 b/g/n, 2x2:2, 2.4GHz
* Ethernet: 4x 100M (8P4C)

To set up as AP, navigate to `Advanced > DHCP`, set the IP address under `Gateway > Gateway Address`, and disable the DHCP server.

#### Huawei HG533 F.1.01 (TalkTalk)

To be investigated.

Links:

* {{< pl "https://wikidevi.wi-cat.ru/Huawei_HG533" >}}
* {{< pl "https://community.talktalk.co.uk/t5/Computers/Huawei-HG533-as-Wireless-Access-Point-Mystery-Solved/td-p/1145454" >}}
* {{< pl "https://data2.manualslib.com/pdf3/64/6331/633036-huawei/hg533.pdf?23e068576a83cb743e1ec3c901579eca&take=binary" >}}

Specs:

* 2013
* WI1: 802.11 b/g/n, 2x2:2, 2.4GHz
* LAN: 4x 100M
* WAN: 1x 1G

#### D-Link DSL-3680 A1 (TalkTalk)

To be investigated.

Specs:

* WI1: 802.11 b/g/n, 2.4GHz
* LAN: 2x 100M

#### D-Link DSL-2680 A1 (TalkTalk)

To be investigated.

Links:

* {{< pl "https://wikidevi.wi-cat.ru/D-Link_DSL-2680_rev_A1" >}}

Specs:

* WI1: 802.11 b/g/n, 1x1:1, 2.4GHz
* LAN: 2x 100M

#### Sky SR102

I have three of these somehow.
To be investigated.

Links:

* {{< pl "https://openwrt.org/toh/sky/sr102" >}} nope

Specs:

* WI1: 802.11 b/g/n, 2.4GHz
* LAN: 4x 100M (1 optionally WAN)

#### Sky SR101

To be investigated.

Links:
* {{< pl "https://openwrt.org/toh/sky/sr101" >}} nope
* {{< pl "https://www.linearthoughts.co.uk/sky-sr101-broadband-router-review/" >}}

Specs:

* WI1: 802.11 b/g/n, 2.4GHz
* LAN: 4x 100M

#### Technicolor TG588 v2

Provenance uncertain.
To be investigated.

Specs:

* WI1: 802.11 b/g/n, 2x2:?, 2.4GHz
* LAN: 4x 100M

#### Netgear DG834GT (AOL)

After many years of service as a router, this was used as an AP for same further many years until it died.
To be recycled.

Links:

* {{< wl "Netgear_DG834_(series)" >}}
* {{< pl "https://wikidevi.wi-cat.ru/Netgear_DG834GT" >}}
* {{< pl "https://openwrt.org/toh/netgear/dg834gt" >}}

Specs:

* 2004
* WI1: 802.11 b/g ? 2.4GHz
* LAN: 4x 100M

#### Sagemcom F@ST 2504 (Sky)

Also died after long service as router then AP.
To be recycled.

Links:

* {{< pl "https://wikidevi.wi-cat.ru/Sagemcom_F@ST_2504" >}}

Specs:

* WI1: 802.11 b/g ? 2.4GHz
* LAN: 4x 100M

## Troubleshooting

Can't connect to a machine at its expected address after changes?
Check if it's stuck on an old DHCP IP address allocation.

[^1]: In practice, wireless client devices are awful at managing their wireless connection, staying on suboptimal APs instead of hopping to the optimal one.
One way to get around this is to use different SSIDs for APs and even 2.4 and 5 GHz channels on each AP then manually choose the optimal connection on the client.

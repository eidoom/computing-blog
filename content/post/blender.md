+++
title = "Blender on Linux with an Nvidia GPU"
date = 2021-02-07T18:51:32Z
edit = 2021-04-05
categories = ["software"]
tags = ["install","linux","desktop","laptop","fedora","blender","graphics","nvidia","cuda","render","gpu","geforce","gtx-1080-ti","negativo17"]
description = "Getting Nvidia GPU rendering working in Blender on Fedora 33"
toc = true
draft = false
cover = "img/covers/61.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I've been playing with [Blender](https://www.blender.org/) {{< wiki "Blender_(software)" >}} today, following [this video tutorial](https://www.youtube.com/playlist?list=PLjEaoINr3zgEq0u2MzVgAaHEBt--xLB6U).
It is packaged by Fedora and can be installed with
```shell
sudo dnf install blender
```
Rendering is faster [on the GPU](https://docs.blender.org/manual/en/latest/render/cycles/gpu_rendering.html) than the CPU, which is essential on the pretty raytracing [Cycles engine](https://docs.blender.org/manual/en/latest/render/cycles/introduction.html), so I set it up to use my Nvidia GPU.

I previously [installed the Nvidia proprietary drivers]({{< ref "nvidia-drivers-on-fedora" >}}) using the negativo17 packages for my GeForce GTX 1080 Ti.
The negativo17 repository also includes the CUDA drivers we'll need under `nvidia-driver-cuda`, the CUDA toolkit as `cuda` with headers `cuda-devel`, and `nvidia-modprobe` which will [allow](https://blender.stackexchange.com/a/31111) Blender to find the GPU.

I installed everything with
```shell
sudo dnf install blender cuda cuda-devel nvidia-driver-cuda nvidia-modprobe
```

I fired up blender from the command line with `blender` so I could watch the terminal output.
I checked Blender could see the GPU by navigating to `Edit > Preferences > System > Cycles Render Devices > CUDA` and ensuring my GPU was selected.
I entered render mode with `z` `8` and selected the `Render Properties` tab on the bottom right to set the `Render Engine` to `Cycles` and `Device` to `GPU Compute`.
This takes a while on the first run as it builds the CUDA kernel, which we can monitor in the terminal.
Once it's built, we have nippy renders.
Nice.
For example, this proto-donut.
{{< figure src="../../img/donut_wee.jpg" alt="donut" caption="Donut v1" >}}

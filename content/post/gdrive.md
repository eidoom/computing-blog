+++
title = "Google Drive with drive"
date = "2020-01-20"
edit = 2020-07-08
tags = ["environment","workflow","linux","fedora","debian","laptop","server","desktop","drive","go","google-drive","cloud","install"]
categories = ["environment-extra"]
description = "Command line interface to Google Drive"
toc = true
cover = "img/covers/100.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Background

I have found that doing a PhD produces a lot of files.
All of my code projects are on some `git` remote.
I use Google Drive to synchronise files between computers and as a cloud backup solution for everything else.
I interface with Google Drive through the client [`drive`](https://github.com/odeke-em/drive).

Why Google Drive with `drive`?
It was the [cloud service](https://wiki.archlinux.org/index.php/List_of_applications#Cloud_synchronization_clients) that I tried that fulfilled the following requirements:

* Cloud storage
    * Free
    * Sufficient space
* Client
    * Free
    * Linux support
    * CLI program
    * Not background sync (after some bad experiences)

In the past, I've also tried for cloud service:

* [pCloud](https://www.pcloud.com/)
* [Mega](https://mega.nz/)
* [OneDrive](https://onedrive.live.com/)
* [Dropbox](https://www.dropbox.com/)

and to synchronise with my home server,

* BitTorrent Sync, which became [Resilio Sync](https://www.resilio.com/individuals/)

I stopped using my home server for backups as this wouldn't protect against, for example, a fire at home when my laptop was also at home.

## Installing drive

First [install Go]({{< ref "go" >}}).

Then
```shell
go get -u -v github.com/odeke-em/drive/cmd/drive
```

## Using drive

### Mounting Drive

Initialise a directory as a Drive folder with

```shell
drive init <directory path>
```

and follow instructions on the pop-up webpage.

### Updating files

I tend to just [`drive push`](https://github.com/odeke-em/drive#pushing) and [`drive pull`](https://github.com/odeke-em/drive#pulling) in whichever subdirectory of `<directory path>` I'm changing.
There's also `drive pull -ignore-conflict` to allow overwriting local changes.
I also use a [`.driveignore`](https://github.com/odeke-em/drive#excluding-and-including-objects).

## Update
{{< cal 2020-07-08 >}}

I have [migrated my client to `rclone`]({{< ref "server-install#rclone" >}}).
Install on Fedora with
```shell
sudo dnf install rclone
```

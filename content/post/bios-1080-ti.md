+++
title = "Flashing the BIOS of a GTX 1080 Ti"
date = 2020-01-03T08:12:26Z
edit = 2021-02-13
categories = ["firmware"]
tags = ["overclocking","gpu","graphics","nvidia","desktop","watercooling","geforce","gtx-1080-ti","inno3d","ekwb","flash","bios"]
description = "Overclocking a custom watercooled 1080 Ti by flashing a new BIOS"
draft = false
toc = true
cover = "img/covers/91.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Back when I picked up a second hand [Inno3D GeForce GTX 1080 Ti X2](https://www.inno3d.com/products_detail.php?refid=284), I slapped an [EKWB waterblock](https://www.ekwb.com/shop/ek-fc1080-gtx-ti-nickel) on it so I could fit it in my custom watercooling loop.

I used the tools available on [this thread](https://www.overclock.net/threads/how-to-flash-a-different-bios-on-your-1080-ti.1627212/) to flash the overclocked [Inno3D iChill X4 Ultra AIR BOSS BIOS](https://www.techpowerup.com/vgabios/191230/inno3d-gtx1080ti11264-170327).

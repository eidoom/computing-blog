+++
title = "Lily58: another split mechanical keyboard"
date = 2021-09-01T19:59:46+01:00
categories = ["keyboard"]
tags = ["hardware","mechanical-keyboard","split-keyboard","atmega32u4","elite-c","usb-c","keyboard","firmware","qmk","flash","gateron","lily58","ergonomic","c","fedora","fedora-34","linux","mcu","pcb","usb","input"]
description = "A new split keyboard to facilitate split working environments"
toc = true
draft = false
cover = "img/covers/43.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

{{< figure alt="lily58_0" src="../../img/lily58/l0.webp" caption="Completed Lily58 build with temporary keycaps">}}

## Better apart

[Having one split keyboard]({{< ref "split-keyboard" >}}), I naturally needed another: one for home, one for the office.
I was delighted to see we're gaining traction, with new stores in Europe like <https://splitkb.com/> (Netherlands) and <https://mechboards.co.uk> (UK) popping up and meaning increased availability of the 6×4 column staggered grid + thumb cluster layouts which I like.

## Components

I liked the look of the [Lily58 by kata0510](https://github.com/kata0510/Lily58), so I ordered [a kit from Mechboards](https://mechboards.co.uk/shop/kits/lily58-kit/).

I got a hotswap {{< wl PCB "Printed circuit board" >}} with Kailh hotswap sockets and diodes (I ordered the basic PCB but was upgraded by accident), a clear acrylic case, two Elite-C (v4) {{< wl MCUs "Microcontroller" >}}, and some Gateron Brown switches.
I used some spare keycaps I had lying around; I'll swap then for [my friend's new set](https://geekhack.org/index.php?topic=113984.0) when it's out.
I opted for the Elite-C over the Pro Micro and {{< wl SMD "Surface-mount technology" >}} Micro-USB-type clones (all `ATmega32u4`) for the through-hole mounted USB-C port.
In retrospect, I wish I'd committed more cash and gone for a heavier tactile switch---the hotswap switch mounts *would* make swapping them out very easy...

{{< figure alt="lily58_2" src="../../img/lily58/l2.webp" caption="With the OLED displays on in default configuration: keylogger on the left and logo on the right">}}

## Build log

The build was straightforward: just following the [build guide](https://github.com/kata0510/Lily58/blob/master/Pro/Doc/buildguide_en.md).
The steps were:
* The PCBs are printed on both sides such that they're identical. We impose chirality by setting them down mirrored and choosing the front face. Mark the front face as such with masking tape.
* Now we'll mount the T4 diodes on the back side of the PCB. Align the line marked on the diode with the line of the diode symbol on the keyboard. Pre-solder one board contact, then use small pliers to position the diode and solder it. Make sure the second leg has plenty of space on the substrate contact when doing the first. It's very easy to miss out solders here because they're tiny! Use a multimeter in diode test mode on the front side of the board to test the connections and orientations.
* Solder the hotswap switch sockets on the back side of the PCB. Use the pre-solder trick again. The Kailh sockets go in the larger marked socket position; the other position is for a different form factor. Make sure the sockets are firmly pushed into the holes in the PCB using an implement rather than fingers.
* Solder the TRRS jacks and reset switches on the front side of the PCB.
* On the front side of the PCB only, bridge the four jumper pins at the bottom of the MCU section by applying solder. This is for the OLED display.
* Solder the MCU pin headers (left and right strips for each MCU) into the positions marked out on the board (and not the unmarked positions) with the plastic riser of the pin header on the front side of the main PCB. Then put the MCUs on top of the plastic risers of the pin headers, with the MCUs upside-down, i.e. with the MCU side with components mounted on it facing toward the main PCB. Solder the MCU onto the pin headers.
* Lie the OLED display on top of the MCU with the display facing upwards and its pin headers going through the four holes in the main PCB below the MCU. Solder the header pins on the back side of the PCB. You can switch off the soldering iron now and remove the masking tape.
* Attach the 1cm spacers, two below each MCU.
* Peel off the covers from both sides of the acrylic (this is the hardest part of the whole build). Attach the 7mm spacers to the top places (the ones with the switch holes). Be careful not to overtighten screws on the acrylic.
* Push the switches through the top plate holes and into the sockets on the PCB. Carefully apply pressure to the switch with the assembly sitting on a surface so that the weight is on the top place spacers which pass through holes in the PCB to stand on the surface, as opposed to pressing against the PCB at the back since the top place sits above the PCB with a clearance of 2mm, not against the PCB. Ensure that switch pins are lined up and straight before pressing. If any pins get bent, simply remove the switch, straighten the pin, and try again.
* Mount the MCU/OLED cover to the 1cm spacers.

{{< figure alt="lily58_1" src="../../img/lily58/l1.webp" caption="These photos were taken with my [new phone](../fp3). I'll take proper photos when the new caps are on. I'm sorry I didn't photograph the build process, I was too excited to take the time.">}}

## Firmware

On my laptop (Fedora 34), I set up the new QMK CLI with my existing {{< gh qmk qmk_firmware >}}
```shell
sudo dnf install hidapi
pip install --user qmk
qmk setup -H ~/git/qmk_firmware
```
tested the default Lily58 keymap compile
```shell
qmk compile -kb lily58/rev1 -km default
```
then flashed the boards
```shell
qmk flash -kb lily58/rev1 -km default -bl dfu
```
after running this command, press the reset button on the board to put it in flash mode (note that this may not be necessary the first time the MCU is flashed).
The default keymap has a keylogger on the left display, which is useful for testing keys.
At this stage, I identified the switches that had bent pins or for whose diodes I had missed a contact and fixed them.

I started a custom keymap
```shell
qmk new-keymap -kb lily58  # name eidoom
```
and pushed it to {{< gl lily58-keymap >}}.
I also [forked]({{< ref "git-reference#fork" >}}) QMK to {{< gl qmk_firmware >}} and [added my keymaps as submodules]({{< ref "git-reference#add-non-empty-git-repo-with-remote-url-as-a-new-submodule" >}}) to make things [easier](https://gitlab.com/eidoom/qmk_firmware/-/blob/master/README-FORK.md) for myself.

I setup a [layout](https://gitlab.com/eidoom/lily58-keymap/-/raw/main/keymap.c) similar to what I developed for my Iris.
I used the additional thumb key to fully mirror my modifiers.
I am not yet sure what I will settle on for the [OLED displays](https://docs.qmk.fm/#/feature_oled_driver).

I flashed my keymap to each of the MCUs with appropriate [handedness](https://docs.qmk.fm/#/feature_split_keyboard?id=handedness-by-eeprom)
```shell
qmk config user.keymap=eidoom
qmk flash -kb lily58 -bl dfu-split-left
qmk flash -kb lily58 -bl dfu-split-right
```

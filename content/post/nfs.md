+++
title = "File sharing with NFS"
date = "2019-11-09T17:53:14Z"
edit = 2020-06-08
categories = ["network"]
tags = ["server","desktop","linux","install","laptop","debian","fedora","benchmark","lan","network","nfs","home-server","debian-buster","fedora-31","debian-10","dd","application-layer","layer-7"]
description = "Setting up NFSv4 on my home server to share files on the LAN"
toc = true
cover = "img/covers/5.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

I set up an NFS server {{< wiki "Network File System" >}} {{< arch "NFS" >}} on my home server to share files with my desktop.
The server runs Debian 10 *Buster* (now 11 *Bullseye*) and the desktop runs Fedora 31 (now 34).
My main reference resource was the [Debian wiki](https://wiki.debian.org/NFS).
There are also some instructions under `/usr/share/doc/nfs-common/README.Debian.nfsv4`.

## Server

### Install

On Debian, install the [NFS kernel server](https://packages.debian.org/stable/nfs-kernel-server) (the fastest and most stable implementation)

```shell
sudo apt install nfs-kernel-server
```

### Configure for version 4 only (optional)

<!-- Note that if you want to use [satellite MPD]({< ref "rpi-mpd" >}}), it uses NFSv3, so you should skip this section. -->

Note that if you want to use satellite MPD, it uses NFSv3, so you should skip this section.

To configure for version 4 only, we need to edit a couple of files:
```shell
sudo vi /etc/default/nfs-common
```
```vim
...
NEED_STATD="no"
...
NEED_IDMAPD="yes"
...
```
and, where `RPCNFSDOPTS` must be added,
```shell
sudo vi /etc/default/nfs-kernel-server
```
```vim
...
RPCNFSDOPTS="-N 2 -N 3"
...
RPCMOUNTDOPTS="--manage-gids -N 2 -N 3"
...
```
and we can mask `rpcbind`
```shell
sudo systemctl mask rpcbind.service
sudo systemctl mask rpcbind.socket
```
Then restart the service to apply the changes
```shell
sudo systemctl restart nfs-server
```

To test which versions of NFS the server is running,
```shell
$ sudo cat /proc/fs/nfsd/versions
-2 -3 +4 +4.1 +4.2
```
which shows only version 4, as desired.

### Configure file shares

Set up the files to be shared by editing [`/etc/exports`](https://linux.die.net/man/5/exports) and recreating the NFS table {{< ext "https://linux.die.net/man/8/exportfs" >}}.
For example, to share a single directory, say `USER`'s home directory, with write permissions to clients with addresses `192.168.1.*`

```shell
echo 'echo "/home/USER 192.168.1.0/24(rw,no_subtree_check)" >> /etc/exports' | sudo sh
sudo exportfs -arv
```
`no_subtree_check` is a default included explicitly to suppress a warning about the old API changing.

To share multiple directories, it's a little more complicated.
All shares must be relative to a NFS root, *i.e.* are all subdirectories of one root directory on the server filesystem.
If the directories to be shared do not follow this pattern, [ArchWiki describes how to use bind mounts to set this up](https://wiki.archlinux.org/title/NFS#Server).
However, [other sources](https://wiki.debian.org/NFS/Kerberos) say that this is a myth?

For [this setup]({{< ref "server-install#mounting-data-disks" >}}), including a [union filesystem]({{< ref "server-install#union-filesystem" >}}), the directories are all in `media` anyway so we don't need to worry about it.
```shell
sudo vi /etc/exports
```
```vim
/media 192.168.1.0/24(rw,no_subtree_check)
/media/disk-3a 192.168.1.0/24(rw,no_subtree_check)
/media/pool 192.168.1.0/24(fsid=1,rw,no_subtree_check)
```
The root is `media` (`fsid=root`), `disk-3a` will have its UUID as `fsid`, and `pool` has to have a manual `fsid` set as the union filesystem doesn't have a UUID or device number.

Check the current exports with
```shell
sudo exportfs -v
```

Note that the [Linux file permissions](https://wiki.archlinux.org/title/File_permissions_and_attributes) on the server will determine a client user's access, and that the numerical [user](https://wiki.archlinux.org/title/Users_and_groups) IDs must match between the systems.

## Client

On Fedora, install the NFS client

```shell
sudo dnf install nfs-utils
```
(For Debian clients, it's [`nfs-common`](https://packages.debian.org/stable/nfs-common).)

For a server with address or hostname `HOSTNAME`, available shares can be listed on the client with
```shell
showmount -e HOSTNAME
```

Mount the desired share (which can be a subdirectory of an exported directory) with
```shell
mkdir ~/mnt
sudo mount HOSTNAME:/media/disk-3a/data ~/mnt
```
Done.

Unmount with
```shell
sudo umount ~/mnt
```
and add `-l` if there are problems.

## Performance

I tested the speed of my new NFSv4 network share following [this](https://serverfault.com/questions/324438/measure-benchmark-the-speed-latency-of-file-access-on-a-mounted-nfs-share).
The machines are connected by a gigabit ethernet connection.
First, measure the speed of copying a file from the client to the server

```shell
time dd if=/dev/zero of=~/mnt/testfile bs=16k count=128k
```
```vim
131072+0 records in
131072+0 records out
2147483648 bytes (2.1 GB, 2.0 GiB) copied, 29.9249 s, 71.8 MB/s
dd if=/dev/zero of=testfile bs=16k count=128k  0.10s user 1.38s system 4% cpu 29.933 total
```
and again
```vim
131072+0 records in
131072+0 records out
2147483648 bytes (2.1 GB, 2.0 GiB) copied, 31.6195 s, 67.9 MB/s
dd if=/dev/zero of=testfile bs=16k count=128k  0.12s user 1.33s system 4% cpu 31.774 total
```

Then measure the speed of copying a file from the server to the client

```shell
time dd if=~/mnt/testfile of=/dev/null bs=16k
```
```vim
73020+0 records in
73020+0 records out
1196359680 bytes (1.2 GB, 1.1 GiB) copied, 12.3081 s, 97.2 MB/s
dd if=testfile of=/dev/null bs=16k  0.13s user 0.81s system 7% cpu 12.311 total
```
and again
```vim
131072+0 records in
131072+0 records out
2147483648 bytes (2.1 GB, 2.0 GiB) copied, 22.2579 s, 96.5 MB/s
dd if=testfile of=/dev/null bs=16k  0.26s user 1.64s system 8% cpu 22.259 total
```

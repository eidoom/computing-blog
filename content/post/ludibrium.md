+++
title = "Ludibrium"
date = 2022-10-22T15:57:50+02:00
categories = ["web"]
tags = ["mini-project","html","css","game","gitlab","ci","gitlab-ci","js","vanilla.js","javascript","frontend","static-website","website","webpages"]
description = "A simple game I designed to amuse my family on one visit"
toc = true
draft = false
cover = "img/covers/112.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I designed a simple game for three or more players, which can be played online [here](https://eidoom.gitlab.io/ludibrium/).
It's called {{< wl Ludibrium >}}, which means a trivial game.
Source at {{< gl ludibrium >}}.

+++
title = "Upgrading to Debian 11"
date = 2021-08-17T09:39:02+01:00
categories = ["operating-system"]
tags = ["debian", "debian-buster","debian-bullseye","debian-10","debian-11","server","upgrade","debian-testing"]
description = "From *buster* to *bullseye*"
toc = false
draft = false
cover = "img/covers/45.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[Debian 11 *bullseye* was released](https://www.debian.org/News/2021/20210814), so I upgraded my server from Debian 10 *buster*.
The Debian wiki provides a [generic upgrade guide](https://wiki.debian.org/DebianUpgrade) and the [Debian 11 release notes](https://www.debian.org/releases/bullseye/amd64/release-notes/) give specific details.

First, I ensured everything was up to date in the old release.
```shell
sudo apt update
sudo apt upgrade -y
```
Because of [the way I have `etc/apt/sources.list.d/*` set up]({{< ref "server-install#installing-selectively-from-testing" >}}), for `testing`, `testing-security`, and `testing-updates`, I had to manually acknowledge the change in `Codename` that has accompanied the release, for example,
```shell
E: Repository 'https://deb.debian.org/debian testing InRelease' changed its 'Codename' value from 'bullseye' to 'bookworm'
N: This must be accepted explicitly before updates for this repository can be applied. See apt-secure(8) manpage for details.
Do you want to accept these changes and continue updating from this repository? [y/N] y
```
I rebooted to get on the latest kernel (of the old release).
```shell
sudo reboot
```
I updated the `codename` in my package sources file
```shell
sudo perl -pi -e "s|buster|bullseye|g;" /etc/apt/sources.list.d/stable.list
```
and changed the security suite lines in `/etc/apt/sources.list.d/stable.list` for [the new naming convention](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#security-archive), giving
```vim
deb https://deb.debian.org/debian bullseye main contrib non-free
deb-src https://deb.debian.org/debian bullseye main contrib non-free

deb https://deb.debian.org/debian-security bullseye-security main contrib non-free
deb-src https://deb.debian.org/debian-security bullseye-security main contrib non-free

deb https://deb.debian.org/debian bullseye-updates main contrib non-free
deb-src https://deb.debian.org/debian bullseye-updates main contrib non-free

deb https://deb.debian.org/debian bullseye-backports main contrib non-free
deb-src https://deb.debian.org/debian bullseye-backports main contrib non-free
```
I refreshed the package lists with the new sources
```shell
sudo apt clean
sudo apt update
```
Since I was working remotely, [before the full upgrade I upgraded the `ssh` server](https://www.debian.org/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available)
```shell
sudo apt upgrade openssh-server
```
I lost `ssh` connection during this upgrade either due to my laptop's ethernet (`RTL8153`) dying, as it sometimes does, or because we were upgrading the `ssh` server, after all.
I had foolishly forgotten to run it in `tmux` or `screen`, so had to plug in a keyboard and screen to the server and recover it locally with `sudo dpkg --configure -a`.
This failed with
```shell
Setting up initramfs-tools (0.140) ...
update-initramfs: deferring update (trigger activated)
Processing triggers for initramfs-tools (0.140) ...
update-initramfs: Generating /boot/initrd.img-5.10.0-8-amd64

gzip: stdout: No space left on device
E: mkinitramfs failure gzip 1
update-initramfs: failed for /boot/initrd.img-5.10.0-8-amd64 with 1.
dpkg: error processing package initramfs-tools (--configure):
 installed initramfs-tools package post-installation script subprocess returned error exit status 1
Errors were encountered while processing:
 initramfs-tools
needrestart is being skipped since dpkg has failed
E: Sub-process /usr/bin/dpkg returned an error code (1)
```
However, this is just because for some reason my boot partitions are set up weirdly, as we can see [here]({{< ref "server-install#checking-configuration" >}}), so `/boot` ran out of space and thus couldn't fit the new kernel.
This was easily fixed with ([details]({{< ref "server-install#maintaining-boot" >}}))
```shell
sudo apt remove linux-image-VERSION
```
where `VERSION` was the oldest redundant kernel version.

I did the full release upgrade
```shell
sudo apt full-upgrade
```
I removed obsolete packages and rebooted
```shell
sudo apt autoremove
sudo reboot
```

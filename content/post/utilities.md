+++
title = "Utilities reference"
date = 2020-01-13T12:19:30Z
edit = 2021-07-02
categories = ["cheatsheet"]
tags = ["linux","terminal","environment","desktop","laptop","fedora","server","shell","zsh","pandoc","markup","markdown","ssh","dnf","install","scp","debian","apt","regex","grep","ripgrep","sed","htop","rsync","public-key","tar","du","df","find","archive","perl","posix","rpm","head","tail","cat","sort","cut","free","top","diff","evince","pdf","djvu","document","xz","gz","zstd","tmux","dd","random","docker","podman","container"]
description = "Reminder of some useful Linux commands"
toc = true
cover = "img/covers/16.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## System

The following packages are specific to Fedora and only tested on versions 31--38.
Note also that my system is configured like ~~[this]({{< ref shell >}})~~ [now this](https://gitlab.com/eidoom/dotfiles-active/).

At one point I had a highly customised Linux environment with many non-standard tools installed, the legacy of which remains in this partially deprecated post.
These days, I prefer to use a simpler setup, mostly using default programs.
Most of the customisation is in aliases in my mostly-portable dotfiles.
This makes getting running on fresh machines much faster and easier.

## The terminal as your development environment

Step one: [use]({{< ref "nvim2" >}}) [`vim`]({{< ref "vim-cheatsheet" >}}).

### Convert markup language

Install [`pandoc`]({{< ref "pandoc" >}})
```shell
sudo dnf install pandoc
```
then compile [pdfs](https://gitlab.com/eidoom/current) with
```shell
pandoc --pdf-engine=luatex -H head.tex -f markdown -t latex -V colorlinks -o doc.pdf doc.md
```
or [webpages]({{< ref "nginx#generating-static-content" >}}) with
```shell
pandoc index.md -f markdown -t html5 -s --highlight-style haddock -c pandoc.css -o index.html
```
or [both]({{< ref "pandoc-physics" >}}).

### Copy to clipboard

To make the output of some command available for pasting, pipe it to `xclip` like
```shell
cat file.txt | xclip -selection c
```

### Core Linux tools printing to standard output
* Print a file's contents with `cat`
    ```shell
    cat <file>
    ```
* Only the start with `head`, eg. first `N` lines
    ```shell
    head -N <file>
    ```
* Only the end with `tail`, eg. and follow if file is appended to with
    ```shell
    tail -f <file>
    ```
* Print columns of a table with `cut`, eg.
    ```shell
    git status --short | cut -d" " -f3
    ```
* Word count, eg. line count of a file
    ```shell
    wc -l < <file>
    ```

### Filter text

#### sed

Install [`sed`](https://en.wikipedia.org/wiki/Sed)
```shell
sudo dnf install sed
```

Usage by examples:
* omit the `-i` in the following to print the result to the terminal instead of changing the file
* replace `<search>` with `<replace>` every time it occurs in any file in a directory
    ```shell
    rg -l <search> | xargs sed -i 's/<search>/<replace>/g'
    ```
* comment out line 9 in `<file>` by prepending ` #` to the line
    ```shell
    sed -i '9s/^/# /' <file>
    ```
* add a new line at line 8 to every markdown file in a directory
    ```shell
    for f in *.md; do sed -i '8i<new line>' $f; done
    ```
* delete the first line of all markdown files in a directory
    ```shell
    sed -i '1d' *.md
    ```
* add `NEW` in a new line above every instance of `+++` in all markdown files in a directory
    ```shell
    sed -i 's/\+\{3\}/NEW\n+++/g' *.md
    ```
* print lines `<start>` to `<end>` of `<file>` 
    ```shell
    sed -n '<start>,<end>p' <file>
    ```

To use `sed` with [POSIX extended regex syntax](https://stackoverflow.com/a/6387994):
```shell
sed -E 's/<search>/<replace>/g'
```
* You have to [escape the characters](https://stackoverflow.com/a/400316/5922871) `.^$*+?()[{\|` to use them as literals outside character classes.
* Matching is always greedy with `sed`. It [doesn't support lazy matching](https://stackoverflow.com/a/1103177/5922871).

#### perl

For [modern](https://en.wikipedia.org/wiki/Perl_Compatible_Regular_Expressions) [`regex`](https://en.wikipedia.org/wiki/Regular_expression) syntax, such as [lazy matching](https://stackoverflow.com/a/2301298/5922871), we need a better [regex engine](https://en.wikipedia.org/wiki/Comparison_of_regular-expression_engines). One option is [`perl`'s](https://en.wikipedia.org/wiki/Perl).
```shell
sudo dnf install perl
```
Usage is like `sed`, but with `|` as the delimiter.
For example, to replace all Markdown links with links to an [archived](https://web.archive.org/) version of the webpage and print the result to the terminal,
```shell
perl -pe "s|(\[.+?])\((http.+?)\)|\1(https://web.archive.org/web/*/\2)|g" <file.md>
```
or instead use the flags `-pi -e` to change the file.
Notice the use of capture groups `(...)` and backreferencing `\1` (which works with `sed` too).
The `?` denotes lazy matching.

Perhaps we'd prefer to keep the original links and add the archive links as footnotes:
```shell
perl -pi -e "s|\[(.+?)]\((http.+?)\)|[\1](\2)[^\1]|g" <file.md>
rg -o '\[.+?]\((http.+?)\)(\[\^.+?])' -r '$2: <https://web.archive.org/web/*/$1>' <file.md> > tmp
while read l; do echo "$l" >> 2fa.md; done < tmp
rm -f tmp
```
Notice the `regex` usage of [ripgrep]({{< ref "#search" >}}).

To [match newlines](https://stackoverflow.com/a/13927907/5922871), use `-0777p`.

[This website](https://regex101.com/) is an excellent regex debugging tool.

I most often find myself making changes across multiple files with
```shell
rg -l <old> | perl -pi -e "s|<old>|<new>|g"
```

### Pretty cat

Install `bat`
```shell
sudo dnf install bat
```
for a prettier alternative to `cat`.
It looks bad on low contrast colour schemes like [`solarized`]({{< ref "install-fedora#colours" >}}) though.

### Random

Generate a random string of characters with
```shell
cat /dev/urandom | base64 | head -c <length>
```
Append `&& echo` to return newline at end.

### Redirection
[Details](https://www.gnu.org/software/bash/manual/html_node/Redirections.html)
* standard input redirection: `<`
* standard output redirection: `>`
* standard error redirection: `2>`
* append standard output redirection: `>>`

### Search

Use `grep`, `git grep`, or install `ripgrep`
```shell
sudo dnf install ripgrep
```
for [`rg`](https://github.com/BurntSushi/ripgrep) as a faster alternative to `grep`.
For example, with a literal or `regex` `<search>`,
* search contents of all files in current directory and subdirectories
    ```shell
    rg <search>
    ```
    this is the equivalent of
    ```shell
    grep -R <search> .
    ```
* do case-insensitive search
    ```shell
    rg -i <search>
    ```
* search filenames
    ```shell
    ls | rg <search>
    ```
* show files that contain `<search>`
    ```shell
    rg -l <search>
    ```
* show files that do not contain `<search>`
    ```shell
    rg --files-without-match <search>
    ```
* do word search
    ```shell
    rg -w <search>
    ```
* [regex]({{< ref "#perl" >}}) replacements
    ```shell
    rg '<search>' -r '<replace>' <file>
    ```
    Backreferences use the `$1` syntax.
    Use flag `--passthru` and force redirection `>!` to overwrite files in `zsh`
    ```shell
    rg --passthru '<search>' -r '<replace>' <file> >! <file>
    ```

### Shell examples
Demonstrating control [structures](https://askubuntu.com/a/678919).

* Append file
    ```shell
    echo -e "\nContent" >> <file>
* Get all URLs for `png` images from a document, `DOC`, and download them to the current directory
    ```shell
    while read p; do sed "s/^.*'\(http.*.png\)'.*$/\1/g" <<<"$p"; done < DOC | rg http | xargs wget
    ```
* [List](https://unix.stackexchange.com/a/53738) all files in current directory and subdirectories [except](https://stackoverflow.com/a/4210072) `.git` and order by size
    ```shell
    find . -type d -path ./.git -prune -false -o -type f -exec du -h {} + | sort -h
    ```

### Terminal multiplexing

Get windows and panes with [`tmux`]({{< ref "tmux" >}}) ([usage]({{< ref "tmux-cheatsheet" >}})).

### Testing in a container

Test in an image,
```shell
podman --rm -it -v $PWD:/mnt:Z <image-url>
```

### Version control

Version control projects with [`git`]({{< ref "git-reference" >}}).

### Word diff files

Install `wdiff`
```shell
sudo dnf install wdiff
```
for a better `diff` (depending on the job).

## File management

### `dd`

Copy disk images with `dd` using these [settings](https://www.man7.org/linux/man-pages/man1/dd.1.html) for speed,

```shell
sudo dd if=<name>.iso of=/dev/sd<x> bs=8M oflag=direct status=progress
```

* `bs=8M`: read/write in 8MiB chunks
* `oflag=direct`: use direct I/O for data write (rather than synchronised)
* `status=progress`: show periodic transfer statistics

### `find` files

`find` should be installed by default, otherwise
```shell
sudo dnf install findutils
```
Find files in the current directory and subdirectories with
```shell
find . -name <filename>
```
where `<filename>` supports {{< wl globbing "glob_(programming)" >}}.

You can also act on found files.
For [example](https://stackoverflow.com/a/11191868/5922871), to delete all `.DS_Store` files in a directory and its subdirectories,
```shell
find . -name .DS_Store -type f -delete
```

Similarly for [directories](https://unix.stackexchange.com/a/89929/156381),
```shell
find . -type d -exec rm -rf "{}" \;
```
Note `\;` does a separate command for each match, while `\+` provides all matches as arguments to a single command.

`find` accepts logical chaining of multiple conditions, *e.g.* with `or` as `-o`
```shell
find . -name "<first>" -o -name "<second>"
```

You can filter by file size, for example, to find all files of size greater than 300M,
```shell
find . -size +300M
```

You can negate conditions too, *e.g.* find all files that aren't `flac` files with
```shell
find . ! -name "*.flac"
```

Can be powerful tool within pipes, for example, to find all unique file suffixes,
```shell
find . -type f | grep -o "\.[a-zA-Z0-9]\+$" | sort -u
```

### `fd` files

An alternative to `find` is `fd` {{< ghubn "sharkdp/fd" >}}.

Example usage, using regex,
```shell
fd "[mp]{5}.cpp"
```

### Lazy move

Install `zsh`
```shell
sudo dnf install zsh
```
and [`chsh`]({{< ref "#changing-default-shell" >}}) then enable `zmv` with
```shell
echo "autoload -U zmv" >> ~/.zshrc
```
or with your zsh configuration framework, eg. [Prezto](https://github.com/sorin-ionescu/prezto/blob/master/runcoms/zpreztorc#L28), then use with, for example,
```shell
zmv -W '*gggAA*' '*3g2A*'
```

Even better,
```shell
echo "alias mmv='noglob zmv -W'" >> ~/.zshrc
```
then
```shell
mmv *gggAA* *3g2A*
```

### Lazy (un)archive

One can use [`archive` and `unarchive`](https://github.com/sorin-ionescu/prezto/tree/master/modules/archive) to avoid [confusion](https://www.gnu.org/software/tar/manual/html_section/tar_21.html), but it's really easier to just use [`tar`]({{< ref "#tar" >}}).

### Tar
We can use `tar` such that the formats are detected automatically from the file suffix `X`,
* unpack with
    ```shell
    tar xf <archive>.tar.X
    ```
* pack with
    ```shell
    tar caf <archive>.tar.X <files>
    ```

using `X` as:
* `gz` {{< wiki Gzip >}} for quick compute but poor compression performance
* `zst` {{< wiki zstd >}} for a decent compression ratio with fast decompression
* `xz` {{< wiki XZ_Utils >}} for minimal archive size but slowest decompression

[One thing](https://stackoverflow.com/a/27541309) about `zx`: you can run compression multithreaded by setting the environment variable
```shell
XZ_DEFAULTS="-T <threads>"
```

### Merge directories

I [found](https://unix.stackexchange.com/a/149986/156381) that [`rsync`]({{< ref "#incremental-file-transfer-over-network" >}}), the network transfer program, can be used locally,
```shell
rsync -avhP /path/to/source/ /path/to/destination/
```
where the contents of the `source` directory ends up *inside* the `destination` directory. 

### Pretty list

Install `exa`
```shell
sudo dnf install exa
```
for a more colourful alternative to `ls`.

### See progress bar on move

Install `progress`
```shell
sudo dnf install progress
```
then
```shell
mv <source> <dest> | progress -m
```
shows the progress for moving single files.

### Access permissions

ArchWiki explains how [users](https://wiki.archlinux.org/title/Users_and_groups) and [file permissions](https://wiki.archlinux.org/title/File_permissions_and_attributes) work (including the [numbers](https://wiki.archlinux.org/title/File_permissions_and_attributes#Numeric_method)).

## Monitoring

### Disk IO

I [found](https://unix.stackexchange.com/a/55227/156381) `dstat` to be a nice solution for viewing usage of individual disks.
Install on Fedora with
```shell
sudo dnf install dstat
```
or on Debian with
```shell
sudo apt update
sudo apt install dstat
```
I use it on my server to view read and write rates for the disks with
```shell
dstat -tdD sda,sdb,sdc,sdd,sde 60
```

You can also enable tabs in [`htop`]({{< ref "#processes" >}}) to see I/O there.

### Processes

See memory usage with `free -h` and processes with `top`.

For monitoring with terminal graphics, install `htop`
```shell
sudo dnf install htop
```
and run with
```shell
htop
```

With my Solarised colour scheme, I have to set the `Colors` as `Broken Gray` in `Setup`.
Under `Display Options`, I prefer to show CPU clock speed rather than the default percentage usage in the bars.
I also like to install ([`lm_sensors-libs`](https://packages.fedoraproject.org/pkgs/lm_sensors/lm_sensors-libs/) on Fedora | [`libsensors5`](https://packages.debian.org/buster/libsensors5) on Debian) to show the CPU core temperature.

### Network usage

There are [so many options](https://www.binarytides.com/linux-commands-monitor-network/).

* `nload` is good for total download/upload with some terminal graphs.
* `nethogs` shows usage per process.

### Test internet speed

Install `speedtest-cli`
```shell
sudo dnf install speedtest-cli
```
then 
```shell
speedtest-cli
```

Or [self-host an `iperf3` server]({{< ref "fixed-wireless-broadband#bandwidth" >}}).

### Watch
Update output of `<command>` every `N` seconds
```shell
watch -n <N> <command>
```

## Operating system

### Change default shell

Install `util-linux-user`
```shell
sudo dnf install util-linux-user
```
then, for example,
```shell
chsh -s /bin/zsh
```

### Find package that COMMAND belongs to

```shell
dnf provides COMMAND
```

### List hardware

Install `lshw`
```shell
sudo dnf install lshw
```
then
```shell
sudo lshw
```

### Locate path executables

Use `where <query>`.

### Package management
Fedora's package manager is `dnf`.
#### Repositories
* List installed repos
    ```shell
    dnf repolist
    ```
* Disable a repo
    ```shell
    sudo dnf config-manager --set-disabled <repo-name>
    ```
#### Packages
* List installed packages
    ```shell
    dnf list --installed
    ```
* [List](https://linoxide.com/linux-how-to/list-installed-packages-size-centos-fedora-arch-linux/) the ten installed packages [which](https://docs.fedoraproject.org/en-US/Fedora_Draft_Documentation/0.1/html/RPM_Guide/RPM_Guide-Using_RPM_DB-Query_format_tags.html) have the greatest disk usage (size [displayed](https://github.com/rpm-software-management/rpm/commit/08d39aa4ca39fe5fcaeafd963ce20fa89456aa42#) in [KiB]({{< ref "jargon#binary" >}}), etc)
    ```shell
    rpm --all --query --queryformat '%5{size:humaniec} %{name}\n' | sort -hr | head -10
    ```
* Get package information
    ```shell
    dnf info <package-name>
    ```
* Install a package from `testing`
    ```shell
    sudo dnf --enablerepo=updates-testing update <package-name>
    ```

### Stop background processes

I [found](https://unix.stackexchange.com/a/104825/156381)
```shell
pgrep <query>
```
or for more information
```shell
ps -eaf | grep <query>
```
to check running processes, then kill all processes matching the query with
```shell
pkill <query>
```
or kill a specific process by its process ID `<pid>` with
```shell
kill <pid>
```

### View disk usage

Install `coreutils`
```shell
sudo dnf install coreutils
```
to get `du` and `df`. 

Example usages:
* `df -h` for total used space on system disks
* `du -s` for directory total only
* `du -h` for all children of current directory, with binary prefix notation
* `du --si` uses SI/denary prefix notation
* `du -ahd1 | sort -h` for files in current directory only, sorted by size in ascending order

For a GUI solution, I like the exploded doughnuts of [GNOME Disk Usage Analyser](https://wiki.gnome.org/Apps/DiskUsageAnalyzer) aka `baobab`.

### Disk usage diet

To slim down system data,

* [Find culprit data]({{< ref "#view-disk-usage" >}})
* `sudo dnf remove` unnecessary (and especially fat) [packages]({{< ref "#packages" >}}) then `sudo dnf autoremove`
* Clean up unused packages (including old versions) on [Flatpak/Snap]({{< ref "software-deploy" >}}) if used
* [Trim](https://unix.stackexchange.com/a/194058/156381) the `journal`, eg
    ```shell
    journalctl --vacuum-size=500M
    ```

## Network

### Set up SSH key

Ensure `ssh` is installed on the local machine
```shell
sudo dnf install openssh-clients
```
Generate an SSH key of [Ed25519 authentication type](https://wiki.archlinux.org/index.php/SSH_keys#Choosing_the_authentication_key_type) with 
```shell
ssh-keygen -t ed25519 -f ~/.ssh/<identity file name>
```
where -f defaults to `~/.ssh/id_ed25119`.
See the public key at
```shell
cat ~/.ssh/id_ed25519.pub
```

### Copy over SSH

[`openssh-clients`]({{< ref "#set-up-ssh-key" >}}) includes `scp`, for [example]({{< ref "xps-install#moving-files-over" >}}),
```shell
scp -v -r user@from_address:/remote/directory /local/directory/
```

Alternatively, if diffing would be useful, use [rsync]({{< ref "#incremental-file-transfer-over-network" >}}).

### Copy public key to remote

With password login allowed on the remote server, [copy over the public key](https://wiki.archlinux.org/index.php/SSH_keys#Simple_method)
```shell
ssh-copy-id <remote_hostname>
```
Be sure to [disable password login]({{< ref "xps-install#moving-files-over" >}}) on the remote afterwards.

If it's not working, be sure that [all the things have the correct permissions](https://unix.stackexchange.com/a/36687).
Yes, even `~`!

### SSH configuration file

Ensure [correct](https://serverfault.com/a/253314) [permissions]({{< ref "#access-permissions" >}}): `-rw-------`, which is readable and writeable only by the owing user.
Set this and owner with
```shell
chown $USER ~/.ssh/config
chmod 600 ~/.ssh/config
```

#### ProxyJump

Under `~/.ssh/config`, we can set up
```vim
Host login_node
    Hostname <login node hostname>
    User <username>
    IdentityFile <path to identity private key>

Host internal_node
    Hostname <internal node hostname>
    User <username>
    ProxyJump login_node
```
to be able to directly `ssh internal_node` where `internal_node` is only accessible through `login_node`.

#### Port

We can set custom a SSH port like

```vim
Host alias alterative_alias
    Hostname <address>
    User <username>
    Port <port>
```

### Incremental file transfer over network

We can use `rsync` [instead](https://stackoverflow.com/a/20257021/5922871) of [`scp`]({{< ref "#copy-over-ssh" >}}) if synchronising a nonlocal destination and source.
Install with
```shell
sudo dnf install rsync
```
For example, to copy from remote to local,
```shell
rsync -avhP <username>@<address>:/path/to/source/ /path/to/destination/ 
```
If the source path is a directory and the trailing slash is included on the path, `source/path/`, the contents of the directory are copied into the destination directory (the same as `source/path/*`).
If the trailing slash is omitted, `source/path`, the directory is copied into the destination directory.

`<username>@<address>` can be replaced with `<name>` defined in [`~/.ssh/config`]({{< ref "#ssh-configuration-file" >}}).

`rsync` can also be used [locally]({{< ref "#merge-directories" >}}).

Note that if you want to use globbing `*` and you're on [Prezto]({{< ref "shell" >}}), you'll [need](https://unix.stackexchange.com/a/343421) to `unalias rsync` to remove the `noglob`.

Note that `rsync` must also be installed on the destination machine.

Alternatively, for bidirectional transfers, consider [Unison]({{< ref "unison" >}}).

### Scan local IP addresses

We can use `arp-scan` {{< ghub  royhills arp-scan >}} {{< docs "https://www.royhills.co.uk/wiki/index.php/Arp-scan_Documentation" >}}
```shell
sudo dnf install arp-scan
sudo arp-scan -l
```
or `nmap`
```shell
sudo dnf install nmap
nmap -sP 192.168.0.0/24  # or whatever subnet you want
```

### SSH server

For temporary SSH connections over LAN, start an SSH server with,
```shell
sudo dnf install openssh-server
sudo systemctl start sshd
```
You can check the IP address of a machine with,
```shell
ip a
```

## Documents

### Evince

[Evince](https://wiki.gnome.org/Apps/Evince) {{< wiki "Evince" >}} {{< glsh "https://gitlab.gnome.org/GNOME/evince" >}} ("to express clearly") is the GNOME document viewer.
Install it, if it isn't already, with
```shell
sudo dnf install evince
```
Support for additional formats can be installed, such as DjVu ("déjà vu")
```shell
sudo dnf install evince-djvu
```
Navigate positions when using hyperlinks with

Direction|Command
---|---
Previous | `Alt`+`P`
Next | `Alt`+`N`

### PDFs

See [the PDF tools posts]({{< ref "pdfs" >}}) for details.

### Printing

See available printers with
```shell
lpstat -a
```
and print with
```shell
lpr -P <printer name> <file name>
```
where `<file name>` supports multiple files and globbing.

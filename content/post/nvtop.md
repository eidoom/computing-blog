+++
title = "Monitoring Nvidia GPU usage on Linux"
date = 2021-02-07T19:47:49Z
edit = 2021-03-14
categories = ["software"]
tags = ["install","linux","desktop","laptop","fedora","nvidia","gpu","geforce","nvtop","gtx-1050m","gtx-1080-ti","negativo17"]
description = "Using NVTOP"
toc = true
draft = false
cover = "img/covers/60.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

To monitor the usage of my Nvidia GPU on Fedora 33, I use [NVTOP](https://github.com/Syllo/nvtop).

Having already installed the core negativo17 [drivers]({{< ref "nvidia-drivers-on-fedora#negativo17" >}}) and [`nvidia-driver-cuda`]({{< ref "blender" >}}), I only needed to install `cuda-nvml-devel` for `fedora-nvidia` (the negativo17 repo) dependencies.
So, I installed all [dependencies](https://github.com/Syllo/nvtop#fedora--redhat--centos) then  [downloaded, built and installed the program](https://github.com/Syllo/nvtop#nvtop-build) with
```shell
sudo dnf install cmake ncurses-devel git cuda-nvml-devel
cd ~/git
git clone https://github.com/Syllo/nvtop.git
mkdir -p nvtop/build
cd nvtop/build
cmake .. -DNVML_RETRIEVE_HEADER_ONLINE=True
make -j
sudo make install
```
Run with `nvtop`.

> {{< cal "2022-01-29" >}}
>
> It can be uninstalled with
> ```shell
> sudo make uninstall
> ```

+++
title = "Tmux cheatsheet"
date = 2020-01-07T17:10:04Z
edit = 2020-04-03
categories = ["cheatsheet"]
tags = ["terminal-multiplexer","environment","tmux","linux","desktop","server","laptop","terminal","shell"]
description = "Personal reminder of tmux commands"
toc = true
cover = "img/covers/89.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I use `tmux`, set up as described in [this previous post]({{< ref "tmux" >}}) with [this configuration](https://gitlab.com/eidoom/dotfiles-tmux).
Here, I record my frequently used commands.
Custom commands have their description appended with `!`.

## Shell commands

* `tmux new -s NAME`   create new session `NAME` and attach to it
* `tmux a -t NAME`     attach to existing session `NAME`
* `tmux ls`            list existing sessions

## Tmux commands

Note that

* `+` means hold previous key (used with modifiers)
* `-` means release previous key (used after prefix)
* `Arrow` means the arrow keys
* `jkl;` means Vim movement keys shifted one key right

and I use 

* `Ctrl`+`b`          local prefix`!`
* `Ctrl`+`a`          remote prefix

The commands are

* `Prefix`-`c`        new window
* `Prefix`-`d`        detach from session
* `Ctrl`+`d`          quit current pane/window/session (if last LEFT, then also RIGHT)
* `Prefix`-`z`        toggle fullscreen mode for current pane
* `Prefix`-`Alt`+`1`  equal width vertical panes
* `Prefix`-`Alt`+`2`  equal height horizontal panes 
* `Prefix`-`{`        swap panes up
* `Prefix`-`}`        swap panes down
* `Shift`+`Arrow`     switch current window`!`
* `Alt`+`jkl;`        switch current pane`!`
* `Prefix`-`\`        new outer pane by vertical split`!`
* `Prefix`-`|`        new inner pane by vertical split`!`
* `Prefix`-`-`        new inner pane by horizontal split`!`
* `Prefix`-`_`        new outer pane by horizontal split`!`

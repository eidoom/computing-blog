+++
title = "Getting terminal access to a broken system"
date = 2019-12-20T11:26:46Z
edit = 2020-01-22
categories = ["maintenance"]
tags = ["linux","grub","desktop","laptop","server","terminal"]
description = "How to get started on a fix"
toc = true
cover = "img/covers/98.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

During the process of [installing Nvidia drivers on Fedora]({{< ref "nvidia-drivers-on-fedora" >}}), I often ended up with a broken system which would fail for some reason to do with video on boot.
The easiest way to get terminal access to the system so I could diagnose and fix the problem was to change the `runlevel` in [`grub2`](https://wiki.archlinux.org/index.php/GRUB).

## Changing GRUB settings on boot

In the `grub2` boot screen, I navigated to the appropriate systemr: the Fedora with the latest kernel in this case.
I pressed `e` to access the boot parameters interface.
I navigated to the line starting with `linux` and appended `3` to this line.
I pressed `Ctrl`+`x` to start the system with these boot parameters.

## Conclusion

This starts the system in runlevel 3, which is the non-graphical mode, giving terminal access to the system.

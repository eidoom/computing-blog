+++
title = "Home cinema on Linux with mpv"
date = 2021-06-12T20:19:21+01:00
edit = 2021-07-05
categories = ["video"]
tags = ["rpmfusion","linux","fedora","mpv","video","desktop","laptop","media","interpolation","install","configure","fedora-34","nvidia","f34","gpu","projector","gnome","rpmfusion","fedora-35","f35","negativo17","ffmpeg","fruit","debian","debian-bookworm","debian-12"]
description = "The less we have to use that Windows partition, the better"
toc = true
draft = false
cover = "img/covers/3.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I previously used [a home cinema setup]({{< ref "home-cinema" >}}) that, unacceptably, was built on Windows.
Here, I rectify that by migrating to Linux.
All software used is now libre, with the exception of the [drivers for my Nvidia GPU]({{< ref "nvidia-drivers-on-fedora" >}}) (unfortunately the open-source driver Nouveau does not support CUDA or OpenCL).
We receive the added benefit of settings files <i class="fas fa-heart"></i> rather than options GUIs.
I also convince myself to lighten my over-the-top historical rendering configuration.

## Video player

We'll use [`mpv`](https://mpv.io/) {{< ghub mpv-player mpv >}} {{< docs "https://github.com/mpv-player/mpv/wiki" >}} {{< docs "https://mpv.io/manual/stable/" >}} {{< arch "Mpv" >}} as our video player.
Why?
I liked the docs---an indication of greatness, or at least sympathetic opinion.

### Installation

On Fedora, `mpv` is provided by the [RPM Fusion](https://rpmfusion.org/) Free repository.
We covered installation of RPM Fusion [earlier]({{< ref "install-fedora#rpmfusion" >}}).
The repository also provides `ffmpeg` which we need for the audio and video decoders.
We install them with
```shell
sudo dnf install ffmpeg mpv
```
Out-of-date software is always a worry when replying on a package, but, at least today (12 Jun 2021),
```shell
$ dnf list mpv
mpv.x86_64 0.33.1-1.fc34 @rpmfusion-free
```
shows we have the [latest release](https://github.com/mpv-player/mpv/releases/).

> {{< cal 2023-02-23 >}}
>
> These packages are also available from the base Fedora repositories, but the RPMFusion version supports more codecs.

> {{< cal 2023-05-18 >}}
>
> Note that `ffmpeg` (from RPM Fusion) relies on the `openh264` package from the `fedora-cisco-openh264` repo, which can be enabled with
> ```shell
> sudo dnf config-manager --set-enabled fedora-cisco-openh264
> ```

> #### Debian
>
> {{< cal "2024-03-02" >}}
>
> I recently installed `mpv` on Debian 12/`bookworm` too.
> This system has an old AMD GPU (Radeon HD 7970), so at least there were no problems with [video drivers](https://github.com/mpv-player/mpv/wiki/FAQ#amdgpu-open-source) (which Debian installs automatically).
> However, when using [`mpv` on `stable`/`bookworm` (0.35.1-4)](https://packages.debian.org/stable/mpv), it [crashed](https://github.com/mpv-player/mpv/issues/13062) when quitting or seeking without first pausing or seeking too fast (by holding right arrow) while paused.
> I wanted to try a more up-to-date version of `mpv` to see if that solved the issue, specifically the latest [release](https://github.com/mpv-player/mpv/releases), being v0.37.0.
> Using [`testing`](https://packages.debian.org/testing/mpv)  for `mpv` [only]({{< ref "server-install#installing-selectively-from-testing" >}}) wasn't an option since the dependency tree runs too deep (pulling in gnome to the update...).
> The `mpv` [installation page](https://mpv.io/installation/) recommends to use {{< ghn "mpv-player/mpv-build" >}} or a third-party repository, for which two are suggested.
> The first is [deb-multimedia](https://deb-multimedia.org/dists/testing/main/binary-amd64/package/mpv), but I was put off using it due to rumours of conflicts with the official Debian packages (ffmpeg etc) and [their maintainers](https://wiki.debian.org/DebianMultimedia/FAQ#There_is_.27Debian_Multimedia_Maintainers.27_and_.27deb-multimedia.org.27._So_what.27s_the_difference.3F).
> The other is [fruit](https://fruit.je/apt), which I liked because it's set up so you only pull in what you [want](https://apt.fruit.je/debian/bookworm/mpv/).
> I installed it (after uninstalling the official version) with
> ```shell
> sudo apt update
> sudo apt upgrade
> sudo apt install curl
> sudo curl \
>     --output-dir /etc/apt/trusted.gpg.d \
>     --remote-name \
>     https://apt.fruit.je/fruit.gpg
> sudo vim /etc/apt/sources.list.d/fruit.list 
> ```
> ```vim
> deb https://apt.fruit.je/debian bookworm mpv
> ```
> ```shell
> sudo apt update
> sudo apt install -t fruit-debian-bookworm mpv
> ```
> However, `mpv` still hangs for a while on quit...

### Configuration

My up-to-date settings can be found at {{< gl dotfiles-mpv >}} and installed with
```shell
git clone git@gitlab.com:eidoom/dotfiles-mpv.git ~/.config/mpv
```

Our [configuration file](https://mpv.io/manual/stable/#configuration-files) then resides at
```shell
$ cat ~/.config/mpv/mpv.conf
# use high quality GPU-accelerated video output
vo=gpu
profile=gpu-hq

# enable hardware decoding
hwdec=auto

# disable display of subtitles but still select and decode them
sub-visibility=no

# choose English audio track by default
alang=eng

# enable temporal video frame interpolation
interpolation=yes
video-sync=display-resample
tscale=oversample

# use digital audio passthrough over S/PDIF optical interface
audio-exclusive=yes
audio-spdif=ac3,dts
af=lavcac3enc=tospdif=yes
audio-device=alsa/iec958:CARD=PCH,DEV=0

save-position-on-quit=yes
```
I mainly follow the recommendations of the docs author since we seem to have compatible preferences.
* I set [video output](https://mpv.io/manual/stable/#options-vo) to be [GPU-accelerated](https://mpv.io/manual/stable/#video-output-drivers-gpu) ([the default](https://github.com/mpv-player/mpv/wiki/Which-VO-should-I-use)).
* [GPU rendering](https://mpv.io/manual/stable/#gpu-renderer-options) is set to the `gpu-hq` profile, which comprises
    ```shell
    $ mpv --show-profile=gpu-hq
    Profile gpu-hq: 
     scale=spline36
     cscale=spline36
     dscale=mitchell
     dither-depth=auto
     correct-downscaling=yes
     linear-downscaling=yes
     sigmoid-upscaling=yes
     deband=yes
    ```
    The upscaling and downscaling options are not really relevant because I generally view 1080p content on a 1080p display, although [chroma scaling](https://mpv.io/manual/stable/#options-cscale) `cscale` does come into play.
* I don't [display subtitles](https://mpv.io/manual/stable/#options-no-sub-visibility) by default.
* I set English as the default [audio language](https://mpv.io/manual/stable/#options-alang).
* I enable [hardware decoding](https://mpv.io/manual/stable/#options-hwdec).
* I enable the all-important interpolation, which I discuss in [the next section]({{< ref "#interpolation" >}}).
* I [configure the audio](https://mpv.io/manual/stable/#audio) so that supported compressed digital audio codecs are passed to my [audio receiver]({{< ref "home-cinema#audio-codecs" >}}), after being [encoded](https://mpv.io/manual/master/#audio-filters-tospdif) if necessary, via optical audio cable.
* I set `mpv` to [start files from last playback position](https://mpv.io/manual/stable/#options-save-position-on-quit)

## Interpolation

The `mpv` wiki provides a [nice writeup](https://github.com/mpv-player/mpv/wiki/Interpolation) on the options for temporal video frame interpolation and even samples for reference at {{< gh haasn interpolation-samples >}}.
These present the problems of *stutter* from low frame rate sources and *judder* from refresh rate mismatch, and discuss the ups and downs of the various solutions. 

In my settings, I [enable interpolation](https://mpv.io/manual/stable/#options-interpolation) with the [filter](https://mpv.io/manual/stable/#options-tscale) [`oversample`](https://github.com/mpv-player/mpv/wiki/Interpolation#smoothmotion) and set the [video synchronisation method](https://mpv.io/manual/stable/#options-video-sync) (see [this page](https://github.com/mpv-player/mpv/wiki/Display-synchronization) from the wiki) to the recommended option.

This marks a change from the [motion-based interpolation](https://github.com/mpv-player/mpv/wiki/Interpolation#motion-based-interpolation) I [used before]({{< ref "home-cinema#svp-1" >}}).
Although nowhere near as continuous in motion, it's *much* cheaper and doesn't produce the visible artifacting.
Playing this game is always a compromise between computational cost, blurring, and artifacting against buttery smoothness.

The default filter, `mitchell`, is a ["convolution-based interpolation"](https://github.com/mpv-player/mpv/wiki/Interpolation#convolution-based-interpolation) method.
[There](https://wiki.archlinux.org/title/mpv#High_quality_configurations) [are](https://github.com/mpv-player/mpv/issues/8414) [a](https://github.com/mpv-player/mpv/issues/2685) [number](https://old.reddit.com/r/mpv/comments/d54p4h/interpolation_svp_like/) [of](https://old.reddit.com/r/mpv/comments/jlbcw9/best_conf_to_make_mpv_as_smooth_and_sharp_as_svp/) posited "best" interpolation settings, mostly using [`tscale=box`](https://mpv.io/manual/stable/#options-tscale) with [`tscale-window=sphinx` or `tscale-window=quadratic`](https://mpv.io/manual/stable/#options-tscale-window), [`tscale-clamp=0.0`](https://mpv.io/manual/stable/#options-tscale-clamp), and various [`tscale-radius`](https://mpv.io/manual/stable/#options-tscale-radius), but I didn't really see a difference between them and the default.

I also tried out [SVP on Linux]({{< ref linux-svp >}}) for that great soap opera effect.

I settled with `oversample`---which simply fixes judder---because it's the sharpest, albeit the least smooth.
Unfortunately, I find the stutter very noticeable but I'll try to adapt to the true movie experience.

## Network mount

We'll use [the NFS share]({{< ref "server-install#nfs" >}}) to access the data on the server
```shell
mkdir ~/mnt
sudo mount ryanserver: ~/mnt
```

## Display configuration

I set up the projector display (OTM Optoma 1080P) to mirror a secondary display (DELL U2311H) of the same resolution.
Annoyingly, [GNOME's display settings](https://wiki.gnome.org/Design/SystemSettings/Displays) are too simplistic to allow configuring this, [so](https://askubuntu.com/q/576870) I used the Nvidia X Server Settings.
`xrandr` could also be used to do this from the terminal.
This is quite a handy configuration because the computer is in a different room to the projector.
{{< figure src="../../img/linux-video/nvidia-settings.webp" alt="nvidia settings" caption="Nvidia display settings" >}}

I also had to change the colour calibration settings for the projector under `GNOME Settings > Colour > Optoma 1080P Monitor`.
The automatic profile `Automatic - Optoma 1080P` was awful; I found the profile `Standard Space - sRGB` to work better.
{{< figure src="../../img/linux-video/colour-settings.webp" alt="nvidia settings" caption="GNOME colour calibration settings" >}}

## Nvidia driver woes

I see some tearing when using the main monitor (which includes upscaling on 1080p sources); I think these are [driver issues](https://github.com/mpv-player/mpv/wiki/FAQ#nvidia) having seen similar effects before with Linux Nvidia drivers.
Perhaps it would work better with the [`nouveau` drivers](https://nouveau.freedesktop.org/)?
At least it does not affect the (unscaled) projector.

> An option here is to use Windows with the new `mpv` setup.
> It has a [`choco`](https://chocolatey.org/) [package](https://community.chocolatey.org/packages/mpv) ([recall]({{< ref "home-cinema#chocolatey" >}}))
> ```shell
> choco install mpv
> ```
> (I also like the look of the [Scoop](https://scoop.sh/) [git build](https://github.com/lukesampson/scoop-extras/blob/master/bucket/mpv-git.json)) and we can use the above configuration at the [Windows location](https://mpv.io/manual/stable/#files-on-windows)
> ```shell
> git clone git@gitlab.com:eidoom/dotfiles-mpv.git %APPDATA%/mpv
> ```
> Note that you have to remember to change the system audio device appropriately before starting `mpv`.

## Update on drivers
{{< cal "2021-11-25" >}}

On Fedora 35 (GNOME 41.1), there was a strange tearing effect at the top of the screen.
I switched from [`negativo17`]({{< ref "nvidia-drivers-on-fedora#negativo17" >}}) to [RPM Fusion]({{< ref "install-fedora#rpmfusion" >}}), which both offer packages of the proprietary Nvidia drivers, and that seemed to fix it.
F35 actually ships with the open-source Nvidia driver Nouveau, but we need the proprietary drivers for CUDA support.

* Remove the `negativo17` drivers
    ```shell
    sudo dnf remove '*nvidia*'
    ```
* [Disable the `negativo17` repo]({{< ref "utilities#repositories" >}})
    ```shell
    sudo dnf config-manager --set-disabled fedora-nvidia
    ```
* [Install the RPM Fusion drivers (including CUDA)](https://rpmfusion.org/Howto/NVIDIA#Current_GeForce.2FQuadro.2FTesla)
    ```shell
    sudo dnf install xorg-x11-drv-nvidia-cuda
    ```
<!-- * [Edit the `grub` config]({{< ref "nvidia-drivers-on-fedora#installation" >}}) to not blacklist Nouveau -->
* Reboot
* Check it worked with
    ```shell
    $ modinfo -F version nvidia
    <version>
    ```

> {{< cal 2023-05-18 >}}
>
> This is how I've started using Nvidia on Fedora [everywhere]({{< ref "futhark#nvidia-drivers" >}}).

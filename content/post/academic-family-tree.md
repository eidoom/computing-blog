+++
title = "Academic family tree"
date = 2022-10-22T15:57:22+02:00
categories = ["web"]
tags = ["mini-project","python3","python","selenium","json","pip","js","javascript","npm","d3.js","css","html","visualisation","datavis","data","graph","interactive","website","webpage","static-webpage","gitlab","gitlab-ci","ci"]
description = "Scraping the Mathematics Genealogy Project and creating a force-directed graph with d3"
toc = true
draft = false
cover = "img/covers/105.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

For fun, I made an interactive visualisation of my academic family tree.
Details at {{< gl academic-family-tree >}}.

<iframe src="https://eidoom.gitlab.io/academic-family-tree" class="window"></iframe>

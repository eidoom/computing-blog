+++
title = "Home DNS again from NLnet Labs"
date = 2021-08-24T16:51:42+01:00
categories = ["dns"]
tags = ["raspberry-pi","raspi","rpi","unbound","nsd","self-host","dns","debian-11","debian-bullseye","linux","arm","server","headless","dd","random","secret","home","lan","network"]
description = "Setting up NSD and Unbound on a Raspberry Pi"
toc = true
draft = false
cover = "img/covers/19.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I have been very happy [using Unbound]({{< ref dns-unbound >}}) as my DNS resolver behind [Pi]({{< ref "home-network-ad-blocking-with-pi-hole" >}})-[hole]({{< ref "server-install-containers#pi-hole" >}}), although I recently [moved the primary nameserver setup onto a Raspberry Pi]({{< ref "rpi-dns" >}}) and found the experience a little sour.
I realised I don't actually need Pi-hole as I can just give [Unbound]({{< ref "#unbound" >}}) the blocklist and use [NSD]({{< ref "#nsd" >}}) as an authoritative DNS server.
This is a simpler, lighter setup, and means I can use [*bullseye*]({{< ref "debian-11" >}}) without incompatibility woes.

Maybe this configuration will even stick, who knows?

## System

I [flashed]({{< ref "rpi-dns#flashing" >}}) a [Debian *bullseye* RPi image]({{< ref "rpi-dns#operating-system" >}}) on an [SD card]({{< ref "sd-card" >}}).
I wrote an initialisation script at {{< gl init-rpi-dns >}} for running on the fresh Debian RPi image.
It performs the setup steps similar to [last]({{< ref "rpi-dns#security" >}}) [time]({{< ref "rpi-dns#network" >}}).
The repo README details its usage so I won't duplicate that here.

In future, I'll consider using {{< wl "configuration management software" "Comparison_of_open-source_configuration_management_software" >}} like {{< wl Ansible >}} for this, or maybe even building stateless images for deployment.

We'll run the authoritative nameserver with NSD on a custom port and only accessible on `localhost`, while the recursive nameserver Unbound will be on the standard DNS port 53, accessible over LAN, and able to pass incoming queries to NSD.

## NSD

We'll use [Name Server Daemon (NSD)](https://www.nlnetlabs.nl/projects/nsd/about/) {{< wiki NSD >}} {{< ghub NLnetLabs nsd >}} {{< docs "https://www.nlnetlabs.nl/documentation/nsd/" >}} as our authoritative DNS server for the LAN.

### Configuration

NSD configuration is discussed on {{< l "https://www.digitalocean.com/community/tutorials/how-to-use-nsd-an-authoritative-only-dns-server-on-ubuntu-14-04" >}}, {{< l "https://calomel.org/nsd_dns.html" >}}, {{< l "https://blog.socruel.nu/freebsd/how-to-implement-unbound-and-nsd-on-freebsd.html" >}}, and {{< l "https://wiki.archlinux.org/title/NSD" >}}.

On the Pi, we install [`nsd`](https://packages.debian.org/en/stable/nsd), which also provides a sample configuration at `/usr/share/doc/nsd/examples/nsd.conf.sample`
```shell
sudo apt install nsd
sudo nsd-control-setup
```
We set it up as
```shell
sudo vi /etc/nsd/nsd.conf.d/with-unbound.conf
```
```vim
server:
    server-count: 1
    ip-address: 127.0.0.1
    port: 53530
    do-ip4: yes
    hide-version: yes
    identity: "Home network authoritative DNS"
    zonesdir: "/etc/nsd"
remote-control:
    control-enable: yes
    control-interface: 127.0.0.1
    control-port: 8952
    server-key-file: "/etc/nsd/nsd_server.key"
    server-cert-file: "/etc/nsd/nsd_server.pem"
    control-key-file: "/etc/nsd/nsd_control.key"
    control-cert-file: "/etc/nsd/nsd_control.pem"
key:
    name: "ryankey"
    algorithm: hmac-sha256
    secret: "<secret>"
zone:
    name: "lan"
    zonefile: "lan.forward.zone"
zone:
    name: "1.168.192.in-addr.arpa"
    zonefile: "lan.reverse.zone"
```
[generating the random key `<secret>` with]({{< ref "utilities#random" >}})
```shell
dd if=/dev/random of=/dev/stdout count=1 bs=32 status=none | base64
```
and the zone files (which use BIND zone file syntax): forward zone
```shell
sudo vi /etc/nsd/lan.forward.zone
```
```zone
$ORIGIN lan.    ; default zone domain
$TTL 30M        ; default time to live

; Start of authority record
@       IN      SOA     rpi-dns.lan.            admin.lan. (
                        2021090501              ; serial number
                        1H                      ; refresh
                        15M                     ; retry
                        2W                      ; expire
                        30M                     ; min ttl
                        )

; Name servers
                    IN      NS      rpi-dns.lan.

; A records
router              IN      A       192.168.1.1
ryanserver          IN      A       192.168.1.2
rpi-dns             IN      A       192.168.1.3
rpi-mpd             IN      A       192.168.1.4
```
and reverse zone
```shell
sudo vi /etc/nsd/lan.reverse.zone
```
```zone
$ORIGIN lan.            ; default zone domain
$TTL 30M                ; default time to live

; Start of authority record
1.168.192.in-addr.arpa.       IN      SOA     rpi-dns.lan.            admin.lan. (
                        202110051               ; serial number
                        1H                      ; refresh
                        15M                     ; retry
                        2W                      ; expire
                        30M                     ; min ttl
                        )

; PTR records
1.1.168.192.in-addr.arpa.     IN      PTR     router
2.1.168.192.in-addr.arpa.     IN      PTR     ryanserver
3.1.168.192.in-addr.arpa.     IN      PTR     rpi-dns
4.1.168.192.in-addr.arpa.     IN      PTR     rpi-mpd
```

We check the configuration
```shell
$ sudo nsd-checkconf /etc/nsd/nsd.conf  # no output is success
$ sudo nsd-checkzone lan /etc/nsd/lan.forward.zone
zone lan is ok
```
then start up `nsd`
```shell
sudo systemctl enable nsd.service
sudo systemctl start nsd.service
```
The easiest way to reload the configuration at any time is
```shell
sudo systemctl restart nsd.service
```

### Test

We test the service
```shell
$ sudo systemctl status nsd
● nsd.service - Name Server Daemon
     Loaded: loaded (/lib/systemd/system/nsd.service; enabled; vendor preset: enabled)
     Active: active (running) since Sun 2021-09-05 17:37:03 UTC; 10min ago
       Docs: man:nsd(8)
   Main PID: 3264 (nsd: xfrd)
      Tasks: 3 (limit: 2002)
     Memory: 83.1M
        CPU: 958ms
     CGroup: /system.slice/nsd.service
             ├─3264 /usr/sbin/nsd -d -P
             ├─3265 /usr/sbin/nsd -d -P
             └─3379 /usr/sbin/nsd -d -P

Sep 05 17:37:03 rpi2-20210717 systemd[1]: Starting Name Server Daemon...
Sep 05 17:37:03 rpi2-20210717 nsd[3264]: nsd starting (NSD 4.3.5)
Sep 05 17:37:03 rpi2-20210717 nsd[3265]: nsd started (NSD 4.3.5), pid 3264
Sep 05 17:37:03 rpi2-20210717 systemd[1]: Started Name Server Daemon.
```
We install
```shell
sudo apt install dnsutils
```
and check (`+short` can be added to these for brief output):
* domain nameserver
    ```shell
    $ dig NS lan. @127.0.0.1 -p 53

    ; <<>> DiG 9.16.15-Debian <<>> NS lan. @127.0.0.1 -p 53
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18313
    ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1472
    ;; QUESTION SECTION:
    ;lan.                           IN      NS

    ;; ANSWER SECTION:
    lan.                    1654    IN      NS      rpi-dns.lan.

    ;; Query time: 0 msec
    ;; SERVER: 127.0.0.1#53(127.0.0.1)
    ;; WHEN: Tue Oct 05 16:58:28 UTC 2021
    ;; MSG SIZE  rcvd: 54
    ```
* DNS resolution
    ```shell
    $ dig router.lan @127.0.0.1 -p 53530

    ; <<>> DiG 9.16.15-Debian <<>> router.lan @127.0.0.1 -p 53530
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10480
    ;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 2
    ;; WARNING: recursion requested but not available

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ;; QUESTION SECTION:
    ;router.lan.                    IN      A

    ;; ANSWER SECTION:
    router.lan.             1800    IN      A       192.168.1.1

    ;; AUTHORITY SECTION:
    lan.                    1800    IN      NS      rpi-dns.lan.

    ;; ADDITIONAL SECTION:
    rpi-dns.lan.            1800    IN      A       192.168.1.3

    ;; Query time: 0 msec
    ;; SERVER: 127.0.0.1#53530(127.0.0.1)
    ;; WHEN: Tue Oct 05 13:35:04 UTC 2021
    ;; MSG SIZE  rcvd: 93
    ```
* reverse DNS lookup
    ```shell
    $ dig -x 192.168.1.1 @127.0.0.1 -p 53530

    ; <<>> DiG 9.16.15-Debian <<>> -x 192.168.1.1 @127.0.0.1 -p 53530
    ;; global options: +cmd
    ;; Got answer:
    ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 59936
    ;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
    ;; WARNING: recursion requested but not available

    ;; OPT PSEUDOSECTION:
    ; EDNS: version: 0, flags:; udp: 1232
    ;; QUESTION SECTION:
    ;1.1.168.192.in-addr.arpa.      IN      PTR

    ;; ANSWER SECTION:
    1.1.168.192.in-addr.arpa. 1800  IN      PTR     router.lan.

    ;; Query time: 0 msec
    ;; SERVER: 127.0.0.1#53530(127.0.0.1)
    ;; WHEN: Tue Oct 05 14:46:27 UTC 2021
    ;; MSG SIZE  rcvd: 77
    ```

Alternatively, install `ldnsutils` and use `drill` instead of `dig`---they both use the same syntax.

## Unbound

I set up Unbound [yet]({{< ref "dns-unbound" >}}) ([man](https://man.archlinux.org/man/unbound.conf.5)) [again]({{< ref "rpi-dns#unbound" >}}), but this time I let it handle the blocklist.
I was inspired by [this post](https://deadc0de.re/articles/unbound-blocking-ads.html) and later found many other such accounts, referred to the [Unbound configuration file documentation](https://unbound.docs.nlnetlabs.nl/en/latest/manpages/unbound.conf.html), and was helped best by my old friend, [ArchWiki](https://wiki.archlinux.org/title/Unbound#Domain_blacklisting).

Pairing Unbound with NSD is covered by {{< l "https://calomel.org/unbound_dns.html" >}} and {{< l "https://wiki.archlinux.org/title/NSD#Configuring_unbound" >}}.

### Configuration

My up-to-date configuration and update scripts are at {{< gl conf-unbound >}}.
This repo contains scripts to install, configure, and update Unbound as I want it, using the NSD server as the authoritative nameserver for the `lan.` domain.
It also includes a script to generate a blocklist that Unbound can consume from [Steven Black's blocklist](https://github.com/StevenBlack/hosts).

I installed my configured Unbound on the Pi with
```shell
git clone git@gitlab.com:eidoom/conf-unbound.git
cd conf-unbound
sudo ./init.sh
```

### Check configuration

I checked the configuration files with
```shell
$ sudo unbound-checkconf /etc/unbound/unbound.conf
unbound-checkconf: no errors in /etc/unbound/unbound.conf
```

### Dog fooding

To use the new DNS system for name resolution on the machine itself,
```shell
echo "nameserver 127.0.0.1" > /etc/resolv.conf
```

## Seeing it in action

On my laptop (Fedora 34), install `nslookup` tool with
```shell
sudo dnf install bind-utils
```
(or use `dig` from `bind-utils`, or `drill` from `ldns-utils`) and try something that should resolve
```shell
$ nslookup computing-blog.netlify.app 192.168.1.3
Server:         192.168.1.3
Address:        192.168.1.3#53

Non-authoritative answer:
Name:   computing-blog.netlify.app
Address: 206.189.52.23
Name:   computing-blog.netlify.app
Address: 206.189.50.60
Name:   computing-blog.netlify.app
Address: 2a03:b0c0:3:d0::143f:d001
Name:   computing-blog.netlify.app
Address: 2a03:b0c0:3:d0::d26:4001
```
and now something on the blocklist
```shell
$ nslookup incoming.telemetry.mozilla.org 192.168.1.3
Server:         192.168.1.3
Address:        192.168.1.3#53

** server can't find incoming.telemetry.mozilla.org: REFUSED
```
and finally something that `nsd` is the authority for
```shell
$ nslookup router.lan 192.168.1.3
Server:         192.168.1.3
Address:        192.168.1.3#53

Non-authoritative answer:
Name:   router.lan
Address: 192.168.1.1
```

## Using the nameserver

To use the nameserver for all devices on my LAN, I configured my router (the default gateway of the network) to set the default nameserver to the static IP address of the Raspberry Pi.
This can be quite a different process depending on the [hardware]({{< ref "wap#inventory" >}}).
For instance, on [OpenWrt](https://openwrt.org/start) via the [LuCI web interface](https://openwrt.org/docs/guide-user/luci/start) ([recall]({{< ref "openwrt-services" >}})), the appropriate field is found at `Network > DHCP and DNS > General Settings > DNS forwardings`.

> ### Dnsmasq
> That OpenWrt configuration is setting the upstream nameserver for its [Dnsmasq](https://dnsmasq.org/doc.html) {{< docs "https://man.archlinux.org/man/extra/dnsmasq/dnsmasq.8.en" >}} {{< pl "https://wiki.debian.org/dnsmasq" >}} {{< arch "Dnsmasq" >}} process.
> Dnsmasq is a wee DHCP and DNS server that can be run on a router or something like a Raspberry Pi.
> It could be used as an alternative way to provide a nameserver for the LAN ([eg](https://alanporter.com/blog/2014-03-09/dnsmasq-unbound)).
> The upstream nameserver (Unbound for us) is set by `server`.
> It reads the local `/etc/hosts` for setting DNS A (address) records, or they can be set in the configuration files at `/etc/dnsmasq.conf` or `/etc/dnsmasq.d/*.conf` with the `host-record` field.
> CNAME records can be set with the field `cname`, including wildcards if the nameserver is authoritative.
> These fields can be used to configure Dnsmasq itself to provide the LAN DNS records, or the `lan` domain could be directed to NSD, eg `server=/lan/127.0.0.1#53530`.
> The blocklist could optionally be moved to Dnsmasq too.

## Outlook

I'm considering running a DHCP server (eg [Dnsmasq]({{< ref "#dnsmasq" >}})) too to centrally manage IP address allocation on my home LAN rather than setting static IP addresses on each machine.

## Appendices

### Qemu

I tried following [this](https://wiki.debian.org/RaspberryPi/qemu-user-static) to run the image under emulation and prepare it before inserting into the Pi.

```shell
sudo apt install qemu qemu-user-static binfmt-support
unxz 20210717_raspi_2_bullseye.img.xz
```
```shell
$ sudo fdisk -lu 20210717_raspi_2_bullseye.img
Disk 20210717_raspi_2_bullseye.img: 1.46 GiB, 1572864000 bytes, 3072000 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x3fb5f4d3

Device                         Boot  Start     End Sectors  Size Id Type
20210717_raspi_2_bullseye.img1        8192  614399  606208  296M  c W95 FAT32 (LBA)
20210717_raspi_2_bullseye.img2      614400 3071999 2457600  1.2G 83 Linux
```
```shell
sudo dd if=/dev/zero bs=1M count=1024 >> 20210717_raspi_2_bullseye.img
```
```shell
$ sudo losetup -f -P --show 20210717_raspi_2_bullseye.img
/dev/loop0
```
```shell
$ sudo parted /dev/loop0
(parted) rm 2
(parted) mkpart primary 315MB 2647MB
```
```shell
sudo resize2fs /dev/loop0p2
```
```shell
mkdir ~/mnt
sudo mount /dev/loop0p2 -o rw ~/mnt
sudo mount /dev/loop0p1 -o rw ~/mnt/boot
sudo mount --bind /dev ~/mnt/dev/
sudo mount --bind /sys ~/mnt/sys/
sudo mount --bind /proc ~/mnt/proc/
sudo mount --bind /dev/pts ~/mnt/dev/pts
```
```shell
sudo cp /usr/bin/qemu-arm-static ~/mnt/usr/bin
```
```shell
sudo chroot ~/mnt bin/bash
```
Play around...
```shell
sudo umount ~/mnt
sudo losetup -d /dev/loop0
```
I copied my edited image back to my laptop with `rsync` and wrote it to the SD card,
```shell
sudo dd if=20210717_raspi_2_bullseye_edit.img of=/dev/sda bs=64k oflag=dsync status=progress
```
but something had gone wrong as it wouldn't boot.

+++
title = "Heading anchors in Hugo"
date = 2020-01-24T08:20:06Z
edit = 2020-04-01
categories = ["web"]
tags = ["meta","front-end","html","css","hugo","static-website"]
description = "Discovering Hugo cross referencing and adding anchor symbols to headings"
toc = true
cover = "img/covers/39.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Cross referencing in Hugo

Hugo [automatically creates anchors for headings](https://gohugo.io/content-management/cross-references/#hugo-heading-anchors). 
These headers have unique identifiers, but Hugo abstracts away this implementation detail with some handy builtin [shortcodes](https://gohugo.io/content-management/shortcodes/): 
[`ref` and `relref`](https://gohugo.io/content-management/cross-references/#use-ref-and-relref).
These provide a flexible [cross referencing](https://en.wikipedia.org/wiki/Cross-reference) solution with absolute and relative links respectively. 
For example, I can make an absolute link to this section with
```hugo
[link]({{</* ref "#cross-referencing-in-hugo" */>}})
```
which compiles to this [link]({{< ref "#cross-referencing-in-hugo" >}}), or generate this [link]({{< ref "meta#lets-get-started" >}}) with
```hugo
[link]({{</* ref "meta#lets-get-started" */>}})
```
> By the way, shortcodes are printed in fenced code blocks using a [special syntax](https://discourse.gohugo.io/t/how-is-the-hugo-doc-site-showing-shortcodes-in-code-blocks/9074). See [this page's source](https://gitlab.com/eidoom/computing-blog/blob/master/content/post/anchors.md) for an example.

Discovering `ref`, I went ahead and [changed](https://gitlab.com/eidoom/computing-blog/commit/060053a909ba2fe1f664c7bd73fd5a39bafea9a5) all my cross references to use it.

## Adding anchor symbols to headings

Hugo doesn't offer an automatic way to add anchor symbol links to headings like those common on [git platforms](https://gitlab.com/eidoom/computing-blog/blob/master/README.md) or in the [Hugo documentation](https://gohugo.io/content-management/cross-references/).
I found this [discussion](https://discourse.gohugo.io/t/adding-anchor-next-to-headers/1726/14), which presented an easy way to achieve this. 
The solution was to do a [regex replace](https://gohugo.io/functions/replacere/) on the page content, so instead of using `{{ .Content }}` in the page layout, using
```html
    {{- with .Content -}}
      {{ . | replaceRE "(<h[1-3] id=\"([^\"]+)\".+)(</h[1-3]+>)" `${1}<a href="#${2}" class="hanchor" ariaLabel="Anchor"> <i class="fas fa-link"></i></a> ${3}` | safeHTML }}
    {{- end -}}
```

I [added](https://gitlab.com/eidoom/computing-blog/commit/d38d73c82efd99500551fe2b6002a601a4c10b8e) this to this site, as you can see by the link icons beside headings. 
Here is the new [single page layout](https://gitlab.com/eidoom/computing-blog/blob/master/layouts/_default/single.html)
and
[styling](https://gitlab.com/eidoom/computing-blog/blob/master/static/style.css).


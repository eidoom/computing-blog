+++
title = "PythonX.Y virtual environments"
date = 2021-05-27T11:38:35+01:00
categories = ["coding"]
tags = ["python","fedora","fedora-34","linux","desktop","laptop","server","pip","pipenv","pdm","poetry","pyenv","python3","python2"]
description = "It's easier than [global package management](https://en.wikipedia.org/wiki/Dependency_hell)"
toc = true
draft = false
cover = "img/covers/9.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

A Python virtual environment constitutes a self-contained environment that is independent of the system environment, with its own Python binary and set of installed packages.
Here we see how to set up a virtual environment with a particular version of Python binary.

## Python 2 virtual environment


To make Python 2 virtual environments from Python 3, we need the tool `virtualenv` {{< docs "https://virtualenv.pypa.io/en/latest/" >}} {{< pack "https://pypi.org/project/virtualenv/" >}} {{< ghub pypa virtualenv >}} .

* On Fedora, install it with
    ```shell
    sudo dnf install python3-virtualenv
    ```
* Set up a new virtual environment `py2-name` under `~/venvs` which will use `python2.7` with
    ```shell
    mkdir ~/venvs
    virtualenv ~/venvs/py2-name -p /usr/bin/python2
    ```
* Activate it with 
    ```shell
    source ~/venvs/py2-name/bin/activate
    ```
* Deactivate with 
    ```shell
    deactivate
    ```

## Python 3.x virtual environments

In Python 3, virtual environments can be created with the builtin module [`venv`](https://docs.python.org/3/library/venv.html).
The virtual environment inherits the version of Python that it is created with.
For example, if we wanted a virtual environment with `python3.6`, here's how we'd do it.

### Python version

#### DNF package manager

On Fedora, Python 3.6 can be installed with
```shell
sudo dnf install python36
```
As of today on Fedora 34, versions 3.5--3.10 are available under the same package name syntax (i.e. `python35`--`python310`), with 3.9 being the default `python`.
> {{< cal "2024-10-15" >}}
>
> On Fedora 40, it's `python3.6`--`python3.13` excepting 3.12 which is `python3`. 

#### Pyenv

Alternatively, if we need finer version control (3.x.y), or a version not offered by the package manager, or a distro independent solution, there's {{< gh pyenv pyenv >}}.
It's a very clever shell script to manage using different Python versions on the same machine.

##### Install Pyenv

Install along with Python build dependencies with
```shell
sudo dnf install \
    make \
    gcc \
    patch \
    zlib-devel \
    bzip2 \
    bzip2-devel \
    readline-devel \
    sqlite \
    sqlite-devel \
    openssl-devel \
    tk-devel \
    libffi-devel \
    xz-devel \
    libuuid-devel \
    gdbm-libs \
    libnsl2
curl https://pyenv.run | bash
```

##### Shell configuration

To set up the shell environment for Pyenv (assuming we're using [`.bashrc.d`]({{< ref "shell#deprecated" >}})),
```shell
vim ~/.bashrc.d/pyenv.bash
```
```shell
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
```
Then restart shell.

##### Install Python versions

Install specific version with
```shell
pyenv install 3.x.y
```
or with optimisations on,
```shell
env PYTHON_CONFIGURE_OPTS='--enable-optimizations --with-lto' \
    PYTHON_CFLAGS='-march=native -mtune=native' \
    pyenv install --verbose 3.x.y
```

##### Use Python version

Activate the newly installed Python with
```shell
pyenv shell 3.x.y
```
and deactivate with
```shell
pyenv shell --unset
```

##### Other commands

We can also

* view available versions with `pyenv install -l`.
* use `pyenv local <version>` to set the version for a directory (using `.python-version`) and unset with `pyenv local --unset`.
* update Pyenv with `pyenv update`.
* uninstall Python versions with `pyenv uninstall <version>`.
* uninstall Pyenv with `rm ~/.bashrc.d/pyenv.bash && rm -rf $(pyenv root)`.

### Virtual environment

* Create the virtual environment using
    ```shell
    mkdir ~/venvs
    python3.6 -m venv ~/venvs/py3.6-name
    ```
* Activate with
    ```shell
    source ~/venvs/py3.6-name/bin/activate 
    ```
* Deactivate with 
    ```shell
    deactivate
    ```

## Project environments

### Simple shell script

The previous examples created a virtual environment in the user directory `~/venvs`, but it can be placed anywhere.
A common choice is to create it under `.venv` in the root of the project.
I like to make a file `init.sh` which can be sourced to activate the virtual environment, or initialise it if it's the first run.
```shell
vi init.sh
```
```vim
echo "Note that this must be run as \`source init.sh\`"

if [ ! -d ".venv" ]; then
	python3 -m venv .venv
	source .venv/bin/activate
	if [ -f "requirements.txt" ]; then
		python3 -m pip install -r requirements.txt
	fi
else
	source .venv/bin/activate
fi

echo "venv activated"
echo "use \`deactivate\` when done"
```
This uses `pip` {{< docs "https://pip.pypa.io/" >}} {{< ghubn "pypa/pip" >}} {{< wiki "Pip_(package_manager)" >}}, the de facto package manager for Python, to manage packages.
`pip` uses the [Python Package Index (PyPI)](https://pypi.org/) {{< wiki "Python Package Index" >}} repository (not to be confused with the Python compiler [Pypy]({{< ref pypy >}})) by default.

Packages are installed (with the environment activated) with
```shell
pip install <package>
```
updated with
```shell
pip install --upgrade <package>
```
and removed with
```shell
pip remove <package>
```
The dependencies are written in the file `requirements.txt`, which is generated by running
```shell
pip freeze > requirements.txt
```

### Virtual environment managers

Alternatively, there are some powerful tools which can manage virtual environments, dependencies and packaging.
One immediate advantage of these tools over the previous shell script is that they generate a lock file with the specific versions of dependencies, making the project more reproducible.

#### Pipenv

There's Pipenv {{< docs "https://pipenv.pypa.io/" >}} {{< ghubn "pypa/pipenv" >}} {{< pypi "pipenv" >}}, a tool which abstracts over virtual environments and `pip` package management to provide a single conventient interface.

Install on Fedora with
```shell
sudo dnf install pipenv
```

#### Poetry

Another option is [Poetry](https://python-poetry.org/).
It seems the best tool for packaging since it manages publishing to PyPI.
```shell
sudo dnf install poetry
```

#### PDM

My current go-to is [PDM](https://pdm-project.org/en/latest/).
It's similar to Pipenv but uses more Pythonic configuration files and is more featureful.
```shell
curl -sSL https://pdm-project.org/install-pdm.py | python3 -
```

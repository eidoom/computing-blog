+++
title = "Surface screen removal"
date = 2020-08-23T17:09:41+01:00
edit = 2020-08-26
categories = ["hardware"]
tags = ["surface","microsoft","surface-pro","surface-pro-4","repair","tablet","fix","touchscreen","digitiser","n-trig","stylus","motherboard","chassis","screen"]
description = "Removing a broken screen from a Surface Pro 4"
toc = false
cover = "img/covers/72.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

{{< figure alt="before" src="../../img/surface-remove/before.jpg" caption="A Surface with a damaged screen">}}

To my anguish, I smashed the screen of my Surface Pro 4.
The first step in replacing the damaged screen with a new one is removing the old one.
This proved to be a nontrivial exercise.
It was quite similar to removing a stubborn 700x25C tyre from a bicycle wheel, except of course the tyre was made of glass.

I [prepared](https://www.youtube.com/watch?v=XjhvAcCye4M) by [watching](https://www.youtube.com/watch?v=E_X-xIIeJM0) a [bunch](https://www.youtube.com/watch?v=n74Pc_luSGI) of [videos](https://www.youtube.com/watch?v=GMWzeQTAF2I) and [reading](https://www.ifixit.com/Teardown/Microsoft+Surface+Pro+4+Teardown/51568#s112635) [some](https://blog.kamens.us/2017/05/10/lessons-learned-from-diy-microsoft-surface-pro-4-screen-replacement/) [articles](https://most-useful.com/screen-flickering-on-surface-pro-4-solved/).

{{< figure alt="tools" src="../../img/surface-remove/tools.jpg" caption="A happenstance screen remover's toolset">}}

I assembled some tools from around the house: 
* a hair drier as a surrogate heat gun
* a Stanley knife (or retractable-bladed utility knife if we're going to not commit synecdoches)
* a suction cup borrowed from a set of suction cup blackout blinds
* a pair of tweezers
* a plastic carpentry spacer
* a pair of guitar plectrums

Some points on safety:
* I would recommend eye protection for this as the glass is very brittle and pings out when it shatters
* I allowed the battery to discharge before commencing the disassembley as it can be dangerous to overheat Lithium-ion batteries (even when I'm wielding but the humble hair drier)

{{< figure alt="during" src="../../img/surface-remove/during.jpg" caption="Getting the screen off">}}

Starting the removal was the hardest bit, I think.
I started near the bottom right corner, where the screen was looser due to damage, but not completely shattered.
I used the hair drier on maximum heat, directing it at the edge to warm and loosen the glue and tape securing the screen.
I used the suction cup, attached close to the edge, to pull the screen up while gently working a plectrum between the screen and the chassis to unstick the glue and tape there.
I slowly worked my way around the edge like this, using the spacer (which is a bit thicker than the plectrums) to keep the screen separated from the chassis in the region near where I was working.
The shattered side was a mess; I had to use the knife and tweezers to remove the debris and disconnect the screen edge as cleanly as possible.
I left the top edge until last since the camera array is there along with some delicate antennas directly under the tape, as we'll see once I expose them.

{{< figure alt="inside" src="../../img/surface-remove/inside.jpg" caption="Just after the screen came off">}}

Once the edges of the screen were loose from the chassis, the two ribbon cables attaching the N-trig pen and touch module and the screen itself had to be detached from the motherboard.
They have electromagnetic interference (EMI) shields which can be pinged off with gentle persuasion from the tip of the knife to expose the connection.
The connectors are easily similarly levered off.

Next I cleaned up the leftover tape and bits of shattered glass from the chassis.

{{< figure alt="inside-clean" src="../../img/surface-remove/inside-clean.jpg" caption="Inside the Surface, now with edges cleaned up">}}

I noticed that in removing the screen, I had inadvertently damaged one of the antennas.

{{< figure alt="antenna" src="../../img/surface-remove/antenna.jpg" caption="The damaged antenna, stuck back together">}}

The antenna is just a conductive strip attached to some tape, so I just stuck the tape back down and tried to ensure the conductive strips pieces were touching where they had previously ripped apart.
When I later tested the Surface, the 2.4 GHz wifi, 5 GHz wifi, and bluetooth were all working, so apparently the antenna was fine.
Phew.

{{< figure alt="screen" src="../../img/surface-remove/screen.jpg" caption="The back of the screen">}}

With the chassis, done I turned to the screen.
I removed the shields and ribbon cables from the screen similarly to how they came off the motherboard.

{{< figure alt="n-trig" src="../../img/surface-remove/n-trig.jpg" caption="The N-trig module, still attached to the screen">}}

The N-trig module that controls the touch and pen capabilities was attached to the screen under one of the EMI shields I had removed to detach a ribbon cable.
The two small ribbon cables attaching the screen to the N-trig module were detached by pushing up the black piece of plastic which secured the connection.
The module was stuck onto the screen with conductive tape, which I was careful not to damage while prying off the module as this forms part of the circuit that operates the touchscreen.
With the conductive tape intact, I can just stick the module onto the replacement screen and the electrical contact should be fine.

{{< figure alt="ribbons" src="../../img/surface-remove/ribbons.jpg" caption="The removed ribbon cables, EMI shields, and N-trig board">}}

With that, everything was disassembled.
I tested the Surface by attaching it to an external monitor via the miniDP connection; everything was in working order.

The screen had a serial number `LTL123YL01`, which I used to order a suitable [replacement](https://www.ebay.co.uk/itm/401169599367).

{{< figure alt="serial" src="../../img/surface-remove/serial.jpg" caption="The serial number of the screen">}}

When the new screen arrives, I'll [put it together]({{< ref "surface-screen-replacement" >}}) and hopefully I'll have my Surface back.

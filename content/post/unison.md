+++
title = "File synchronisation with Unison"
date = 2023-01-23T14:48:41+01:00
categories = ["software"]
tags = ["unison","server","laptop","desktop","file-synchronisation","backup","fedora","debian","debian-11","debian-bullseye","debian-testing"]
description = "For fully bi-directional synchronisation"
toc = true
draft = false
cover = "img/covers/123.avif"
coveralt = "stable-diffusion-2:'a cyberpunk visualisation of data flowing between two nodes in cyberspace as described in William Gibson's novel Neuromancer'+ImageMagick"
+++

## Introduction

I wanted a solution to synchronise directories between different machines.
[Rsync]({{< ref "/tags/rsync" >}}) is my preferred choice for uni-directional transfers.
For this situation, I was tempted by [Git]({{< ref "/tags/git" >}}), but that was too heavy.
I found [Unison](https://www.cis.upenn.edu/~bcpierce/unison/) {{< ghubn bcpierce00 unison >}} {{< wiki "Unison_(software)" >}}, which allows bi-directional transfers that propagate all changes in each location.
It seemed the cleanest solution, without the pollution of multiple cloud platform support that competitors exhibited, and simply used by manual call from the command line.

> Remember that Git, Rsync, and Unison can all be used to sync directories locally on a single machine, too, of course.

Unison, which uses the language {{< wl OCaml >}}, must be installed on both machines involved in the transfer.
Before Unison version 2.52, both sides had to have matching versions of Unison and OCaml.
[Since 2.52](https://github.com/bcpierce00/unison/blob/master/NEWS.md#changes-in-2520), [the version doesn't matter](https://github.com/bcpierce00/unison/wiki/2.52-Migration-Guide), so I'll be sure to stick to newer versions.

## Installation

### Debian

We need to use the `testing` channel to get Unison of version 2.52 or later:
* On Debian `bullseye`/`stable` (11), the [Unison package](https://packages.debian.org/bullseye/unison) is currently of a version less than 2.52,
* and `bullseye-backports` doesn't have a Unison package,
* so we need to [use `bookworm`/`testing`]({{< ref "server-install#installing-selectively-from-testing" >}}) to get a [Unison package](https://packages.debian.org/bookworm/unison) of an appropriate version.

```shell
sudo apt update
sudo apt install -t testing unison
unison -version
```

### Fedora

[Unfortunately, Fedora stopped packaging Unison](https://fortintam.com/blog/unison-packages-dropped-from-fedora-linux/).
There is a [`copr`](https://copr.fedorainfracloud.org/coprs/croadfeldt/Unison/), but I elected to just use [the latest binary release](https://github.com/bcpierce00/unison/releases/).

> {{< cal 2023-04-20 >}}
>
> Updated versions

```shell
UNISON_V=2.53.2
OCAML_V=4.14.1
mkdir -p ~/tar/unison ~/bin
cd ~/tar/unison
wget https://github.com/bcpierce00/unison/releases/download/v${UNISON_V}/unison-v${UNISON_V}+ocaml-${OCAML_V}+x86_64.linux.tar.gz
tar xf unison-v${UNISON_V}+ocaml-${OCAML_V}+x86_64.linux.tar.gz
cp bin/unison ~/bin/
cd
unison -version
```

[Note that I have `~/bin` in my `PATH`](https://gitlab.com/eidoom/dotfiles-bash/-/blob/main/bashrc#L11).
Other common choices include `~/.local/bin` and `~/local/bin`.

## Usage

### Initialisation

It's as simple, for example, as:
```shell
cd ~/sync
unison . ssh://<remote>/sync
```
where `<remote>` can be either:
* `<remote_user>@<remote_host>`,
* or the name of a `Host` defined in `~/.ssh/config`.

Here's a sample output for an initial use:
```shell
Unison 2.53.0 (ocaml 4.14.0): Contacting server...                                                                    
Connected [//<local host>//home/<local_user>/sync -> //<remote_host>//home/<remote_user>/sync]                              
  
Looking for changes
Warning: No archive files were found for these roots, whose canonical names are:
        /home/<local_user>/sync
        //<remote_host>//home/<remote_user>/sync
This can happen either
because this is the first time you have synchronized these roots, 
or because you have upgraded Unison to a new version with a different
archive format.  

Update detection may take a while on this run if the replicas are 
large.

Unison will assume that the 'last synchronized state' of both replicas
was completely empty.  This means that any files that are different
will be reported as conflicts, and any files that exist only on one
replica will be judged as new and propagated to the other replica.
If the two replicas are identical, then no changes will be reported.

If you see this message repeatedly, it may be because one of your machines
is getting its address from DHCP, which is causing its host name to change
between synchronizations.  See the documentation for the UNISONLOCALHOSTNAME
environment variable for advice on how to correct this.

Donations to the Unison project are gratefully accepted: 
http://www.cis.upenn.edu/~bcpierce/unison


Press return to continue.[<spc>] 
  Waiting for changes from server
Reconciling changes

local          <remote_host>
dir      ---->              [f] f

Proceed with propagating updates? [] y
Propagating updates


Unison 2.53.0 (ocaml 4.14.0) started propagating changes at 15:50:28.87 on 23 Jan 2023
[BGN] Copying  from /home/<local_user>/sync to //<remote_host>//home/<remote_user>/sync
[END] Copying
Unison 2.53.0 (ocaml 4.14.0) finished propagating changes at 15:50:31.66 on 23 Jan 2023, 2.796 s


Saving synchronizer state
Synchronization complete at 15:50:31  (1 item transferred, 0 skipped, 0 failed)
```

Notice we press:
* `Enter` to continue after the initialisation message
* `f` to accept the recommended action from Unison, which is to copy over the directory
* `y` to accept the list of updates

### Synchronisation

For normal usage, we can auto-accept non-conflicting changes, for example with:
```shell
touch test
unison -auto . ssh://<remote>/sync
```
```shell
Unison 2.53.0 (ocaml 4.14.0): Contacting server...
Connected [//<local host>//home/<local_user>/sync -> //<remote_host>//home/<remote_user>/sync]
 
Looking for changes
  Waiting for changes from server
Reconciling changes

local          <remote_host>       
new file ---->            test

Proceed with propagating updates? [] y
Propagating updates


Unison 2.53.0 (ocaml 4.14.0) started propagating changes at 15:52:28.36 on 23 Jan 2023
[BGN] Copying test from /home/<local_user>/sync to //<remote_host>//home/<remote_user>/sync
[END] Copying test              
Unison 2.53.0 (ocaml 4.14.0) finished propagating changes at 15:52:28.46 on 23 Jan 2023, 0.099 s


Saving synchronizer state       
Synchronization complete at 15:52:28  (1 item transferred, 0 skipped, 0 failed)
```

+++
title = "Advent of Code 2022 with Rust"
date = 2023-01-19
categories = ["advent-of-code"]
tags = ["advent-of-code-2022","rust","coding","programming","aoc","aoc22"]
description = "Returning to Rust"
toc = true
draft = false
cover = "img/covers/121.avif"
coveralt = "stable-diffusion-2:'fantasy cityscape with towers in the clouds'+RealESRGAN_x4plus+ImageMagick"
+++

I used Rust for 2022's Advent of Code at {{< gl aoc22 >}}.
I did the days before I started travelling.

+++
title = "Headless Deluge"
date = 2023-10-31T20:31:30Z
categories = ["home"]
tags = ["torrent", "bittorrent","deluge","lan","network","remote","client","server","laptop","desktop","fedora","fedora-38","debian","debian-12","debian-testing","debian-bookworm"]
description = "Setting up a Deluge daemon on Debian"
toc = true
draft = false
cover = "img/covers/storm-at-sea.webp"
#coveralt = "?"
+++

## deluged

On Debian 12, we install the [Deluge](https://deluge-torrent.org/) {{< wiki "Deluge_(software)" >}} [daemon](https://packages.debian.org/stable/deluged) along with the [web server](https://packages.debian.org/stable/deluge-web):

```shell
sudo apt update
sudo apt install deluged deluge-web
```

## bug

However, there is an annoying [bug](https://dev.deluge-torrent.org/ticket/3514) with the current `stable`/`bookworm` `deluged` (2.0.3) which is [fixed](https://github.com/deluge-torrent/deluge/commit/89189adb24321c3db6bfa816ec557d7d8367ba24) in 2.0.4.
I opted to do the lazy [trick]({{< ref "server-install#installing-selectively-from-testing" >}}) of installing it from `testing`/`trixie` by adding the repo
```shell
sudo vim /etc/apt/sources.list.d/testing.list
```
```vim
deb http://deb.debian.org/debian/ trixie main
```
and de-prioritising it
```shell
sudo vim /etc/apt/preferences.d/testing.pref
```
```vim
Package: *
Pin: release a=testing
Pin-Priority: 1
```
then installing from it
```shell
sudo apt update
sudo apt install -t testing deluged deluge-web deluge-common
```

## service

The Debian package maintainers have helpfully set up a `systemd` service under the user `debian-deluged`.
We make a quick edit to the service to allow execution and read access to everyone:

```shell
sudo EDITOR=vim systemctl edit deluged
```

```systemd
### Editing /etc/systemd/system/deluged.service.d/override.conf
### Anything between here and the comment below will become the new contents of the file

[Service]
UMask=002

### Lines below this comment will be discarded

### /lib/systemd/system/deluged.service
# [Unit]
# Description=Deluge Bittorrent Client Daemon
# Documentation=man:deluged
# After=network-online.target
# 
# [Service]
# Type=simple
# UMask=007
# User=debian-deluged
# Group=debian-deluged
# ExecStart=/usr/bin/deluged -d -c /var/lib/deluged/config -l /var/log/deluged/daemon.log -L info
# 
# Restart=on-failure
# 
# # Time to wait before forcefully stopped.
# TimeoutStopSec=300
# 
# [Install]
# WantedBy=multi-user.target
```

As the comments explain, this creates a drop-in file `/etc/systemd/system/deluged.service.d/override.conf` that overrides (a subset of the entries in) the main file `/lib/systemd/system/deluged.service`.
Here we change the `UMask` to `002` (corresponding to permissions `775`) so the `other` permissions are `r-x`.
We must restart the service to apply the new configuration:

```shell
sudo systemctl restart deluged
```

## location

We [set up](https://wiki.archlinux.org/title/Deluge#Shared_directories_for_downloads/uploads) a download location `<location>` to be accessible by the daemon (user: `debian-deluged`) and also `<user>`:

```shell
mkdir <location>
chown -R debian-deluged:debian-deluged <location>
chmod 775 <location>
usermod -a -G debian-deluged <user>
```

## configuration

We `stop` the service to edit the Deluge configuration, [allowing](https://dev.deluge-torrent.org/wiki/UserGuide/ThinClient#EnableRemoteConnection) non-web-client remote access and setting the default download location:

```shell
sudo systemctl stop deluged
sudo vim /var/lib/deluged/config/core.conf
```

[](https://dev.deluge-torrent.org/wiki/UserGuide/ThinClient#EnableRemoteConnection)

```vim
...
    "allow_remote": true,
...
    "download_location": "<location>",
...
```

## user

We also [add](https://dev.deluge-torrent.org/wiki/UserGuide/ThinClient#AddUsertotheauthenticationfile) a Deluge user `<username>` for non-web clients with power level 10 for admin:

```shell
sudo vim /var/lib/deluged/config/auth
```

```vim
...
<username>:<password>:10
```

Then start the daemon again:

```shell
sudo systemctl start deluged
```

## clients

Now we can remotely control the daemon through:

* the GTK UI (package `deluge-gtk`, select `ThinClient` mode)
* the console (package `deluge-console`)
* the web client (default address: `http://<host address>:8112/`, password may be set in web client on first access).

## outlook

All in all, while Deluge is more featureful, it's also more buggy than [Transmission]({{< ref "transmission-bittorrent" >}}), so I'll go back to using Transmission.
Perhaps [qBittorrent](https://www.qbittorrent.org/) would be worth a try.

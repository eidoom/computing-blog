+++
title = "Presentations on the web"
date = 2021-04-05T10:04:14+01:00
categories = ["typesetting"]
tags = ["pandoc","html","css","markdown","markup","mathematica","microsoft","surface","onenote","reveal.js","academia","pdf","svg","gitlab-ci","gitlab-pages","gitlab","slides","presentation","inkscape","web"]
description = "Presentation slides with pandoc and reveal.js"
toc = true
draft = false
cover = "img/covers/52.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

For a recent PhD [conference](https://conference.ippp.dur.ac.uk/event/937/), I presented a quick pedagogical overview of my field.
There's a recording of it [here](https://conference.ippp.dur.ac.uk/event/937/contributions/5039/attachments/4088/4823/RyanMoodieYTF20.mp4).
The slides are [here](https://eidoom.gitlab.io/ytf20), with source at {{< gl ytf20 >}}.

I wrote the content in `markdown`, which was converted to web slides with [`pandoc`](https://pandoc.org/) and [reveal.js](https://revealjs.com/) using `pandoc`'s `revealjs` output format option.
I make a couple graphs with Mathematica.
I drew the diagrams with OneNote on my Surface, exported them as PDFs, and used `pdf2svg` to [convert]({{< ref "pdfs#pdf-to-svg" >}}) them to SVGs, touching up with [Inkscape]({{< ref "inkscape" >}}) where necessary.
The slides are deployed with [GitLab CI]({{< ref "gitlab-ci" >}}).

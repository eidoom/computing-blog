+++
title = "A dashboard for my home server"
date = "2019-12-02T14:51:04Z"
edit = 2020-02-10
tags = ["linux","server","debian","nginx","pandoc","static-website","lan","network","css","html","markdown","make","automate","front-end","back-end","debian-buster","debian-10","home-server"]
categories = ["web","home"]
description = "Using Nginx to serve a webpage with links to services on my home server"
toc = true
cover = "img/covers/102.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I have [a few services running on my home server]({{< ref "/tags/server" >}}), so I set up a simple web page with links to each service.
To generate (static) content, I decided to use [pandoc](https://pandoc.org/) to generate html from a Markdown file.
I used [Nginx](https://nginx.org/) to serve the web page.

## Installing Nginx and pandoc

Simple! Since we're on Debian

```shell
sudo apt install nginx pandoc net-tools
```

I included `net-tools` for diagnostics. 

## Setting a custom port for Nginx

Since [Pi-hole was running on this machine]({{< ref "home-network-ad-blocking-with-pi-hole" >}}), I had to change the port that Nginx would use.
Choosing a memorable number, I decided to set the port to be {{< wlc 24601 "Jean_Valjean" >}}.

Open up the settings file

```shell
sudo vim /etc/nginx/sites-available/default
```

and change

```vim
listen 80 default_server;
listen [::]:80 default_server;
```

to

```vim
listen 24601 default_server;
listen [::]:24601 default_server;
```

Then

```shell
sudo systemctl restart nginx
```

and confirm success with 

```shell
sudo netstat -tlpn | grep nginx
```
```vim
tcp        0      0 0.0.0.0:24601           0.0.0.0:*               LISTEN      5377/nginx: master  
tcp6       0      0 :::24601                :::*                    LISTEN      5377/nginx: master  
```

## Serving static content

I decided to keep the content in my home directory.

```shell
mkdir ~/www
```

To tell Nginx this, open 

```shell
sudo vim /etc/nginx/sites-available/default
```

and change

```vim
root /var/www/html;
```

to

```vim
root /home/USER/www;
```

then

```shell
sudo systemctl restart nginx
```

## Generating static content

Write the content in Markdown

```shell
cd ~/www
vim index.html
```

I lazily styled it with the css from a pandoc example

```shell
wget https://pandoc.org/demo/pandoc.css
```

I wrote a short Makefile for the generation step

```shell
echo "all:\n\tpandoc index.md -f markdown -t html5 -s --highlight-style haddock -c pandoc.css -o index.html" > Makefile
```

Generate the html with `make` and access the dashboard at <http://ryanserver:24601/>.

## Adding a file browser
{{< cal "21 Dec 2019" >}}

I added a simple file browser to my webpage, using the builtin Nginx file browser feature.
Installation was as simple as adding the following snippet to `/etc/nginx/sites-available/default`

```vim
location /files {
		alias root/directory/to/list;
		autoindex on;
}
```

and restarting with

```shell
sudo systemctl restart nginx
```

The `/files` after `location` determined the URL of the page, so the file browser could then be accessed at <http://ryanserver:24601/files>.

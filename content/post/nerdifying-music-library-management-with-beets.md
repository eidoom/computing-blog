+++
title = "Nerdifying music library management with beets"
date = "2019-11-03T13:50:54Z"
edit = 2020-07-11
tags = ["home","linux","server","media","install","music","automate","debian","database","library","beets","home-server","debian-buster","debian-10","audio"]
categories = ["audio"]
description = "My favourite music library management software"
toc = true
cover = "img/covers/97.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[Beets](http://beets.io/) is the endgame music library management system for me. 
I set it up on my linux server (running Debian Buster).

## Set up

### Installation

I like living on the bleeding-edge (despite my stability policy on my server) and intended to tinker with the code, so I installed beets from the [GitHub repo](https://github.com/beetbox/beets) as an editable [pip](https://pypi.org/project/pip/) package as described [here](https://github.com/beetbox/beets/wiki/Hacking#getting-the-source).

```shell
cd ~/git
git clone git@github.com:beetbox/beets.git
cd ~/git/beets
python3 -m pip install --user -e .
```

I made an alias `beet` for quick terminal access

```shell
echo "alias beet='python3 -m beets'" >> ~/.zshrc
source ~/.zshrc
```

That was it for install.

### Plugins

The next exciting part was the plugin feature.
There are some great plugins available; I've mentioned two below.

Plugins are activated by adding their name to `plugins` in the [beets configuration file]({{< ref "#configuration" >}}).
I installed most dependancies with `apt`. The main exception was Python packages that weren't packaged for Debian, which I just installed with pip

```shell
python3 -m pip install --user PACKAGE
```

#### Web

The [web plugin](https://beets.readthedocs.io/en/stable/plugins/web.html) is basic but functional.
I've been using to play music on devices in my local network from the music library on my server.
It requires
```shell
sudo apt install python3-flask
```

#### Chromaprint/Acoustid Plugin

The [Chromaprint/Acoustid Plugin](https://beets.readthedocs.io/en/stable/plugins/chroma.html) identifies songs by [acoustic fingerprinting](https://en.wikipedia.org/wiki/Acoustic_fingerprint).
This is expensive, but useful if the files have no metadata to start with.

It requires the `pyacoustid` Python library, which I installed with
```shell
sudo apt install python3-acoustid
```

The [Chromaprint](https://acoustid.org/chromaprint) binary package is also required.
I downloaded the [latest binary](https://github.com/acoustid/chromaprint/releases/download/v1.4.3/chromaprint-fpcalc-1.4.3-linux-x86_64.tar.gz), unarchived it and put `fpcalc` in `/usr/local/bin`:
```shell
cd ~/downloads
wget https://github.com/acoustid/chromaprint/releases/download/v1.4.3/chromaprint-fpcalc-1.4.3-linux-x86_64.tar.gz
tar -xvf chromaprint-fpcalc-1.4.3-linux-x86_64.tar.gz
sudo cp chromaprint-fpcalc-1.4.3-linux-x86_64/fpcalc /usr/local/bin
rm -r chromaprint-fpcalc-1.4.3-linux-x86_64*
```

### Configuration

Configuration was easy thanks to some great [docs](https://beets.readthedocs.io/en/stable/reference/config.html).
The configuration file is accessed via

```shell
beet config -e
```
I set mine up as
```vim
directory: /media/three-tb-disk/Audio
import:
        move: yes
        incremental: yes
        quiet_fallback: asis
        resume: yes
        duplicate_action: keep
asciify_paths: yes
plugins: web random duplicates info fromfilename discogs fuzzy play fetchart lastgenre beatport missing bpd chroma lyrics
chroma:
    auto: no
web:
        host: ryanserver.local
match:
            strong_rec_thresh: 0.10
```

## Usage

### Importing

On [importing](https://beets.readthedocs.io/en/stable/reference/cli.html#import) music to the beets database with
```shell
beet import <path/to/music>
```
beets analyses the metadata of the files and compares it to online catalogues of such data to correct the information.
Music in the library can also be reanalysed with
```shell
beet import -L <query>
```
and other useful switches include 
* `-t` for timid, so matches always require user confirmation
* `-A` to disable autotagging, so music is imported with its original metadata

Queries `<query>` support: 
* [fields](https://beets.readthedocs.io/en/stable/reference/query.html#specific-fields) like `artist:ryan` 
* [paths](https://beets.readthedocs.io/en/stable/reference/query.html#path-queries), for example `path/to/music` 
* and [more](https://beets.readthedocs.io/en/stable/reference/query.html#)

### Managing

Here are some examples of useful tools for managing the music library:
* Beets has a powerful CLI for [querying](https://beets.readthedocs.io/en/stable/reference/cli.html#list) the database,
    ```shell
    beet ls <query>
    ```
    The output format can be customised in terms of [metadata fields](https://beets.readthedocs.io/en/stable/reference/pathformat.html#available-values) by adding, for example,
    ```shell
    -f '$track - $title - $album - $artist - $format - $path'
    ```
    Be sure to use `'`s, not `"`s.
* Music files can be deleted through the same interface as `ls` with
    ```shell
    beet rm -d <query>
    ```
* Similarly, metadata can be edited by
    ```shell
    beet modify <query> field="<new value>"
    ```
* The [duplicates plugin](https://beets.readthedocs.io/en/v1.3.14/plugins/duplicates.html) can be used to merge a splintered album with
    ```shell
    beet dup --album --merge --delete album:"<album title>"
    ```
* Further commands are details [here](https://beets.readthedocs.io/en/stable/reference/cli.html#).

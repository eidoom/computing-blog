+++
title = "Matrix Synapse"
date = 2023-01-20T16:39:01+01:00
categories = ["self-host"]
tags = ["matrix","synapse","postgresql","nginx","firewalld","element","eturnal","systemd","server","federation","open","libre","database","debian","debian-bullseye","debian-11","bullseye-backports","cloud-server","cloud"]
description = "Setting up a Matrix home server"
toc = true
draft = false
cover = "img/covers/122.avif"
coveralt = "stable-diffusion-2:'a cyberpunk matte painting of an edge runner in the matrix showing neon abstract visualisations of data like skyscrapers in a city'+ImageMagick"

+++

[Matrix](https://matrix.org/) {{< wiki "Matrix_(protocol)" >}} {{< arch Matrix >}} is an open federated communication protocol for instant messaging and VoIP.
It's one of two such ecosystems, the other being the older [XMPP]({{< ref "/tags/xmpp" >}}).
The flagship Matrix client is [Element, which I wrote about before]({{< ref "element" >}}).
The reference Matrix server is [Synapse](https://matrix.org/docs/projects/server/synapse) {{< ghubn matrix-org synapse >}} {{< docs "https://matrix-org.github.io/synapse/latest/" >}} {{< pl "https://packages.debian.org/bullseye-backports/matrix-synapse" >}}, written in Python.
Its successor [Dendrite](https://matrix.org/docs/projects/server/dendrite), written in Go, is in beta currently.

## Synapse

We'll [install](https://matrix-org.github.io/synapse/latest/setup/installation.html) Synapse on Debian Bullseye (11) (on my [cloud server]({{< ref "cloud" >}})) using the [Debian package](https://packages.debian.org/bullseye-backports/matrix-synapse) from `bullseye-backports`.
It's currently version [1.73.0](https://github.com/matrix-org/synapse/releases/tag/v1.73.0), which is unfortunately behind upstream [1.75.0](https://github.com/matrix-org/synapse/releases/tag/v1.75.0), but I'd rather the ease of using the official Debian package than any alternatives.

Make sure backports are enabled in `/etc/apt/sources.list` (or `/etc/apt/sources.list.d/stable.list` if [using testing]({{< ref "server-install#installing-selectively-from-testing" >}})), ie. the following lines are there (optionally with `contrib` and `nonfree`)

```vim
deb https://deb.debian.org/debian bullseye-backports main # contrib non-free
deb-src https://deb.debian.org/debian bullseye-backports main # contrib non-free
```

Then install with

```shell
sudo apt update
sudo apt install -t bullseye-backports matrix-synapse
```

> {{< cal "2023-05-15" >}}
>
> I've been seeing some bugs in the Element client with decrypting messages (or rather, not decrypting them on some devices, specifically messages between Element on the web and on iOS).
> In the hope of fixing this, I decided to use a more up-to-date version of Synapse.
> The `bullseye-backports` package is on version [1.78.0](https://github.com/matrix-org/synapse/releases/tag/v1.78.0), while [1.83.0](https://github.com/matrix-org/synapse/releases/tag/v1.83.0) is the latest stable release.
> To use the latest version, I migrated to the [Matrix.org package](https://matrix-org.github.io/synapse/latest/setup/installation.html#matrixorg-packages),
> ```shell
> sudo systemctl stop matrix-synapse
> sudo apt remove matrix-synapse
> sudo apt install -y lsb-release wget apt-transport-https
> sudo wget -O /usr/share/keyrings/matrix-org-archive-keyring.gpg https://packages.matrix.org/debian/matrix-org-archive-keyring.gpg
> echo "deb [signed-by=/usr/share/keyrings/matrix-org-archive-keyring.gpg] https://packages.matrix.org/debian/ $(lsb_release -cs) main" |
>     sudo tee /etc/apt/sources.list.d/matrix-org.list
> sudo apt update
> sudo apt install matrix-synapse-py3
> ```
> I went with the `Matrix.org` version of `/etc/matrix-synapse/homeserver.yaml` rather than keeping the `bullseye-backports` version.
> All my custom settings are safe in `/etc/matrix-synapse/conf.d/*.yaml` anyway.

Useful file locations:
* The logs can be accessed at `/var/log/matrix-synapse/homeserver.log`.
* The default [settings](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html) are at `/etc/matrix-synapse/homeserver.yaml`.


We'll set the servername in a new file `/etc/matrix-synapse/conf.d/server_name.yaml` as
```yaml
server_name: "<domain>"
```

## Database

We'll use [PostgreSQL](https://www.postgresql.org/) {{< wiki PostgreSQL >}} {{< git "https://git.postgresql.org/gitweb/?p=postgresql.git" >}} {{< arch PostgreSQL >}} {{< pl "https://wiki.debian.org/PostgreSql" >}} {{< pl "https://packages.debian.org/bullseye/postgresql" >}} for the [Matrix database](https://matrix-org.github.io/synapse/latest/postgres.html).
We'll use [`ident`](https://en.wikipedia.org/wiki/Ident_protocol) authentication, as it's the default, so we'll also install the [`oidentd`](https://janikrabe.com/projects/oidentd/) {{< docs "https://oidentd.janikrabe.com/" >}} {{< wiki Oidentd >}} {{< ghubn janikrabe oidentd >}} {{< arch Oidentd >}} daemon.

```shell
sudo apt install postgresql oidentd libpq5
```

As the `postgres` Linux user, we'll create a PostgreSQL user `synapse_user` and make them a new database `synapse`.

```shell
sudo -u postgres bash
createuser --pwprompt synapse_user
createdb \
    --encoding=UTF8 \
    --locale=C \
    --template=template0 \
    --owner=synapse_user \
    synapse
```

We configure Synapse to use the PostgreSQL database in `/etc/matrix-synapse/conf.d/postgresql.yaml`
```yaml
database:
  name: psycopg2
  args:
    user: synapse_user
    password: <password>
    database: synapse
    host: localhost
    cp_min: 5
    cp_max: 10
```

If we ever want to reset the database, we can
```shell
sudo -u postgres bash
dropdb synapse
createdb --encoding=UTF8 --locale=C --template=template0 --owner=synapse_user synapse
```

## Firewall

We have to open ports for Matrix and TURN using [`firewalld`](https://firewalld.org/) {{< wiki firewalld >}} {{< ghubn firewalld firewalld >}} {{< arch Firewalld >}} {{< pl "https://packages.debian.org/stable/firewalld" >}}

```shell
sudo firewall-cmd --permanent --zone=public --add-service=matrix

sudo firewall-cmd --permanent --new-service=turn
sudo firewall-cmd --permanent --service=turn --add-port=3478/udp
sudo firewall-cmd --permanent --service=turn --add-port=3478/tcp
sudo firewall-cmd --permanent --service=turn --add-port=5349/udp
sudo firewall-cmd --permanent --service=turn --add-port=5349/tcp
sudo firewall-cmd --permanent --service=turn --add-port=49152-65535/udp
sudo firewall-cmd --permanent --zone=public --add-service=turn

sudo firewall-cmd --check-config
sudo firewall-cmd --reload
```

## Reverse proxy

We already have [Nginx set up as a reverse proxy]({{< ref "cloud#__reverse_proxy" >}}) ([configuration](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__reverse_proxy/manifest)), so we just need to [add Synapse to it](https://matrix-org.github.io/synapse/latest/reverse_proxy.html).

Create `/etc/nginx/sites-available/synapse.conf`
```nginx
server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    # For the federation port
    listen 8448 ssl http2 default_server;
    listen [::]:8448 ssl http2 default_server;

    server_name synapse.<domain>;

    ssl_certificate /etc/letsencrypt/live/<name>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<name>/privkey.pem;

    location ~ ^(/_matrix|/_synapse/client) {
        # note: do not add a path (even a single /) after the port in `proxy_pass`,
        # otherwise nginx will canonicalise the URI and cause signature verification
        # errors.
        proxy_pass http://localhost:8008;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Host $host;

        # Nginx by default only allows file uploads up to 1M in size
        # Increase client_max_body_size to match max_upload_size defined in homeserver.yaml
        client_max_body_size 50M;
    
        # Synapse responses may be chunked, which is an HTTP/1.1 feature.
        proxy_http_version 1.1;
    }
}
```
Symlink to `sites-enabled`.

Create `/etc/matrix-synapse/conf.d/nginx.yaml`
```yaml
listeners:
  - port: 8008
    tls: false
    bind_addresses:
      - '::1'
      - '127.0.0.1'
    type: http

    x_forwarded: true

    resources:
      - names: [client]
        compress: true
      - names: [federation]
        compress: false
```

## Well-known URI

To set up the {{< wl "well-known URI" >}} for the [client](https://matrix-org.github.io/synapse/latest/setup/installation.html#client-well-known-uri) and for the [server](https://matrix-org.github.io/synapse/latest/delegate.html), edit `/etc/nginx/sites-available/<main>.conf`
```nginx
server {
    ...

    location /.well-known/matrix/server {
        return 200 '{"m.server": "synapse.<domain>:443"}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
    }

    location /.well-known/matrix/client {
        return 200 '{"m.homeserver": {"base_url": "https://synapse.<domain>"}}';
        default_type application/json;
        add_header Access-Control-Allow-Origin *;
    }
}
```

And create `/etc/matrix-synapse/conf.d/url.yaml`
```yaml
public_baseurl: "https://synapse.<domain>"
```

## Registration

Now, to [add a user](https://matrix-org.github.io/synapse/latest/setup/installation.html#registering-a-user).
We want to [allow](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html#enable_registration) users to [register](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html#registration) themselves, but [only](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html#registration_requires_token) if they have a [token](https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/registration_tokens.html).

We create `/etc/matrix-synapse/conf.d/registration.yaml`
```yaml
enable_registration: true
registration_requires_token: true
registration_shared_secret: '<private string>'
```

To generate the token, we need an admin account.
Then we can use the [admin API](https://matrix-org.github.io/synapse/latest/usage/administration/admin_api/).

We make the admin account with
```shell
synapse_register_new_matrix_user \
    --admin \
    --user admin \
    --password "<password>" \
    --config /etc/matrix-synapse/conf.d/registration.yaml
```

Login with a client to get `admin`'s `access_token`.
For example, in Element, navigate to `Settings > Help & About > Advanced > Access Token`.

Then we can generate the access token, using default settings (unlimited usage of token, no expiration), with
```shell
curl \
    --header "Authorization: Bearer <access token>" \
    --data {} \
    --request POST http://127.0.0.1:8008/_synapse/admin/v1/registration_tokens/new
```

To view it again later,
```shell
curl \
    --header "Authorization: Bearer <access token>" \
    --request GET http://127.0.0.1:8008/_synapse/admin/v1/registration_tokens
```

## Federation

Everything *should* be set up correctly that our server is part of the [federation](https://matrix-org.github.io/synapse/latest/federate.html).
You can test that [here](https://federationtester.matrix.org/).

## VoIP

For VoIP, we need to [set up TURN](https://matrix-org.github.io/synapse/latest/turn-howto.html) with [`eturnal`](https://matrix-org.github.io/synapse/latest/setup/turn/eturnal.html) (or `coturn`).

[We already set up set that up]({{< ref "cloud#__eturnal" >}}) ([configuration](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__eturnal/manifest)).

We just need to tell Synapse with `/etc/matrix-synapse/conf.d/turn.yaml`
```yaml
turn_uris: [ "turn:<domain>?transport=udp", "turn:<domain>?transport=tcp" ]
turn_shared_secret: "<secret>"
turn_user_lifetime: 86400000
turn_allow_guests: true
```

Note that we are not using TURNS (TURN over TLS) because currently the Chromium WebRTC library that Element Android and iOS use [won't accept](https://bugs.chromium.org/p/webrtc/issues/detail?id=11710) Let's Encrypt TLS certificates, which is what I use.
The call itself is still encrypted, so TURNS only becomes necessary for privacy in [extreme situations](https://security.stackexchange.com/questions/184886/what-is-the-point-of-turns-in-webrtc), anyway.

## URL previews

For [URL previews](https://matrix-org.github.io/synapse/latest/setup/installation.html#url-previews),

```shell
sudo apt install libxml2-dev
```

and make `/etc/matrix-synapse/conf.d/url-previews.yaml`

```yaml
url_preview_enabled: true
url_preview_ip_range_blacklist:
 - '127.0.0.0/8'
 - '10.0.0.0/8'
 - '172.16.0.0/12'
 - '192.168.0.0/16'
 - '100.64.0.0/10'
 - '169.254.0.0/16'
 - '::1/128'
 - 'fe80::/64'
 - 'fc00::/7'
max_spider_size: "10M"
```

This doesn't seem to work...

## Success

Simply
```shell
sudo systemctl reload nginx
sudo systemctl restart matrix-synapse
```
and restart your client, then it *should* work.

Of course, it won't first time.
Good luck.

{{< figure src="/img/ai/matrix.avif" alt="stable-diffusion-2:'a cyberpunk matte painting of an edge runner in the matrix showing neon abstract visualisations of data like skyscrapers in a city'+ImageMagick" caption="Yes, I got the width and height the wrong way around in the first attempt to generate a cover image..." position="center" >}}

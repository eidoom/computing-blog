+++
title = "Video conferencing on Linux"
date = 2020-04-02T19:27:25+01:00
categories = ["software"]
tags = ["fedora","linux","gnome","skype","zoom","teams","gnome-extensions","desktop","laptop","install","communication","video-call","discord","video-conference","environment","snap","element","matrix","slack","voip"]
description = "Installing video call tools on Fedora"
toc = true
cover = "img/covers/83.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

In light of everybody working from home to curb the COVID-19 pandemic, videotelephony has never been so useful.
Here's how I installed some such software for my PhD work on Fedora 32--34.

Unfortunately, all the popular platforms which I've had to use for official university business are proprietary and predictably treat Linux users as third-class citizens.

### Top icons for Gnome
Currently [using Gnome as my desktop environment]({{< ref "install-fedora#gnome-configuration" >}}), I find the following programs are best used with tray icons in the top panel.
This can be achieved with the [Gnome extension]({{< ref "install-fedora#gnome-extensions" >}}) ~~[TopIcons Plus Git](https://extensions.gnome.org/extension/2311/topicons-plus/)~~ [Tray Icons: Reloaded](https://extensions.gnome.org/extension/2890/tray-icons-reloaded/) (see [F34 upgrade]({{< ref "fedora-upgrade#fedora-34" >}})).

### Installation methods
As option one, the following apps can be installed on Fedora with `dnf`, either through a community repository, or by a direct URL.

All these apps can also be installed with the software packaging and deployment utilities (SPDU) `flatpak` or `snap`.
See [this post]({{< ref "software-deploy" >}}) for discussion of and installation instructions for these utilities.
The short answer is: I favour `flatpak` unless `snap` offers a later version of the package.

The non-traditional SPDU method is recommended as it offers a form of package management for these proprietary softwares, which often aren't supported by official or mainstream community repositories (with good reason).

## Applications

### Discord
While designed for gamers, Discord is used for social channels at my university.

Discord can be installed through the system package manager with a third-party repository: it comes in `rpmfusion-nonfree`, which I [already installed]({{< ref "install-fedora#rpmfusion" >}}).
Get Discord with
```shell
sudo dnf install discord
```

It has a [snap](https://snapcraft.io/discord)
```shell
sudo snap install discord
```
and a [`flathub` package](https://flathub.org/apps/details/com.discordapp.Discord)
```shell
sudo flatpak install flathub com.discordapp.Discord
flatpak run com.discordapp.Discord
```

> {{< cal 2021-09-28 >}}
>
> RPMFusion gives v0.0.15, while the two other options offer the same higher version (v0.0.16).

It can also be used in [the browser](https://discord.com/app).

### Skype
Skype still seems to be a popular choice amongst professors.

Install it manually with
```shell
sudo dnf install https://go.skype.com/skypeforlinux-64.rpm
```

I found that this Skype package intermittently has problems with broken dependencies, which is nonideal.

There is [a classic snap for Skype](https://snapcraft.io/skype), which can be installed with
```shell
sudo snap install --classic skype
```
but I've seen the version lag behind the `rpm` package and [the `flathub` release](https://flathub.org/apps/details/com.skype.Client), so I'd recommend using `flatpak`
```shell
sudo flatpak install flathub com.skype.Client
flatpak run com.skype.Client
```
where the initial run seems to install the [desktop entry](https://wiki.archlinux.org/index.php/Desktop_entries).

> {{< cal 2021-09-28 >}}
>
> The two SPDU options offer the same version (v8.75.0.140).

Skype can no longer be used in the browser (unless you're on Edge).

### Slack

While not a video call app, Slack is a widely used communication platform.
It's been used at several virtual conferences I have attended for out-of-lecture discussion and announcements.

You can get an `rpm` package from [their site](https://slack.com/intl/en-gb/downloads/linux) for a manual `dnf` installation.

It has a classic [snap](https://snapcraft.io/slack), installed with
```shell
sudo snap install --classic slack
```
or a [`flathub` package](https://flathub.org/apps/details/com.slack.Slack),
```shell
flatpak install flathub com.slack.Slack
flatpak run com.slack.Slack
```

> {{< cal 2021-09-28 >}}
>
> The two SPDU options offer the same version (v4.19.2).

Slack can also be used in their [web app](https://app.slack.com).

### Teams
Teams is the newest Microsoft product that everybody hates but the university wants us to use because they paid for it.
We elected to use it for a [conference](https://conference.ippp.dur.ac.uk/event/937/) because it provides automated live closed captions, a must-have accessibility feature.

[Install](https://docs.microsoft.com/en-us/microsoftteams/get-clients#install-manually-from-the-command-line) a "preview" Linux client with
```shell
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
 
sudo sh -c 'echo -e "[teams]\nname=teams\nbaseurl=https://packages.microsoft.com/yumrepos/ms-teams\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/teams.repo'
 
sudo dnf check-update
sudo dnf install teams
```

This Teams app irritating sets itself as your default browser.
Change this back at `GNOME settings > Details > Default Applications > Web`.
It also comes on at startup by default, also annoyingly.
Fix this by unchecking `Teams settings > General > Application > Auto-start application` after logging in, or right-clicking the tray icon and choosing `Do not auto-start Teams` under `Settings` before logging in.

You can also get it from [Snap](https://snapcraft.io/teams)
```shell
sudo snap install teams
```
or from [`flathub`](https://flathub.org/apps/details/com.microsoft.Teams)
```shell
sudo flatpak install flathub com.microsoft.Teams
flatpak run com.microsoft.Teams
```

> {{< cal 2021-09-28 >}}
>
> The two SPDU options offer the same version (v1.4.00.13653).

We are second-rate citizens of Teams on Linux.
The Linux client is missing many features compared to the Windows client, the most annoying of which is that you can only display four video feeds at once on screen.

Teams also has a [web app](https://teams.microsoft.com).

### Zoom
Zoom seems to be the canonical choice for conferences and teaching in 2020.

[Install](https://support.zoom.us/hc/en-us/articles/204206269-Installing-Zoom-on-Linux#h_b6ce9fba-dd38-4448-80c0-ac2e58db3acc) it manually with
```shell
sudo dnf install https://zoom.us/client/latest/zoom_x86_64.rpm
```

or use the [snap](https://snapcraft.io/zoom-client)
```shell
sudo snap install zoom-client
```
or get it from [`flathub`](https://flathub.org/apps/details/us.zoom.Zoom)
```shell
sudo flatpak install flathub us.zoom.Zoom
flatpak run us.zoom.Zoom
```

> {{< cal 2021-09-28 >}}
>
> The Snap (v5.8.0.16) is more up to date than the Flatpak (v5.7.31792.0820).

> {{< cal 2021-11-23 >}}
> 
> The desktop clients have been super buggy for me this year on Fedora.
> Most recently, I've found that I can't share my screen on version 5.8.4.210, apparently because the Zoom team are using some custom API that doesn't play nice with modern GNOME (41.1 on F35).
> My solution is just to use it from the browser at
> ```
> https://<work domain>.zoom.us
> ```

Closed captions can be provided by third-party services, although I found our university doesn't allow the use of Zoom plugins for security reasons.
Advanced settings have to be configured on the website, not the app, for some reason.

Free accounts have a time limit of one hour on meetings.

### Signal

[Signal](https://signal.org/) {{< wiki "Signal_(software)" >}} {{< ghubn "signalapp" >}} is an instant messaging service, but it does offer one-to-one and group video calls.

It's cross-platform, although identity is tied to your phone number.
It's encrypted and privacy-focussed.
It's open source, unlike the previous entries, but it is centralised.

Install with
```shell
sudo snap install signal-desktop
```

> {{< cal "2021-11-23" >}}
>
> I found that it was extremely slow to start and apparently loading more messages than I have ever send in Signal.
> I reinstalled and that seemed to fix it.

### Element (bonus)

Element, frontend to the Matrix network, is not video conferencing software, but a full communication platform.
It's libre software, and it's different because it's the first item on this page that's not a walled garden!
I wrote about it [here]({{< ref "element" >}}).

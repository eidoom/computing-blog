+++
title = "Starting to learn Rust with Advent of Code 2018"
date = 2020-07-08T16:52:26+01:00
edit = 2021-03-11
categories = ["advent-of-code"]
tags = ["rust","game","linux","fedora","dnf","advent-of-code-2018","aoc","aoc18"]
description = "Trying out a new programming language: Rust (mainly with AoC18)"
toc = true
cover = "img/covers/76.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

It seems [Rust](https://en.wikipedia.org/wiki/Rust_(programming_language)) has been coming up on my radar a lot recently.
Being the ["most loved language" in the Stack Overflow Developer Surveys](https://insights.stackoverflow.com/survey/2019#technology-_-most-loved-dreaded-and-wanted-languages) for four years is quite the endorsement, and I like the focus on safety in a performant language.
After seeing one too many seg faults in C++, I decided to take the plunge and check it out.

Installing everything I needed on Fedora was handled by the package manager:
```shell
sudo dnf install rust cargo
```

I started by reading through [this introduction](https://stevedonovan.github.io/rust-gentle-intro/) I found, which has served as a good point of reference so far.
Armed with [the book](https://doc.rust-lang.org/book/) and [the examples](https://doc.rust-lang.org/stable/rust-by-example/), I briefly worked on [a text-based adventure game](https://gitlab.com/eidoom/trying-rust) to learn by doing.
I learnt that strings are complex beasts.

I properly got into it by starting to work through [Advent of Code 2018](https://adventofcode.com/2018) ([recall]({{< ref "advent" >}})) in Rust [here](https://gitlab.com/eidoom/advent-of-code-2018).
The directory for each day contains some notes about what I learnt.

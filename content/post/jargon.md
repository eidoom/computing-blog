+++
title = "Technobabble"
date = 2021-03-29T17:15:35+01:00
categories = ["cheatsheet"]
tags = ["polymorphism","imperative","object-oriented","declarative","functional","overloading","dispatching"]
description = "Translating coding jargon"
toc = true
force_toc = true
draft = false
cover = "img/covers/57.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Paradigms
{{< wiki Programming_paradigm >}}
Classifications of programming languages and styles by features.

### Structured
{{< wiki Structured_programming >}}
Using [block structure]({{< ref "#block" >}}), [control flow constructs]({{< ref "#control-flow" >}}), and [functions]({{< ref "#function" >}}).

### Imperative
{{< wiki "Imperative programming" >}}
Telling the computer *how* to do what you want it to do.

#### Procedural
{{< wiki Procedural_programming >}}
Programming with {{< anc functions function >}}.

#### Object-oriented
{{< wiki Object-oriented_programming >}}
Programming with objects, which contain data and {{< anc functions function >}}.

### Declarative
{{< wiki "Declarative programming" >}}
Telling the computer *what* you want it to do.

#### Functional
{{< wiki "functional programming" >}}
Coding by applying and composing {{< anc functions function >}} that map values to other values.

##### Pure functional
{{< wiki "Purely_functional_programming" >}}
Functions are [pure]({{< ref "#pure" >}}) and data is [immutable]({{< ref "#mutability" >}}).

### Data-driven
{{< wiki "Data-driven_programming" >}}
Coding by matching and processing data.

## Components

The constructs and collections of constructs of a program.

### Block
{{< wiki "Block_(programming)" >}}
A group of code lines, usually {{< anc scoped scope >}}.

### Control flow
{{< wiki Control_flow >}}
Controlling the order of execution.
* Loops (`for`,`while`)
* Conditionals (`if`,`else`) {{< wiki "Conditional_(computer_programming)" >}}

### Function
{{< wiki subroutine >}}
A subset of the code packaged as a unit.

{{< ste "https://softwareengineering.stackexchange.com/q/20909" >}}
{{< sto "https://stackoverflow.com/q/6048344" >}}
This idea comes with many names:
* Function (often has output)
* Subprogram
* Subroutine (often modifies input and doesn't output)
* Procedure (as subroutine)
* Method (a member function of an object) {{< wiki "Method_(computer_programming)" >}}

#### Pure
The {{< anc function >}} depends only on its inputs.

## Concepts

### Scope
{{< wiki "Scope_(computer_science)" >}}
The subset of the program in which a name exists.
* Lexical scope: a part of the source code
* Dynamic scope: the program state at run time (execution context)

### Mutability
{{< wiki Immutable_object >}}
Whether an object can change or not.

Examples:
Mutability|C++|Rust|Python
---|---|---|---
Immutable|`const`|default|`str`,`tuple`,`bytes`
Mutable|default|`mut`|`list`,`bytearray`

## Nomenclature

### Argument vs parameter
{{< wiki "Parameter_(computer_programming)" >}}
{{< sto "https://stackoverflow.com/a/1788926" >}}
{{< sto "https://stackoverflow.com/q/156767" >}}
{{< sto "https://stackoverflow.com/q/427653" >}}
#### Argument
The data which you call the {{< anc function >}} with.
#### Parameter
The variable which you declare the {{< anc function >}} interface with.
#### Example
```python
def fn(parameter):
    return parameter + 1

argument = 1
fn(argument)
```

### Dynamic vs static
* dynamic = runtime
* static = compile time

### Iteration vs recursion
* Iteration: when a [block]({{< ref "#block" >}}) is repeated {{< wiki iteration >}}
* Recursion: when a [function]({{< ref "#function" >}}) calls itself {{< wiki "recursion (computer science)" >}}

### Inheritance chain
In {{< anc object-oriented >}} programming, there are various prefixes for `class` which denote an inheritance relationship:
* `super` > `sub`
* `base` > `derived`
* `parent` > `child`

## Type system

A {{< wl "type system" >}} assigns a property called a {{< wl type "Type (computer science)" >}} to each construct and defines logical rules for how they interact.

### Classifications

In the following, the order is from {{< wl "strong to weak typing" "Strong and weak typing" >}}.

#### Type checking
* {{< wl "Static" "Type_system#Static_type_checking" >}}:
    * types are checked at **compile time**
    * e.g. C++
* {{< wl "Dynamic" "Type_system#Dynamic_type_checking_and_runtime_type_information" >}}:
    * types are checked at **runtime**
    * e.g. Python

#### Type compatibility and equivalence
* {{< wl Nominal "Nominal_type_system" >}} (nominative, name-based): 
    * determined by explicit declaration and/or type name
    * e.g. C++ variables, functions, classes
* {{< wl Structural "Structural_type_system" >}} (property-based): 
    * determined by type's definition
    * statically type checked---concerns whole structure of type
    * e.g. C++ template system (on type arguments of template functions and classes)
* {{< wl "Duck typing" >}}
    * determined by the presence of certain methods and properties
    * dynamically type checked---concerns only the part of the type's structure that is accessed at runtime
    * e.g. Python

#### Type declaration
* {{< wl Manifest "Manifest_typing" >}}:
    * the type of the variable is explicitly stated on declaration
    * e.g. in C++, `int a;`
* {{< wl Inferred "Type_inference" >}}:
    * the type of the variable is automatically detected
    * e.g. in C++ `auto a{1};`

### Polymorphism 
{{< wiki "Polymorphism_(computer_science)" >}}
One interface accepts different {{< anc arguments argument >}} to do different things.

#### Overloading
{{< wiki "Ad_hoc_polymorphism" >}}
Different implementations are bound to the same {{< anc function >}} name and distinguished by different {{< anc parameters parameter >}}.
Each implementation is explicitly defined.

#### Parametric polymorphism/generic programming
{{< wiki "Parametric_polymorphism" >}} {{< wiki "Generic_programming" >}}
A type-generic implementation is provided for a {{< anc function >}} by templating one or more components.

Examples:
* C++ [templates](https://en.cppreference.com/w/cpp/language/templates)
* Rust [generics](https://doc.rust-lang.org/book/ch10-01-syntax.html)

#### Subtyping
{{< wiki "subtyping" >}}
When a **subtype** is related to a **supertype** by {{< wl substitutability "Liskov substitution principle" >}}. 

Often, {{< anc function >}} {{< anc parameters parameter >}} with the type of the supertype being called with a subtype as the {{< anc argument >}}.
For example, in C++, calling a class by the type of one of its bases.

* {{< wl "Virtual function" >}}
    * an inheritable and overridable method which is dynamically dispatched
* {{< wl "Dynamic dispatch" >}}
    * choosing the implementation of a {{< anc polymorphic polymorphism >}} {{< anc function >}} at run time
* {{< wl "Single dispatch" "Dynamic_dispatch#Single_and_multiple_dispatch" >}}
    * {{< anc subtyping >}} on a single argument
* {{< wl "Multiple dispatch" >}}
    * {{< anc subtyping >}} on a combination of more than one {{< anc argument >}}

## Bits and bytes

The {{< wl bit >}} (binary digit) is the basic unit of information.
I abbreviate it as b.

There are 8 bits in a {{< wl byte >}}, abbreviated as B.
A byte can be represented in various bases.

Denary|Binary|Hexadecimal
---|---|---
255|0b11111111|0xff

> There's also a 4-bit {{< wl nibble >}}, if you're not that hungry. It can be represented by a single hexadecimal digit.
> 
> Alternatively, for those with an appetite, a {{< wl chomp "hextet" >}} is 16 bits. It is commonly represented by 4 hexadecimal digits and used for each of the 8 blocks of an IPv6 address (128 bits).

When counting bytes, the unit prefixes can be {{< wl denary >}} (decimal, base-10, SI) or {{< wl binary binary_prefix >}} (base-2), although they're not consistently labelled in the wild, so watch out!

### Denary

Number of bytes|{{< wl Abbreviation Metric_prefix >}}|Name
---|---|---
1000|kB|{{< wl kilobyte >}}
1000^2|MB|{{< wl megabyte >}}
1000^3|GB|{{< wl gigabyte >}}
1000^4|TB|terabyte

### Binary

Number of bytes|{{< wl Abbreviation Binary_prefix >}}|Name
---|---|---
1024|KiB|kibibyte
1024^2|MiB|mebibyte
1024^3|GiB|gibibyte
1024^4|TiB|tebibyte

I may even use these prefixes for {{< wl pixels >}} sometimes too, in defiance of convention and sense.

## TODO
* floating point numbers (& fixed) {{< wiki Floating-point_arithmetic >}}
* compilers/VM bytecode/interpreters/AOT/JIT {{< wiki "Execution_(computing)" >}}
* drivers/firmware/kernel/user space/operating system
* evaluation {{< wiki "Evaluation strategy" >}}
* reflection, metaprogramming
* closures

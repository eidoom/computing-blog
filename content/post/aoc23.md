+++
title = "Advent of Code 2023 with Haskell"
date = 2023-12-21T13:59:08Z
categories = ["advent-of-code"]
tags = ["advent-of-code-2023","haskell","coding","programming","aoc","aoc23","functional","declarative","pure","purely-functional","statically-typed","static-typing","ghc","glasgow-haskell-compiler","cabal","type-inference","lazy-evaluation","pure-functions"]
description = "The purity"
toc = true
cover = "img/covers/hs.webp"
coveralt = "stable-diffusion-xl:'haskell',R-ESRGAN_x4+"
+++

It's that time of year again. I tried out {{< wl Haskell >}} at {{< gl aoc23 >}}.

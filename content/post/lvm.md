+++
title = "Logical Volume Management"
date = 2022-07-04T14:49:27+02:00
categories = ["operating-system"]
tags = ["rocky-linux","rocky-linux-8.6","rocky-linux-8","rocky-linux-green-obsidian","green-obsidian","lvm","gdisk","fstab","mkfs","server"]
description = "Using LVM to manage logical volumes"
toc = true
draft = false
cover = "img/covers/15.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

This concerns some high performance servers.

In the wake of Red Hat killing [CentOS](https://www.centos.org/) {{< wiki "CentOS" >}} {{< pl "https://distrowatch.com/table.php?distribution=centos" >}}, I chose [Rocky Linux](https://rockylinux.org/) {{< wiki "Rocky_Linux" >}} {{< glsh "https://git.rockylinux.org/" >}} {{< pl "https://distrowatch.com/table.php?distribution=rocky" >}} for the operating system.
[AlmaLinux](http://almalinux.org/) {{< wiki AlmaLinux >}} {{< ghub "https://github.com/AlmaLinux" >}} {{< pl "https://distrowatch.com/table.php?distribution=alma" >}} would have been another choice.

I performed the following as `root`.

## Set up several disks as a single logical volume

Each server has four fresh 2 TB data drives.
I set up each to have an 8 TB logical volume using [Logical Volume Manager (LVM)](https://sourceware.org/lvm2/) {{< wiki "Logical_Volume_Manager_(Linux)" >}} {{< git "https://sourceware.org/git/?p=lvm2.git" >}} {{< arch "LVM" >}} ([recall]({{< ref "server-install#configuring-logical-volumes" >}})) with XFS {{< wiki XFS >}} {{< arch "XFS" >}} as the file system.
This provides the scratch drive.

### Partition table and partitions

Use `lsblk` to check the state of the disk partitions.

I used `gdisk` {{< arch "GPT_fdisk" >}} ([recall]({{< ref "openwrt-services#partition" >}})) to [set up](https://wiki.archlinux.org/title/GPT_fdisk#Create_a_partition_table_and_partitions) each disk as a single partition.

Enter the utility for disk `/dev/sd<x>` with
```shell
gdisk /dev/sd<x>
```
then accept the default options for:

* `o` to write a new GUID partition table (GPT)
* `n` to write a new partition filling the entire available space
* `p` to check the configuration
* `w` to write the changes to the disk

### Logical Volume Manager

#### Physical volumes

Mark each partition as a physical volume (PV) {{< arch "LVM#Physical_volumes" >}}:
```shell
pvcreate /dev/sd<x>1
```
Show created PVs with
```shell
pvs
```

#### Volume group

Create a volume group (VG) {{< arch "LVM#Volume_groups" >}} over the PVs:
```shell
vgcreate <volume group name> /dev/sda1 /dev/sdb1 /dev/sdc1 /dev/sdd1
```
Show created VG with
```shell
vgs
```

#### Logical volume

Create a logical volume (LV) {{< arch "LVM#Logical_volumes" >}} using all available capacity:
```shell
lvcreate -l +100%FREE <volume group name> -n <logical volume name>
```
Show created LV with
```shell
lvs
```
> If you make an error in the name, rename a LV with
> ```shell
> lvrename <volume group name> <old logical volume name> <new logical volume name>

### Format

I [formatted](https://wiki.archlinux.org/title/File_systems#Create_a_file_system) the LVs with XFS:
```shell
mkfs.xfs /dev/<volume group name>/<logical volume name>
```

### Mount

I made the directory
```shell
mkdir <mount point>
```
and added an entry to `/etc/fstab` {{< arch "Fstab" >}} to mount the new scratch disk on boot:
```vim
/dev/<volume group name>/<logical volume name>  <mount point>   xfs defaults    0   0
```
and mounted immediately
```shell
mount -a
```

## Extend a logical volume onto another disk

We will extend a LV `<new_lv>` which is part of a VG `<new_vg>`.
The VG `<new_vg>` is currently entirely on `/dev/sdX`, but we will extend it onto another disk `/dev/sdY` which contains old partitions.

I removed an existing VG `<old_vg>` from a disk `/dev/sdY`
```shell
vgremove <old_vg>
```
> You can also delete a single LV `<old_lv>` on `<old_vg>` with
> ```shell
> lvremove <old_vg>/<old_lv>
> ```
I started
```shell
gdisk /dev/sdY
```
and

* checked the partitions with `p`
* deleted the partition that `<old_vg>` used to be on and other unnecessary partitions with `d`
* made a new partition `/dev/sdY2` with `n`, accepting the default options to make it fill all available space
* wrote changes to disk with `w`

I told the kernel to use the new partition table with
```shell
partprobe
```
I set `/dev/sdY2` as a PV with
```shell
pvcreate /dev/sdY2
```
I extended the VG `<new_vg>` over the new PV
```shell
vgextend <new_vg> /dev/sdY2
```
I extended the LV `<new_lv>` over all available space in the VG `<new_vg>`
```shell
lvresize -l +100%FREE --resizefs <new_vg>/<new_lv>
```
I checked new space with
```shell
df -h
```

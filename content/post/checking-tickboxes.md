+++
title = "Automating unchecking tickboxes"
date = 2020-07-06T12:23:42+01:00
categories = ["web"]
tags = ["javascript","browser","firefox","automate","linux","laptop","desktop","browser-dev-tools"]
description = "Using browser developer tools to uncheck all tickboxes on a webpage"
toc = true
cover = "img/covers/78.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

After one unnecessary email too many, I recently tried to unsubscribe from all email communication from a website.
The only way the site offered for users to do this was to manually untick 107 checkboxes.
Pure filth.
Luckily, it's [quite easy](https://stackoverflow.com/a/14245862) to automate this process.

To uncheck all tickboxes on a webpage with Firefox, open the [Firefox Developer Tools](https://developer.mozilla.org/en-US/docs/Tools) with `F12` and click the [Console tab](https://developer.mozilla.org/en-US/docs/Tools/Web_Console).
Type `allow pasting` and return to enable pasting, then paste the following Javascript snippet:
```javascript
(function() {
    const boxes = document.querySelectorAll("input[type=checkbox]");
    console.log(boxes.length)
    for (let i = 0; i < boxes.length; i++){
        boxes[i].checked = false;
    }
})()
```
Et voilà, the ticks are gone and we have the satisfaction of seeing the size of the clickfest we just avoided.

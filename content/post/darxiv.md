+++
title = "Daily arXiv"
date = 2022-10-22T15:57:30+02:00
categories = ["web"]
tags = ["project","js","javascript","html","css","arxiv","api","svelte","frontend","rollup","json","npm","website","webpage","static-website","svelte.js","rollup.js","webpage"]
description = "Yet another arXiv frontend"
toc = true
draft = false
cover = "img/covers/108.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I made [a simple interface for the arXiv](https://eidoom.gitlab.io/darxiv) to check daily submissions.
Source at {{< gl darxiv >}}, named in the tradition of [myrxiv]({{< ref "myrxiv" >}}).

+++
title = "Pandoc citeproc with reference links"
date = 2020-12-05T09:58:06Z
edit = 2021-06-18
categories = ["typesetting"]
tags = ["academia","pandoc","latex","pdf","linux","documents","markdown","markup"]
description = "Including external links in the bibliography"
toc = false
cover = "img/covers/65.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I recently had to produce a [PDF document](https://eidoom.gitlab.io/statement-during-phd-md/statement.pdf) {{< glab "statement-during-phd-md" >}} with a bibliography.
I decided to use `pandoc`, like [before]({{< ref "pandoc-physics" >}}) but simpler as only producing a PDF.
I also derived a [project template](https://gitlab.com/eidoom/pandoc-pdf-template) from it.

I found customising the bibliography quite easy with `pandoc-citeproc`.
I installed it [here]({{< ref "pandoc-physics#locally" >}}) and it's enabled in the [YAML configuration](https://gitlab.com/eidoom/statement-during-phd-md/-/blob/master/statement.yml) (which is passed as the argument of `--defaults` to `pandoc`) with
```yaml
citeproc: true
```

You find your [CSL](https://citationstyles.org/) {{< wiki "Citation_Style_Language" >}} style [here](https://www.zotero.org/styles) and [pass it to `pandoc`](https://gitlab.com/eidoom/statement-during-phd-md/-/blob/master/Makefile).

> If you want to edit one, you can find the style sources at {{< gh citation-style-language styles >}}.
> The syntax of the XML-like file is described in the [specification](https://github.com/citation-style-language/documentation/blob/master/specification.rst).

You populate a [`.bib`](https://gitlab.com/eidoom/statement-during-phd-md/-/blob/master/statement.bib) and feed that to `pandoc` too.
In the [body](https://gitlab.com/eidoom/statement-during-phd-md/-/blob/master/statement.md), cite with `[@...]` or  `[@...; @...]` for multiple.
Adding
```yaml
metadata:
  reference-section-title: References
  link-citations: true
```
to the YAML configuration enables internal hyperlinks from citations in the body to their bibliography entry and gives the bibliography a title, and
```yaml
variables:
  colorlinks: true
```
adds a splash of colour to distinguish those links.

However, [CSL doesn't allow you to add links to things](https://github.com/citation-style-language/documentation/blob/master/specification.rst#appendix-vi-links), and I wanted the title of reference papers to link to their [INSPIRE-HEP](https://en.wikipedia.org/wiki/INSPIRE-HEP) entry, which I recorded in the `url` field of the `.bib` entries.
The best solution I could come up with was to replace the `title` with an appropriate `\href`.
I used some `regex` with `perl` to [script this](https://gitlab.com/eidoom/statement-during-phd-md/-/blob/master/linkify.sh):
```bash
#!/usr/bin/env bash

perl -0777p -e "
    1 while s|(title = \")([^\\\][^h][^r][^e][^f].*?)(\".*?url = \")(.*?)(\")|\1\\\href\{\4\}\{\2\}\3\4\5|gs; 
    " \
    $1 >$2
```
`-0777p` allows matches over multiple lines, the `s` suffix inclues `\n` in `.`, and the `1 while` is a neat trick to repeatedly apply the regex replacement until there are no further matches are made.

> {{< cal "17 Jun 2021" >}}
>
> While working on [this report](https://eidoom.gitlab.io/phd-completion-statement/document.pdf) {{< glab phd-completion-statement >}}, I improved this script to automatically extract a URL from the arXiv preprint information rather than having to manually add a `url` field to each entry.
> The title then becomes a link to the arxiv URL.
> I've updated the template project accordingly.
> The new `perl` is
> ```perl
> 1 while s|(title = \")((?!\\\href).*?)(\".*?eprint = \")(.*?)\"|\1\\\href\{https://arxiv.org/abs/\4\}\{\2\}\3\4\"|gs;
> ```

Honestly, this point about links in the bibliography is a huge hassle.
If I wanted a style which had the journal information hyperlinked to the DOI address (as in the JHEP style), I don't know how I'd do that.
One could compile to latex [using](https://martinhjelm.github.io/2018/05/30/Removing-Hypertarget-For-Pandoc-Markdown-to-Latex/) a [`--defaults`](https://pandoc.org/MANUAL.html#default-files) `yaml` configuration like
```yaml
from: markdown-auto_identifiers
to: latex
standalone: true
citeproc: true
cite-method: biblatex
```
and then `latexmk -pdf` to build the `pdf` with `pdflatex` and `biblatex`.
Or, one could just use plain LaTeX to write `pdf` documents that involve hyperlinked text in the bibliography.
I'm inclined to go with the latter.

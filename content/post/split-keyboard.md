+++
title = "The Iris: a split mechanical keyboard"
date = 2020-06-13T13:05:10+01:00
edit = 2020-10-10
categories = ["keyboard"]
tags = ["hardware","keyboard","split-keyboard","iris","mechanical-keyboard","linux","qmk","keycool","obins","anne-pro","magicforce","qisan","gateron","cherry-mx","keycaps","keyboard-switches","kailh","ergonomic","firmware","c","flash","make","atmega32u4","mcu","ws2812b","open-source","gpl","gplv2"]
description = "What's better than a single mechanical keyboard?"
toc = true
cover = "img/covers/80.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Context 

### Mechanical keyboards

I was converted to using [mechanical keyboards](https://www.reddit.com/r/MechanicalKeyboards/wiki/index) over five years ago now.

{{< figure alt="keycool" src="../../img/keeb/old/keycool.jpg" caption="Keycool 108">}}

My first was a Keycool keyboard (108 keys, [ANSI](https://deskthority.net/wiki/ANSI_vs_ISO) layout) with plate-mounted [Cherry MX Brown switches](https://deskthority.net/wiki/Cherry_MX_Brown) and white LED keycap backlights that I got in Febuary, 2015.
I put a set of Vortex backlit [double-shot](https://deskthority.net/wiki/Double-shot_moulding) [PBT](https://deskthority.net/wiki/Keycap_material#PBT) keycaps on it.
It is a solid keyboard.
I use this keyboard with my main desktop at home.

{{< figure alt="magicforce" src="../../img/keeb/old/magicforce.jpg" caption="MagicForce 68">}}

I next got a Qisan MagicForce keyboard (68 keys, ANSI layout) with plate-mounted [Gateron KS-8 Brown switches](https://deskthority.net/wiki/Gateron_KS-8_series) and white LED keycap backlights in January, 2017.
I substituted the backlit double-shot [ABS](https://deskthority.net/wiki/Keycap_material#ABS) keycaps it came with for the backlit [laser engraved](https://deskthority.net/wiki/Keycap_printing#Ablation) [POM](https://deskthority.net/wiki/Keycap_material#POM) keycaps that originally came with the Keycool.
The manufacturing process is described [here](https://blog.wooting.nl/what-are-the-best-mechanical-keyboard-keycaps/).
It has an aesthetic and stiff aluminium backplate and is an excellent budget 60% mechanical keyboard.
I used this one at my desk at work before I got the split keyboard.
Now it services my secondary desktop.

{{< figure alt="anne" src="../../img/keeb/old/anne.jpg" caption="Anne Pro (yes, I did accidentally swap those two keys)">}}

I also got an Obins Anne Pro keyboard (61 keys, ANSI layout) with plate-mounted [Gateron KS-9 Brown switches](https://deskthority.net/wiki/Gateron_KS-9_series) and [RGB keycap backlights]({{< ref "#fun-with-lights" >}}) in March, 2017.
I left the manufacturer keycaps on this time, which are backlit double-shot PBT keycaps.
This is a wireless keyboard, using bluetooth to interface with a host.
It was the best budget wireless mechanical keyboard I could find.
I originally used it with my [Surface Pro 4 tablet]({{< ref "surface-linux" >}}).
I now exclusively use the Surface for pen work, so keyboard use is not required.
I instead use the keyboard for general portability.

For details of [switches]({{< ref "#non-split-keyboards-switches" >}}), and keycap [sets]({{< ref "#keycap-sets" >}}) and their [profiles]({{< ref "#keycap-profiles" >}}) and [materials]({{< ref "#keycap-materials" >}}), see the [appendices]({{< ref "#appendices" >}}).

### Ergonomics

I have pursued a healthier, more ergonomic work space by using a standing desk in my office.
I find typing from standing very natural, although comfort is highly sensitive to the height of the keyboard.
The backs of my hands become strained at the end of the day if they're held too high or too low.
This is easily avoided, of course.

However, I still find the close-handed posture required to type on a conventional keyboard caused tightness in my upper back and shoulders over long working time periods.
I came across the solution in the form of split mechanical keyboards, with which I can position my hands at exactly the separation distance and angle I find most confortable.

## Obtaining a split mechanical keyboard

### Options

The market for split mechanical keyboards, despite being extremely niche, is nonetheless [impressively diverse](https://github.com/diimdeep/awesome-split-keyboards).
This is one example of the great success of open-source software and hardware, fostering technological progress and seeding a creative and thriving community.
I realised that keyboards using rubber-dome switches and being a single unit were not the only design aspects which I had previously taken for granted despite them perhaps not being such great ideas.
The salient points were:
* Conventional keyboards waste our strongest and most versatile finger on a single shared key: the space bar. 
I found new designs which included a **thumb cluster** to put thumbs to the better usage they deserve.
* Conventional keyboards have the keys laid out in a row-staggered configuration. 
This is an [historical artefact from the typewriter](https://deskthority.net/wiki/Staggering#Classic_typewriter_keyboard) (put simply, to stop the typebars from different rows on the keyboard from getting in each other's way) that has been [carried over](https://en.wikipedia.org/wiki/Path_dependence) to keyboards.
That's great, but if I flex my fingers, they don't naturally follow the positions of these row-staggered keys.
In fact, my fingers don't even naturally fall in the straight line of a conventional keyboard's home row when I relax them.
The fairly obvious solution is a [**column-staggered layout**](https://deskthority.net/wiki/Staggering#Columnar_layout), which plenty of the options employed.
* Of course, speaking of path dependency in keyboard design leads to the elephant in the room that is the QWERTY layout.
Hopefully I'll pluck up the courage to switch to a more **efficient key layout** like [Colemak](https://en.wikipedia.org/wiki/Colemak) or [Dvorak](https://en.wikipedia.org/wiki/Dvorak_keyboard_layout) in the near future.
* I had already picked up a taste for **minimal layouts** from my 60% keyboards, that is, only having as many keys as you need and no more.
This is apparently a well-shared sentiment as most designs I found catered for this.
Some even go beyond something comparable to a 60% layout by additionally removing the number row, although I'm not yet brave enough to go that far.
* Lastly, but not least: once we're playing around with what a keyboard looks like to this extend, it's very useful to be able to prototype and alter layouts.
Many choices are personal rather than universal.
This level of flexibiliy is easily accomplishable since most designs are **fully programmable**.

### Choosing a design

From this, I was able to distill what I wanted into a short checklist:
* thumb cluster
* column-staggered layout
* number row (but otherwise, the smaller the better)
* well documented, easily sourceable parts
* keyboard not to be wireless - since it'll be in stationary use at a desk anyway, I'd rather not worry about batteries and wireless connectivity issues

<!-- I also knew I wanted the following, although these are mostly design agnostic -->
<!-- * tactile switches -->
<!-- * key caps not to be blank, since I can't touch type - as I'm sure you inferred from the backlights on my old keyboards -->

The top designs to match these criteria in my mind were, in decreasing order of preference:
* [Redox](https://github.com/mattdibi/redox-keyboard)
* [Iris](https://docs.keeb.io/iris-rev3-build-guide/)
* [ErgoDash](https://github.com/omkbd/ErgoDash)

Although some split keyboards are available to buy ready-made, they are prohibitively expensive.
In any case, I eagerly anticipated sourcing the parts then soldering and assembling them into my new keyboard.
Pretty much the only option in Europe for buying specialised parts, like custom printed circuit boards ([PCBs](https://en.wikipedia.org/wiki/Printed_circuit_board)), was [Falbatech](https://falba.tech/).
I really liked what they offered, particularly the use of bamboo for the keyboard chassis, but even when buying components I was dissuaed by the price.
I looked to the secondhand market instead to not break the bank.

### Purchasing a keyboard

{{< figure alt="iris-perspective" src="../../img/keeb/iris/iris-perspective.jpg" caption="Iris keyboard (1)">}}

I discovered the subreddit [r/mechmarket](https://www.reddit.com/r/mechmarket/wiki/index), a marketplace for all things mechanical keyboard.
I found an Iris revision 3 [for sale](https://www.reddit.com/r/mechmarket/comments/e8d153/euit_h_iris_v3_w_paypal/) in Europe --- or rather, they found me after I submitted a [W]anted post -- which I bought.
I guess the experience is mainly dependent on who you end up trading with, but I found using r/mechmarket great and would recommend it.

## Playing with the Iris

### A new keyboard

Amusingly, this Iris was the most expensive keyboard (excluding musical ones) I had bought so far and it had the least keys: only 56.

{{< figure alt="iris-switches-both" src="../../img/keeb/iris/iris-switches-both.jpg" caption="Kailh Browns mounted on the Iris">}}

It came with plate-mounted [Kailh PG1511 Brown switches](https://deskthority.net/wiki/Kailh_PG1511_series), which suited me well.
My preference is for tactical switches and though I would perhaps prefer the heavier Clears, the price difference is greater than my curiosity.

{{< figure alt="iris-keys" src="../../img/keeb/iris/iris-keys.jpg" caption="Subset of the Big Bang keycap set for the Iris">}}

It also came with [MelGeek Big Bang Ortholinear MDA-profile PBT keycaps](https://www.melgeek.com/collections/keycaps-1/products/melgeek-mda-big-bang-ortholinear-keycap-set), which I was also happy with.
It is a quality set.
The MDA profile seems to suit the column-staggered layout and I enjoy the feeling of the spherical indentation of the key tops, rather than the cylindrical indentation of [my other keycap sets]({{< ref "#keycap-profiles" >}}).

When the keyboard arrived I [completely disassembled it]({{< ref "#iris-disassembly-in-pictures" >}}).
The PCBs come with on-board [ATmega32u4](https://www.microchip.com/wwwproducts/en/atmega32u4) microcontroller units ([MCUs](https://en.wikipedia.org/wiki/Microcontroller)) and electronic components from [the manufacturer](https://keeb.io/products/iris-keyboard-split-ergonomic-keyboard). 
When they're new, it only remains for the switches to be soldered on.
I resoldered them all just to be on the safe side.
The two sides are joined by a 3.5mm [TRRS cable](https://en.wikipedia.org/wiki/Phone_connector_(audio)).
The PCBs both have USB C ports, either of which can be used to connect the keyboard to a computer.
The PCBs also have [WS2812B](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) underglow LEDs.

{{< figure alt="pcb+middle" src="../../img/keeb/iris/pcb+middle.jpg" caption="Iris with bottom plate removed and PCB exposed">}}

### Warning

One thing I learnt about this keyboard is that it really doesn't like if you attach the TRRS cable after powering the PCBs by plugging in the USB-C cable.
I recommend always attaching the TRRS cable _before_ connecting the keyboard to a computer---[it is not hotswapable!](https://docs.qmk.fm/#/feature_split_keyboard?id=considerations).
The two sides are, however, content to work independently as well as connected together.

### Firmware

Firmware for the MCUs is [part](https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/iris/keymaps) of [QMK Firmware](https://qmk.fm/), a highly-polished open-source keyboard firmware project.
It is also easily customisable to create personal keyboard layouts.
The project also provides a useful [keymap tester](https://config.qmk.fm/#/test).

### Custom keymap

{{< figure alt="iris-top" src="../../img/keeb/iris/iris-top.jpg" caption="Iris keyboard (2)">}}

Using QMK, I have been tweaking [my personal layout for the Iris](https://gitlab.com/eidoom/iris-keymap) over the last four months.
The keymap is defined in a file `keymap.c`, using [key codes](https://docs.qmk.fm/#/keycodes) to designate keys.
It has [layers](https://beta.docs.qmk.fm/using-qmk/software-features/feature_layers):
```c
enum layers {
  _QWERTY,
  _LOWER,
  _RAISE,
  _ADJUST,
};
```
which are accessible via modifier keys thanks to some [QMK functions](https://beta.docs.qmk.fm/using-qmk/software-features/feature_layers#switching-and-toggling-layers-id-switching-and-toggling-layers):
```c
#define RAISE MO(_RAISE)
#define LOWER MO(_LOWER)
#define ADJUST MO(_ADJUST)
```
The keymap is current configured as:
```c
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  [_QWERTY] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
     KC_LGUI, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                               KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_GRV,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                               KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_BSLS,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     KC_ESC,  KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                               KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_DEL,           KC_BSPC, KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,
  //└────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
                                    KC_LALT, LOWER,   KC_ENT,                    KC_SPC,  RAISE,   KC_RCTL
                                // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
  ),

  [_LOWER] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
     _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     _______, KC_EQL,  KC_PLUS, KC_MINS, KC_UNDS, _______,                            _______, KC_LEFT, KC_DOWN, KC_UP,   KC_RGHT, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     _______, _______, _______, _______, _______, _______, _______,          _______, _______, KC_HOME, KC_PGDN, KC_PGUP, KC_END,  _______,
  //└────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
                                    _______, _______, _______,                   _______, _______, _______
                                // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
  ),

  [_RAISE] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
     KC_F12,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,                              KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     _______, KC_LABK, KC_LCBR, KC_LBRC, KC_LPRN, _______,                            _______, KC_RPRN, KC_RBRC, KC_RCBR, KC_RABK, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     _______, _______, _______, _______, _______, _______, _______,          _______, _______, _______, _______, _______, _______, _______,
  //└────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
                                    _______, _______, _______,                   _______, _______, _______
                                // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
  ),

  [_ADJUST] = LAYOUT(
  //┌────────┬────────┬────────┬────────┬────────┬────────┐                          ┌────────┬────────┬────────┬────────┬────────┬────────┐
     _______, _______, _______, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     RESET,   _______, KC_VOLU, _______, _______, _______,                            _______, _______, _______, _______, _______, _______,
  //├────────┼────────┼────────┼────────┼────────┼────────┤                          ├────────┼────────┼────────┼────────┼────────┼────────┤
     _______, KC_MPRV, KC_VOLD, KC_MPLY, KC_MNXT, KC_BRIU,                            _______, RGB_RMOD,RGB_HUI, RGB_SAI, RGB_VAI, RGB_TOG,
  //├────────┼────────┼────────┼────────┼────────┼────────┼────────┐        ┌────────┼────────┼────────┼────────┼────────┼────────┼────────┤
     _______, _______, KC_MUTE, _______, _______, KC_BRID, _______,          _______, _______, RGB_MOD, RGB_HUD, RGB_SAD, RGB_VAD, _______,
  //└────────┴────────┴────────┴───┬────┴───┬────┴───┬────┴───┬────┘        └───┬────┴───┬────┴───┬────┴───┬────┴────────┴────────┴────────┘
                                    _______, _______, _______,                   _______, _______, _______
                                // └────────┴────────┴────────┘                 └────────┴────────┴────────┘
  ),
};
```

The rationals behind this layout design are:
* to keep the QWERTY alphabet (for now). This is a bit of lazy backwards compatibility for my brain.
* to maximise balanced modifier usage: if I use one or more modifiers with the thumb and/or pinky of one hand, the following key in the sequence should be pressed by the other hand. Notice the left/right `Shift` key pair and the positioning of `Ctrl`, `Alt`, and `Super`.
* to support a [`tmux`]({{< ref "tmux" >}})+[`neovim`]({{< ref "neovim" >}})-based Linux [terminal]({{< ref "shell" >}}) development environment (within GNOME). Notice the telltale `vim`-friendly `Esc` position and navigation positioning.
* to maintain a symmetry between the two sides that is ergonomic: both hands should be equally used.
* to maintain a symmetry between the two sides that is intuitive: the opposite (sister? reflection? parity conjugation?) of a key should be related in some sense where possible. These pairs include:
    * layer-accessed bracket keys
    * layer modifier keys
    * `Space` and `Enter`
    * `Del` and `Backspace`
    * conventional modifier keys
* to intuitively position media keys

### Installing QMK

> I've noticed in the [documentation](https://docs.qmk.fm/#/) that the QMK project has introduced new simplified methods for installing, building and flashing since I started playing around with it.
> Now you can [install everything with a pip package](https://docs.qmk.fm/#/newbs_getting_started?id=linux), and [build](https://docs.qmk.fm/#/newbs_building_firmware?id=build-your-firmware) and [flash](https://docs.qmk.fm/#/newbs_flashing?id=flash-your-keyboard-from-the-command-line) through a `qmk` CLI program.
> I've not tested it, but it looks neat!

To [install](https://docs.qmk.fm/#/newbs_getting_started) the QMK software on Linux:
```shell
cd ~/git
git clone --recurse-submodules git@github.com:qmk/qmk_firmware.git
cd ~/git/qmk_firmware
./util/qmk_install.sh
```
Then grab my configuration:
```shell
cd ~/git/qmk_firmware/keyboards/keebio/iris/keymaps
git clone git@gitlab.com:eidoom/iris-keymap.git eidoom
```

Alternatively, to update after the initial installation:
```shell
cd ~/git/qmk_firmware
git pull --recurse-submodules -j8
./util/qmk_install.sh
cd ~/git/qmk_firmware/keyboards/keebio/iris/keymaps/eidoom
git pull
```

### Building the firmware

[Build](https://docs.qmk.fm/#/newbs_building_firmware) the firmware with:
```shell
cd ~/git/qmk_firmware
make keebio/iris/rev3:eidoom
```
Note that the default Iris rev. 3 configuration, [keebio/iris/rev3](https://github.com/qmk/qmk_firmware/tree/master/keyboards/keebio/iris/rev3), is used as the base of the firmware.

### Linux permissions for flashing

To flash the ATmega32u4 MCUs, we need elevated privileges.
Using `sudo` would give us those permissions, but would be in bad taste.
The correct solution is to [edit the `udev` rules](https://docs.qmk.fm/#/faq_build?id=linux-udev-rules).
Open up:
```shell
sudo vi /etc/udev/rules.d/50-atmel-dfu.rules
```
and write:
```vim
# Atmel ATMega32U4
SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2ff4", TAG+="uaccess", RUN{builtin}+="uaccess"
# Atmel USBKEY AT90USB1287
SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2ffb", TAG+="uaccess", RUN{builtin}+="uaccess"
# Atmel ATMega32U2
SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2ff0", TAG+="uaccess", RUN{builtin}+="uaccess"
```
then run:
```shell
sudo udevadm control --reload-rules
sudo udevadm trigger
```

### Flashing the MCUs

Now we can [flash](https://docs.qmk.fm/#/newbs_flashing) with:
```shell
cd ~/git/qmk_firmware
make keebio/iris/rev3:eidoom:flash
```
The software will start well:
```shell
QMK Firmware 0.9.10
Making keebio/iris/rev3 with keymap eidoom and target flash

avr-gcc (Fedora 9.2.0-3.fc32) 9.2.0
Copyright (C) 2019 Free Software Foundation, Inc.
This is free software; see the source for copying conditions.  There is NO
warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

Size before:
   text    data     bss     dec     hex filename
      0   26584       0   26584    67d8 .build/keebio_iris_rev3_eidoom.hex

Copying keebio_iris_rev3_eidoom.hex to qmk_firmware folder                                          [OK]
Checking file size of keebio_iris_rev3_eidoom.hex                                                   [OK]
 * The firmware size is fine - 26584/28672 (92%, 2088 bytes free)
```
then cry:
```shell
dfu-programmer: no device present.
ERROR: Bootloader not found. Trying again in 5s.
```
but this is fine: the MCU must be put in [`RESET` mode](https://docs.qmk.fm/#/newbs_flashing?id=put-your-keyboard-into-dfu-bootloader-mode) before it will accept the flash.
This operation mode is also known as device firmware update (DFU) mode and bootloader mode.
The [default keymap](https://github.com/qmk/qmk_firmware/blob/master/keyboards/keebio/iris/keymaps/default/keymap.c) has `RESET` mapped to what is on my keyboard `LEFT MOD`+`TAB` (`LOWER` layer).
My custom keymap uses `RIGHT MOD`+`LEFT MOD`+`TAB` (`ADJUST` layer).
Once `RESET` is activated, the flash should proceed:
```shell
Bootloader Version: 0x20 (32)
Erasing flash...  Success
Checking memory from 0x0 to 0x6FFF...  Empty.
Checking memory from 0x0 to 0x67FF...  Empty.
0%                            100%  Programming 0x6800 bytes...
[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>]  Success
0%                            100%  Reading 0x7000 bytes...
[>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>]  Success
Validating...  Success
0x6800 bytes written into 0x7000 bytes memory (92.86%).
```
Note that each PCB has an independent MCU, so both sides of the split keyboard must be flashed separately if you want to be able to use both USB C ports with your custom keymap.
I've found that the flash only works if the two PCBs are connected by the TRRS cable.

If you don't know the key combination for `RESET` on the keyboard's current configuration, as I did not when I first received the keyboard, or if you accidentally lock yourself out of the layer containing the `RESET` key as I have just [done](https://gitlab.com/eidoom/iris-keymap/-/commit/c4e071892ebeb5808ff287dc3f50f0e772dd4d4f), all is not lost.
The keyboards will have to be disassembled to access it, but there is a reset button on the PCBs.
It is the only push button on the board, so hard to miss.

## Appendices

### Iris disassembly in pictures

I catalogued a disassembly of the right-hand-side unit with photographs.

{{< figure alt="bottom-plate" src="../../img/keeb/iris/bottom-plate.jpg" caption="Black acrylic bottom plate" style="width: 50%; margin: auto auto;">}}
{{< figure alt="middle-layer" src="../../img/keeb/iris/middle-layer.jpg" caption="Clear acrylic middle layer" style="width: 50%; margin: auto auto;">}}
{{< figure alt="pcb" src="../../img/keeb/iris/pcb.jpg" caption="Iris PCB">}}
{{< figure alt="mcu" src="../../img/keeb/iris/mcu.jpg" caption="On-board ATmega32u4 MCU">}}
{{< figure alt="pcb-close" src="../../img/keeb/iris/pcb-close.jpg" caption="Close up of the PCB">}}
The other main components are clear here:
* USB C port at the top
* TRRS port on the right
* `RESET` button in upper middle
* Four WS2812B [SMD](https://en.wikipedia.org/wiki/Surface-mount_technology) LEDs are visible
* Risers can be mounted on the gold rings of the PCB and secured by screws through the top and bottom plates.
I may add some of these in the future to ease removing keycaps.
Currently, the switches move up and down in the top plate when pulling keycaps as they are only secured in the downwards direction by their plate mount.
They move up until the PCB hits the top plate.

{{< figure alt="iris-switches-tilt" src="../../img/keeb/iris/iris-switches-tilt.jpg" caption="Kailh switches plate-mounted in the black acrylic upper layer">}}
{{< figure alt="iris-switches" src="../../img/keeb/iris/iris-switches.jpg" caption="Naked unit">}}
{{< figure alt="kailh-switch" src="../../img/keeb/iris/kailh-switch.jpg" caption="Close up of a Kailh Brown" style="width: 66%; margin: auto auto;">}}
Note that the Kailh switches support through-hole LEDs.
Perhaps I'll add some in the future.

### Keycap profiles

All the keycap sets for the [conventional keyboards]({{< ref "#history" >}}) are [OEM profile](https://deskthority.net/wiki/Keyboard_profile#OEM_profile).

{{< figure alt="vortex-pbt-profile" src="../../img/keeb/vortex-pbt-profile.jpg" caption="Vortex keycap set">}}
{{< figure alt="keycool-pom-profile" src="../../img/keeb/keycool-pom-profile.jpg" caption="Keycool keycap set">}}
{{< figure alt="magicforce-abs-profile" src="../../img/keeb/magicforce-abs-profile.jpg" caption="MagicForce keycap set">}}
{{< figure alt="anne-pro-pbt-profile" src="../../img/keeb/anne-pro-pbt-profile.jpg" caption="Anne Pro keycap set">}}

The Big Bang set is MDA profile:

{{< figure alt="bb-profile" src="../../img/keeb/iris/bb-profile.jpg" caption="Big Bang keycap set">}}
Here, I show the keys as I have them on the Iris.
From left to right, they are `R4`, `R3`, `R2`, `R1`, and a reversed `R1`.

Keyboard profiles are described [here](https://mechlab.cc/a-guide-to-keycap-profiles/) and [here](https://thekeeblog.com/overview-of-different-keycap-profiles/#Cherry_/_OEM_/_GMK_/_DCS).

### Keycap materials

A variety of materials and fabrication techniques were used for the backlit keycap sets.
Here's a sample key of each set for reference, in left-to-right order of:
* Vortex
* Keycool
* Anne Pro
* Magicforce

{{< figure alt="keycaps-compare-top" src="../../img/keeb/keycaps-compare-top.jpg" caption="Keycaps from above">}}
{{< figure alt="keycaps-compare-bottom" src="../../img/keeb/keycaps-compare-bottom.jpg" caption="Keycaps from below">}}

In left-to-right order of `R4`, `R3`, `R2`, `R1`, here is one of each type of Big Bang key from above and below:

{{< figure alt="bb-sample-top" src="../../img/keeb/iris/bb-sample-top.jpg" caption="Big Bang keycap set sample from above">}}
{{< figure alt="bb-sample-bottom" src="../../img/keeb/iris/bb-sample-bottom.jpg" caption="Big Bang keycap set sample from below">}}

### Keycap sets

{{< figure alt="keycool-caps" src="../../img/keeb/old/keycool-caps.jpg" caption="Keycool keycap set">}}
{{< figure alt="vortex" src="../../img/keeb/old/vortex.jpg" caption="Vortex keycap set">}}
{{< figure alt="magicforce-caps" src="../../img/keeb/old/magicforce-caps.jpg" caption="Magicforce keycap set">}}
{{< figure alt="keycool-caps-for-magicforce" src="../../img/keeb/old/keycool-caps-for-magicforce.jpg" caption="Subset of Keycool keycap set for 68 key layout">}}
{{< figure alt="anne-caps" src="../../img/keeb/old/anne-caps.jpg" caption="Anne Pro keycap set">}}

### Non-split keyboards: switches 

{{< figure alt="keycool-switches" src="../../img/keeb/old/keycool-switches.jpg" caption="Keycool switches (1)">}}
{{< figure alt="keycool-switches-angle" src="../../img/keeb/old/keycool-switches-angle.jpg" caption="Keycool switches (2)">}}
{{< figure alt="magicforce-switches" src="../../img/keeb/old/magicforce-switches.jpg" caption="Magicforce switches (1)">}}
{{< figure alt="magicforce-switches-angle" src="../../img/keeb/old/magicforce-switches-angle.jpg" caption="Magicforce switches (2)">}}
{{< figure alt="anne-switches" src="../../img/keeb/old/anne-switches.jpg" caption="Anne switches, LEDs off">}}
{{< figure alt="anne-switches-light" src="../../img/keeb/old/anne-switches-light.jpg" caption="Anne switches, LEDs on">}}

### Fun with lights

{{< figure alt="keycool-switches-light" src="../../img/keeb/old/keycool-switches-light.jpg" caption="Keycool">}}
{{< figure alt="xmas-front" src="../../img/keeb/old/xmas-front.jpg" caption="Anne Pro front">}}
{{< figure alt="xmas-flat" src="../../img/keeb/old/xmas-flat.jpg" caption="Anne Pro flat">}}
{{< figure alt="xmas-back" src="../../img/keeb/old/xmas-back.jpg" caption="Anne Pro back">}}

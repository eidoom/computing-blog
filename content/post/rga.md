+++
title = "Search documents with ripgrep-all"
date = 2021-06-27T11:15:14+01:00
categories = ["software"]
tags = ["linux","ripgrep","fedora","fedora-34","desktop","laptop","grep","documents","install","search","find"]
description = "Very useful"
toc = true
draft = false
cover = "img/covers/47.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

`rga`, or [ripgrep-all](https://phiresky.github.io/blog/2019/rga--ripgrep-for-zip-targz-docx-odt-epub-jpg/) {{< ghub phiresky ripgrep-all >}}, is a useful tool to search documents with [`rg`]({{< ref "utilities#search" >}}).

Install [the latest release](https://github.com/phiresky/ripgrep-all/releases) on Fedora 34 with
```shell
cd ~/scratch
wget https://github.com/phiresky/ripgrep-all/releases/download/v0.9.6/ripgrep_all-v0.9.6-x86_64-unknown-linux-musl.tar.gz
tar xf ripgrep_all-v0.9.6-x86_64-unknown-linux-musl.tar.gz
cd ripgrep_all-v0.9.6-x86_64-unknown-linux-musl
cp rga* ~/.local/bin  # this directory is on my path
```
then use as
```shell
rga <search term> <document>
```

+++
title = "Addressing the Transmission web client by hostname"
date = "2019-11-03T09:41:46Z"
edit = 2020-01-20
tags = ["linux","bittorrent","server","automate","transmission-bittorrent","lan","network","debian","home-server","debian-buster","debian-10"]
categories = ["home"]
description = "For anyone still addressing by IP address"
toc = true
cover = "img/covers/36.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

I have [Transmission]({{< ref transmission >}}) running on my linux server for BitTorrent transfers. 
The server runs Debian Buster.
I have been accessing the web interface using the IP address of the server in the URL like a dirty animal.
<http://192.168.1.2:9091>? Yuk.
It's time to use the hostname of my server for this.

## Configuration

The hostname of the machine can be found with

```shell
hostname
```
```shell
ryanserver
```

Before making any changes to the settings, Transmission has to be stopped otherwise it will overwrite any changes

```shell
sudo systemctl stop transmission-daemon
```

Open the settings for editing

```shell
vi ~/.config/transmission-daemon/settings.json
```

Make sure `rpc-host-whitelist-enabled` is `true` and add the hostname(s) of the server to `rpc-host-whitelist`

```vim
...
"rpc-enabled": true,
"rpc-host-whitelist": "ryanserver,ryanserver.local",
"rpc-host-whitelist-enabled": true,
...
```

My server can be accessed at <http://ryanserver> or <http://ryanserver.local> on my LAN, hence the multiple hostnames.

Start up Transmission again

```shell
sudo systemctl start transmission-daemon
```

Enjoy accessing the Transmission web interface at <http://ryanserver:9091>.

## Proxy

{{< cal "2024-01-10" >}}

Let's be honest, addressing by the port number is still pretty undesirable.
It's better to use a {{< tag "reverse proxy" >}} to address by a [subdirectory]({{< ref "proxy#transmission" >}}) or, better yet, a [subdomain]({{< ref "caldav#update" >}}) (which also requires a {{< tag DNS >}} record).
For instance, with {{< tag nginx >}},

```nginx
server {
    listen 80;

    server_name transmission.DOMAIN;

    location / {
        proxy_pass http://127.0.0.1:9091/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

In that case, the Debian's defaults for the relevant settings are appropriate: `rpc-host-whitelist` can be left empty and the `rpc-whitelist` can remain as the local host only,

```json
    ...
    "rpc-host-whitelist": "",
    ...
    "rpc-whitelist": "127.0.0.1",
    ...
```

+++
title = "Segrete e Draghi"
date = 2022-10-22T15:57:37+02:00
categories = ["web"]
tags = ["project","game","web","website","webpage","spa","svelte","svelte.js","rollup","rollup.js","database","api","python","js","javascript","sqlite","sqlite3","flask","fastapi","frontend","backend","ci","gitlab-ci","static-website","sql","orm","peewee","flask-login","sessions","authentication","htmx","web-app","mpa","python3","htmx.js","pico.css","gunicorn","dnd","dnd-5e","nginx","reverse-proxy","json","css","html","jinja","jinja2","templates","pallets","dynamic-website","elixir","beam","erlang","erlang-vm","phoenix","postgresql","node","nodejs","npm","identd","fedora","debian","server","mobile","desktop","laptop","migration","db","mvc","rest","restful","crud","acid","relational","rdbms","relational-db"]
description = "The title is an Italian translation of Dungeons and Dragons for absolutely no good reason"
toc = true
draft = false
cover = "img/covers/109.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
At {{< gl draghi >}}, I've been trying out various technologies with the goal of producing a digital Dungeons and Dragons character sheet.

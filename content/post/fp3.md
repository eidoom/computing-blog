+++
title = "microG on Fairphone"
date = 2021-08-27T15:57:28+01:00
categories = ["smartphone"]
tags = ["android","fairphone","microg","lineageos","lineageos-for-microg","f-droid","adb","fastboot","fairphone-3","fp3","de-google","fenix","fennec","mozilla","firefox","aurora-store","osmand","element","newpipe","fairemail","wikipedia","matrix","phone","mobile"]
description = "A fixable, de-Googled, less-evil phone"
toc = true
draft = false
cover = "img/covers/44.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Fairphone 3

I [got](https://www.ethicalconsumer.org/technology/shopping-guide/mobile-phones) a [Fairphone 3](https://shop.fairphone.com/en/fairphone-3) {{< wiki Fairphone_3 >}} {{< ext "https://www.gsmarena.com/fairphone_3-10397.php" >}}.

## LineageOS for microG

I chose [LineageOS for microG](https://lineage.microg.org/) {{< wiki "MicroG#LineageOS_for_MicroG" >}} for my OS.
[microG](https://microg.org/) {{< wiki "MicroG" >}} replaces the Google Android libraries.

I downloaded and checked the [latest build for the Fairphone 3](https://download.lineage.microg.org/FP3/)
```shell
VERSION=18.1
DATE=20210811
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3.zip
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3.zip.sha256sum
sha256sum --check lineage-$VERSION-$DATE-microG-FP3.zip.sha256sum
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3-recovery.img
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3-recovery.img.sha256sum
sha256sum --check lineage-$VERSION-$DATE-microG-FP3-recovery.img.sha256sum
```

The [LineageOS installation guide for the Fairphone 3](https://wiki.lineageos.org/devices/FP3/install) is mostly relevant for installing LineageOS for microG too.

## Android Tools

On Fedora 34, I installed
```shell
sudo dnf install android-tools
```
giving versions
```shell
$ dnf list android-tools
android-tools.x86_64    1:31.0.2-1.fc34    @updates
$ adb --version
Android Debug Bridge version 1.0.41
...
```
[Alternatively, get them straight from Google.]({{< ref "#up-tp-date-android-tools" >}})

## Unlock bootloader

The bootloader unlocking procedure is described on the [Fairphone website](https://support.fairphone.com/hc/en-us/articles/10492476238865-Manage-the-Bootloader).

### In Android

I let the default OS update itself, found the {{< wl IMEI "International Mobile Equipment Identity" >}}1 under `Settings` → `About phone` → `IMEI (SIM slot 1)`, found the serial number on the same page under `Model & Hardware` → `Serial number`, then entered these two codes to get the unlock code on [Fairphone's portal](https://www.fairphone.com/en/bootloader-unlocking-code-for-fairphone-3/).
Then, still in `About phone`, I pressed `Build number` seven times in the usual ritual to enable developer mode, went to `Settings` → `System` → `Advanced` → `Developer options` and enabled `OEM unlocking` (and `USB debugging`, although it's not required here).

### In fastboot

I entered `fastboot` mode (also called the `bootloader`) by powering off and holding `power button` and `volume down`.

> Alternatively, enable `USB debugging` in the `Developer options`, attach the phone by USB, then
> ```shell
> sudo adb reboot fastboot
> ```

With the phone connected via USB, on the laptop I checked the connection
```shell
$ sudo fastboot devices
<serial number>    fastboot
```
and tried to unlock the bootloader
```shell
$ sudo fastboot oem unlock
FAILED (Write to device failed (Protocol error))
fastboot: error: Command failed
```
except it didn't work using a USB 3 port on the laptop.
I connected instead through a USB 2 hub (it's a thing)
```shell
$ sudo fastboot oem unlock
OKAY [  0.088s]
Finished. Total time: 0.088s
```
and it succeeded.

## Recovery

Back with USB 3, I flashed `recovery`
```shell
$ sudo fastboot --set-active=a
Setting current slot to 'a'                        OKAY [  0.022s]
$ sudo fastboot flash boot_a lineage-18.1-20210811-microG-FP3-recovery.img
Sending 'boot_a' (65536 KB)                        OKAY [  2.392s]
Writing 'boot_a'                                   OKAY [  0.658s]
Finished. Total time: 3.222s
```

Then selected `recovery` mode on the phone (navigating the `bootloader` menu with `volume down`, `volume down`, `power`)

In the LineageOS recovery, I wiped the phone with `Factory reset` → `Format data / factory reset` → `Format data`, then did `Apply Update` → `Apply from ADB` and on the laptop
```shell
$ sudo adb sideload lineage-$VERSION-$DATE-microG-FP3.zip
Total xfer: 1.00x
```
It hung on 47% for a while then finished.

I went back (click the back arrow in the top left of the screen) then selected `Reboot system now`.

The OS started up fine.

## Relock bootloader

At this point, go back to [fastboot]({{< ref "#in-fastboot" >}}) and

```shell
sudo fastboot oem lock
```

to relock the bootloader.
This must be done now as it will wipe user data.

Then disable [`OEM unlocking`]({{< ref "#in-android" >}}) in the OS.

## F-Droid apps

On [F-Droid](https://f-droid.org/) {{< wiki F-Droid >}}, I installed a few apps, including
* A [Firefox for Android](https://www.mozilla.org/en-US/firefox/browsers/mobile/) {{< wiki "Firefox for Android" >}} {{< ghubn "mozilla-mobile/fenix" >}} build called [Fennec](https://f-droid.org/packages/org.mozilla.fennec_fdroid/).
* [OsmAnd](https://osmand.net/) {{< ghubn "osmandapp/Osmand" >}} {{< wiki OsmAnd >}} {{< pl "https://f-droid.org/en/packages/net.osmand.plus/" >}} for (offline) maps.

    > {{< cal 2023-04-12 >}}
    >
    > [Organic Maps](https://organicmaps.app/) {{< ghubn "organicmaps/organicmaps" >}} {{< wiki Organic Maps >}} {{< pl "https://f-droid.org/packages/app.organicmaps/" >}} is another good one.

* [Wikipedia](https://www.mediawiki.org/wiki/Wikimedia_Apps) {{< ghubn "wikimedia/apps-android-wikipedia" >}} {{< pl "https://f-droid.org/en/packages/org.wikipedia/" >}}.
* [Element messenger for Matrix]({{< ref "element#android" >}}).

    > {{< cal 2023-04-12 >}}
    >
    > Previously I used [Conversations for XMPP communication]({{< ref "cloud#__prosody_wrapper" >}}).

* [NewPipe](https://newpipe.net/) {{< ghubn "TeamNewPipe/NewPipe" >}} {{< pl "https://f-droid.org/en/packages/org.schabi.newpipe/" >}} for YouTube videos and SoundCloud music.
* [K-9 Mail](https://k9mail.app/) {{< ghubn "thundernest/k-9" >}} {{< pl "https://f-droid.org/packages/com.fsck.k9/" >}} for email.

    > {{< cal 2023-02-20 >}}
    >
    > [FairEmail](https://email.faircode.eu/) {{< ghubn "M66B/FairEmail" >}} {{< pl "https://f-droid.org/en/packages/eu.faircode.email/" >}} is an alternative for email.
    >
    > To use OAuth, you need to use the [GitHub release](https://github.com/M66B/FairEmail/releases), not the F-Droid build.
    >
    > I used to use it to support [work accounts from Outlook/Office 365]({{< ref outlook >}}).
    > However, I never did have much luck with that Outlook account on FairEmail.
    > I no longer have it so don't have to worry about it any more.

    > {{< cal 2024-11-02 >}}
    >
    > K-9 Mail has [become](https://blog.thunderbird.net/2024/10/thunderbird-for-android-8-0-takes-flight/) [Thunderbird Mobile](https://www.thunderbird.net/en-GB/mobile/).

* [DAVx5, Birthday Adapter, and jtx Board]({{< ref "caldav#android" >}}) for Card/CalDAV clients.
* [WireGuard]({{< ref "vpn-wireguard#android" >}}) for VPN client.
* [MuPDF viewer](https://mupdf.com/) {{< wiki MuPDF >}} {{< git "https://git.ghostscript.com/?p=mupdf-android-viewer.git;a=summary" >}} {{< pl "https://f-droid.org/en/packages/com.artifex.mupdf.viewer.app/" >}} for viewing PDFs.
* QuickDic {{< ghubn "rdoeffinger/Dictionary" >}} {{< pl "https://f-droid.org/en/packages/de.reimardoeffinger.quickdic/" >}} for an offline translation dictionary.
* fWallet {{< glabn "TheOneWithTheBraid/f_wallet" >}} {{< pl "https://f-droid.org/packages/business.braid.f_wallet/" >}} for {{< wl PKPASS >}} tickets.
* [Aegis Authenticator](https://getaegis.app/) {{< pl "https://f-droid.org/packages/com.beemdevelopment.aegis/" >}} for [MFA]({{< ref 2fa >}}).

## Play Store apps

Being de-Googled, there's no Play Store.
To installed Play Store apps, I used [Aurora Store](https://auroraoss.com/download/AuroraStore/) {{< glab AuroraOSS AuroraStore >}}.
Install it with [F-Droid](https://f-droid.org/en/packages/com.aurora.store/).
I logged in with a throwaway Google account so the localisation was correct (otherwise you may not find desired regional apps).

The only problem I had was that my banking app wouldn't run.
This is due to it using Google {{< wl SafetyNet >}}, an anti-modding API.

> The solution is obviously to change banks while boycotting anything using SafetyNet.
> A change of bank account was in the cards anyway as I wanted to switch from my Bank ([RBS](https://www.rbs.co.uk/)) to a Building Society ([Nationwide](https://www.nationwide.co.uk/)) with more ethical policies concerning society and the environment.
> The app for the new account does work.

### Edit for Android 14/LineageOS 21 (2024)

{{< cal "2024-12-23" >}}

Three years down the line and Google is battening down the hatches in our game of cat and mouse.
Many apps, specifically banking apps including Nationwide's, now require SafetyNet verification.
In addition, the SafetyNet Attestation API is deprecated in favour of the [Play Integrity API](https://developer.android.com/google/play/integrity), meaning more work for the mice.

To have any chance of running these apps, I made sure to [relock the bootloader after OS installation]({{< ref "#relock-bootloader" >}}), not root the phone, and enable the `Google Services` under `microG Settings`:

* `Account`: add throw-away Google account
* `Google device registration`: enabled (`Register device`) with `Automatic: Native` profile.
* `Cloud Messaging`: enabled (`Receive push notifications`)
* `Google SafetyNet`: enabled (`Allow device attestation`)

However, this was still not enough.
After some sleuthing online, I had a suspicion this was due to strings like the build fingerprint not conforming to Google's list of allowed configurations.
Thankfully, there is a tool {{< ghn "luk1337/ih8sn" >}} ("I hate SafetyNet") which spoofs the fingerprint and related metadata for us.

First, I enabled `USB debugging` and `Rooted debugging` under `Settings` > `System` > `Developer options` > `Debugging` on the phone and attached it via USB to my laptop.
Then on the latop, I downloaded the [latest release](https://github.com/luk1337/ih8sn/releases/tag/latest) for the `arm64` architecture and unzipped it,
```shell
wget https://github.com/luk1337/ih8sn/releases/download/latest/ih8sn-aarch64.zip
mkdir ih8sn
mv ih8sn-aarch64.zip ih8sn
unzip ih8sn-aarch64.zip
```
then borrowed the appropriate FairPhone 3 configuration from a [fork](https://github.com/LOS-Munch/ih8sn/blob/master/system/etc/ih8sn.conf.FP3)
```shell
cd system/etc
wget https://raw.githubusercontent.com/LOS-Munch/ih8sn/refs/heads/master/system/etc/ih8sn.conf.FP3
cd ../..
```
before running the patch
```shell
sudo ./push.sh
```
allowing the connection on the dialogue which pops up on the phone, then rebooting the phone,
```shell
sudo adb reboot
```

The `Test SafetyNet attestation`, under `microG Settings` > `Google Services` > `Google SafetyNet`, still fails with `Warning: CTS profile does not match`.
However, the Nationwide banking app now works.

> {{< cal 2024-12-29 >}}
>
> I checked that the following apps are working:
> 
> * Atom
> * Monzo
> * Nationwide
> * Tembo
> 
> while these don't:
> 
> * Revolut
> * Starling

## Updating

Patches are distributed and installed via the system updater.

Major version upgrades must be performed [manually](https://wiki.lineageos.org/devices/FP3/upgrade).
For example, download and check files
```shell
VERSION=20.0
DATE=20230410
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3-boot.img
sha256sum --check https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3-boot.img.sha256sum
wget https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3.zip
sha256sum --check https://download.lineage.microg.org/FP3/lineage-$VERSION-$DATE-microG-FP3.zip.sha256sum
```
[Load `bootloader`]({{< ref "#unlock-bootloader" >}}) and flash `recovery` (using different partition than current latest `recovery`)
```shell
sudo fastboot flash boot_b lineage-$VERSION-$DATE-microG-FP3-boot.img
sudo fastboot --set-active=b
```
[Boot `recovery`]({{< ref "#recovery" >}}) and flash system
```shell
sudo adb sideload lineage-$VERSION-$DATE-microG-FP3.zip
```

## Backups

LineageOS includes [SeedVault](https://calyxinstitute.org/projects/seedvault-encrypted-backup-for-android) {{< ghubn "seedvault-app/seedvault" >}} for backups.
I thought it'd be nice to backup to a {{< wl WebDAV >}} server [through DAVx5](https://manual.davx5.com/webdav_mounts.html).
It'd be nicer still to use Nginx for this since I'm running it anyway, but [that sucked]({{< ref "#webdav-on-nginx" >}}).
Instead, I [used](https://linuxconfig.org/webdav-server-setup-on-ubuntu-linux) [Apache](https://httpd.apache.org/) as the WebDAV server and [proxied](https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-18-04-server#step-6-installing-and-configuring-nginx) it through Nginx.

```shell
sudo apt install apache2
```

Edit port in `/etc/apache2/ports.conf`.

```shell
sudo a2dissite 000-default
sudo a2ensite webdav
```

Create `/etc/apache2/sites-available/webdav.conf`:

```apache2
<VirtualHost localhost:8888>
        ServerAdmin localhost
        ServerName localhost

        DocumentRoot /var/www/webdav
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/webdav/>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride None
                Order allow,deny
                allow from all
                DAV on
        </Directory>
</VirtualHost>
```

```shell
sudo a2ensite webdav
sudo a2enmod dav_fs
sudo systemctl restart apache2
```

To proxy through Nginx with auth,

```nginx
server {
    listen [::]:443 ssl;

    server_name webdav.<domain>;

    ssl_certificate /etc/letsencrypt/live/star.<domain>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/star.<domain>/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:8888/;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }

    auth_basic "webdav";
    auth_basic_user_file /etc/apache2/.htpasswd-webdav;
}
```

The client setup is the same as [before]({{< ref "#client" >}}).

> {{< cal "2024-12-28" >}}
>
> This has proved unreliable.
> Today I'm seeing the backup failing, with no error messages on the phone and nothing in the server logs.
> Perhaps the best thing to do is backup to the flash disk on the phone and copy it over to the server manually.

## Caller ID with 3

For some reason, when either `Settings` > `Network and Internet` > `SIMs` > `3`:

* \> `Calling` > `Wi-Fi Calling`
* \> `4G Calling`

is enabled on a Three SIM card in a Fairphone 3 running LineageOS (tested with version 21), the number is not shown for incoming calls (it displays `Unknown`).
This is particularly annoying because we don't know who made missed calls.
Disabling Wi-Fi and 4G calling so the 3G mobile network is used lets us see the incoming caller's number, but then we can't take advantage of Wi-Fi calling in houses with no signal or improved audio quality on 4G.
This has been [reported](https://gitlab.com/LineageOS/issues/android/-/issues/5024) by [others](https://gitlab.com/LineageOS/issues/android/-/issues/6302) too, but no fix is forthcoming.

An EE SIM works properly for me.
The Three SIM works properly in another phone.

<br>
<hr>
<br>

## Appendices

### F-Droid repo managers for Play apps

One could also host an F-Droid repository server that provided Play Store apps.
Two options are
* [Playmaker](https://github.com/NoMore201/playmaker)
    I tried the [OCI image](https://hub.docker.com/r/nomore201/playmaker) with Docker Hub
    ```shell
      playmaker:
        image: nomore201/playmaker
        container_name: playmaker
        ports:
          - "5000:5000"
        volumes:
          - /srv/fdroid:/data/fdroid
        environment:
          LANG_LOCALE: "en_GB"
          LANG_TIMEZONE: "Europe/London"
          DEVICE_CODE: "walleye"
        restart: unless-stopped
    ```
    but it errored.

* [GPlayWeb](https://github.com/matlink/gplayweb)
    I tried the [OCI image](https://hub.docker.com/r/matlink/gplayweb) with Docker Hub
    ```shell
      gplayweb:
        image: matlink/gplayweb
        container_name: gplayweb
        ports:
          - "8888:8888"
        volumes:
          - ./fdroid/:/data/fdroid
    ```
    but it also errored.

### LineageOS

I also tried out the vanilla LineageOS
```shell
wget https://mirrorbits.lineageos.org/recovery/FP3/20210823/lineage-18.1-20210823-recovery-FP3.img
wget https://mirrorbits.lineageos.org/full/FP3/20210823/lineage-18.1-20210823-nightly-FP3-signed.zip
wget 'https://mirrorbits.lineageos.org/recovery/FP3/20210823/lineage-18.1-20210823-recovery-FP3.img?sha256'
wget 'https://mirrorbits.lineageos.org/full/FP3/20210823/lineage-18.1-20210823-nightly-FP3-signed.zip?sha256'
sha256sum --check lineage-18.1-20210823-nightly-FP3-signed.zip\?sha256
sha256sum --check lineage-18.1-20210823-recovery-FP3.img\?sha256
```
Bootloader's already unlocked.
```shell
$ fastboot --set-active=a
Setting current slot to 'a'                        OKAY [  0.023s]
Finished. Total time: 0.028s
$ fastboot flash boot_a lineage-18.1-20210823-recovery-FP3.img
Sending 'boot_a' (65536 KB)                        OKAY [  2.323s]
Writing 'boot_a'                                   OKAY [  0.653s]
Finished. Total time: 3.093s
```
Reboot into `recovery`
```shell
$ adb sideload lineage-18.1-20210823-nightly-FP3-signed.zip
```
It ways "Signature verification failed. Install anyway?", which is odd.
If I choose "Yes" it fails anyway.
Maybe a bad build?

This set up would also need [OpenGApps](https://opengapps.org/), version `ARM64 11.0 pico`.

### TWRP

Being [supported](https://twrp.me/fairphone/fairphone3.html), one could use TWRP recovery images instead of the LineageOS ones.
[Download](https://dl.twrp.me/FP3/twrp-3.5.2_9-0-FP3.img) and
```shell
$ fastboot flash boot_a twrp-3.5.2_9-0-FP3.img
Sending 'boot_a' (30902 KB)                        OKAY [  1.276s]
Writing 'boot_a'                                   OKAY [  0.318s]
Finished. Total time: 1.687s
```
Reboot into recovery.
I did `mount` → `select storage` → `internal storage` then copied over the LineageOS image and tried installing it, but it failed with
```shell
error applying update: 28 errorcode::kdownloadoperationexecutionerror
```
The source of the problem from before?

### /e/

An alternative deGoogled OS is [/e/](https://doc.e.foundation/devices/FP3/install).
It's based on Android 10, versus LineageOS for microG being based on Android 11.

It comes with its own app store providing a subset of Play Store apps; Aurora Store seems to have better app support---although even though it was provided and would install, I did find [my banking app wouldn't work]({{< ref "#play-store-apps" >}}).

I haven't tried /e/.

### Up-to-date Android tools

[Android tools](https://developer.android.com/studio/releases/platform-tools) can be [downloaded straight from Google](https://dl.google.com/android/repository/platform-tools-latest-linux.zip) rather than though the distro's package manager if the distro version is out of date.
For my case, distro was `31.0.2`, while upstream was `31.0.3`.

### WebDAV on Nginx

This section documents a failed attempt to run a WebDAV server using Nginx modules on a Debian machine to be used with SeedVault via DAVx5 on Android.

#### Server

On the server, we ensure the necessary Nginx modules,

* [`ngx_http_dav_module`](https://nginx.org/en/docs/http/ngx_http_dav_module.html) (included in Debian's [`nginx`]())
* {{< ghn "arut/libnginx-mod-http-dav-ext" >}} {{< pl "https://packages.debian.org/bookworm/libnginx-mod-http-dav-ext" >}}

are installed,
```shell
sudo apt update
sudo apt install nginx libnginx-mod-http-dav-ext
```

create some directories and set ownership and access permissions,

```shell
sudo mkdir -p /var/www/webdav
sudo mkdir -p /var/tmp/nginx/webdav
sudo chown -R www-data:www-data /var/www/webdav
sudo chown -R www-data:www-data /var/tmp/nginx
sudo chmod -R 755 /var/tmp/nginx
sudo chmod -R 755 /var/www/webdav
```

set up a user and password [using `apache2-utils`]({{< ref "proxy#file-browser" >}}) for Nginx `auth_basic`,

```shell
sudo htpasswd -B -c /etc/apache2/.htpasswd-webdav <user>
```

then [configure](https://www.davx5.com/tested-with/nginx) Nginx at [`/etc/nginx/sites-available/public`]({{< ref "ipv6#reverse-proxy" >}}) (or similar) as

```nginx
server {
    listen [::]:443 ssl http2;

    server_name webdav.<domain>;

    ssl_certificate /etc/letsencrypt/live/star.<domain>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/star.<domain>/privkey.pem;

    root /var/www/webdav/;

    client_body_temp_path /var/tmp/nginx/webdav/;

    dav_methods PUT DELETE MKCOL COPY MOVE;
    dav_ext_methods PROPFIND OPTIONS;

    create_full_put_path  on;
    dav_access            user:rw group:rw all:r;

    client_max_body_size 1G;

    auth_basic "webdav";
    auth_basic_user_file /etc/apache2/.htpasswd-webdav;

    location ^~ / {
        # workaround for https://trac.nginx.org/nginx/ticket/1966
        if ($request_method = MKCOL) {
            rewrite ^(.*[^/])$ $1/ break;
        }
    }
}
```

and apply the new configuration,

```shell
sudo systemctl restart nginx
```

After creating a test text file `test.txt` in the mount directory on the server,
```shell
cd /var/www/webdav/
sudo -u www-data touch test.txt
```
We can test this by checking for a `200` response from
```shell
curl --user '<username>:<password>' -I -L --http2 https://webdav.<domain>/test.txt
```

We can check for errors at `/var/log/nginx/error.log`.

#### Client

On the phone, after adding the WebDAV mount to DAVx5, it becomes available in the Files app.
Access and edit `text.txt` from Files to check the mount is working.

The backup is configured under `Settings` > `System` > `Backup` > `Seedvault Backup`.
SeedVault has beta support for "integrated direct WebDAV access" but I elected to use the DAVx5 mount since that's stable.

When I try to initialise the backup, however, I get the error:

> An error occured while accessing the backup location.

The Nginx error logs show several of:

> ... mkdir() "/var/www/webdav/.SeedVaultAndroidBackup" failed (17: File exists), client: <address\>, server: webdav.<domain\>, request: "MKCOL /.SeedVaultAndroidBackup/ HTTP/2.0", host: "webdav.<domain\>"

The problem seems to be [this](https://github.com/seedvault-app/seedvault/issues/500#issuecomment-1382561919): the PROPFIND in the Nginx module for extended WebDAV support [ignores](https://github.com/arut/nginx-dav-ext-module/issues/41) directories starting with `.`, so the client doesn't think `.SeedValutAndroidBackup` exists then fails when it tries to write to an existing path.
How annoying.

Using SeedVault's built-in WebDAV also [fails](https://github.com/seedvault-app/seedvault/issues/500#issuecomment-2185980472) with Nginx errors,

> ... directory index of "/var/www/webdav/.SeedVaultAndroidBackup/" is forbidden ...

despite access permissions being set.

Apparently we can't fix this by changing the name of the directory to not have a dot because the SeedVault project [won't](https://github.com/seedvault-app/seedvault/issues/143) add this option (for ROMs with baked-in SeedVaults).

There is a fork of the Nginx module with a [fix](https://github.com/mid1221213/nginx-dav-ext-module/commit/51185d0aa980bafcd02f5a039cf6e46e75b35935), but the Debian package still tracks the original repo, which seems to be abandoned.
All in all, not very healthy.

+++
title = "NixOS"
date = 2025-01-12T11:38:54Z
categories = []
tags = []
description = ""
toc = true
draft = true
#cover = "img/covers/?.avif"
#coveralt = "?"
#clip = 25
+++

https://gitlab.com/eidoom/nixos-config
details within

Dual boot
Windows first
install from usb stick
wired internet
partition - single btrfs, labels, swapfile
https://askubuntu.com/questions/236681/filesystem-label-rename

systemd-boot

https://nixos.wiki/wiki/NixOS_Installation_Guide
https://nixos.org/manual/nixos/stable/index.html#ch-installation

encryption
https://nixos.wiki/wiki/Full_Disk_Encryption#zimbatm.27s_laptop_recommendation
https://unix.stackexchange.com/a/538622

btrfs swap
https://nixos.wiki/wiki/Btrfs#Swap_file
https://wiki.archlinux.org/title/Btrfs#Swap_file

Not entirely stable. Gave up for server. Will learn by running on laptops.

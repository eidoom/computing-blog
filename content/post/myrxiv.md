+++
title = "myrxiv"
date = 2021-04-05T09:20:11+01:00
categories = ["web"]
tags = ["javascript","mini-project", "static-website","academia","arxiv","api","fetch","rss","atom","jsonp","cors","json","xml","css","html","gitlab","gitlab-ci","gitlab-pages"]
description = "Static web app to show arXiv preprints"
toc = true
draft = false
cover = "img/covers/54.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I put together a little JavaScript script to show preprints matching some criteria from [arXiv](https://arxiv.org/), for example all submissions with a particular author.
It uses the arXiv API.

The source and a description are at {{< gl myrxiv >}} with a live example showing my papers [here](https://eidoom.gitlab.io/myrxiv/).

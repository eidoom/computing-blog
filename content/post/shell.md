+++
title = "Z shell configuration with Prezto"
date = "2019-11-12T11:47:00Z"
edit = 2020-02-10
tags = ["environment","linux","terminal","desktop","laptop","fedora","zsh","shell","install","configure"]
categories = ["environment-core"]
description = "Zshhhhhh"
toc = true
cover = "img/covers/104.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## DEPRECATED
{{< cal "2022-04-08" >}}

Now I'm using a [customised Bash setup](https://gitlab.com/eidoom/dotfiles-bash/).

## Zsh

I use [`zsh`](https://www.zsh.org/) as my shell, configured with the help of the [Prezto](https://github.com/sorin-ionescu/prezto) configuration framework and themed with [Powerlevel10k](https://github.com/romkatv/powerlevel10k).
My configuration also relies on [some useful utilities]({{< ref "utilities" >}}).

The configuration and install scripts are all [here](https://gitlab.com/eidoom/dotfiles-zsh).

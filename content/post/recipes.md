+++
title = "Recipes"
date = 2022-10-22T16:00:12+02:00
categories = ["web"]
tags = ["project","static-website","frontend","website","svelte","svelte.js","routify","python","pyyaml","yaml","json","water.css","rollup","rollup.js","routify.js","js","css","html","javascript","ui","ux","interactive","web-app","webpage","spa","spa-router"]
description = "For when people ask what I eat as a vegan"
toc = true
draft = false
cover = "img/covers/113.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I made a static website for sharing vegan recipes [here](https://eidoom.gitlab.io/recipes).
Source at {{< gl recipes >}}.

Now I'm playing with a dynamic version at {{< gl recipes2 >}}.

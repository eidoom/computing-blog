+++
title = "Inhouse DNS resolution with Unbound"
date = 2021-05-15T19:04:47+01:00
categories = ["dns"]
tags = ["home-server","unbound","dns","name-server","debian","debian-10","debian-buster","apt","pihole","docker","dot","doh","dns-over-tls","dns-over-https","home","network","lan"]
description = "Self-hosting a DNS resolver at home"
toc = true
draft = false
cover = "img/covers/21.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

*DEPRECATED: [I moved my primary DNS server to a Raspberry Pi and kept the below setup on the main server as a secondary DNS server.]({{< ref "rpi-dns" >}})*

## Upstream provider

I [previously]({{< ref "home-network-ad-blocking-with-pi-hole#upstream-dns-provider" >}}) switched from the {{< wl "domain name system" >}} (DNS) {{< wl "resolver" "Domain_Name_System#DNS_resolvers" >}} provided by my {{< wl "internet service provider" >}} (ISP ) to a {{< wl "public upstream DNS resolver" "Public recursive name server" >}}.
This provides some [benefits](https://www.howtogeek.com/664608/why-you-shouldnt-be-using-your-isps-default-dns-server/) for security, privacy and performance:
* {{< wl "DNS over HTTPS" >}} (DoH) or {{< wl "DNS over TLS" >}} (DoT) support to prevent eavesdropping and interception
* ISP can't track your browsing history by {{< wl "uniform resource locator" >}} (URL)---although of course they still can by IP address, only a {{< wl "virtual private network" >}} (VPN) like [this one]({{< ref "vpn-wireguard" >}}) can stop that
* they often have a lower ping

However, we can do better.
This setup requires trusting the public DNS resolver to not track us and, as high-stake targets, to not fall afoul of {{< wl "DNS cache poisoning" >}}.
While the likes of {{< wl Cloudflare >}} seem quite worthy of this trust, we can just roll our own resolver instead.

## How does DNS work?

Consider a simple picture for the journey of a DNS resolution query leaving a client and navigating {{< wl "name servers" "Name server" >}}.
The queried {{< wl "fully qualified domain name" >}} (FQDN) is `d2.d1.d0.`.
This maps to, for example, `computing-blog.netlify.app.`.
The trailing dot denotes the {{< wl "DNS root zone" >}} and is generally omitted in the browser.

* The client sends the query `d2.d1.d0.` to a *recursive DNS server*, also called a *DNS resolver*.
* The rightmost named domain `.d0.` is the {{< wl "top-level domain" >}} (TLD), *e.g.* a {{< wl ccTLD >}} or {{< wl gTLD >}}. The DNS resolver queries the {{< wl "root servers" "Root name server" >}} for who is handling `.d0.`.
    * A root server responds with the appropriate TLD servers for `.d0.`
* The DNS resolver queries these TLD servers for who is handling the {{< wl "second-level domain" >}} (2LD) `d1.d0.`
    * A TLD server responds with the 2LD servers for `d1.d0.`
* The DNS resolver queries these 2LD servers for who is handling the {{< wl subdomain >}} `d2.d1.d0.`
    * A 2LD server responds with the subdomain servers for `d2.d1.d0.`
* The DNS resolver queries these subdomain servers for the IP address of `d2.d1.d0.`
    * A subdomain name server returns the IP address for `d2.d1.d0.`
* The DNS resolver returns the IP address for `d2.d1.d0.` to the client

All name servers after the DNS resolver were *authoritative servers*.
For addresses with further subdomains, the steps recursively repeat.
Added to these steps, caching is employed at every level.

## Unbound
### Choice
I opted to use [Unbound](https://nlnetlabs.nl/projects/unbound/about/) {{< wiki "Unbound (DNS server)" >}} {{< arch "Unbound" >}} {{< ghub NLnetLabs unbound >}} {{< docs "https://nlnetlabs.nl/documentation/unbound/" >}} as my DNS resolver.
I'll run it on my [home server]({{< ref "home-server" >}}).

[Knot resolver](https://www.knot-resolver.cz/) {{< arch "Knot_Resolver" >}} also looked like a good alternative.

### Plan
I'll continue to use [`pihole`]({{< ref "home-network-ad-blocking-with-pi-hole" >}}) on [`docker`]({{< ref "server-install-containers#pi-hole" >}}) as a content-blocking, caching DNS server, but it will now query `unbound`, which will be the validating, recursive, caching DNS server.
Note that `unbound` caches the full recursive chain of names, while `pihole` only caches the full name.

So, to add to our [picture]({{< ref "#how-does-dns-work" >}}), we can prepend the steps with:

* the user requests an address in the browser
    * the browser may check a cache and a blocklist (*e.g.* [uBlock Origin](https://ublockorigin.com/))
    * otherwise, it forwards to the host
* the host machine may check a cache
    * otherwise, it forwards to `pihole`
* `pihole` checks its cache and blocklist
    * otherwise, it forwards to `unbound`
* here, we enter the previous step list with `unbound` as the DNS resolver

Responses are cached for each step with a cache.

`unbound` supports blocklisting, but I kept `pihole` as it automatically manages the blocklists.

The Pi-hole documentation provides [guidance](https://docs.pi-hole.net/guides/dns/unbound/) for this setup.

### Installation
I installed [`unbound`](https://packages.debian.org/stable/unbound) with the package manager along with some tools (namely {{< wlc "dig" "dig (command)" >}}) for testing.
```shell
sudo apt install unbound dnsutils
```
The `root.hints` file (list of root servers) is automatically installed with the dependency [`dns-root-data`](https://packages.debian.org/stable/dns-root-data).

### Configuration
I used the [Pi-hole docs configuration](https://docs.pi-hole.net/guides/dns/unbound/#configure-unbound) as a template to configure `unbound` for my setup
```shell
sudo vi /etc/unbound/unbound.conf.d/pi-hole.conf
```
```vim
server:
    # If no logfile is specified, syslog is used
    # logfile: "/var/log/unbound/unbound.log"
    verbosity: 0

    # allow access from local network
    access-control: 0.0.0.0/0 allow

    # listen on all interfaces
    interface: 0.0.0.0

    port: 5335

    do-ip4: yes
    do-ip6: no

    do-udp: yes
    do-tcp: yes

    # You want to leave this to no unless you have *native* IPv6. With 6to4 and
    # Terredo tunnels your web browser should favor IPv4 for the same reasons
    prefer-ip6: no

    # Trust glue only if it is within the server's authority
    harden-glue: yes

    # Require DNSSEC data for trust-anchored zones, if such data is absent, the zone becomes BOGUS
    harden-dnssec-stripped: yes

    # Don't use Capitalization randomization as it known to cause DNSSEC issues sometimes
    # see https://discourse.pi-hole.net/t/unbound-stubby-or-dnscrypt-proxy/9378 for further details
    use-caps-for-id: no

    # Reduce EDNS reassembly buffer size.
    # Suggested by the unbound man page to reduce fragmentation reassembly problems
    edns-buffer-size: 1472

    # Perform prefetching of close to expired message cache entries
    # This only applies to domains that have been frequently queried
    prefetch: yes

    # One thread should be sufficient, can be increased on beefy machines. In reality for most users running on small networks or on a single machine, it should be unnecessary to seek performance enhancement by increasing num-threads above 1.
    num-threads: 1

    # Ensure kernel buffer is large enough to not lose messages in traffic spikes
    so-rcvbuf: 1m

    # Ensure privacy of local IP ranges
    private-address: 192.168.0.0/16
    private-address: 169.254.0.0/16
    private-address: 172.16.0.0/12
    private-address: 10.0.0.0/8
    private-address: fd00::/8
    private-address: fe80::/10
```
To touch up on IP subnet notation {{< wiki "Classless Inter-Domain Routing" >}}, see [this](https://serverfault.com/questions/49765/how-does-ipv4-subnetting-work/226445).

Since `pihole` is running in a Docker container, I lazily opened access to the whole local network (the default is just `localhost`).
Note the server is behind the router and port `5335` is not forwarded, otherwise this would be unsafe without appropriate firewalling.

The package handles the root server list.

The default kernel buffer maximum size is
```
$ sudo sysctl -a | grep net.core.rmem_max
net.core.rmem_max = 212992
```
so I needed to increase that to fit `so-rcvbuf: 1m`
```shell
sudo vi /etc/sysctl.d/unbound.conf
```
```vim
net.core.rmem_max=1048576
```
where the number comes from the `sudo systemctl status unbound.service` [error message](https://askubuntu.com/questions/1285370/trying-to-allocate-more-ram-to-unbound-and-getting-a-warning-to-fix-start-with) that pops up if we don't increase the kernel buffer limit.
I applied it with
```
sudo sysctl --system
```

I ensured that [`resolvconf` for `unbound`](https://docs.pi-hole.net/guides/dns/unbound/#disable-resolvconf-for-unbound-optional) was disabled as we don't need it
```shell
sudo systemctl disable unbound-resolvconf.service
sudo systemctl stop unbound-resolvconf.service
```

I checked the configuration
```shell
$ unbound-checkconf /etc/unbound/unbound.conf.d/pi-hole.conf
unbound-checkconf: no errors in /etc/unbound/unbound.conf.d/pi-hole.conf
```

Then I was ready to start and enable the `unbound` service
```
sudo systemctl start unbound.service
sudo systemctl enable unbound.service
```

### Testing
#### Service
I checked the service to ensure it had started without errors
```
$ sudo systemctl status unbound.service
● unbound.service - Unbound DNS server
   Loaded: loaded (/lib/systemd/system/unbound.service; disabled; vendor preset: enabled)
   Active: active (running) since Sun 2021-05-16 09:42:33 BST; 4s ago
     Docs: man:unbound(8)
  Process: 11497 ExecStartPre=/usr/lib/unbound/package-helper chroot_setup (code=exited, status=0/SUCCESS)
  Process: 11500 ExecStartPre=/usr/lib/unbound/package-helper root_trust_anchor_update (code=exited, status=0/SUCCESS)
 Main PID: 11505 (unbound)
    Tasks: 1 (limit: 4915)
   Memory: 6.2M
   CGroup: /system.slice/unbound.service
           └─11505 /usr/sbin/unbound -d

May 16 09:42:33 ryanserver systemd[1]: Starting Unbound DNS server...
May 16 09:42:33 ryanserver package-helper[11500]: /var/lib/unbound/root.key has content
May 16 09:42:33 ryanserver package-helper[11500]: success: the anchor is ok
May 16 09:42:33 ryanserver unbound[11505]: [11505:0] info: start of service (unbound 1.9.0).
May 16 09:42:33 ryanserver systemd[1]: Started Unbound DNS server.
```
which it had.

#### Resolution
A quick test with `dig` on the server
```shell
$ dig computing-blog.netlify.app @127.0.0.1 -p 5335

; <<>> DiG 9.11.5-P4-5.1+deb10u5-Debian <<>> computing-blog.netlify.app @127.0.0.1 -p 5335
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 57040
;; flags: qr rd ra; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;computing-blog.netlify.app.	IN	A

;; ANSWER SECTION:
computing-blog.netlify.app. 20	IN	A	206.189.50.215
computing-blog.netlify.app. 20	IN	A	18.192.76.182

;; Query time: 40 msec
;; SERVER: 127.0.0.1#5335(127.0.0.1)
;; WHEN: Sun May 16 12:51:34 BST 2021
;; MSG SIZE  rcvd: 87
```
shows that `status: NOERROR` that we want.

#### DNSSEC
I tested {{< wl "DNSSEC" "Domain Name System Security Extensions" >}} validation with
```shell
$ dig sigfail.verteiltesysteme.net @127.0.0.1 -p 5335

; <<>> DiG 9.11.5-P4-5.1+deb10u5-Debian <<>> sigfail.verteiltesysteme.net @127.0.0.1 -p 5335
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 33639
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;sigfail.verteiltesysteme.net.  IN      A

;; Query time: 1363 msec
;; SERVER: 127.0.0.1#5335(127.0.0.1)
;; WHEN: Sat May 15 19:03:44 BST 2021
;; MSG SIZE  rcvd: 57
```
which returned `status: SERVFAIL` as expected, and
```shell
$ dig sigok.verteiltesysteme.net @127.0.0.1 -p 5335

; <<>> DiG 9.11.5-P4-5.1+deb10u5-Debian <<>> sigok.verteiltesysteme.net @127.0.0.1 -p 5335
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 5186
;; flags: qr rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1472
;; QUESTION SECTION:
;sigok.verteiltesysteme.net.    IN      A

;; ANSWER SECTION:
sigok.verteiltesysteme.net. 60  IN      A       134.91.78.139

;; Query time: 34 msec
;; SERVER: 127.0.0.1#5335(127.0.0.1)
;; WHEN: Sat May 15 19:04:20 BST 2021
;; MSG SIZE  rcvd: 71
```
returning `status: NOERROR` as required.

Note that Debian [provides](https://wiki.debian.org/DNSSEC) the public keys/[trust anchor](https://www.nlnetlabs.nl/documentation/unbound/howto-anchor/).

#### Inside the container
Next I entered the `pihole` container
```shell
docker exec -it pihole bash
```
and repeated these `dig` checks.
I found that [all connections had the IP address of the container's network gateway](https://stackoverflow.com/questions/47537954/how-to-make-docker-container-see-real-user-ip), `172.18.0.1`, so used that for the IP address of `unbound`.

### Pi-hole
I configured `pihole` to use `unbound` as its upstream DNS server in the web interface at `Settings > DNS`.
I used the IP address <http://192.168.1.2:8053/admin> to access the web interface rather than the URL <http://pihole.ryanserver.lan/admin> provided by the reverse proxy and `pihole` so I wouldn't lose access if I messed up.
I disabled all public servers and entered `unbound` under `Custom 1 (IPv4)` with address `172.18.0.1#5335`.

> Better than that, if using `docker`, set `dns` in the `docker-compose` configuration.

### Statistics

To view Unbound statistics, we can use 
```shell
unbound-control stats_noreset
```
after [some configuration](https://www.nlnetlabs.nl/documentation/unbound/howto-setup/) (see following).
Omitting the `_noreset` will also reset the counters.
Note that the Debian package puts the `unbound*` utilities in `usr/sbin/`, which is not on the `PATH` by default (at least for my configuration).

First, we run 
```shell
unbound-control-setup
```
Then we enable remote control by adding
```shell
sudo vi /etc/unbound/unbound.conf.d/control.conf
```
```vim
remote-control:
    control-enable: yes
```
Then after
```shell
sudo systemctl restart unbound
```
it's ready to go.

[Extended statistics](https://www.nlnetlabs.nl/documentation/unbound/howto-statistics/) can also be enabled, but I didn't do that.

## Evaluation
That's it.
The setup of `unbound` was extremely simple, only complicated by having `pihole` in a container.
I haven't noticed a difference in page loading speeds since switching either.

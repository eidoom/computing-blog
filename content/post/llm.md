+++
title = "Local large language models"
date = 2024-05-29T21:17:19+01:00
categories = ["machine-learning"]
tags = ["llm","large-language-model","deep-learning","neural-network","language-model","ai","artificial-intelligence","transformer","generative-ai","meta-llama","llama"]
description = "Playing around with locally-hosted LLMs"
toc = false
draft = false
cover = "img/covers/llama.webp"
coveralt = "stable-diffusion-xl:'local large language model llama',R-ESRGAN_x4+"
+++

I tried out the interface {{< gh "oobabooga/text-generation-webui" >}} on my laptop (4GB VRAM), which is similar to [A1111]({{< ref "a1111" >}}).

```shell
git clone git@github.com:oobabooga/text-generation-webui.git
cd text-generation-webui
./start_linux.sh
```

This downloads and installs miniconda to set a conda environment, then downloads all the dependencies.
(I might use my system Python 3.11 with a venv if I did this again.)
The UI is available at <http://localhost:7860>.

I wanted to try Llama 3, eg [Meta-Llama-3-8B-Instruct](https://huggingface.co/meta-llama/Meta-Llama-3-8B-Instruct) (nb model access was given within an hour of request), but was aware the VRAM limit would be problematic.
I opted to use the llama.cpp {{< ghubn "ggerganov/llama.cpp" >}} {{< wiki "Llama.cpp" >}} backend with the model [Meta-Llama-3-8B-Instruct-Q4_K_M.gguf](https://huggingface.co/NousResearch/Meta-Llama-3-8B-Instruct-GGUF/blob/main/Meta-Llama-3-8B-Instruct-Q4_K_M.gguf) (~5GB) from [NousResearch/Meta-Llama-3-8B-Instruct-GGUF](https://huggingface.co/NousResearch/Meta-Llama-3-8B-Instruct-GGUF).
Using the ahead-of-time 4-bit quantised model saved a bit of download bandwidth.
I downloaded it through the Model tab of the oobabooga UI, selected the model, set `n-gpu-layers` to 16 (due to VRAM limitation), then loaded the model.
Be sure to enable `tensorcores` on RTX cards.

Back in the Chat tab, I talked with the AI.

## Test

Prompt: tell me a story

Response (concatenated): [link]({{< ref "llm/thow" >}})

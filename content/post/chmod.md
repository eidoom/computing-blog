+++
title = "Linux file permissions"
date = 2022-10-22T16:03:58+02:00
categories = ["operating-system"]
tags = ["mini-project","linux","permissions","file-permissions","files","chmod","desktop","laptop","server","file","file-system","environment"]
description = "`chmod` reminder"
toc = true
draft = false
cover = "img/covers/107.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I always feel I have to double check numeric representations of Linux file permissions, so I made a wee tool to help at {{< gl chmod-permissions >}}.

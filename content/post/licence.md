+++
title = "Software licences"
date = 2021-09-28T20:10:26+01:00
categories = ["cheatsheet"]
tags = [""]
description = ""
toc = true
draft = true
#cover = "img/covers/?.avif"
#coveralt = "?"
+++

* Proprietary (non-free, closed-source) {{< wiki "Proprietary software" >}}
* Open source {{< wiki "Open-source software" >}} {{< wiki "Open-source license" >}}
* Free (libre) {{< wiki "Free software" >}}
    * Liberty not price (=gratis)

{{< wl "Alternative terms for free software" >}}

{{< wl "Gratis_versus_libre" >}}

{{< wl "Portal:Free_and_open-source_software" >}}

{{< wl "Outline of free software" >}}

[Free Software Foundation](https://www.fsf.org/) {{< wiki "Free Software Foundation" >}}

[Open Source Initiative](https://opensource.org/) {{< wiki "Open Source Initiative" >}}

{{< wl Copyleft >}}, permissive, public-domain

Centralised, decentralised, federated, peer-to-peer

walled garden

{{< wl "Comparison of free and open-source software licences" >}}

{{< wl "MIT License" >}}

{{< wl "Apache License" >}}

{{< wl "GNU General Public License" >}} GPLv3

{{< wl "Free content" >}}

[Creative Commons](https://creativecommons.org/) {{< wiki "Creative Commons" >}}

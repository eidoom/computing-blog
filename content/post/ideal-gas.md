+++
title = "A particle box"
date = 2021-03-11T10:48:09Z
categories = ["simulation"]
tags = ["physics","web","mini-project","javascript","canvas","static-website","gitlab","gitlab-ci","gitlab-pages","numerical-integration","collisions","dynamics","kinetics","2d"]
description = "Simulating a closed box of ideal gas"
toc = true
draft = false
cover = "img/covers/58.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Take a [bunch of particles](https://en.wikipedia.org/wiki/Ideal_gas) in a closed 2D system.
Assume they're identical small rigid circles which start with equal speed but randomised direction vectors.
Allow [elastic collisions](https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects) between them.
[Simulate](https://gitlab.com/eidoom/particle-box/-/blob/master/script.js) their motion with [numerical integration](https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet).
Follow the path of the blue particle.
Histogram their energies.
Watch them [thermalise](https://en.wikipedia.org/wiki/Thermalisation) on the [Maxwell-Boltzmann distribution](https://en.wikipedia.org/wiki/Maxwell%E2%80%93Boltzmann_distribution).
Fun.

<iframe src="https://eidoom.gitlab.io/particle-box" class="window" id="particle-box"></iframe>

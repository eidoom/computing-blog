+++
title = "Surface screen installation"
date = 2020-08-26T11:01:55+01:00
edit = 2020-09-05
categories = ["hardware"]
tags = ["surface","microsoft","surface-pro","surface-pro-4","repair","tablet","fix","touchscreen","digitiser","n-trig","stylus","motherboard","chassis","screen"]
description = "Installing a new screen on the Surface Pro 4"
toc = false
draft = false
cover = "img/covers/70.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

In the [last post]({{< ref "surface-screen-remove" >}}), I recorded the removal of the broken screen from my Surface Pro 4.
Now it's time to fit the new one.

{{< figure alt="boxed" src="../../img/surface-replace/boxed.jpg" caption="The new screen in its packaging">}}

The new screen arrived today.

{{< figure alt="unwrapped" src="../../img/surface-replace/unwrapped.jpg" caption="The unwrapped new screen">}}

It looked inorder on unboxing.
It is the correct model, `LTL123YL01`, which is a good start.

{{< figure alt="ntrig1" src="../../img/surface-replace/ntrig1.jpg" caption="The N-trig module hastily attached to the screen">}}

I started by attaching the N-trig module in the bottom-right corner.
I attached the two ribbon cables from the screen by pushing them into their slots on the circuit board as far as they would go and folding over the black securing pieces of plastic until they clicked.
There was some sticky tape on the screen and I had left the conductive tape on the bottom of the module, so I just pressed down on the module to secure it, applying no new adhesive.

{{< figure alt="assembled1" src="../../img/surface-replace/assembled1.jpg" caption="The screen newly adorned with ribbon cables">}}

Then I attached the ribbon cables which will connect to the motherboard by gently pushing the male connector against the female until it snapped in.

{{< figure alt="attaching" src="../../img/surface-replace/attaching.jpg" caption="Carefully attaching the ribbon cables to the motherboard">}}

At this point, I had not yet applied new tape to secure the screen to the chassis, or attached the EMI shields. 
First, I wanted to test the screen.
I attached the other ends of the ribbon cables to the motherboard and let the screen lie in the chassis.
I powered on the Surface to find that while the screen worked, the touchscreen did not and the pen was glitching.


{{< figure alt="ntrig2" src="../../img/surface-replace/ntrig2.jpg" caption="The N-trig module, properly installed with its shield">}}

I detached the screen from the motherboard once again and resecured the N-trig module, making sure it was in exactly the same spot as before the original disassembley and in tight contact with the screen via the conductive tape.

{{< figure alt="assembled2" src="../../img/surface-replace/assembled2.jpg" caption="The screen fully equipped with N-trig module, ribbon cables, and shields">}}

I also installed the EMI shields on the screen side of the ribbon cables.

{{< figure alt="testing" src="../../img/surface-replace/testing.jpg" caption="Testing the screen, touchscreen, and pen">}}

This time when I put things together, everything worked.
Hooray!

I removed the screen to apply double-sided sticky tape to the inner edges of the chassis.

{{< figure alt="taped" src="../../img/surface-replace/taped.jpg" caption="Doubled-sided sticky tape applied to the chassis">}}

Just before I stuck the screen down, I realised that I had taped over the speakers.
I gently pried off the tape from this region with a pair of tweezers, cut it with scissors, and reapplied it around the speaker grille.

{{< figure alt="speaker" src="../../img/surface-replace/speaker.jpg" caption="Doubled-sided sticky tape application fixed for the left forward-facing speaker">}}

Then I reattached the ribbon cables, applied the remaining EMI shields to the motherboard, and stuck the screen down on the chassis by pressing firmly around the edges.

My Surface is now back in working order.
I'll attempt not to smash it again.

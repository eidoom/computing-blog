+++
title = "Download from GOG API on Linux"
date = 2023-10-07T13:08:44+01:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
#cover = "img/covers/?.avif"
#coveralt = "?"
+++

https://github.com/Sude-/lgogdownloader

Download [latest tar](https://github.com/Sude-/lgogdownloader/releases)

```shell
mkdir ~/tar
cd ~/tar
wget https://github.com/Sude-/lgogdownloader/releases/download/v3.11/lgogdownloader-3.11.tar.gz
tar xf lgogdownloader-3.11.tar.gz
cd lgogdownloader-3.11
```

Fedora 38 dependencies

```shell
sudo dnf install \
    libcurl-devel \
    rhash-devel \
    jsoncpp-devel \
    htmlcxx-devel \
    tinyxml2-devel \
    boost-devel \
    zlib-devel \
    qt5-qtwebengine-devel \
    pkg-config \
    cmake \
    ninja-build \
    help2man
```

Build and install

```shell
cmake \
    -B build \
    -DCMAKE_INSTALL_PREFIX=/usr/local \
    -DCMAKE_BUILD_TYPE=Release \
    -DUSE_QT_GUI=ON \
    -GNinja
ninja -Cbuild
sudo ninja -Cbuild install
```

Docs

```shell
man lgogdownloader
```

Usage example

```shell
mkdir ~/games/gog
cd ~/games/gog
lgogdownloader --login
lgogdownloader \
    --threads 4 \
    --galaxy-platform windows \
    --galaxy-language english \
    --galaxy-install cyberpunk_2077_game/0
```

+++
title = "Write once with pandoc"
date = 2020-08-29T16:05:22+01:00
edit = 2021-06-03
categories = ["typesetting"]
tags = ["markup","markdown","pandoc","html","pdf","latex","parse","convert","css","yaml","documents","physics","katex","tikz","haskell","cabal","image","container","docker","gitlab","podman","continuous-deployment","feynman-diagrams","cloud","linux","fedora","dnf","sed","texlive","make","academia","fedora-32","svg","web"]
description = "Using pandoc to decouple output format and styling from markup content"
toc = true
cover = "img/covers/71.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I'm writing up some PhD work just now at [this repo](https://gitlab.com/eidoom/njet-ir-notes/).
I'm writing the content in Markdown [here](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/njet-ir.md).
This is processed to make a [web version](https://eidoom.gitlab.io/njet-ir-notes/) and a [print version](https://eidoom.gitlab.io/njet-ir-notes/njet-ir.pdf).
It's really great to be able to write once in a pleasant markup language (*i.e.* not `latex`, apart from the Maths) and get a web page and a PDF out at the end.
Usually I wouldn't desire a print version---my workflow has been entirely paperless since April 2015---but unfortunately academia still loves its printed papers even if I don't.

## The build process
The compilation is programmed in a [`make` file](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/Makefile).
The parsing of the markup is performed by [pandoc](https://pandoc.org/) {{< docs "https://pandoc.org/MANUAL.html" >}} {{< ghub "jgm" "pandoc" >}} {{< wiki "Pandoc" >}}.
I am using some [filters](https://pandoc.org/filters.html):
* [citeproc](https://github.com/jgm/pandoc-citeproc) to manage references from a [`bib` file](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/njet-ir.bib). 
The styling is controlled via a [`csl` file](https://citationstyles.org/).
I get my `bib` snippets from [inspirehep](https://inspirehep.net/) and the styles [here](https://github.com/citation-style-language/styles).
* [crossref](https://github.com/lierdakil/pandoc-crossref) to handle links within the document.
* [include-code](https://github.com/owickstrom/pandoc-include-code) to include code snippets from [external files](https://gitlab.com/eidoom/njet-ir-notes/-/tree/master/code-examples). 
This is great because I can write code that I can compile, test, and edit without ever worrying about copy errors or updates for the version in the document.

The `pandoc` [base settings](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/shared.yml), and also format specific configuration for [`pdf`](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/pdf.yml) and [`html`](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/html.yml) output, are stored in `yaml` files.

I do some processing of `\includegraphics` for styling and to manage pathing.
This is by [`sed`](https://www.gnu.org/software/sed/manual/sed.html) scripts for [`html`](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/md4html.sh) and [`pdf`](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/md4pdf.sh)
It would be cleaner if I wrote a little pandoc filter to do this instead.

The images are mainly Feynman diagrams.
I produce them in `latex` using the [`feynhand`](https://arxiv.org/pdf/1802.00689.pdf) package, which is based on [`tikz-feynman`](https://arxiv.org/pdf/1601.05437.pdf) and [`tikz`](https://www.texample.net/media/pgf/builds/pgfmanualCVS2012-11-04.pdf).
I have a [small library](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/tikz/lib.tex) of helper functions for generating the diagrams.
The [`tex` source files](https://gitlab.com/eidoom/njet-ir-notes/-/tree/master/tikz) (which are really just `tikz` environments) are `\input` to a [main `tex` file](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/draw.tex) which is processed with `pdflatex -draft-mode` so that the `pdf` document is not created, but the `tikz` images are rendered and saved individually as `pdf` files via `tikzexternalize`.
These `pdf` diagrams are used directly in the final `pdf` document, and [converted]({{< ref "pdfs#pdf-to-svg" >}}) to `svg` files with [`pdf2svg`](https://github.com/dawbarton/pdf2svg) for the web page.

The equations are, of course, beautifully rendered by `latex` for `pdf`.
For the web, I use [`katex`](https://katex.org/) {{< docs "https://katex.org/docs/supported.html" >}} {{< ghub "KaTeX" "KaTeX" >}}.
It's not as fully featured as the alternative [`mathjax`](https://github.com/mathjax/MathJax), but I've managed to do everything I've wanted so far.
To include images in my equations, I need to enable [`trust`](https://katex.org/docs/supported.html#html), which I add to the `<script>` with a `sed` rule.

## Locally

I used [`entr`](https://github.com/eradman/entr) to write some `make` commands which rebuild whenever changes are detected.
I use `evince` to view `pdf` files, which automatically reloads the updated file.
I host the webpage with [`live-server`](https://www.npmjs.com/package/live-server) for live reloading in the browser too.

On Fedora 32, most of the dependencies can be installed with the package manager:
```shell
sudo dnf install entr texlive-scheme-full pdf2svg
```
The Makefile includes a line to download and install a `katex` release from GitHub for the web page.
`pandoc` and some of its filters are in Fedora's repositories, but they are out of date.
The up-to-date versions can be installed by downloading the pre-compiled binaries from the GitHub releases and moving them to `~/.local/bin`, but it's too easy to enter [dependency hell](https://en.wikipedia.org/wiki/Dependency_hell) that way.
I found the best way was to build and install them using the [Haskell](https://www.haskell.org/) tool [`cabal`](https://en.wikipedia.org/wiki/Dependency_hell).

Pandoc and its filters are written in Haskell, which has a build tool `cabal`.
Haskell pacakges are archived on [Hackage](https://hackage.haskell.org/).
The Haskell environment is installed and updated with 
```shell
sudo dnf install haskell-platform
cabal update
cabal install Cabal cabal-install
```
Then we can install `pandoc` and friends with:
```shell
cabal install pandoc
cabal install pandoc-citeproc pandoc-crossref
```
The final filter, `include-code` is not up-to-date on Hackage, being compiled against an older version of the dependency `pandoc-types` than the other filters we've just compiled.
So, we'll grab it straight from the source:
```shell
git clone git@github.com:owickstrom/pandoc-include-code.git
cd pandoc-include-code
cabal configure
cabal install
```

## With a container

Alternatively, I've [created a Docker image](https://gitlab.com/eidoom/pandoc-physics-image/-/tree/master) with all the dependencies.
It's available [here](https://hub.docker.com/r/eidoom/pandoc-physics).
The Makefile is written such that (use `podman` instead of `docker` on Fedora 32)
```shell
docker pull docker.io/eidoom/pandoc-physics:edge
export PANDOC='podman run -it --rm pandoc-physics:edge pandoc'
```
will make it use the image.

The image is currently [crudely built](https://gitlab.com/eidoom/pandoc-physics-image/-/raw/master/Dockerfile) and so far larger than it needs to be.
I'll rectify this at some point.

## In the cloud

I also have GitLab CI [set up](https://gitlab.com/eidoom/njet-ir-notes/-/raw/master/.gitlab-ci.yml) to build the documents using the [Docker image]({{< ref "#with-a-container" >}}) every time I push an update, the result of which is what the links at the top point to.

+++
title = "Custom ROMs on the Xiaomi Redmi 4 Prime"
date = 2020-06-19T10:10:33+01:00
edit = 2021-04-05
categories = ["smartphone"]
tags = ["operating-system","environment","xiaomi","redmi","redmi-4-prime","markw","linux","fedora","mobile","android","oreo","android-8","install","gapps","rooting","flash","twrp","custom-rom","firmware","android-one","miui","redmi-4-pro","phone"]
description = "Prolonging the lifetime of mobile hardware with custom software"
toc = true
cover = "img/covers/79.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

My mobile phone is a [Xiaomi Redmi 4 Prime](https://www.gsmarena.com/xiaomi_redmi_4_prime-8263.php) (also modelled as the Redmi 4 Pro and codenamed `markw`).
I use "custom ROMs" on it, which is to say I do not use the manufacturer-provided operating system, but instead use community-developed distributions of Android.
This means the software my phone is running is up to date with modern features and security patches years beyond when the manufacturer stopped supporting this model.
Custom ROMS can also provide useful additional functionality.

## Set up fastboot 
To modify the phone's filesystem via USB from a computer, we use the [`fastboot` protocol](https://en.wikipedia.org/wiki/Android_software_development#Fastboot).
To use `fastboot`, switch the phone off then hold `POWER`+`VOLUME DOWN` to reboot it in `fastboot` mode and attach it to the computer with USB.
On the computer, `fastboot` comes as part of the `androids-tools` package:
```shell
sudo dnf install android-tools
```
We also need to [set the `udev` permissions](https://gist.github.com/smac89/251368c8df2ccd645baedc3e1238fdb4), [as per usual]({{< ref "split-keyboard#linux-permissions-for-flashing" >}}).
Find the vendor ID of the phone (it's the four characters after `ID` and before the colon) using `lsusb`:
```shell
lsusb
```
```shell
Bus 004 Device 002: ID 2109:0813 VIA Labs, Inc. USB3.0 Hub
Bus 004 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 003 Device 003: ID cb10:1256  
Bus 003 Device 002: ID 2109:2813 VIA Labs, Inc. USB2.0 Hub
Bus 003 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID 04f3:24a0 Elan Microelectronics Corp. Touchscreen
Bus 001 Device 002: ID 0cf3:e300 Qualcomm Atheros Communications 
Bus 001 Device 008: ID 18d1:d00d Google Inc. Android
Bus 001 Device 004: ID 1bcf:2b95 Sunplus Innovation Technology Inc. Integrated_Webcam_HD
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```
My phone is showing up as `Google Inc. Android`, so the ID is `18d1`.
Then create a new `udev` rule with this ID:
```shell
sudo vi /etc/udev/rules.d/51-android.rules
```
```vim
# Xiaomi Redmi 4 Prime
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", MODE="0666", GROUP="plugdev"
```
and restart `udev`:
```shell
sudo udevadm control --reload-rules
sudo udevadm trigger
```
Now `fastboot` should work.
Test it by listing devices with
```shell
fastboot devices
```
which for me shows
```shell
d123c6bb7d63    fastboot
```
Note that
```shell
no permissions; see [http://developer.android.com/tools/device.html]    fastboot
```
means the `udev` permissions are not set correctly.

## Unlocking

The Redmi 4 Prime, as with most phones, comes from the factory with the bootloader locked.
In this state, it is not possible to install a custom ROM, flash the recovery partition, or root the device.
I unlocked my phone back in January 2017 following a process similar to [this](https://forum.xda-developers.com/redmi-4-prime/development/xiaomi-redmi-4-prime-pro-unlock-t3519811).

For new Xiaomi phones, these steps are now obsolete: it is thankfully no longer necessary to request unlocking permissions and wait for some arbitrary number of days.
Unlocking is achieved by downloading the [Mi Unlock app](https://en.miui.com/unlock/download_en.html) (which unfortunately only supports Windows) and following the instructions (here are guides for [MIUI](https://c.mi.com/thread-1635834-1-1.html) and [Android One](https://miui.blog/any-devices/unlock-bootloader-xiaomi-mi-android-one/)).


In any case, an unlocked phone shows:
```shell
fastboot oem device-info
```
```shell
(bootloader)    Device tampered: false
(bootloader)    Device unlocked: true
(bootloader)    Device critical unlocked: true
(bootloader)    Charger screen enabled: true
(bootloader)    Display panel: 
OKAY [  0.058s]
Finished. Total time: 0.058s
```
Notice both `Device unlocked` and `Device critical unlocked` are true.

## Recovery

The recovery mode can be booted into by holding `POWER`+`VOLUME DOWN`+`VOLUME UP` when the phone is off.
Alternatively, the advanced reboot menu in custom ROMs often offers an option to reboot into recovery.

A custom recovery can be installed by obtaining the `img` and flashing the recovery partition using `fastboot`:

```shell
fastboot flash recovery <recovery.img>
```

I use recovery images from [TWRP](https://twrp.me/).
Unfortunately, TWRP does not support the Redmi 4 Prime, so I have to use unofficial images.
I am currently using `twrp-3.2.3-0-markw-treble.img` by brigudav.
I've noticed a new [version by redispade](https://forum.xda-developers.com/redmi-4-prime/development/recovery-twrp-3-3-1-11-t4030421) with [releases on GitHub](https://github.com/redispade/device_xiaomi_markw_twrp/releases) that [I will update to]({{< ref "#a-new-rom">}}).

## Rooting

I have also [rooted](https://en.wikipedia.org/wiki/Rooting_(Android)) my phone so that I can run specialised apps that require elevated privileges. These include:
* [CF.lumen](https://play.google.com/store/apps/details?id=eu.chainfire.lumen&hl=en) for adjusting the screen to a warmer colour temperature at night
* [Titanium Backup](https://play.google.com/store/apps/details?id=com.keramidas.TitaniumBackup&hl=en) to backup apps and configurations
* [AdAway](https://www.f-droid.org/en/packages/org.adaway/) to block ads

I used [Magisk](https://magisk.me/) to root my phone.
I installed it by flashing the `Magisk-v*.zip` in recovery: I downloaded the `zip` to my phone, rebooted into recovery, then pressed `Install` and selected the `zip`.

## Custom ROM

I find custom ROMs for my phone on [XDA](https://forum.xda-developers.com/redmi-4-prime/development) or [4PDA](https://4pda.ru/forum/index.php?showtopic=794653).
I am currently using [MiracleDroid by razziell](https://4pda.ru/forum/index.php?showtopic=794653&st=11900#entry72210653), which can be downloaded [here](https://yadi.sk/d/xe6Wa3Ai3Voca7).
It is an unofficial build of [MiracleDroid](https://sourceforge.net/projects/miracledroid/) for `markw`.
It is a great ROM: very stable and with excellent battery life.

Sometimes the fingerprint sensor doesn't work after boot, but I just restart the device and that fixes it.

## GApps

Custom ROMs usually don't come with the Google apps infrastructure, including the app store, [Google Play](https://en.wikipedia.org/wiki/Google_Play). 
These packages are provided by [Open GApps](https://opengapps.org/).
They are installed by flashing a `zip` in recovery.
The platform for my phone is `ARM64` and I prefer the `pico` variant.

## A new ROM

Unfortunately, MiracleDroid by razziell is no longer maintained.
It is based on [Android 8.1.0 (Oreo)](https://en.wikipedia.org/wiki/Android_Oreo) with the [1 August 2019](https://source.android.com/security/bulletin/2019-08-01) security patch.
Since then, Android [9 (Pie)](https://en.wikipedia.org/wiki/Android_Pie) and [10 (Q)](https://en.wikipedia.org/wiki/Android_10) has been released and Android has received [monthly security patches](https://source.android.com/security/bulletin/).
Therefore, I felt it was time I updated to a modern distribution.

Sadly, `markw` doesn't seem to be targeted by the AOSP-based Android distribution projects, so I turned again to unofficial builds.
Having used a [version by razziell](https://4pda.ru/forum/index.php?showtopic=794653&st=12660#entry72652457) of [crDroid](https://crdroid.net/) {{< wiki "CrDroid" >}} {{< ghub "crdroidandroid" "android" >}} in the past, I was happy to find [a new version of crDroid by redispade](https://forum.xda-developers.com/redmi-4-prime/development/rom-crdroid-6-0-t4020259).
It is based on Android 10 and seems to be actively maintained.
The latest image is up to date with security patches.
I will try it out to see if it's any good.

Before starting, I backed up all of my apps and data with [Titanium Backup]({{< ref "#rooting" >}}) and copied the files to my computer as a backup.

To install the new ROM, I first had to update the recovery.
I tested the [new TWRP image by redispade]({{< ref "#recovery" >}}) by downloading and live booting it: 
```shell
wget https://github.com/redispade/device_xiaomi_markw_twrp/releases/download/twrp-3.4.0-0-markw/twrp-3.4.0-0-markw.img
fastboot boot twrp-3.4.0-0-markw.img
```
It seemed to work, so I selected `Reboot > Bootloader` to get back into `fastboot` mode and flashed the new recovery image:
```shell
fastboot flash recovery twrp-3.4.0-0-markw.img
```
I then rebooted into system with
```shell
fastboot reboot
```
and selected yes in the pop-up that appears on start regarding giving diagnostic bridge permissions to the host computer.
I used these privileges to reboot into recovery:
```shell
adb reboot recovery
```
Now I was ready to install the new ROM.

I started by wiping the phone with the default factory reset option (data, cache and dalvik), although this is probably redundant as the installation will likey perform a wipe as the first step.
I copied the ROM `zip` over from the computer in `nautilus` file manager.
Then I selected `Install` in TWRP and chose the new `zip`.
It installed successfully so I did `Reboot > System` to check it out.
Everything seemed in order, so I rebooted into recovery using the advanced restart menu (`Hold POWER > Restart > Recovery`).
I copied over the `zip`s for [Magisk]({{< ref "#rooting" >}}) and [GApps]({{< ref "#gapps" >}}) (that I had downloaded onto my computer) to my phone and installed them.
I rebooted back into system and set about configuring the phone.
I used Titanium Backup to restore app data for apps that support this because I'm lazy, but installed apps from Google Play or [F-Droid](https://f-droid.org/en/about/) rather than using the backed-up apps since they are for an older version of Android so may not be compatible.

## Update

So far, the [new ROM](https://forum.xda-developers.com/redmi-4-prime/development/rom-crdroid-6-0-t4020259) seems good after over a month of usage.
Battery life is worse than that of MiracleDroid, but that's worth paying for the improved performance I've noticed and the patches; I was happy to see it received a timely update after the [July security patch](https://source.android.com/security/bulletin/2020-07-01).

I also found a [stats page](https://stats.crdroid.net/markw), which is fun.

## Update 2

I've decided to switch to a different ROM since development seems to have stopped on the unofficial crDroid and I was having some issues: satellite GPS didn't seem to work and battery life was poor.

After a [failed attempt to build one myself]({{< ref "build-android" >}}), I went with [Havok-OS](https://forum.xda-developers.com/redmi-4-prime/development/rom-havoc-os-v2-5-pie-t3931286), which can be downloaded [here](https://sourceforge.net/projects/havoc-os/files/markw/).

+++
title = "Firefox media keys"
date = 2020-09-10T17:41:29+01:00
categories = ["environment-extra"]
tags = ["firefox","fedora","browser","linux","desktop","laptop","music","media","audio","mozilla","short"]
description = "Using hardware media keys in Firefox"
toc = true
cover = "img/covers/67.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I've been using Jellyfin lately for listening to music. 
I [host a server at home]({{< ref "server-install#jellyfin" >}}) and access it through the browser on my computer, which is
```shell
$ firefox --version
Mozilla Firefox 80.0.1 
```
on Fedora 32.

I programmed some [media keys on my keyboard]({{< ref "split-keyboard#custom-keymap" >}}) to control volume and music playback.
Firefox [recently got media key support](https://www.ghacks.net/2020/03/23/firefox-will-soon-support-hardware-media-controls/), but it's disabled by default.
To enable them, go to `about:config`, search for `media.hardwaremediakeys.enabled`, and click the toggle.

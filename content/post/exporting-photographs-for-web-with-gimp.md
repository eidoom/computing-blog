+++
title = "Exporting JPEGs for the web"
date = 2020-06-13T21:08:34+01:00
edit = 2021-05-03
categories = ["web"]
tags = ["gimp","raster-graphics","picture","photograph","graphics-editor","raster-graphics-editor","gnu","linux","fedora","laptop","desktop","image","jpeg","subsampling","image-processing","avif","automate","hsl","rgb","hsb","hsv","optimise","script-fu","scheme","xcf","compression","lossy-compression","png","lossless-compression","webp","imagemagick","meta","jpg"]
description = "Quick editing and compression for phone photographs to be used here"
toc = true
cover = "img/covers/26.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

It's [important](https://developer.mozilla.org/en-US/docs/Learn/Performance/Multimedia) that any images I add to this blog are [optimised for the web](https://web.dev/choose-the-right-image-format/).

## GIMP

My usual workflow for raster images is to take `jpg` photographs with my [phone's]({{< ref "custom-rom" >}}) mediocre camera (apologies), then touch them up in [GIMP](https://www.gimp.org/) {{< wiki "GIMP" >}} {{< glsh "https://gitlab.gnome.org/GNOME/gimp" >}} {{< pl "https://packages.fedoraproject.org/pkgs/gimp/gimp/" >}}.
I use the default camera app that comes with whichever custom ROM I'm using, which has always been the LineageOS Snap camera since all the custom ROMs I've used on this phone have been LineageOS derivates.

I am using
```shell
$ gimp --version
GNU Image Manipulation Program version 2.10.20
```
on Fedora 32. It can be installed with
```shell
sudo dnf install gimp
```

## Editing

### Colour correction

For casual editing, I just rotate the image to align it if necessary, and crop it appropriately.

If the image looks a bit grey, I might apply [contrast stretching](https://theailearner.com/2019/01/30/contrast-stretching/) with [`Colours > Auto > Stretch Contrast HSV`](https://docs.gimp.org/en/gimp-filter-stretch-contrast-hsv.html).
The auto feature sometimes gives overly-corrected results, in which case I do it manually.

{{< figure alt="image-histogram" src="../../img/gimp/colour-levels.webp" caption="GIMP red channel image histogram editor dialog" style="width: 80%; margin: auto auto;">}}

This colour adjustment looks at the [image histogram](https://en.wikipedia.org/wiki/Image_histogram) ([`Colours > Levels...`](https://docs.gimp.org/en/gimp-tool-levels.html) in GIMP) and maps the extremal values of the histogram to the limits of the range, stretching out everything in between.
This is called min-max stretching.
An alternative is to additionally cut out a small percentage of the data at the tails; this is percentile stretching.

{{< figure alt="adjusted-image-histogram" src="../../img/gimp/levels-adjusted.webp" caption="Red channel image histogram after contrast stretching" style="width: 80%; margin: auto auto;">}}

This makes details of the image better resolved by enhancing [contrast](https://en.wikipedia.org/wiki/Contrast_(vision)).

The above screenshots demonstrate the principle.
However, the operation is not performed on the colour channels of an [RGB](https://en.wikipedia.org/wiki/RGB_color_model) space as in this example (and by the similar `White Balance` and `Stretch Contrast...` tools in `Colour > Auto`, which use percentile and min-max stretching respectively).
Instead, the [HSL colour space](https://en.wikipedia.org/wiki/HSL_and_HSV) is used, which encodes colours by their [hue](https://en.wikipedia.org/wiki/Hue) (effectively which wavelength of visible light it is), [saturation](https://en.wikipedia.org/wiki/Colorfulness) (a shadowed object would be desaturated), and [lightness](https://en.wikipedia.org/wiki/Lightness) (also called *value*, hence the V in GIMP's tool name).
This better preserves the original colours.

Another trick to liven up washed-out photographs is adjusting the [chroma](https://en.wikipedia.org/wiki/Munsell_color_system#Chroma), which is accessed at [`Colours > Hue-Chroma...`](https://docs.gimp.org/en/gimp-filter-hue-chroma.html).
This effectively brings colours closer in hue to the nearest primary colour, giving the image more "pop".
It should only be used sparing.

I'm certainly no expert at editing photographs, but these steps seem to help mitigate the results of my phone's budget camera in suboptimal lighting conditions.

### Scaling

Images on this blog are 760 CSS pixels at 100% width on the largest screen size.
Allowing for high DPI screens, for which [2x is fine](https://observablehq.com/@eeeps/visual-acuity-and-device-pixel-ratio) (*i.e.* each dimension of the screen is double the CSS dimension, so a CSS pixel maps to four screen pixels), that means images [should have](https://web.dev/serve-images-with-correct-dimensions/) a maximum width of 1520 pixels.
Hence, I downscale photographs to this with `Image > Scale Image...`.
This includes an [option](https://docs.gimp.org/en/gimp-tools-transform.html#gimp-tool-interpolation-methods) for the [interpolation](https://en.wikipedia.org/wiki/Multivariate_interpolation) method.
These methods (excluding `None`) produce similar results; I use `Cubic`, `LoHalo`, or `NoHalo`.

I take the photographs at the highest resolution my phone camera supports.
I find the resulting image to be superior when downscaled in post-processing rather than taking lower resolution photographs in the first place.

Really, I should [provide responsive images](https://web.dev/serve-responsive-images/), so differently resolutioned images for differently sized screens.
Maybe next time.

I didn't reduce resolution beyond this because compression does [a better job](https://matthews.sites.wfu.edu/misc/graphics/ResVsComp/JpgResVsComp.html) of optimising file size for quality.

## Exporting

### Format

I currently export images in the [`jpg` format](https://en.wikipedia.org/wiki/JPEG).
In future, when there is widespread browser support, I would like to use the [superior](https://netflixtechblog.com/avif-for-next-generation-image-coding-b1d75675fe4) [AVIF](https://aomediacodec.github.io/av1-avif/) {{< wiki "AV1#AV1_Image_File_Format_(AVIF)" >}}, but for now, `jpg` is fine.
I suppose [polyfills](https://en.wikipedia.org/wiki/Polyfill_(programming)) [exist](https://www.npmjs.com/search?q=keywords:AVIF), but I'd prefer to keep things simple.
I can play with `avif` in the meantime using `avifenc` from `libavif-tools` and view in `eog` with `avif-pixbuf-loader`.

On the other hand, there is [another option](https://web.dev/choose-the-right-image-format/) which is currently maybe [the best](https://web.dev/serve-images-webp/) for optimal file size and compatibility: [WebP](https://en.wikipedia.org/wiki/WebP).
I really should use it in place of both the [`png`s](https://en.wikipedia.org/wiki/Portable_Network_Graphics) I use for screenshots ([lossless](https://en.wikipedia.org/wiki/Lossless_compression)) and the `jpg`s discussed in this post ([lossy](https://en.wikipedia.org/wiki/Lossy_compression)).
Perhaps that'll be my next [web]({{< ref "/categories/web" >}}) post.

> {{< cal 2021-04-17 >}}
>
> And [here it is]({{< ref "webp" >}}).

Or perhaps `jpg` really is fine and I should just use [a better encoder library](https://siipo.la/blog/is-webp-really-better-than-jpeg): [`mozjpeg`](https://github.com/mozilla/mozjpeg).
GIMP and ImageMagick (see [below]({{< ref "#imagemagick" >}})) use [`libjpeg`](https://en.wikipedia.org/wiki/Libjpeg)---or rather its fork [`libjpeg-turbo`](https://github.com/libjpeg-turbo/libjpeg-turbo) in many builds, including Fedora's---which `mozjpeg` aims to beat.

Reading online, the competition between modern `jpg` and `webp` doesn't seem to have a clear winner for generic usage.
So, I did some quick checks, with the emphasis on quick.
I used [`cwebp`]({{< ref "webp" >}}) to encode test photographs from my phone in `webp` with default settings, and compared them against similarly sized `jpg` images from [`jpegoptim`]({{< ref "#jpegoptim" >}}) (built against `libjpeg-turbo`).
Here, at least, `webp` was better, with noticeably less artifacting.
A fair test would require using `mozjpeg`, however.
Maybe next time.

For this post, I have stuck with `jpg`, using `libjpeg-turbo` since that's the library my distro ships.

### Options

{{< figure alt="jpg-export" src="../../img/gimp/export.webp" caption="GIMP JPEG export dialog" style="width: 80%; margin: auto auto;">}}

I use the following `jpg` [export settings](https://docs.gimp.org/en/gimp-images-out.html#gimp-using-fileformats-export-dialog): 
* I set the *quality* by eye, generally around 30% is sufficient. I take the photographs on my phone at the highest quality setting (*i.e.* least compression) since I trust this post-processing to do a better job of compression than my phone.
* I don't save any metadata (*Exif*, *XMP*, *IPTC*), mainly because I don't want the blog images to contain [geotags](https://en.wikipedia.org/wiki/Geotagging).
* I enable *optimise* fir optimisation of entropy encoding parameters to minimise the file size.
* *Arithmetic coding* results in a lower file size, however, I do not use it for compatibility.
* While the *progressive* option slightly increases the file size, it is a good idea for web since it allows files to load in a smart way: instead of loading in scanning lines from the top, it starts with a pixelated version of the image which becomes progressively clearer.
* I enable *restart markers* since they allow partial downloads to be resumed after a broken connection.
* I set *[DCT](https://en.wikipedia.org/wiki/Discrete_cosine_transform) method* to float for highest quality. This sets the number type used in the [Fourier-related transform](https://en.wikipedia.org/wiki/List_of_Fourier-related_transforms) in the JPEG algorithm. (`Int` is a better choice here if time is a concern as it produces practically identical results and is faster.)
* I set *[subsampling](https://en.wikipedia.org/wiki/Chroma_subsampling)* to [4:2:0](https://en.wikipedia.org/wiki/Chroma_subsampling#4:2:0) for minimal file size with negligible change in percieved quality. 
We've come across subsampling [before]({{< ref "home-cinema#madvr" >}}).
It is quite cool. 
The image is decomposed as: black-and-white information, or the [luma](https://en.wikipedia.org/wiki/Luma_(video)) channel; and colour information, or [chroma](https://en.wikipedia.org/wiki/Chrominance) channels (this is, unfortunately, a different use of the same word "chroma" from [before]({{< ref "#editing" >}})).
This [colour space](https://en.wikipedia.org/wiki/Color_space) is called [YCbCr](https://en.wikipedia.org/wiki/YCbCr).
The eye is more sensitive to luma detail, so reducing the resolution of the chroma channels allows the file size to be reduced with a generally small effect on quality.
For 4:2:0, the two chroma channels are subsampled by a factor of two both horizontally and vertically (so quartering their resolutions), halving the file size compared to 4:4:4 (no subsampling).
This technique doesn't work well with sharp borders of solid colours, but is well suited to the photographs I deal with.

The [subsampling notation](http://vektor.theorem.ca/graphics/ycbcr/Chroma_subsampling_notation.pdf) of #:#:#, where # is a number, is a little confusing.
The first # is the horizontal sampling reference number for the luma channel and exists only to establish the ratio of luma to chroma sampling.
The second # is the chroma horizontal sampling factor, so 4:2:# means chroma is horizontally subsampled compared to luma as 4:2, or 2:1, which we'd call horizontal subsampling by a factor of 2, or say chroma is sampled half as much as luma horizontally.
The final # is either: the same as the second, indicating no vertical subsampling; or zero, for 2:1 vertical chroma subsampling.

ImageMagick's `identify -verbose` uses a slightly different notation for `jpeg:sampling-factor`, denoting reference width times height sampling for the luma (Y) and two chroma (Cb and Cr) channels respectively. 
In this notation, for example, 4:2:0 becomes 2x2,1x1,1x1.

<!-- Exports look desaturated? -->

I also [found](https://unix.stackexchange.com/a/312946/156381) `jhead` for removing metadata (if I forgot before)
```shell
sudo dnf install jhead
jhead -purejpg $(*.jpg)
```

## Automating

GIMP offers CLI automation with its [Script-Fu](https://docs.gimp.org/en/gimp-scripting.html) tool, which uses [Scheme](https://en.wikipedia.org/wiki/Scheme_%28programming_language%29).
Scripts are saved at `/usr/share/gimp/2.0/scripts` and can be browsed at [`Filters > Script-Fu > Console > Browse...`](https://docs.gimp.org/2.10/en/gimp-filters-script-fu.html).
Custom scripts can be added at `~/.config/GIMP/2.10/scripts`.

### Exporting JPEGs from XCFs

I find exporting GIMP files to be tedious, so I used some scripts to automate this.
This command exports all GIMP projects in the current directory as `jpg`s with the previously discussed [settings]({{< ref "#exporting" >}}) and [dimensions]({{< ref "#scaling" >}}) (assuming photographs are landscape and have greater width than 1520).
```shell
gimp -i -f -b "(with-files \"*.xcf\" (let* ( (height (car (gimp-image-height image))) (width (car (gimp-image-width image))) (ratio (/ 1520 width)) ) (gimp-image-scale image 1520 (* height ratio)) (file-jpeg-save 1 image layer (string-append basename \".jpg\") (string-append basename \".jpg\") 0.3 0 1 1 \"\" 0 1 16 2 )))" -b "(gimp-quit 0)"
```

For documentation, the script `with-files` is in `/usr/share/gimp/2.0/scripts/script-fu-util.scm`, and `file-jpeg-save` and `gimp-image-scale` can be found with the Script-Fu console in GIMP.

There's a caveat here: the `xcf`s must be flattened in GIMP using `Image > Flatten Image`. 
Otherwise, the original image is exported, not the edited image.
I tried adding the scripts `gimp-image-merge-visible-layers` and `gimp-image-flatten`, but neither seemed to work.
Why this behaviour?
I have no idea.

In general, the experience of using Script-Fu was less than savory.
Perhaps that's why [Python-Fu](https://www.gimp.org/docs/python/) exists.

### Processing JPEGs directly

If there's no rotating, cropping, or colour correcting to be done, the GIMP GUI can be skipped altogether with:
```shell
gimp -i -f -b "(with-files \"*.jpg\" (let* ( (height (car (gimp-image-height image))) (width (car (gimp-image-width image))) (ratio (/ 1520 width)) ) (gimp-image-scale image 1520 (* height ratio)) (file-jpeg-save 1 image layer (string-append basename \".edit.jpg\") (string-append basename \".edit.jpg\") 0.3 0 1 1 \"\" 0 1 16 2 )))" -b "(gimp-quit 0)"
```

### ImageMagick

For automated batch processing, there's really no need for GIMP to be used.
A more suitable program is [ImageMagick](https://imagemagick.org) {{< wiki "ImageMagick" >}} {{< ghub "ImageMagick" >}}.

Install it on Fedora 32 with
```shell
sudo dnf install ImageMagick
```
Note that this gives version 6, which is a version behind [the latest release](https://imagemagick.org/script/download.php#unix).
Also, [GraphicsMagick](http://www.graphicsmagick.org/) {{< wiki "GraphicsMagick" >}} {{< src "https://sourceforge.net/p/graphicsmagick/code/ci/default/tree/" >}} is [a fork](https://www.linux.com/news/imagemagick-or-graphicsmagick/) that [may offer higher performance](https://stackoverflow.com/questions/862051/what-is-the-difference-between-imagemagick-and-graphicsmagick).

For example, we can use ImageMagick to process images by
* downscaling
    * using [`resize`](http://www.imagemagick.org/Usage/resize/)
    * to width `1520`
    * [resizing on a linear colour space](http://www.imagemagick.org/Usage/resize/#resize_colorspace)
    * using the default [resample filter](http://www.imagemagick.org/Usage/filter), `Lanczos`
    * applying an [unsharp mask](https://en.wikipedia.org/wiki/Unsharp_masking) to sharpen the image with [recommended settings from the manual](http://www.imagemagick.org/Usage/resize/#resize_unsharp)
* compressing (similar to [before]({{< ref "#exporting" >}}))
    * [`strip`ping](https://imagemagick.org/script/command-line-options.php#strip) off metadata
    * with [quality](https://imagemagick.org/script/command-line-options.php#quality) `30`
    * with [subsampling](https://imagemagick.org/script/command-line-options.php#sampling-factor) `4:2:0`
    * with [progression](https://imagemagick.org/script/command-line-options.php#interlace)
    <!-- * with optimisation -->
    <!-- * with `restart` markers every `16` MCU rows -->

with
```shell
for f in *.jpg; do
  convert $f \
    -strip \
    -quality 30 \
    -sampling-factor 4:2:0 \
    -interlace plane \
    -colorspace RGB \
    -resize 1520 \
    -colorspace sRGB \
    -unsharp 0x0.75+0.75+0.008 \
    ${f%.jpg}.edit.jpg
done
```

### jpegoptim

Another option is [`jpegoptim`](https://github.com/tjko/jpegoptim).
It offers a very simple interface, in contrast to the overwhelmingly customisable ImageMagick.
It can't do any resizing or retouching, but is still useful for compression.

Install it on Fedora 32 with
```shell
sudo dnf install jpegoptim
```
and use to obtain a similar quality level as previously with
```shell
jpegoptim <input_file>.jpg -d <output_directory> -m30 -s --all-progressive
```

+++
title = "Installing LaTeX on Linux"
date = 2021-06-03T11:51:44+01:00
categories = ["typesetting"]
tags = ["linux","latex","texlive","miktex"]
description = ""
toc = true
draft = true
+++

[LaTeX](https://www.latex-project.org/) {{< wiki LaTeX >}} {{< ghub latex3 latex2e >}}

[TeX](https://tug.org/)
{{< wiki TeX >}}

font {{< wiki Computer_Modern >}}

distribution

MiKTeX https://miktex.org/
{{< wiki MiKTeX >}}

Tex Live https://tug.org/texlive/
https://wiki.archlinux.org/title/TeX_Live
{{< wiki TeX_Live >}}
install with dnf, lags 1 year, but easier

engine pdfTex

format pdflatex

bibtex, biber

`latexmk -pdf`

packages
https://ctan.org/pkg/siunitx
https://github.com/josephwright/siunitx
v2:{{< docs "https://texdoc.org/serve/siunitx.pdf/0" >}}

https://ctan.org/pkg/cleveref
https://ctan.org/pkg/mathtools

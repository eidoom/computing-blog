+++
title = "Split album files into tracks"
date = 2021-11-27T16:31:21Z
categories = ["audio"]
tags = ["home","media","flac","music","debian","debian-11","debian-bullseye","cue","cuetools","audio","sound"]
description = "Use the `cue` metadata to put album `flac` files into the more standard track file format"
toc = true
draft = false
cover = "img/covers/40.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Using [`cuetools`](https://packages.debian.org/bullseye/cuetools) and [`shntool`](https://packages.debian.org/bullseye/shntool) {{< ext "https://bbs.archlinux.org/viewtopic.php?id=90278" >}} {{< ext "https://stillstup.blogspot.com/2008/07/split-lossless-audio-ape-flac-wv-wav-by.html" >}} {{< ext "https://danilodellaquila.com/en/blog/how-to-split-an-audio-flac-file-using-ubuntu-linux" >}}

```shell
sudo apt update
sudo apt install cuetools flac shntool
shnsplit -f *.cue -o flac *.flac
cuetag *.cue split-track*.flac
find . ! -name "*.flac" -delete  # careful, this may not be what you want!
rm <original>.flac
```

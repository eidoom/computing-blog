+++
title = "earxiv: exploring arXiv"
date = 2023-06-13T17:39:47+02:00
categories = ["data"]
tags = ["arxiv","api","scrape","harvest","python","python3","python-stdlib","urllib","re","regex","sqlite","sqlite3","sql","db","database","collections","xml","xml.etree.ElementTree","urllib.request","urllib.error","oai-pmh","metadata","analysis","visualisation","mini-project","matplotlib","plot","chart","graph"]
description = "A little project to mess around with arXiv metadata"
toc = true
cover = "img/covers/131.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking, concept art, dramatic, verdant, fantasy, paradise'-'borders writing signature'"
+++

See {{< gl earxiv >}}.

+++
title = "Replacing the SD card on a Kobo Clara HD"
date = 2023-05-30T12:25:33+02:00
categories = ["ereader"]
tags = ["eink","epub","kobo","kobo-clara-hd","epaper","ebook","koreader","quickdic","mobile","eink","firmware","libreria-reader","booky-mcbookface-ereader","pmos","postmarketos","inkbox","alpine","linux","fedora","fedora-38","dd","gdisk"]
description = "Reviving my ereader"
toc = true
cover = "img/covers/120.avif"
coveralt = "stable-diffusion-2:'scifi digital concept art detailed vivid dynamic dramatic'+RealESRGAN_x4plus+ImageMagick"
+++

## Problem 1: failed SD card, or Diagnosis

My [Kobo]({{< ref "kobo" >}}) has been acting up for a while: laggy response to input, randomly crashing.
It finally froze after an update attempt, showing a white blank screen and a flashing power button.

A [manual reset](https://help.kobo.com/hc/en-us/articles/360017605314) attempt failed: no change in status of device.

I [disassembled](https://www.youtube.com/watch?v=hRdV5ukt_Q0) it, resourcefully using a blunt table knife to pry the back off, and removed the [SD card]({{< ref "sd-card" >}}) (SanDisk Edge `MicroSD` `SDHC` `I` `C4` 8GB).

{{< figure src="../../img/kobo/board.avif" position="center" alt="Photograph of the inside of the Kobo Clara HD." caption="Naked Kobo." >}}

{{< figure src="../../img/kobo/old-sd.avif" position="center" alt="Photograph of the old SD card showing its specifications." caption="Old SD card. Number 27, apparently." >}}

I attempted to [clone]({{< ref "utilities#dd" >}}) an image of the card,
```shell
sudo dd if=clara_hd.img of=/dev/mmcblk0 bs=8M status=progress
```
but it failed and returned an error.
The card is clearly faulty and needs to be replaced.

## Stopgap: ebooks on phone

I always feel like I could do without most material things to a greater degree than my fellow conversationalists.
However, I cannot conceive of a life without books.
Short of the opulence of paper, which would be an inconvenience under current circumstances---not limited to that the local bookstores are filled with writing in the wrong language---reading on my [phone]({{< ref "fp3" >}}) for a day will have to suffice.

After connecting with USB, enabling file transfer on the phone, and mounting (automatic on Fedora 38), we can [use](https://askubuntu.com/a/789898) [`rsync`]({{< ref "rsync" >}}) to copy files (luxuriously) via {{< wl "Media Transfer Protocol" >}} (MTP):

```shell
rsync \
    --verbose --progress --omit-dir-times --no-perms --recursive --inplace \
    "<from>" \
    "/run/user/<uid>/gvfs/mtp:host=<name>/<path>"
```

### Apps

And how to read these `epub`s?

#### KOReader

[KOReader](https://koreader.rocks/) {{< ghubn koreader koreader >}} {{< pl "https://f-droid.org/packages/org.koreader.launcher.fdroid/" >}}.
Aimed at eink devices.
Supports built-in offline dictionaries.
Overrides system brightness.
Cumbersome to change brightness.
Not the best solution for a smartphone.

#### Booky McBookface eReader

[Booky McBookface eReader](https://github.com/quaap/BookyMcBookface) {{< pl "https://f-droid.org/en/packages/com.quaap.bookymcbookface/" >}}.
Honourable mention.
Have to press buttons not areas to change pages.

#### Librera Reader 

[Librera Reader](https://librera.mobi/) {{< ghubn foobnix LibreraReader >}} {{< pl "https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/" >}}.
Can send words to [QuickDic]({{< ref "fp3#f-droid-apps" >}}) dictionary.
Favourite.

(I think [MuPDF viewer]({{< ref "fp3#f-droid-apps" >}}) is better for PDFs.)

## Problem 2: failed SD card, or The synergy of proprietary software and obsolescence

Kobo does not make available images for their devices (licenced firmware...) and I cannot clone the old card since it is corrupted.

Even if we [jumped ship]({{< ref "#alternative-oss" >}}) to greener pastures, the factory SD card contains device-specific secrets about the eink panel:

> The unpartitioned space at the beginning of the SD card contains the E-ink panel waveform information. This information is device specific...
>
> Quoted from: [pmOS wiki](https://web.archive.org/web/20230530131458/https://wiki.postmarketos.org/wiki/Kobo_Clara_HD_(kobo-clara)#Installation).

Sadly, any forum files I [could](https://www.mobileread.com/forums/showpost.php?p=3962248&postcount=1) [find](https://4pda.to/forum/index.php?showtopic=931476&st=80#entry85447923) are now unavailable.
It's against the rules of the venerable [mobileread forum](https://www.mobileread.com/forums/) to post links to licenced firmware, I guess.

## Begging for alms: obtaining a disk image

It remains to [appeal](https://www.mobileread.com/forums/showpost.php?p=4327279&postcount=1571) to Internet strangers to send their image privately.
This [thread](https://www.mobileread.com/forums/showthread.php?t=309881) contains hundreds of instances of [two](https://www.mobileread.com/forums/member.php?u=124358) [heroes](https://www.mobileread.com/forums/member.php?u=71989) sending their images to panicking bibliophagists!
I [received](https://www.mobileread.com/forums/showpost.php?p=4327426&postcount=1573) an image within hours of my request.

I will leave the image [here](https://files.eidoom.duckdns.org/kobo_clara_hd.tar.xz) for now, but don't expect that link to last forever.

## The fix: good as new

I explored the local neighbourhood until I found a(n open) shop selling SD cards and picked up a Kingston Canvas Select Plus `MicroSD` `SDHC` `I` `C10`/`U1`/`V10` `A1` 32 GB.
2 GB would have sufficed, but 32 GB was the smallest capacity available.
Perhaps [`A1`]({{< ref "sd-card#random-iops" >}}) is even a beneficial standard here.

{{< figure src="../../img/kobo/new-sd.avif" position="center" alt="Photograph of the new SD card showing its specifications." caption="New SD card." >}}

I copied the new image to the new SD card,
```shell
sudo dd if=/dev/mmcblk0 of=kobo_sdcard.img bs=8M status=progress
```

I used [GNOME Disks](https://wiki.gnome.org/Apps/Disks) to increase the size of the data partition (the third partition `/dev/mmcblk0p3` labelled `KOBOeReader`) to fill up the available space on the new SD card.

I mounted the data partition (can alternatively use the Fedora 38 automount at `/run/media/<user>/KOBOeReader/`) and navigated to `.kobo`,
```shell
sudo mount /dev/mmcblk0p3 ~/mnt
cd ~/mnt/.kobo
```
to bypass registration by [adding a dummy account](https://www.mobileread.com/forums/showpost.php?p=4210907&postcount=85),
```shell
sqlite3 KoboReader.sqlite
```
```sql
sqlite> INSERT INTO user (UserID, UserKey) VALUES (1, '');
```

I also preloaded the [latest update](https://pgaskin.net/KoboStuff/kobofirmware.html),
```shell
cd ~/tmp
wget https://kbdownload1-a.akamaihd.net/firmwares/kobo7/Dec2022/kobo-update-4.35.20400.zip
unzip kobo-update-4.35.20400.zip
cp -r KoboRoot.tgz manifest.md5sum upgrade ~/mnt/.kobo/
```
which gets installed when the Kobo next boots.

While I had the SD card connected to the computer, I copied over my ebook library too,
```shell
cp -r <library> ~/mnt/
```
which gets imported after the next time the Kobo is disconnected from being connected to the computer.

Finally, I `umount`ed all partitions, plugged it into the Kobo, pressed the back back on, and powered up the Kobo.
After it finished updating, it started fine.
I connected it to the computer via USB then immediately disconnected it to force detection of sideloaded ebooks.
I fiddled with the settings to get it as I liked it.
Resurrection complete!
Now please excuse me, I have a novel to get back to.

Bonus: [megathread of hacks](https://www.mobileread.com/forums/showthread.php?t=217160).

## Alternative OSs

### InkBox 

Sadly, [InkBox](https://inkbox.ddns.net/) {{< glsh "https://inkbox.ddns.net/git/explore" >}} doesn't currently support the Kobo Clara HD.
Discussion on [Discord](https://discord.com/invite/uSWtWbY23m).

### postmarketOS

[pmOS](https://postmarketos.org/) {{< wiki postmarketOS >}} {{< glsh "https://gitlab.com/postmarketOS" >}} (based on [Alpine](https://www.alpinelinux.org/) {{< wiki "Alpine Linux" >}} {{< glsh "https://gitlab.alpinelinux.org/alpine" >}}) [supports](https://wiki.postmarketos.org/wiki/Kobo_Clara_HD_(kobo-clara)) it.
Could work well with [KOReader]({{< ref "#koreader" >}}).

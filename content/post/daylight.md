+++
title = "Daylight: orbit simulation"
date = 2023-06-13T17:56:22+02:00
categories = ["simulation"]
tags = ["lua","n-body","solar-system","planets","orbits","physics","physics-engine","python","matplotlib","gravity","force","position","velocity","speed","acceleration","collisions","collision-detection","velocity-verlet","numerical-integration","centripetal","stars","python3","luajit","kepler","newton","classical-mechanics","angular-momentum","maths","geometry","equations","ellipse","circle","mini-project","dynamics","particles","kinetics","rigid-body","ideal-particle","point-particle","2d"]
description = "Fun with physics"
toc = true
cover = "img/covers/132.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking, concept art, dramatic, verdant, fantasy, paradise, ocean, seascape, coastline'-'borders writing signature'"
+++

I'm playing around with simulating planetary orbits and measuring quantities in the simulation at {{< gl daylight >}}.

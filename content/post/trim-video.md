+++
title = "Trim video"
date = 2020-12-16T20:08:48Z
edit = 2021-02-24
categories = ["environment-extra"]
tags = ["ffmpeg","linux","terminal","video"]
description = "Quick how-to on trimming videos"
toc = true
cover = "img/covers/64.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
Use `ffmpeg`:
```shell
sudo dnf install ffmpeg
```

[Don't re-encode](https://stackoverflow.com/a/42827058) by setting `-c copy`.
Set start time with `-ss` and finish time with `-to`:
```shell
ffmpeg -i <input file> -c copy -ss HH:MM:SS -to HH:MM:SS <output file>
```

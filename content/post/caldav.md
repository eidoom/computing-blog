+++
title = "Self-hosting calendar and contacts with Radicale"
date = 2021-09-03T10:31:04+01:00
categories = ["self-host"]
tags = ["de-google","caldav","carddav","radicale","davx5","evolution","contacts","calendar","nginx","tls","ssl","lets-encrypt","reverse-proxy","journal","memos","tasks","dynamic-dns","port-forwarding","https","hosting","infrastructure","backend","freedns","duckdns","noip","server","address-book","dairy","jtx-board","notes","journals","task-list","birthday-calendar","birthday-adapter","cron","cronjob","cron-job","systemd-timer","systemd","systemd-service","certbot","home","home-server"]
description = "Claiming ownership of another district of my online presence"
toc = true
draft = false
cover = "img/covers/8.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

> DEPRECATED
>
> {{< cal 2022-05-03 >}}
>
> See [this]({{< ref "cloud#__radicale" >}}).

I decided to start afresh with my online calendar and address book.
I wanted to self-host a calendar/contacts server on my [home server machine]({{< ref "server-install" >}}) that could be accessed by a standard protocol from a variety of clients on different platforms.

## CalDAV and CardDAV

For the protocol, I found [CalDAV](https://datatracker.ietf.org/doc/html/rfc4791) {{< wiki CalDAV >}} and [CardDAV](https://datatracker.ietf.org/doc/html/rfc6352) {{< wiki CardDAV >}}, which seem to be the lingua francas for calendars and contacts respectively.

## Server

There are many servers with {{< wl "free-software licences" "free-software license" >}} that implement these protocols, such as those on {{< wl "this list on Wikipedia" "Comparison_of_CalDAV_and_CardDAV_implementations#Server_implementations" >}}.
I found [a blog](https://www.williamjbowman.com/blog/2015/07/24/setting-up-webdav-caldav-and-carddav-servers/) which discusses some of them.

I chose [Radicale](https://radicale.org/3.0.html) {{< ghub Kozea Radicale >}} {{< pypi "Radicale" >}} {{< arch "Radicale" >}}
{{< pl "https://wiki.debian.org/Radicale" >}} mainly because I enjoy vegetable puns as software names.

Another option is [Nextcloud](https://nextcloud.com/) {{< wiki Nextcloud >}} {{< ghub nextcloud server >}}, which aims to provide an entire home cloud platform.
I'd rather have the control and flexibility to pick and choose each component of my "home cloud", even though it probably means more headaches too.

### Radicale

To install Radicale on Debian, we use the package [`radicale`](https://packages.debian.org/stable/radicale) and set appropriate file permissions
```shell
sudo apt install radicale
sudo chown -R radicale:radicale /var/lib/radicale
```
To handle passwords with Radicale (as opposed to [authentication through a reverse proxy](https://radicale.org/3.0.html#tutorials/reverse-proxy/manage-user-accounts-with-the-reverse-proxy)), we also need `apache2-utils` for `htpasswd` and `python3-bcrypt` for encryption (the most secure option)
```shell
sudo apt install apache2-utils python3-bcrypt
```
then we can create a user `<user>` and set their password in the resultant prompt
```shell
sudo htpasswd -c -B /etc/radicale/users <user>
```
`<user>`'s password can be reset with
```shell
sudo htpasswd -B /etc/radicale/users <user>
```
We configure Radicale as
```shell
sudo vim /etc/radicale/config
```
```cfg
...
[server]
hosts = 127.0.0.1:5232
...
[auth]
type = htpasswd
htpasswd_filename = /etc/radicale/users
htpasswd_encryption = bcrypt
...
```
putting it on port 5232 with `localhost` access.

Then `start` and `enable` the server
```shell
sudo systemctl start radicale.service
sudo systemctl enable radicale.service
```

### Importing existing calendar and contacts

I previously kept my calendar and contacts in the Google ecosystem.
Thankfully, they can easily be exported.
At <https://contacts.google.com>, click `Export` in the side menu (`Main menu`) and export as `vCard (for iOS Contacts)`, which will provide you with a `.vcf` file.
At <https://calendar.google.com>, navigate to `Settings menu (cog symbol) > Settings > Import & Export > Export` to get an `.ics` file.
Import these on the Radicale web interface.

### Backup

We'll [version control our calendar and contacts with Git](https://radicale.org/3.0.html#tutorials/versioning-with-git).

We may have to set up keys for the `radicale` user for authenticating with the `git` remote
```shell
sudo -u radicale bash -c 'ssh-keygen -t ed25519'
```
and add the public key
```shell
sudo cat /var/lib/radicale/.ssh/id_ed25519.pub
```
to the `git` remote (note that `radicale`'s home directory is at `/var/lib/radicale`).
Then we can set up a `git` repo in the `radicale` collections directory
```shell
cd /var/lib/radicale/collections
sudo -u radicale bash -c 'git init --initial-branch=main'
sudo -u radicale bash -c 'git remote add origin <remote url>'
```
We'll ignore `radicale` internals and ssh keys
```shell
sudo -u radicale bash -c 'vim .gitignore'
```
```vim
.Radicale.cache
.Radicale.lock
.Radicale.tmp-*
.ssh/
*.swp
.viminfo
.gitconfig
.lesshst
```
and add everything to the repo
```shell
sudo -u radicale bash -c 'git add --all'
```
Before we commit, we need to identify the user
```shell
sudo -u radicale bash -c 'git config --global user.name "radicale@<hostname>"'
sudo -u radicale bash -c 'git config --global user.email "<email>"'
```
then
```shell
sudo -u radicale bash -c 'git commit -m "Initial commit"'
sudo -u radicale bash -c 'git push -u origin main'
```
We also set up `radicale` to automatically update the local repo and push the changes to the remote every time anything is changed
```shell
sudo vim /etc/radicale/config
```
```cfg
...
[storage]
hook = git add -A && (git diff --cached --quiet || git commit -m "Changes by "%(user)s)
```

To push changes to the remote, one can either
* Add `&& git push` to the `storage` `hook` command above, although this will lead to [HTTP 500 errors](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/500) if the remote is unavailable for some reason
* Manually run
    ```shell
    cd /var/lib/radicale/collections
    sudo -u radicale bash -c 'git push'
    ```
* Set up a [`systemd` timer](https://wiki.archlinux.org/title/Systemd/Timers) to automatically run the `push` on a regular interval, say once a day. This is my preferred solution.

We restart the service to load the new settings
```shell
sudo systemctl restart radicale.service
```

#### Update
{{< cal "2024-04-22" >}}

To use the `systemd` timer method, with

* the repo root at `/var/lib/radicale` (this offers better compatibility than the root inside `collections/` as above for using alternative locations through `radicale --storage-filesystem-folder=...`),
* no storage hook in the radicale config,

create the following files:

* `/var/lib/radicale/backup.sh`

    ```shell
    #!/usr/bin/env sh

    git add --all

    git diff --cached --quiet || (git commit -m "Daily backup" && git push)
    ```

* `/etc/systemd/system/radicale-backup.timer`

    ```systemd
    [Unit]
    Description=Backup radicale collections every day at 2:13:23

    [Timer]
    OnCalendar=*-*-* 2:13:23
    Persistent=true

    [Install]
    WantedBy=timers.target
    ```

* `/etc/systemd/system/radicale-backup.service`

    ```systemd
    [Unit]
    Description=Backup radicale collections

    [Service]
    Type=oneshot

    User=radicale
    Group=radicale

    WorkingDirectory=/var/lib/radicale/
    ExecStart=/var/lib/radicale/backup.sh
    ```

Then

```shell
sudo systemctl daemon-reload
sudo systemctl enable radicale-backup.timer
sudo systemctl start radicale-backup.timer
```

Run the service immediately to test it with
```shell
sudo systemctl start radicale-backup.service
```

## Reverse proxy

We will [secure Radicale behind a reverse proxy](https://radicale.org/3.0.html#tutorials/reverse-proxy) and make it accessible over the Internet.
We will [configure Nginx as a reverse proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/).

### Nginx

To install [Nginx](https://www.nginx.com/)
{{< docs "https://nginx.org/en/docs/" >}}
{{< wiki Nginx >}}
{{< arch "Nginx" >}}
{{< pl "https://wiki.debian.org/Nginx" >}}
on Debian, we'll use the 
[`nginx-light`](https://packages.debian.org/stable/nginx-light)
package, then [set it up as a reverse proxy](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/).

```shell
sudo apt install nginx-light
```
We'll disable the default site
```shell
sudo rm /etc/nginx/sites-enabled/default
```
set up a landing page that just welcomes you to the address `<domain>:<port>`
```shell
vim /home/<user>/www/index.html
```
```html
<!DOCTYPE html>
<html>
<head>
<title><domain>:<port></title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>You have arrived at <domain>:<port></h1>
<p>Welcome.</p>
</body>
</html>
```
and set up the reverse proxy
```shell
sudo vim /etc/nginx/sites-available/reverse_proxy.conf
```
```nginx
server {
    listen <port> default_server;
    listen [::]:<port> default_server;

    root /home/<user>/www;

    index index.html;

    server_name _;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    location /radicale/ {  # The trailing / is important!
        proxy_pass        http://127.0.0.1:5232/;  # The / is important!
        proxy_set_header  X-Script-Name /radicale;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header  Host $http_host;
        proxy_pass_header Authorization;
    }
}
```
I had to put the reverse proxy on port `<port>` because [port 80 was already in use]({{< ref "#forward-port" >}}).
The proxy shows a landing page at the root `/` and at `/radicale/` gives access to our Radicale server on port 5232.

We enable the reverse proxy server
```shell
sudo ln -s /etc/nginx/sites-available/reverse_proxy.conf /etc/nginx/sites-enabled
```

We start up `nginx` and check it's working
```shell
sudo systemctl enable nginx.service
sudo systemctl start nginx.service
sudo systemctl status nginx.service
```

### Forward port

I'm hosting this thing on a server at home.
To access the reverse proxy over the Internet, I forwarded the port on my router by setting a TCP protocol rule to any external IP address from my server IP address with the port `<port>`.
My crappy TalkTalk (TT) router doesn't allow you to forward {{< wl "port 80" "List_of_TCP_and_UDP_port_numbers#Well-known_ports" >}} as it's in use (as the "internal service port") and this can't be changed (or so the TT forums suggest; `Route > IPv4 Service Route` looks broken), so I kept the external port the same as the internal.
This as annoying as we'll have to enter the port explicitly when entering the address.
The settings are at `Advanced Configuration > Forward rules > IPv4 Port Mapping`.

### Dynamic DNS

Not having a static IP address, we'll use {{< wl "dynamic DNS" >}} (DDNS) to provide a URL for our reverse proxy.
There are [several options](https://www.maketecheasier.com/best-dynamic-dns-providers/) for free DDNS services.

I ended up using DuckDNS in the end, but have included the steps for the other services in [an appendix]({{< ref "#alternative-ddns-services" >}}) for posterity.

#### DuckDNS

[DuckDNS](https://www.duckdns.org/) is a free DDNS service.

I set it up by installing a cron job on my server as described [here](https://www.duckdns.org/install.jsp) to tell the DuckDNS nameserver my (dynamic) IP address every five minutes.

> Alternatively, we can use `systemd-timer`.
>
> ```shell
> mkdir ~/duckdns
> ```
>
> ```shell
> vim ~/duckdns/duck.sh
> ```
> ```shell
> #!/usr/bin/env sh
> 
> domains=<domains> # comma separated list of subdomains with respect to duckdns.org
> token=<token>
> 
> echo url="https://www.duckdns.org/update?domains=$domains&token=$token&ip=" | curl -k -o ~/duckdns/duck.log -K -
> ```
>
> ```shell
> chmod +x ~/duckdns/duck.sh
> ```
>
> ```shell
> sudo vim /etc/systemd/system/duckdns.timer
> ```
> ```systemd
> [Unit]
> Description=Update IP address for DuckDNS every 5 minutes
> 
> [Timer]
> # OnUnitActiveSec=5m  # broken? https://github.com/systemd/systemd/issues/6680
>
> OnCalendar=*-*-* *:0/5
> Persistent=true
> 
> [Install]
> WantedBy=timers.target
> ```
>
> ```shell
> sudo vim /etc/systemd/system/duckdns.service
> ```
> ```systemd
> [Unit]
> Description=Update IP address for DuckDNS
> 
> [Service]
> Type=oneshot
> 
> User=ryan
> Group=ryan
> 
> WorkingDirectory=/home/ryan/duckdns/
> ExecStart=/home/ryan/duckdns/duck.sh
> ```
>
> ```shell
> sudo systemctl enable duckdns.timer
> sudo systemctl start duckdns.timer
> ```

My address is `http://<subdomain>.duckdns.org:<port>/`.

### TLS

Since we have authentication going on at `http://<domain>:<port>/radicale`, we should use a secure {{< wl "HTTPS" >}} connection.

{{< wl "HTTP" "Hypertext_Transfer_Protocol" >}} is a {{< wl "request-response" >}} protocol for website servers to talk to web browsers (or {{< wl "other clients" "User_agent" >}}).
It is an {{< wl "application layer" >}} protocol which overlies a {{< wl "transport layer" >}} protocol, usually {{< wl "TCP" "Transmission_Control_Protocol" >}}.
HTTPS is a protocol which encrypts HTTP with {{< wl "TLS" "Transport_Layer_Security" >}}, a {{< wl "public-key" "Public-key_cryptography" >}} {{< wl "cryptographic protocol" >}}.
The encryption provides **privacy**, *i.e.* it protects against eavesdropping on content (although not domains and addresses), and **integrity**, *i.e.* preventing tampering like {{< wl "man-in-the-middle attacks" >}}.
The protocol also includes authenticity verification of the server's {{< wl "public-key  certificate" >}} by a trusted {{< wl "certificate authority" >}}, which proves **identity**.

[Let's Encrypt](https://letsencrypt.org/) {{< wiki "Let's_Encrypt" >}} {{< pl "https://wiki.debian.org/LetsEncrypt" >}} is the de facto free certificate authority. 
We can use them to sign HTTPS certificates for our site.
([ZeroSSL](https://zerossl.com/) is an alternative.)

Usually, that's simply a matter of using [Certbot](https://certbot.eff.org/) {{< arch "Certbot" >}}.
[On Debian](https://certbot.eff.org/lets-encrypt/debianbuster-nginx), it's installed with the [`certbot`](https://packages.debian.org/stable/certbot) package, along with the Nginx plugin [`python3-certbot-nginx`](https://packages.debian.org/stable/python3-certbot-nginx), and run as
```shell
sudo apt install certbot python3-certbot-nginx
sudo certbot --nginx -d <domain>
```
However, this requires HTTP access on port 80, which is blocked by our awful router.
So we have to turn to alternative means: the DNS-01 method of the [`acme.sh`](https://github.com/acmesh-official/acme.sh) shell script (named after the {{< wl "Automated Certificate Management Environment" >}} protocol) is [lauded](https://jmorahan.net/articles/lets-encrypt-without-port-80/) as our answer.

```shell
git clone https://github.com/acmesh-official/acme.sh.git
cd acme.sh
./acme.sh --install --email <email>
# log out and in again
acme.sh --set-default-ca --server letsencrypt
```
DuckDNS is [supported by `acme.sh`](https://github.com/acmesh-official/acme.sh/wiki/dnsapi#27-use-duckdnsorg-api) with
```shell
export DuckDNS_Token="<token>"
acme.sh --issue --dns dns_duckdns --domain <domain>
```

Then I generated the certificates for Nginx
```shell
mkdir ~/.acme.sh/keys
acme.sh --install-cert --domain <domain> --key-file ~/.acme.sh/keys/<domain>-key.pem --fullchain-file ~/.acme.sh/keys/<domain>-cert.pem --reloadcmd "sudo systemctl reload nginx.service"
```
wrote a new configuration to use them
```shell
sudo vim /etc/nginx/sites-available/reverse_proxy_https.conf
```
```nginx
server {
    listen <port> ssl http2 default_server;
    listen [::]:<port> ssl http2 default_server;

    ssl_certificate /home/ryan/.acme.sh/keys/<domain>-cert.pem;
    ssl_certificate_key /home/ryan/.acme.sh/keys/<domain>-key.pem;

    root /home/ryan/www;

    index index.html;

    server_name <domain>;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    location /radicale/ {
        proxy_pass        http://127.0.0.1:5232/;
        proxy_set_header  X-Script-Name /radicale;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header  Host $http_host;
        proxy_pass_header Authorization;
    }
}
```
and switched over
```shell
sudo rm /etc/nginx/sites-enabled/reverse_proxy.conf
sudo ln -s /etc/nginx/sites-available/reverse_proxy_https.conf /etc/nginx/sites-enabled
sudo systemctl reload nginx.service
```

Now my landing page is accessible only at `https://<domain>:<port>/` and Radicale at `https://<domain>:<port>/radicale`.

### Update

{{< cal 2023-02-23 >}}

I switched over my home server to [one in the cloud]({{< ref cloud >}}), with [Radicale](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__radicale/manifest) set up.
Initially, I [had it reverse proxied](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/main/type/__radicale/manifest) at `https://<domain>/radicale`.
I later changed to using [subdomains](https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__tls_wildcard_duckdns), using the `nginx` configuration,
```shell
sudo vim /etc/nginx/sites-available/radicale.conf
```
```nginx
server {
    listen 80;
    listen [::]:80;

    server_name radicale.<domain>;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name radicale.<domain>;

    ssl_certificate /etc/letsencrypt/live/<name>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<name>/privkey.pem;

    location / {
        proxy_pass        http://localhost:5232/;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header  Host $http_host;
        proxy_pass_header Authorization;
    }
}
```
Enabled with
```shell
cd /etc/nginx
sudo ln -s $PWD/sites-available/radicale.conf sites-enabled/
sudo systemctl reload nginx
```

## Clients

As desired, there are also a number of {{< wl "available clients" "Comparison_of_CalDAV_and_CardDAV_implementations#Client_implementations" >}}.
Radicale exposes:
* CalDAV
    * calendar
    * tasks/todos
    * journal/memos
* CardDAV
    * address book

### Android

On Android, we can use [DAVx5](https://www.davx5.com/) {{< glab bitfireAT davx5-ose >}} {{< pl "https://f-droid.org/packages/at.bitfire.davdroid/" >}} (["xxxxx" does not stand for "droid"](https://www.davx5.com/faq/what-does-davx5-stand-for)) to [synchronise](https://www.davx5.com/faq/system-integration) the Radicale CardDav/CalDAV collections with the Android system app content providers (Contacts, Calendar).
Set up DAVx5 and refresh then wait a bit and they'll appear in Contacts and Calendar (system or your favourite).

[Dominik Schürmann's](https://www.schuermann.eu/about/) Birthday Adapter {{< ghubn "SufficientlySecure/birthday-calendar" >}} {{< pl "https://f-droid.org/packages/org.birthdayadapter/" >}} will automatically create a calendar with the birthdays from our contacts.

For journals, notes, and tasks, there is [jtx Board](https://jtx.techbee.at/) {{< glab techbeeat1 jtx >}} {{< pl "https://f-droid.org/en/packages/at.techbee.jtx/" >}}.

(An alternative for tasks is [Tasks](https://tasks.org/) {{< ghub tasks tasks >}} {{< pl "https://f-droid.org/en/packages/org.tasks/" >}}.)

### Linux

We can use [Evolution](https://wiki.gnome.org/Apps/Evolution/) {{< wiki GNOME_Evolution >}}, GNOME's personal information management software, to manage communication with the Radicale server on the Linux desktop.
Install everything with
```shell
sudo dnf install evolution gnome-calendar gnome-contacts 
```

~~Add the address book at `Evolution > Edit > Accounts > Add > Address Book`, and similarly for Calendar, Task List, and Memo List.~~
Add the Radicale server at `Evolution > Edit > Accounts > Add > Collection Account`, entering the `User name` and `Advanced Options > Server` address while checking only `Look up for a CalDAV/CardDAV server`, then the password after `Look Up`.
This also syncs the prettier front ends GNOME Contacts and GNOME Calendar.

In Evolution's calendar view, there is a calendar `Contacts > Birthday & Anniversaries`.
If you right click and select `Properties` then check `CardDAV > <name of Radicale-hosted address book>`, it will automatically populate the calendar with the birthdays of contacts.

There are some annoyances:
* Contacts only shows all accounts; you can't filter by account
* While viewing events in Calendar is great, it has a latency when changing events, especially recurring events, so I find creating or editing events in Evolution to be superior.
* Sadly [GNOME Todo](https://wiki.gnome.org/Apps/Todo) (Fedora package `gnome-todo`) [doesn't support CalDAV](https://gitlab.gnome.org/GNOME/gnome-todo/-/issues/45), so we're stuck without a slick interface and have to use Evolution.

## Appendices

### Alternative DDNS services

#### NoIP

I created the public address `http://<subdomain>.ddns.net` on [NoIP](https://www.noip.com/) (free with manual monthly renewal) and pointed it at my router.

I [configured the router for DDNS](https://www.noip.com/support/knowledgebase/how-to-configure-ddns-in-router/).
On my TT router, this is under `Advanced Configuration > Application > DDNS Function`.

NoIP does have a Port 80 Redirect mode.
However, I don't like it as it redirects to the IP address with port, not the URL with port.
Unfortunately, it also doesn't help the TLS issues [below]({{< ref "#tls" >}}) as TLS doesn't support the redirect.

So, now we can access the reverse proxy at `http://<subdomain>.ddns.net:<port>`.

This service has some problems: 
* the manual monthly renewal
* it isn't supported by `acme.sh` (see [above]({{< ref "#tls" >}}))
* some DDNS services are firewalled by my work network, and it catches the `ddns.net` domain

#### FreeDNS

From looking briefly at the [FreeDNS](https://freedns.afraid.org/) website, I like the community feel of the project.
I set up a subdomain:
* sign up
* registry - choose domain
* subdomain
* takes a while...
* `http://<subdomain>.mooo.com:<port>`

For HTTPS with `acme.sh`, see [this](https://github.com/acmesh-official/acme.sh/wiki/dnsapi#15-use-freedns).

+++
title = "Tracking large files with Git LFS"
date = "2019-12-15T12:15:44Z"
edit = 2020-01-08
tags = ["environment","linux","git","server","desktop","laptop","version-control","git-lfs","lfs","debian"]
categories = ["environment-extra"]
description = "Tracking files with Git that I probably shouldn't"
toc = true
cover = "img/covers/29.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Warning!
{{< cal "2022-10-20" >}}

This old post is a story of things that should not be done!
Git LFS is a great tool for tracking binary files, and can be [set up like this]({{< ref "#setting-up-lfs" >}}) and [used like this]({{< ref "#new-files" >}}), but everything else in this post is dangerous and to be avoided!

## A problem

I version control and backup my configuration files in `$HOME` using `git`.
The public subset is in a submodule [here](https://gitlab.com/eidoom/dotfiles-public).

> {{< cal "2022-10-20" >}}
>
> I no longer use this setup.
> My new dotfiles are listed [here](https://gitlab.com/eidoom/dotfiles-active).

On a recent `git push` on my Debian server, the remote (GitHub) rejected the push because one of my database files had exceed the file size limit of 100MB.
This was a clear sign that
* This is not what `git` should be used for.
* I should get around to setting up binary file tracking properly.
* Seriously, `git` is *not* a good solution for large binary files like this database (unless you really want perpetual versions, I guess).

## Cleaning up

Since I had a few local commits with the offending file, I first removed it from history.
I used `git obliterate` from [`git-extras`](https://github.com/tj/git-extras) for this, with `--cached` so as not to delete the local file.

```shell
sudo apt install git-extras
cd REPO_DIR
git obliterate --cached path/to/big/db/file
```

> {{< cal 2022-01-13 >}}
>
> This method is deprecated in favour of using the tool `git-filter-repo` {{< ghubn "newren/git-filter-repo/" >}} {{< docs "https://htmlpreview.github.io/?https://github.com/newren/git-filter-repo/blob/docs/html/git-filter-repo.html" >}}.
> It can be installed with `pip`
> ```shell
> python3 -m pip install git-filter-repo
> ```
>  Make a copy of the file and
> ```shell
> git filter-repo --path path/to/big/db/file --invert-paths --force
> ```
> This will delete the `git` remotes because we have completely changed the history of the original repository.
> `git` is not designed to be used like that!
> We should consider this new repository as a separate entity from the old one, with different remotes (usually, a new `origin`).
> Force pushing to the original `origin` and synchronising to other repository instances will lead to a host of headaches.

## Setting up LFS

[Git Large File Support](https://github.com/git-lfs/git-lfs) (LFS), or `git lfs` [optimises the treatment of large files](https://www.atlassian.com/git/tutorials/git-lfs).
In short, it only downloads the latest version of LFS-tracked files on `clone`.
Older versions are only downloaded on `checkout`.

It works well when using `git` with an LFS-supporting host like

* [BitBucket](https://bitbucket.org/)
* [Codeberg](https://codeberg.org/)
* [GitBucket](https://gitbucket.github.io/)
* [GitHub](https://github.com/)
* [GitLab](https://gitlab.com/)
* [Gitea](https://gitea.io/)
* [Gogs](https://gogs.io/)

Note that the following do not currently support LFS

* [Sourcehut](https://sourcehut.org/)
* [SourceForge](https://sourceforge.net/)

Also, we should remember `git` does not have to be used with a host, but can be used as a properly distributed peer-to-peer service too by syncing between repo instances directly.

LFS does not come by default with `git` and can be set up on Debian with
```shell
sudo apt install git-lfs
git lfs install
```
Note that it must be installed on any new machine before you clone the repo to it, otherwise you'll wonder where your binaries have gone.

### New files

I set up LFS to track the binary files in my repository.
If I was going to use LFS to track new files, I could do

```shell
git lfs track "*.zip" "*.pickle" "*.db" # configures .gitattributes
git add .gitattributes <paths/to/new/large/files>
git commit -m "Add LFS"
```

### Existing files

However, since I was dealing with existing files, I also had to migrate these files in the repository's history to LFS.

```shell
git lfs migrate import --everything --include="*.zip"
git lfs migrate import --everything --include="*.db"
git lfs migrate import --everything --include="*.pickle"
```

This didn't work for some reason with the offending database file, which is why I deleted it above.

Then, I added back the large database file with `*.db` files being tracked automatically by LFS

```shell
git add <path/to/big/db/file>
git commit -m "Added back LFS-tracked db file"
```

Since I changed history with the migrate, pushing to the remote required

```shell
git push -f
```

However, I did something else.

> {{< cal 2019-12-23 >}}
>
> I also followed this to track output pdfs and image files in some `latex` projects.
> I found that GitLab protects the remote `master` branch by default, so force-pushes to `master` are rejected unless it's unprotected at `Settings > Repository > Protected Branches`.

## Changing remote

Now, GitHub only gives [1GB of LFS bandwidth per month for free](https://help.github.com/en/github/managing-large-files/about-storage-and-bandwidth-usage).
GitLab, on the other hand, doesn't limit the bandwidth.
GitLab does have [a size limit of 10GB per project for LFS files](https://gitlab.com/gitlab-com/www-gitlab-com/issues/1003), versus [GitHub's maximum repository size limit of 100GB](https://help.github.com/en/github/managing-large-files/what-is-my-disk-quota), but I need the bandwidth and I don't need more capacity. 
So, time to jump ship.

```shell
git remote rm origin 
git remote add origin git@gitlab.com:USER/REPO.git
git push -u origin --all
git push -u origin --tags
```

and on other computers, I just [changed the origin]({{< ref "git-reference#change-remote" >}}) and pulled.

> {{< cal "2022-10-20" >}}
>
> With [GitLab having introduced a 5GB limit on free tier namespaces](https://about.gitlab.com/pricing/faq-efficient-free-tier/#storage-limits-on-gitlab-saas-free-tier), I looked into reducing the size of my repos there.
> It turns out there is no way to remove LFS files from a GitLab repo once they're on there, other than rewriting history locally, pushing to a new repo, and deleting the old one.

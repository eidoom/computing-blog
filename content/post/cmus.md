+++
title = "Music in the terminal with cmus"
date = 2021-06-30T13:04:50+01:00
edit = 2021-07-11
categories = ["audio"]
tags = ["music","media","laptop","desktop","terminal","cmus","sound","audio","fedora","fedora-34","cli"]
description = "Simple"
toc = true
draft = false
cover = "img/covers/2.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Long have I searched for my favourite way to play music from my local file server.
Recently, I've rather liked a simple solution.
I mount the NFS share
```shell
sudo mount ryanserver:/ ~/mnt
```
and play it in the terminal with [`cmus`](https://cmus.github.io/) {{< ghub cmus cmus >}} {{< arch "Cmus" >}} {{< ext "https://www.increasinglyadequate.com/cmus.html" >}}, which can be installed on Fedora 34 with
```shell
sudo dnf install cmus
```
and invoked with `cmus`.
The usage is explained by
```shell
man cmus-tutorial
```

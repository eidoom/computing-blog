+++
title = "SVP on Linux"
date = 2021-06-13T09:02:14+01:00
edit = 2021-06-27
categories = ["video"]
tags = ["rpmfusion","linux","fedora","mpv","video","desktop","laptop","media","interpolation","install","configure","fedora-34","svp"]
description = "Setting up SVP on Fedora 34"
toc = true
draft = false
cover = "img/covers/1.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[SVP](https://www.svp-team.com/) is a motion-based temporal video frame interpolation package.
I previous [discussed interpolation]({{< ref "linux-video#interpolation" >}}) and [SVP on Windows]({{< ref "home-cinema#svp" >}}).
Here, we install it [on Linux](https://www.svp-team.com/wiki/SVP:Linux) {{< arch "mpv#SVP_4_Linux_(SmoothVideoProject)" >}}, specifically Fedora 34, using [`mpv` as the video player](https://www.svp-team.com/wiki/SVP:mpv).

I [already covered `mpv` installation on Fedora]({{< ref "linux-video#installation" >}}).
This RPM Fusion build is compiled with [Vapoursynth](http://www.vapoursynth.com/) {{< ghub vapoursynth vapoursynth >}} support as required.

To [download](https://superuser.com/a/301051) and install SVP,
```shell
sudo dnf install python3-vapoursynth
cd ~/build
wget --content-disposition 'https://www.svp-team.com/files/svp4-latest.php?linux'
tar xf svp4-linux.4.5.205-1.tar.bz2  # or newer version
./svp4-linux-64.run
```
On its maiden run, SVP detects system performance and configures itself accordingly.

To play a video with `mpv` and SVP, start SVP then
```shell
mpv --input-ipc-server=/tmp/mpvsocket --hwdec=copy-auto --interpolation=no --no-resume-playback <video file>
```

> Note that this didn't work on the [mirrored display]({{< ref "linux-video#display-configuration" >}}).
> It did kind of work on the main display, albeit with a predilection for crashing on seek.
> Sometimes it also crashed after a few seconds of running with the error
> ```shell
> Option vf-del: -del is deprecated! Use -remove (removes by content instead of by index).
> ```
> Something to do with the `ffmpeg` version?
> Certainly this is not in a sufficiently stable state for usage.
> My `mpv` default configuration is [here]({{< ref "linux-video#configuration" >}}).

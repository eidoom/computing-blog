+++
title = "PDF tools"
date = 2020-03-12T11:35:19Z
edit = 2020-03-14
categories = ["environment-extra"]
tags = ["pdf","environment","laptop","desktop","documents","fedora","svg"]
description = "Some tools for editing PDFs"
toc = true
cover = "img/covers/30.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Crop 

I [came across](https://askubuntu.com/a/179420/827359) the tool `pdfcrop` for cropping `pdf` documents.
Install on Fedora with
```shell
sudo dnf install texlive-pdfcrop
```
Use like
```shell
pdfcrop --margins '<left> <top> <right> <bottom>' input.pdf output.pdf
```
or omit margin options for automatic border detection.
Use negative numbers for the margins to crop.

## Merge 

I [found](https://stackoverflow.com/a/11280219/5922871) the tool `pdfunite` for merging `pdf` documents.
Install on Fedora with
```shell
sudo dnf install poppler
```
and use like
```shell
pdfunite *.pdf output.pdf 
```

## Compress

With
```shell
sudo dnf install ghostscript
```
[you can](https://askubuntu.com/a/243753)
```shell
ps2pdf input.pdf output.pdf
```
which often works well to compress files.

## Rotate

Use
```shell
sudo dnf install texlive-pdfjam
```
to rotate anticlockwise
```shell
pdfjam --angle <angle (degrees)> <input>.pdf -o <output>.pdf
```

## Convert

### svg to pdf

Use
```shell
sudo dnf install librsvg2-tools
```
to convert `svg` to `pdf`
```shell
rsvg-convert <in>.svg -f pdf -o <out>.pdf
```

### pdf to svg

Use
```shell
sudo dnf install pdf2svg
```
to convert `svg` to `pdf`
```shell
pdf2svg <in>.pdf <out>.svg
```

### ps to pdf

Use
```shell
sudo dnf install ghostscript
```
to convert `ps` to `pdf`
```shell
ps2pdf <in>.ps <out>.pdf
```

## GUI

[Use](https://fedoramagazine.org/pdf-modification-tools-fedora/)
```shell
sudo dnf install pdfmod
```
for a simple GUI tool to edit `pdf`s.

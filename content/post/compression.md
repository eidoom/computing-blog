+++
title = "Container and compression formats"
date = 2022-11-21T12:12:22+01:00
categories = ["cheatsheet"]
tags = ["tar","gzip","xz","zstd","brotli","jpg","jpeg","webp","avif","ffmpeg","flac","mp3","vorbis","ogg","opus","mkv","webm","matroska","vp9","av1","audio","sound","video","image","picture","container","archive","compression","lossless","lossy","files","web","desktop","laptop","server","phone"]
description = "A reference list"
toc = true
cover = "img/covers/124.avif"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking concept art'-'borders'"
+++

Only open stuff allowed.

## General files

* [General use of `tar` including compression]({{< ref "utilities#tar" >}}).
* [ArchWiki discussion](https://wiki.archlinux.org/title/Archiving_and_compression).

### Archive

`tar` `.tar` {{< wiki "tar_(computing)" >}}

### Compression

`gzip` `.gz` {{< wiki gzip >}}

`xz` `.xz` {{< wiki XZ_Utils >}}

`zstd` `.zst` {{< wiki zstd >}}

`brotli` `.br` {{< wiki Brotli >}}

## Images

### Lossy

* Using [AVIF]({{< ref "avif" >}}), [WebP]({{< ref "webp" >}}), and [JPEG]({{< ref "exporting-photographs-for-web-with-gimp" >}}) on the web.

JPEG `.jpg` {{< wiki JPEG >}}

WebP `.webp` {{< wiki WebP >}}

AVIF `.avif` {{< wiki AVIF >}}

## Audio

* [Using `ffmpeg` for conversion between formats]({{< ref "conv-audio" >}}).

### Lossless

FLAC `.flac` {{< wiki FLAC >}}

### Lossy

MP3 `.mp3` {{< wiki MP3 >}}

Vorbis `.ogg` {{< wiki Vorbis >}}

Opus `.opus` {{< wiki "Opus_(audio_format)" >}}

## Video

### Container format

Matroska `.mkv` {{< wiki Matroska >}}

WebM `.webm` {{< wiki WebM >}}

### Compression format

* Format is specification; codec is implementation.

VP9 {{< wiki VP9 >}}

AV1 {{< wiki AV1 >}}

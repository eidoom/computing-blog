+++
title = "Password management with Pass"
date = 2021-10-10T09:50:18+01:00
categories = ["self-host"]
tags = ["pass","linux","windows","windows-11","windows-10","fedora","debian","fedora-40","debian-12","debian-bookworm","android","server","laptop","desktop","phone","mobile","gpg","bitwarden","firefox","encryption","openpgp","gopass","zx2c4","password-manager","unix","infrastructure","sysadmin","git","vcs","public-key","ssh","scp","ed25519","elliptic-curve","crypto","cryptography","aead","repo","repository","export","import","transfer","password","passff","password-store","openkeychain","choco","chocolatey","gpg4win","gopass-bridge","gopass-jsonapi","powershell","shell","bash","secret","key"]
description = "Migrating from the BitWarden cloud service to a self-hosted Pass setup"
toc = true
cover = "img/covers/pass.avif"
coveralt = "sd3"
clip = 25
+++

I decided to self-host my own password management infrastructure instead of relying on a cloud service (I previously used [BitWarden]({{< ref "2fa#password-manager" >}})).

[Pass](https://www.passwordstore.org/)
{{< git "https://git.zx2c4.com/password-store/" >}}
([guide](https://medium.com/@chasinglogic/the-definitive-guide-to-password-store-c337a8f023a1))
seems like what I want.
I'll use it on multiple machines, synchronising them using Git through a central "server" repo.

## Setup

We'll setup an encryption key and password store repository on a local machine then duplicate the repo to a server machine.

### Encryption key

For encryption, Pass uses [GNU Privacy Guard](https://gnupg.org/) (or GnuPG or GPG) {{< wiki GNU_Privacy_Guard >}} {{< arch GnuPG >}}.

> GPG is not to be mixed up with {{< wl PGP "Pretty_Good_Privacy" >}} (Pretty Good Privacy).
> They are both programs, with GPG being a libre replacement for the original PGP.
> There is also a standard, [OpenPGP](https://www.openpgp.org/about/) {{< wiki "Pretty_Good_Privacy#OpenPGP" >}}, which is based on PGP and which many encryption programs conform to.
> GPG is [mostly compliant](https://www.gnupg.org/documentation/manuals/gnupg/Compliance-Options.html) with OpenPGP by default.

Default GPG settings are good: non-expiring key using {{< wl "elliptic-curve cryptography" >}} with {{< wl Curve25519 >}}.

```shell
$ gpg --full-generate-key
gpg (GnuPG) 2.4.4; Copyright (C) 2024 g10 Code GmbH
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
   (9) ECC (sign and encrypt) *default*
  (10) ECC (sign only)
  (14) Existing key from card
Your selection? 9
Please select which elliptic curve you want:
   (1) Curve 25519 *default*
   (4) NIST P-384
   (6) Brainpool P-256
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 0
Key does not expire at all
Is this correct? (y/N) y
```

Then answer prompts to set the `<username>` (used as the name for the key) and email, then the passphrase (recommended).

Let's inspect the new key.

```shell
$ gpg --edit-key <username>
...

gpg> showpref
[ultimate] (1). <username> <<email>>
     Cipher: AES256, AES192, AES, 3DES
     AEAD: OCB
     Digest: SHA512, SHA384, SHA256, SHA224, SHA1
     Compression: ZLIB, BZIP2, ZIP, Uncompressed
     Features: MDC, AEAD, Keyserver no-modify
```

We see that AEAD {{< wiki "Authenticated_encryption#Authenticated_encryption_with_associated_data" >}} is enabled.
To be OpenPGP compatible (required for [Android support](https://docs.passwordstore.app/docs/users/common-issues/#gnupg-aead-encryption-2974-2963-2921-2924-2653-2461-2586-2179)), we disable AEAD {{< arch "GnuPG#Disable_unsupported_AEAD_mechanism" >}}.

```shell
gpg> setpref AES256 AES192 AES SHA512 SHA384 SHA256 SHA224 ZLIB BZIP2 ZIP
Set preference list to:
     Cipher: AES256, AES192, AES, 3DES
     AEAD: 
     Digest: SHA512, SHA384, SHA256, SHA224, SHA1
     Compression: ZLIB, BZIP2, ZIP, Uncompressed
     Features: MDC, Keyserver no-modify
Really update the preferences? (y/N) y

...

gpg> save
```

To identify the key for Pass, it is best to use the key fingerprint and not the `<username>` or a key ID (which are just subsets of the fingerprint), which can be found like
```shell
$ gpg --list-keys
[keyboxd]
---------
pub   ed25519 <date> [SC]
      <fingerprint>
uid           [ultimate] <username> <<email>>
sub   cv25519 <date> [E]

```
This ensures [`.gpg-id`](https://git.zx2c4.com/password-store/about/#FILES) [compatibility](https://docs.passwordstore.app/docs/users/invalid-gpg-key-id) on Android.

### Pass

Pass itself is simple to install (on Fedora) and setup using our new GPG key and Git for version control.

```shell
sudo dnf install pass
pass init <fingerprint>
pass git init
```

#### Extensions

[Extensions](https://www.passwordstore.org/#extensions) can be installed for Pass.
For example, the {{< gh roddhjav pass-update >}} extension abstracts over bulk password updating.
It can be installed from the latest GitHub [release](https://github.com/roddhjav/pass-update/releases):

```shell
VERSION=2.2.1
wget https://github.com/roddhjav/pass-update/releases/download/v$VERSION/pass-update-$VERSION.tar.gz
tar xzf pass-update-$VERSION.tar.gz
cd pass-update-$VERSION
sudo make install
```

### Server

I set up a bare repo on a remote server (my [home server]({{< ref "server-install" >}})) to be the primary Git repo for the password store.
The passwords are encrypted in the repo, so can't be read without the GPG key, but the repo should still be private as the names of the files would be a metadata leak otherwise.

I'm using the same username everywhere, so I can omit the `user@` address prefix.

On the remote, we initialise a bare repo:

```shell
git init --bare -b main ~/pass-<username>.git
```

> The `.git` suffix is a convention to show that it is a server repo (hence `--bare`) rather than a client repo.

Then on the local machine:

```shell
pass git remote add origin <remote hostname>:pass-<username>.git
pass git push -u origin main
```

## Populate

I'll export my existing passwords from BitWarden and convert them into a Pass password store.

### Import from BitWarden

Export data as (plaintext) JSON [using BitWarden interface](https://bitwarden.com/help/export-your-data/).

We can use {{< ghn roddhjav pass-import >}} (v3.5) to automate converting the BitWarden export for Pass:

```shell
pip install pass-import
export PASSWORD_STORE_EXTENSIONS_DIR="$(python -m site --user-site)/usr/lib/password-store/extensions/"
export PASSWORD_STORE_ENABLE_EXTENSIONS=true
pass import <bitwarden_export_file>.json
rm <bitwarden_export_file>.json
```

### Platform compatibility

However, the password store repo may contain paths with colons, which is not compatible on Windows system.
We can replace `:` with `_` using

```shell
cd ~/.password-store
for f in *; do
    if [[ "$f" == *":"* ]]; then
        git mv "$f" "$(echo $f | tr ':' '_')";
    fi;
done
```

Then repeating the loop `for f in */*` and finally `for f in */*/*` to catch offenders at each nesting level.
(We can't `mv a:/b a_/b` for example because directory `a_` doesn't exist yet.)
Finally, git commit and push.

## Access

To access my passwords on other machines, I'll:

* setup Pass
* sync it with the server repo
* transfer the GPG key over so the passwords can be decoded on that machine --- to make this available, on the [local]({{< ref "#setup" >}}) machine,
    ```shell
    gpg --export-secret-keys > secret.gpg
    rm secret.gpg  # do this after all machines set up
    ```
* maybe get it talking to a Firefox extension, depending on the platform

The process is different for different platforms.

### Linux

#### Terminal

First, we transfer the GPG key from the local machine to the new machine,
```shell
scp secret.gpg <new hostname>
```

Then on the new machine,

```shell
cd
sudo dnf install pass
gpg --import secret.gpg
rm secret.gpg
git clone <remote hostname>:pass-<username>.git ~/.password-store
```

> If using a custom SSH port on the remote, [configure]({{< ref "utilities#port" >}}) this in `~/.ssh/config`.

Now `pass` should return the list of passwords in the store.
The usual `git` operations can be used prepended with `pass` to manage synchronisation with the server repo, eg `pass git pull`.

It may also be necessary to set the trust level on the GPG key in order to edit the password store,

```shell
$ gpg --edit-key <username>
gpg> trust
...
gpg> q
```

##### Usage

* List all keys: `pass`
* Show `<entry>`: `pass <entry>`
* Copy password: `pass -c <entry>`
* New entry with generated password: `pass generate <entry> <password length>`
* Regenerate password: `pass generate -i <entry>`
* Edit entry: `pass edit <entry>`

#### Firefox

We'll use the Firefox [add-on](https://addons.mozilla.org/en-GB/firefox/addon/passff) passff {{< git "https://codeberg.org/PassFF/passff" >}}.
After installing it in the browser, we install the host program {{< git "https://codeberg.org/PassFF/passff-host" >}} with

```shell
curl -sSL https://codeberg.org/PassFF/passff-host/releases/download/latest/install_host_app.sh | bash -s -- firefox
```

The passwords should now be accessible from the extension in Firefox.

### Android

I set this up on [LineageOS 21 (Android 14) with microG replacing Google services]({{< ref "fp3" >}}).

For encryption, we need [OpenKeychain](https://www.openkeychain.org)
{{< ghubn open-keychain open-keychain >}}
{{< fdroid org.sufficientlysecure.keychain >}}.
Copy the encryption key `secret.gpg` to the device and add it in the OpenKeychain app before deleting the file.

[Password Store](https://passwordstore.app/)
{{< fdroid dev.msfjarvis.aps >}}
{{< ghubn android-password-store Android-Password-Store >}}
{{< docs "https://docs.passwordstore.app" >}}
provides Pass on Android.
It is [no longer under active development](https://github.com/android-password-store/Android-Password-Store/discussions/3260), but exists in a complete manner.
Use it to generate an SSH key pair, copy the public key to the server's `~/.ssh/authorized_keys`, then clone the server repo using SSH authentication and allow Password Store to access OpenKeychain to decrypt the passwords.
Note that the repo URL should look like `<user>@<address>:pass-<user>.git` or, if using a SSH port other than 22, `ssh://<user>@<address>:<port>/~/pass-<user>`

> Note that the development build "snapshot" version of Password Store, which is available at the [GitHub releases page](https://github.com/android-password-store/Android-Password-Store/releases), bundles its own OpenPGP support rather than relying on OpenKeychain.
> However, I opted to use the stable version.

### iOS

{{< cal "2025-02-15" >}}

[`passforios`](https://mssun.github.io/passforios/) is the iOS client.
This is tested with Pass for iOS 0.16.0 on an iPhone 13 with iOS 18.2.1.
The quirk here is that for SSH public key authentication, only PEM-format RSA keys are supported (which is pretty old), so be sure to `ssh-keygen -m pem -t rsa ...` (can transfer to phone using `qrencode`) and on the server, `PubkeyAcceptedAlgorithms +ssh-rsa` and cry at the RSA entry in your `.ssh/authorized_keys`.

### Windows

Getting this to work on Windows 10/11 is a bit annoying.
Using the Chocolately package manager streamlines things a bit.
We'll need:
* [Git](https://git-scm.com/) {{< choco "git" >}} for... Git.

* [Gpg4win](https://www.gpg4win.org/) {{< git "https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gpg4win.git;a=summary" >}} {{< choco "Gpg4win" >}} for encryption.

* [Gopass](https://www.gopass.pw/) {{< ghubn gopasspw gopass >}} {{< choco gopass >}} as a platform independent Pass implementation.

* `gopass-jsonapi` {{< ghubn gopasspw gopass-jsonapi >}} {{< choco gopass-jsonapi >}} to provide an API to Pass for browser extensions.

* `gopass-bridge` {{< ghubn gopasspw gopassbridge >}} {{< ffao gopass-bridge >}} as the Firefox add-on.

Install from Powershell with:

```powershell
# as Administrator
choco install git gpg4win gopass gopass-jsonapi
# rest as User in new shell
# start ssh-agent
Get-Service -Name ssh-agent | Set-Service -StartupType Manual
Start-Service ssh-agent
# generate ssh keys
ssh-keygen -t ed25519
# add ~/.ssh/id_ed25519.pub to remote's ~/.ssh/authorized_keys
# configure git
git config --global user.name <name>
git config --global user.email <email>
# import gpg key
# put secret.gpg here from original machine
gpg --import secret.gpg
rm secret.gpg
# clone password-store repo using ssh key authentication
gopass clone <remote hostname>:pass-<username>.git
# setup the broswer extension backend
gopass-jsonapi configure
```

then install the extension in Firefox.

The user experience on Windows leaves much to be desired, being rather clunky, but I don't often use it so that's OK.

+++
title = "Converting abc to pdf"
date = 2020-03-19T10:46:39Z
edit = 2020-10-14
categories = ["software"]
tags = ["music","linux","fedora","desktop","laptop","pdf","abc","sheet-music","markup","svg","midi"]
description = "Converting sheet music markup notation to a readable score"
toc = true
cover = "img/covers/35.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

I recently discovered [ABC music notation](https://en.wikipedia.org/wiki/ABC_notation) while searching for some fiddle sheet music.
I'm a fan of the concept of [markup languages](https://en.wikipedia.org/wiki/Lightweight_markup_language), so I'm delighted such a format exists for music.

To read and play the score, I converted the `abc` file to rendered score, using [`pdf`](https://en.wikipedia.org/wiki/PDF) as the container since it's the most portable document format (clue's in the name).

## Installation

I first installed some format-conversion tools:
* `abcm2ps` converts `abc` to [`ps`](https://en.wikipedia.org/wiki/PostScript)
* `librsvg2-tools` provides `rsvg-convert`, which I used to [convert]({{< ref "pdfs#svg-to-pdf" >}}) [`svg`](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) to `pdf`
    * I originally used the tool `cairosvg` from the package `python3-cairosvg`, but found that it had difficulties with special unicode characters like the sharps and flats in chords, so swapped to `rsvg-convert`
* `ghostscript` provides `ps2pdf`, which [converts]({{< ref "pdfs#svg-to-pdf" >}}) `ps` to `pdf`

On Fedora, that's
```shell
sudo dnf install abcm2ps librsvg2-tools ghostscript
```

## Single document for all songs

To produce a single document, perhaps for printing, I found the best method was to convert `abc` to `ps`
```shell
abcm2ps <filename>.abc -O =
```
then convert that to `pdf`
```shell
ps2pdf <filename>.ps <filename>.pdf
rm -f <filename>.ps
```

## Multiple documents for different songs

<!-- #### Recommended -->

To output one `pdf` per tune without standard printed page margins, favourable for reading on-screen, using `svg` as the intermediate format worked best
```shell
abcm2ps <filename>.abc -O = -g
for f in *.svg; do 
    rsvg-convert "$f" -f pdf -o "${f%.svg}.pdf"; 
done
rm -f *.svg
```

<!-- #### Alternative -->

<!-- To obtain the same result using `ps` as the intermediate format, I used [`pdfcrop`]({{< ref "pdfs#crop" >}}) -->
<!-- ```shell -->
<!-- abcm2ps <filename>.abc -O = -->
<!-- for f in *.ps; do -->
<!--     g=${f%.ps}.pdf; -->
<!--     ps2pdf "$f" "$g"; -->
<!--     pdfcrop --margins '10 10 10 10' "$g" "$g"; -->
<!--     rm -f "$f"; -->
<!-- done -->
<!-- ``` -->
<!-- However, I found `ps2pdf` did not render some details nicely, such as mordents, so favour `svg` as the intermediate format. -->

## Checking with MIDI

Install
```shell
sudo dnf install abcMIDI timidity++
```
to check `abc` files are correct by playing them.
Convert to MIDI with
```shell
abc2midi <file>.abc
```
then play with
```shell
timidity <file><song number>.mid
```

## An example

I put together some of my favourite fiddle tunes in `abc` at [this repo](https://gitlab.com/eidoom/fiddle-music).

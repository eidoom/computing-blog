+++
title = "Adding icons here with Yarn"
date = 2019-12-22T13:14:50Z
categories = ["web"]
tags = ["front-end","static-website","meta","font-awesome","yarn","hugo","html"]
description = "How I added Font Awesome icons to this website using Yarn package manager"
toc = true
cover = "img/covers/94.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Research

Having enjoyed using free-tier [Font Awesome](https://fontawesome.com/) icons before, I decided to use them for the icons on this site.
I wanted to install the icon package with a package manager to make upgrading to new versions easier.
I've used [npm](https://www.npmjs.com/) before, but also heard good things about [Yarn](https://yarnpkg.com/lang/en/).
Yarn ended up getting the deal because it's easier to [set a custom module installation directory](https://stackoverflow.com/questions/41912328/yarn-install-modules-in-a-specific-directory) with it [than npm](https://stackoverflow.com/questions/14742553/how-to-set-custom-location-for-local-installation-of-npm-package).
Also, [Netlify fully supports Yarn](https://www.netlify.com/blog/2016/11/01/yarn-support-on-netlify/).

## Installation

I installed Yarn with

```shell
sudo dnf install nodejs-yarn
```

and initialised Yarn for my project with

```shell
cd ~/git/computing-blog
yarnpkg init -y
```

Note the [Fedora package specific](https://developer.fedoraproject.org/tech/languages/nodejs/nodejs.html) `yarnpkg` rather than the usual `yarn`.

I had to change the default module installation directory, which is `node_modules`, as the Hugo-compiled site won't see it unless it's in the `static` folder of my project.
It was easy to move it to `static/modules`:

```shell
echo "--modules-folder static/modules" >> .yarnrc
```

I [added the Font Awesome package](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers#installing-free) with:

```shell
yarnpkg add --dev @fortawesome/fontawesome-free
```

Finally, I [added Font Awesome](https://fontawesome.com/how-to-use/on-the-web/setup/using-package-managers#next) using the [CSS framework](https://fontawesome.com/how-to-use/on-the-web/setup/hosting-font-awesome-yourself#using-web-fonts) to `<head>` [here](https://gitlab.com/eidoom/computing-blog/blob/master/layouts/partials/extended_head.html):

```handlebars
<link href={{ "modules/@fortawesome/fontawesome-free/css/all.css" | absURL }} rel="stylesheet">
```

I used this over the [JS framework](https://fontawesome.com/how-to-use/on-the-web/setup/hosting-font-awesome-yourself#using-svg) because the CSS framework loaded faster.

I hid the module files from Git with:

```shell
echo "static/modules/" >> .gitignore
```

and committed these changes to my Git repository with:

```shell
git add .yarnrc package.json yarn.lock
git commit -m "Added yarn"
```

## Usage

Search for icons on the [Font Awesome website](https://fontawesome.com/icons?d=gallery&s=solid&m=free) and use with 

```handlebars
<i class="fas fa-NAME"></i>
```

Find examples [here](https://gitlab.com/eidoom/computing-blog/blob/master/layouts/partials/meta.html).

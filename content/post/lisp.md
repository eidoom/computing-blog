+++
title = "Advent of Code 2021 with a little Lisp"
date = 2022-10-22T16:55:10+02:00
categories = ["advent-of-code"]
tags = ["python","lisp","advent-of-code-2021","janet","common-lisp","racket","clojure","cpp","c++","scheme","hy","owl-lisp","fennel","lfe","sbcl","coding","programming","steel-bank-common-lisp","aoc","aoc21"]
description = "And lots of Python"
toc = true
draft = false
cover = "img/covers/110.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
For 2021's Advent of Code, I satisfied some of my curiosity towards the fabled languages of Lisp at {{< gl aoc21 >}}.
In the later days, I switched to Python to speed up my typing.

+++
title = "Stories blog"
date = 2021-01-23T10:25:38Z
edit = 2021-05-03
categories = ["web"]
tags = ["rust","html","css","sass","zola","static-website","javascript","vercel","linux","fedora","desktop","laptop","markdown","perl","regex","bash","gitlab","project"]
description = "A new blog site with Zola"
toc = true
cover = "img/covers/62.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I built a new blog site, [ˈstɔːriz](https://somestories.vercel.app/) (source  at {{< gl musings >}}), as a place to record and chat about books I've read, films and series I've watched, games I've played, and so on.
Previously I used services like Goodreads, but I wanted to move away from [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism) platforms to something more personal.

## Framework

I have used Hugo before as a static site generator, for [this computing blog]({{< ref ".." >}}), for example, but I wanted to try something new.
I checked [the market](https://jamstack.org/generators/) and noticed [Zola](https://www.getzola.org/) {{< ghub "getzola" "zola" >}}.
It caught my eye because it's simple, fast, and written in [Rust]({{< ref "learning-rust" >}}).
It uses the [Tera template engine](https://tera.netlify.app/), which is inspired by familiar `{{ templates }}` from Python libraries.

The easiest way to get the latest version of Zola on Fedora is to use the [`zola`](https://snapcraft.io/zola) [`snap`]({{< ref "software-deploy#snap" >}})
```shell
sudo snap install --edge zola
```
To run a development server
```shell
zola serve
```
and to compile the site
```shell
zola build
```

## Hosting

Again, rather than repeating [what I've done before](https://eidoom.gitlab.io/), I tried a new hosting service with [Vercel](https://vercel.com/) for this site.
The [Zola documentation](https://www.getzola.org/documentation) had a [quick guide](https://www.getzola.org/documentation/deployment/vercel/) for deployment on Vercel and it was [super easy](https://gitlab.com/eidoom/musings/-/blob/master/vercel.json).
I like their web interface, although I hardly look at it as everything just works.

## Technology

The [Zola docs](https://www.getzola.org/documentation/getting-started/overview/) are brief, but I found it quite easy to put the site together from scratch following them along with the [Tera docs](https://tera.netlify.app/docs) and the [MDN web docs](https://developer.mozilla.org/en-US/).

Content is, of course, in `markdown` with a metadata section at the top.

Zola supports powerful [taxonomies](https://www.getzola.org/documentation/content/taxonomies/), which I took advantage of.

Internal links are [handled](https://www.getzola.org/documentation/content/linking/) by Zola.

For the [search](https://www.getzola.org/documentation/content/search/) bar, I borrowed some [JavaScript](https://github.com/getzola/zola/blob/master/docs/static/search.js) from the source of the Zola docs.
Zola builds a search index for us which [elasticlunr.js](http://elasticlunr.com/) uses for searches.

Zola doesn't process punctuation symbols as I'd like.
For example, I type `'` on my keyboard and this is fine in the `markdown`, but I want the apostrophe `’` to be displayed on the site.
My quick hack solution for now is a preprocessing regex [script](https://gitlab.com/eidoom/musings/-/blob/master/proc.sh) that has to be run manually.

## Styling

Zola [supports](https://www.getzola.org/documentation/content/sass/) the [Sass CSS preprocessor](https://sass-lang.com/) {{< wiki "Sass_(stylesheet_language)" >}}.
[Styling](https://gitlab.com/eidoom/musings/-/blob/master/sass/style.scss) in SassScript is way nicer than vanilla CSS.
I particularly like the `$variables`, being able to do arithmetic, and nesting.

The reference Sass implementation is [Dart Sass](https://sass-lang.com/dart-sass) {{< ghub "sass" "dart-sass" >}} {{< pack "https://pub.dev/packages/sass" >}}.
There is also a C/C++ port of the engine called [LibSass](https://sass-lang.com/libsass), which has wrappers in many languages.
It [looks like](https://www.getzola.org/documentation/getting-started/installation/#from-source) Zola has its own wrapper implementation (it [doesn't seem to use](https://github.com/getzola/zola/blob/master/Cargo.toml) the Sass [advertised](https://sass-lang.com/libsass) [Rust crate](https://docs.rs/sass-rs) {{< ghub "compass-rs" "sass-rs" >}}).
However, [LibSass is deprecated](https://sass-lang.com/blog/libsass-is-deprecated)!

## Design

I found an entertaining [web design prompt](https://jgthms.com/web-design-in-4-minutes/) for initial inspiration and used the ergonomic [Solarized](https://ethanschoonover.com/solarized/) for my colour scheme, which quite strongly defined the aesthetic.
For balance, I used the slightly fun [Merriweather font](https://github.com/SorkinType/Merriweather).

## Font

Since [self-hosted fonts are the fastest](https://wicki.io/posts/2020-11-goodbye-google-fonts/), I self-hosted.
[Variable](https://web.dev/variable-fonts/) [fonts](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Fonts/Variable_Fonts_Guide) are a neat modern way to have total creative control over the font, so I opted to use them.
I grabbed the [Merriweather variable ttf](https://github.com/SorkinType/Merriweather/tree/master/fonts/variable) (both normal and italic versions) and converted it to [woff2](https://www.w3.org/TR/WOFF2/) {{< ext "https://developer.mozilla.org/en-US/docs/Web/Guide/WOFF" >}} {{< wiki "Web_Open_Font_Format" >}} using {{< gh google woff2 >}},
```shell
sudo dnf install woff2-tools
woff2_compress Merriweather\[opsz,wdth,wght\].ttf
woff2_compress Merriweather-Italic\[opsz,wdth,wght\].ttf
```
[included](https://gitlab.com/eidoom/musings/-/tree/master/static/fonts) this in the static files, and [added](https://gitlab.com/eidoom/musings/-/blob/master/sass/style.scss#L44) the font with 
```css
@font-face {
 font-family: 'Merriweather';
 src: url('fonts/Merriweather[opsz,wdth,wght].woff2') format('woff2-variations');
 font-style: normal;
}

@font-face {
 font-family: 'Merriweather';
 src: url('fonts/Merriweather-Italic[opsz,wdth,wght].woff2') format('woff2-variations');
 font-style: italic;
}
```
although the syntax for the source format [will change soon](https://web.dev/variable-fonts/#loading-variable-font-files).

## Name

The site was initially called ˈmjuːzɪŋz as a unicode support test, hence the name of the [source repo](https://gitlab.com/eidoom/musings/).
When I realised the `musings` subdomain was taken on Vercel, I opted for ˈstɔːriz, having become fond of the whacky [IPA](https://en.wikipedia.org/wiki/International_Phonetic_Alphabet) representation.
`stories` was also taken, so I ended up with `somestories` in my URL.

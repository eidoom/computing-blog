+++
title = "Getting Fedora just so"
date = 2020-01-10
edit = 2021-05-28
categories = ["operating-system"]
tags = ["environment","fedora","laptop","desktop","linux","install","configure","gnome","git","ssh","dnf","dejavu-font","solarized","terminal","thinkpad","chromium","hugo","grub","rpmfusion","gnome-extensions","snap","flatpak","dd"]
description = "Installing Fedora again"
toc = true
cover = "img/covers/0.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Hardware

I have configured Fedora 31 as follows previously on a [Lenovo Thinkpad X280 laptop](https://www.lenovo.com/gb/en/laptops/thinkpad/x-series/ThinkPad-X280/p/22TP2TX2800) (fresh install) and recently on a [Lenovo Thinkpad T490 laptop](https://www.lenovo.com/gb/en/laptops/thinkpad/t-series/T490/p/22TP2TT4900) (reinstall).

The first thing to do with these laptops is to remap a couple of the keys on the keyboard since unfortunately they were positioned incorrectly.
Thankfully, there is an option in the BIOS, accessed on startup with `F1`, to swap `Fn` and `Ctrl`.
It is found at `Config > Keyboard/Mouse > Fn and Ctrl Key swap`.
This setup page also has the option for `F1-12 as Primary Function`, as opposed to the volume and screen brightness controls and so on, which is another essential change.

I also installed Fedora on a Dell XPS 15 9560, discussed in [this post]({{< ref "xps-install" >}}).

## Installation

### Preparing the flash install medium

Get the [Fedora ISO](https://getfedora.org/en/workstation/download/) and use it to create a flash install medium with `dd` as described [here](https://docs.fedoraproject.org/en-US/quick-docs/creating-and-using-a-live-install-image/#_using_a_direct_write_method) and [here](https://wiki.archlinux.org/index.php/USB_flash_install_media#Using_dd).
For example, plug in a memory stick and see that it's `dev/sdX` using `lsblk` then

```shell
wget https://download.fedoraproject.org/pub/fedora/linux/releases/31/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-31-1.9.iso
dd if=Fedora-Workstation-Live-x86_64-31-1.9.iso of=/dev/sdX bs=8M status=progress oflag=direct
```

### Starting the install

I rebooted the computer and interrupted the normal start-up by mashing `F12` to access the boot menu.
I chose the memory stick and started the install.
I chose UK for region.

### Partitioning

I prefer the `Advanced custom (Blivet-GUI)` partitioning option.

{{< figure position="center" src="../../img/fedora-install/part1.webp" alt="part1" >}}

There are some existing partitions:

-   a 260 [MiB]({{< ref "jargon#binary" >}}) EFI boot partition, which I set on mountpoint `/boot/efi`
-   a 16 MiB partition due to Windows 
-   a 64 GiB Windows partition

I formatted the remaining space as an `ext4` partition for Fedora, so mountpoint `/`.

{{< figure position="center" src="../../img/fedora-install/part2.webp" alt="part2" >}}

The installer then proceeded and I restarted the computer after it finished.

## General configuration

### Packages

I recorded all installed packages from the previous install using [this](https://gitlab.com/eidoom/dotfiles-system-backup), but decided to not use it in an effort to keep the new system lean.

### Sudo behaviour

The `sudo` configuration [may only be edited](https://wiki.archlinux.org/index.php/Sudo#Using_visudo) via

```shell
sudo visudo
```

I uncommented the following line near the bottom of the file

```shell
## Same thing without a password
%wheel  ALL=(ALL)       NOPASSWD: ALL
```

to stop the password prompt on every use of `sudo`.

### SSH key

I [generated an SSH key]({{< ref "utilities#set-up-ssh-key" >}}) with 

```shell
ssh-keygen -t ed25519
```

I printed the public key to the terminal with

```shell
cat ~/.ssh/id_ed25519.pub
```

to add it to GitHub, GitLab and BitBucket.

To [add it to remote servers]({{< ref "utilities#copy-public-key-to-remote" >}}), I used

```shell
ssh-copy-id SERVER_ADDRESS
```

### Git configuration

I ran 

```shell
git config --global --edit
```

to set `git` name and email.
I will add `~/.gitconfig` to a private dotfile repo to save this step in the future.

### Dotfiles

I used my [public dotfiles repo](https://gitlab.com/eidoom/dotfiles-public) to migrate over saved configurations.

### Boot behaviour

I prefer informative non-graphical boots, so I edited the `grub2` configuration:

```shell
sudo vi /etc/default/grub
```

removing `rhgb` (RedHat Graphical Boot) from the `GRUB_CMDLINE_LINUX` line.
Then I applied the change with

```shell
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

### Software packaging and deployment systems

I installed `flatpak` and `snap` as discussed [here]({{< ref "software-deploy" >}}).

### Hugo

To work on this post in realtime, I installed the [latest version](https://gohugo.io/getting-started/installing/#fedora-red-hat-and-centos) of `hugo` with 

```shell
sudo dnf copr enable daftaupe/hugo
sudo dnf install hugo
```
I didn't use the version in the official repos because it was outdated.

#### Update
{{< cal "13 Jan 2021" >}}

Note that [as of Fedora 33, this repo no longer works]({{< ref "fedora-upgrade#fedora-33" >}}).
Instead, we can use [Snap]({{< ref "#software-packaging-and-deployment-systems" >}}) to grab the latest Hugo release
```shell
sudo snap install hugo
```

### RPMFusion
I [installed](https://rpmfusion.org/Configuration) the [RPMfusion](https://rpmfusion.org/) repositories with
```shell
sudo dnf install \
    https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
    https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```
These give access to useful [sound and video software](https://download1.rpmfusion.org/free/fedora/releases/31/Everything/x86_64/os/repoview/sound-and-video.group.html).

I installed some video decoders with
```shell
sudo dnf install gstreamer1-libav
```
and [Steam](https://download1.rpmfusion.org/nonfree/fedora/releases/31/Everything/x86_64/os/repoview/steam.html) with
```shell
sudo dnf install steam
```

### Internet

After installing the RPMFusion repositories, I installed my browser of choice with
```shell
sudo dnf install chromium-freeworld
```
which bundles [Chromium](https://www.chromium.org/Home) with [free](https://rpmfusion.org/FAQ#Free_repository) codecs to support online video formats.

> {{< cal 2022-10-31 >}}
>
> These days, I use Firefox, which is provided by the default Fedora repositories
> ```shell
> sudo dnf install firefox
> ```

### Backup kernels

I changed the number of kernels kept installed by `dnf` on update with
```shell
sudo vi /etc/dnf/dnf.conf
```
```vim
...
installonly_limit=5
...
```
Although do make sure there's space to increase the number
```shell
df -h /boot/efi
sudo du -hd1 /boot/efi/EFI
```
Having previous kernels available to boot can be useful if updates go wrong, particularly when using [kernel modules]({{< ref "nvidia-drivers-on-fedora" >}}).

## GNOME configuration

Fedora comes with the [GNOME desktop environment](https://www.gnome.org/) by default.
It's my favourite desktop environment with [stacking, or floating,](https://en.wikipedia.org/wiki/Stacking_window_manager) windows, although I'm considering switching to the [tiling](https://en.wikipedia.org/wiki/Tiling_window_manager) window manager [Sway](https://github.com/swaywm/sway).
For now, I'll set up and use GNOME.

I state the settings paths in the following for completeness, although in reality I navigated the settings menus by searching in `Activities Overview`.
I simply press `Super` on the keyboard then start typing my query.

{{< figure position="center" src="../../img/fedora-install/overview.webp" alt="overview" >}}

### Keyboard

#### Layout switching

The system is set up for UK region, with the laptop keyboard being UK layout, but I use a separate mechanical keyboard with the laptop which has US layout.
Therefore, I configured the system so that I can switch between UK and US keyboard layouts with `Super`+`Space`.
I opened `GNOME Settings > Region & Language > Input Sources` and pressed the `+` to add a keyboard layout.
{{< figure position="center" src="../../img/fedora-install/keeb0.webp" alt="keeb0" >}}
I chose `English (United Kingdom)` on the first screen.
{{< figure position="center" src="../../img/fedora-install/keeb1.webp" alt="keeb1" >}}
I selected `English (US)` on the second prompt.
{{< figure position="center" src="../../img/fedora-install/keeb2.webp" alt="keeb2" >}}
This added the US keyboard layout.
{{< figure position="center" src="../../img/fedora-install/keeb3.webp" alt="keeb3" >}}
This configuration is also useful for easily accessing the GBP sign when using a US keyboard layout: I switch layouts with `Super`+`Space`, then `Shift`+`3` to get `£`, then switch back.

#### Key switching

Since I use `Esc` so much while in `nvim`, I remapped the keyboard so that `Caps Lock` and `Esc` are swapped.
I used GNOME Tweaks to achieve this.
I installed it with

```shell
sudo dnf install gnome-tweaks
```

then opened `Tweaks > Keyboard & Mouse > Keyboard`
{{< figure position="center" src="../../img/fedora-install/cap0.webp" alt="cap0" >}}
I clicked `Additional Layout Options`, which has the `Caps Lock behaviour` menu
{{< figure position="center" src="../../img/fedora-install/cap1.webp" alt="cap1" >}}
and chose `Swap ESC and Caps Lock`
{{< figure position="center" src="../../img/fedora-install/cap2.webp" alt="cap2" >}}

### Dark theme

Under `GNOME Tweaks > Appearance > Themes`, I chose `Applications` to be `Adwaita-dark` to enable the dark theme.
{{< figure position="center" src="../../img/fedora-install/dark.webp" alt="dark" >}}

### Touchpad

To add touchpad use by tapping, I enabled `Tap to Click` in `GNOME Settings > Devices > mouse & Touchpad > Touchpad`.
{{< figure position="center" src="../../img/fedora-install/tap.webp" alt="tap" >}}

### Terminal

#### Colours

I customized colours in the terminal by choosing `Preferences` in the image below
{{< figure position="center" src="../../img/fedora-install/term0.webp" alt="term0" >}}
then renaming the default profile and setting as:
{{< figure position="center" src="../../img/fedora-install/term1.webp" alt="term1" >}}
I also created another profile for use in high-light conditions:
{{< figure position="center" src="../../img/fedora-install/term2.webp" alt="term2" >}}

#### Font

I set the font in both profiles to `DejaVu Sans Mono Book` at size 10 from the package `dejavu-sans-mono-fonts`:
{{< figure position="center" src="../../img/fedora-install/term3.webp" alt="term3" >}}

### Workspaces

Another useful GNOME Tweaks options sets the workspace span multiple displays.
Find at `GNOME Tweaks > Workspaces > Display Handling`.
{{< figure position="center" src="../../img/fedora-install/ws.webp" alt="ws" >}}

### Alternative Alt-Tab

Under `GNOME Settings > Devices > Keyboard Shortcuts`, I made `Alt`+`Tab` switch windows rather than applications, as it should be.
I changed application switching to `Super`+`Tab`.
{{< figure position="center" src="../../img/fedora-install/switch0.webp" alt="switch0" >}}

### Night light

I like the colour temperature of my screen to vary with the daylight, so I enabled `Night light` at `GNOME Settings > Devices > Displays > Night light`.

{{< figure position="center" src="../../img/fedora-install/nl.webp" alt="nl" >}}

### No top left hot corner

I disabled the top left hot corner at `GNOME Tweaks > Top Bar > Activities Overview Hot Corner` as I always access `Activities Overview` using the keyboard key `Super`.
{{< figure position="center" src="../../img/fedora-install/tb.webp" alt="tb" >}}

### Gnome extensions

I disabled default extensions at `GNOME Tweaks > Extensions` and installed my preferred extensions in `chromium-browser`  using the [GNOME Shell integration](https://chrome.google.com/webstore/detail/gnome-shell-integration/gphhapmejobijbbhgpjhcjognlahblep) Chrome extension.
The interface is [here](https://extensions.gnome.org/).
These are the extensions I installed:

-   [Quick close in overview](https://extensions.gnome.org/extension/352/middle-click-to-close-in-overview/) to close windows with a middle mouse click in overview.
<!-- Deprecated: now an option in GNOME Tweaks -->
<!-- -   [No top-left hot corner](https://extensions.gnome.org/extension/118/no-topleft-hot-corner/) to get rid of that annoying feature. -->
-   [Gravatar](https://extensions.gnome.org/extension/1015/gravatar/) to have a nice user icon. It neds to be configured in GNOME Tweaks.

{{< figure position="center" src="../../img/fedora-install/ext.webp" alt="ext" >}} 

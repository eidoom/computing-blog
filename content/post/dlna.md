+++
title = "Streaming music with UPnP"
date = 2023-11-07T12:06:08Z
categories = ["audio"]
tags = ["dlna","music","audio","sound","mp3","flac","readymedia","debian","debian-12","bookworm","apt","minidlna","vlc","ogg","vorbis","mpd","home","home-server"]
description = "Playing audio stored remotely over LAN"
toc = true
draft = false
cover = "img/covers/dlna.avif"
#coveralt = "?"
+++

## Protocol

On the desktop, I generally access files from my remote servers via an [NFS]({{< ref nfs >}}) mount.
In the past, I've also [used]({{< ref "rpi-mpd" >}}) MPD to remote control a Raspberry Pi connected to an amplifier and speakers, again using NFS to remote access server files from the Raspberry Pi.

Interested in a common [way](https://www.lesbonscomptes.com/pages/homenet-audio.html) to play sound from a server on a variety of clients, I found the [Digital Living Network Alliance](https://www.dlna.org/) (DLNA) standards {{< pl "https://en.wikipedia.org/wiki/DLNA" >}} ((such an inspired naming committee...)).
This just bundles networking standards and common media formats with, more importantly, the {{< wl "Universal Plug and Play" >}} (UPnP) protocols, so stuff happens over HTTP.

As well as the usual *server*-*player* setup, DLNA/UPnP also supports a *server*-*controller*-*renderer* configuration on supported devices where the server streams directly to the renderer and playback is manipulated on the controller.

## Server

A simple server option on Debian is [ReadyMedia/MiniDLNA](https://sourceforge.net/projects/minidlna/) {{< pl "https://wiki.archlinux.org/title/ReadyMedia" >}} {{< pl "https://wiki.debian.org/minidlna" >}} {{< pl "https://packages.debian.org/stable/minidlna" >}}.
Among others, it supports the following [formats]({{< ref "compression#audio" >}}) (sans transcoding, a boon for economy), which suits my needs:

* MP3
* (Ogg) Vorbis
* FLAC

Install it with

```shell
sudo apt update
sudo apt install minidlna
```

Configure the directories to share as `media_dir` entries at `/etc/minidlna.conf`.
For example,

```shell
sudo vim /etc/minidlna.conf
```
```vim
...
media_dir=A,/path/to/audio
...
```

where the `A` prefix restricts to scan for audio content only.

Ensure the files are accessible to the `minidlna` user.
For example,
```shell
cd /path/to/audio
find . -type d -exec chmod o+rx "{}" \+
find . -type f -exec chmod o+r "{}" \+
sudo systemctl restart minidlna
```

## Player

There are many available [clients](https://en.wikipedia.org/wiki/List_of_UPnP_AV_media_servers_and_clients#UPnP_AV_clients).
One cross-platform example is [VLC Media Player](https://www.videolan.org/vlc/) {{< pl "https://en.wikipedia.org/wiki/VLC_media_player" >}} (note that VLC can also access [Samba]({{< ref "/tags/samba/" >}}) shares).

## Renderer

### RPi update: Buster to Bullseye

I thought it'd be a good idea to resurrect [that]({{< ref "rpi-mpd" >}}) old Raspberry Pi as a renderer.
I plugged it in and remote accessed it,
```shell
ssh pi@raspberrypi
```
I ditched the static IP address by removing the previous added lines in `/etc/dhcpcd.conf` and replaced them with `auto eth0` then rebooted.

It was on Buster, which is [EOL](https://wiki.debian.org/DebianReleases), so I decided to update to Bullseye.
[Upgrading]({{< ref "buster2bullseye" >}}) between major versions on Raspberry Pi OS is [not recommended](https://www.raspberrypi.com/documentation/computers/os.html#upgrading-from-previous-operating-system-versions), so naturally I did it.
It amounted to:

1.
    ```shell
    sudo apt update
    sudo apt full-upgrade
    sudo reboot
    ```
2.
    ```shell
    cd /etc/apt
    sudo sed -i -e "s/buster/bullseye/" \
        sources.list \
        sources.list.d/raspi.list
    ```
3. Repeat 1.
4.
    ```shell
    sudo apt autoremove
    sudo apt autoclean
    ```

However, there was (as promised) a [problem](https://forums.raspberrypi.com/viewtopic.php?f=36&t=320383), and one that annoyingly broke SSH access on reboot since it stopped IPv4 address allocation.
(Apparently you can't hotplug HDMI so need to restart the Pi to get video if started headless.)
A `dhcpcd` service script pointed to an old binary and needed updating:

```shell
sudo sed -i -e "s/lib\/dhcpcd5/sbin/" \
    /etc/systemd/system/dhcpcd.service.d/wait.conf
```

then reboot.

### gmediarender

A simple Debian/Raspberry Pi OS choice of renderer is {{< gh hzeller "gmrender-resurrect" >}} (fork of [GMediaRender](https://gmrender.nongnu.org/)), or [`gmediarender`](https://packages.debian.org/stable/sound/gmediarender) as Debian calls it.
I installed it with a bunch of `gstreamer` packages to be [safe](https://raspberrypi.stackexchange.com/a/79005/86087),
```shell
sudo apt install \
    gstreamer1.0-alsa \
    gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-gl \
    gmediarender
```
That just works.

### upmpdcli

I also tried [`upmpdcli`](https://www.lesbonscomptes.com/upmpdcli/).
It works by sitting between the UPnP server and MPD.
I [reset](https://unix.stackexchange.com/a/444391/156381) the `mpd` configuration with
```shell
DPKG_FORCE=confmiss sudo apt install --reinstall mpd
```
Unfortunately `upmpdcli` isn't packaged by Debian, but the developers offer [repositories](https://www.lesbonscomptes.com/upmpdcli/pages/downloads.html#debian).
```shell
cd /usr/share/keyrings
sudo wget https://www.lesbonscomptes.com/upmpdcli/pages/upmpdcli-rbullseye.list
cd /etc/apt/sources.list.d
wget https://www.lesbonscomptes.com/upmpdcli/pages/upmpdcli-rbullseye.list
sudo apt update
sudo apt install upmpdcli
```

## Controller

On the server/renderer side of things, UPnP (like MPD) works very well.
However, I can't find a client/player/[controller](https://www.lesbonscomptes.com/upmpdcli/pages/control-points.html) (importantly, supporting control point usage) on either Android or Linux that I love.

## Verdict

I think I preferred [MPD]({{< ref "rpi-mpd" >}})...
Everything is a bit too proprietary for my tastes in the UPnP world.

I'll keep [ReadyMedia]({{< ref "#server" >}}) for UPnP streaming direct to phones,

> {{< cal "2023-12-03" >}}
>
> Actually, I think I rather [Samba]({{< ref "samba" >}}) as a solution for this kind of "streaming" (for Android; NFS also works on iOS).
> It's as simple to [set up](https://wiki.debian.org/Samba/ServerSimple) unauthorised read access as
> ```shell
> sudo apt install samba
> ```
> then replace everything at the end of the configuration with
> ```shell
> sudo vim /etc/samba/smb.conf
> ```
> ```vim
> ...
> #======================= Share Definitions =======================
>
> [<name>]
>     comment = <comment>
>     read only = yes
>     path = <path/to/directory>
>     guest ok = yes
> ```
> and
> ```shell
> testparm
> sudo systemctl reload smbd
> ```
> Use a client like VLC to access from the mobile device.

... uninstall the renderers, and reinstate MPD.
In contrast to [before]({{< ref "rpi-mpd" >}}),

* I used the `stable` packages,
* On the satellite, to get sound and [adjust volume on clients](https://forums.raspberrypi.com/viewtopic.php?t=150505) I had to add 
```shell
sudo vim /etc/mpd.conf
```
```vim
...
audio_output {
        type            "alsa"
        name            "ALSA"
        mixer_type      "software"  # so clients can adjust volume
}
...
```
* I ensured `bind_to_address` was set to the default `"any"` on both `mpd` instances to allow satellite access to the server and client access to the satellite,
* I just ran it as a (systemd) service,
* Using dynamic IP addresses for everything, I identified networked machines by `hostname`.

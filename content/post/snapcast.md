+++
title = "Synchronous audio clients with Snapcast"
date = 2024-10-11T11:20:31+01:00
categories = ["audio"]
tags = ["audio","home","music","mpd","home-server","rpi","raspberry-pi","raspian","raspberry-pi-os","raspi","raspios","snapcast","snapweb","snapserver","snapclient","systemd","debian-12","debian-bookworm","apt"]
description = "For multiroom music enjoyment"
toc = true
draft = false
cover = "img/covers/snapcast.webp"
coveralt = "sd3"
clip = 22
+++

Now that I have a couple of [Raspberry Pis controlling hi-fis]({{< ref "rpi-mpd" >}}), I thought it would be cool to set up synchronous playback over them.
This is actually easy to set up thanks to {{< ghn "badaix/snapcast" >}}.
Here's an alternative [guide](https://whynot.guide/posts/howtos/multiroom-media/) for extra tips.

We'll setup Snapcast to pass music played on our server MPD instance to the RPis, keeping playback in sync.
The [existing]({{< ref "rpi-mpd#satellite" >}}) MPD instances on the RPis are not used in this setup, but we'll keep them in case we want local playback.

## Server

This is on my Debian 12 [home server]({{< ref "home-server" >}}).

### MPD

I [previously]({{< ref "rpi-mpd#server" >}}) installed MPD.
Now we [configure](https://github.com/badaix/snapcast/blob/develop/doc/player_setup.md#mpd) MPD to play out to a named pipe `/tmp/snapfifo`,

```shell
sudo vim /etc/mpd.conf
```

```vim
audio_output {
    type            "fifo"
    name            "snapcast"
    path            "/tmp/snapfifo"
    format          "48000:16:2"
    mixer_type      "software"
}
```

with no other audio outputs.

```shell
sudo systemctl restart mpd
```

### Snapcast

Next, we install the Snapcast server [package](https://packages.debian.org/testing/snapserver).
[Installing from `testing`]({{< ref "server-install#installing-selectively-from-testing" >}}) is sinful and not actually necessary here, but there you go (I should start using NixOS instead).

```shell
sudo apt update
sudo apt install -t testing snapserver
```

We'll also install {{< ghn "badaix/snapweb" >}} to provide a web interface for setting sources and volumes of clients.
Check the [latest version](https://github.com/badaix/snapweb/releases) and

```shell
wget https://github.com/badaix/snapweb/releases/download/v0.8.0/snapweb_0.8.0-1_all.deb
sudo apt install ./snapweb_0.8.0-1_all.deb
```

We configure Snapcast to listen to MPD through the named pipe and use the newly installed Snapweb,

```shell
sudo vim /etc/snapclient.conf
```
```vim
[http]
doc_root = /usr/share/snapweb
[stream]
stream = pipe:///tmp/snapfifo?name=MPD
```

Then restart to apply changes,

```shell
sudo apt restart snapserver
```

Snapweb is accessible at `http://<server address>:1780`.
I'd rather have it at a subdomain behind a reverse proxy but I had some [difficulties](https://github.com/badaix/snapweb/issues/54) getting that working.

## Clients

My [RPis]({{< ref "rpi-mpd#setup-the-rpi" >}}) are ready, running Raspberry Pi OS Lite based on Debian 11/Bullseye and 12/Bookworm respectively.
On each RPi, we install the Snapcast client [package](https://packages.debian.org/stable/snapclient),

```shell
sudo apt update
sudo apt install snapclient
```

and configure it to use our server,

```shell
sudo vim /etc/default/snapclient
```
```vim
SNAPCLIENT_OPTS="--host <server address>"
```

Then restart to apply changes,

```shell
sudo apt restart snapclient
```

and that should be it!
Connect to the server MPD instance with your favourite MPD [client]({{< ref "rpi-mpd#clients" >}}) and start enjoying some music while wandering around your house.

## Troubleshooting

* We can check the logs of the snapcast server and clients on their respective machines with
    ```shell
    sudo systemctl status snapserver # or snapclient
    ```
* We can test if the Snapcast server can pass sound to the clients and that the clients can receive and play it by running this on the server machine,
    ```shell
    cat /dev/urandom > /tmp/snapfifo
    ```
* Ensure that the volume is up on MPD! (Yes, this has me foiled for a good bit of time...)

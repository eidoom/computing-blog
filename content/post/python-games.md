+++
title = "Games catalogue"
date = 2020-07-08T18:28:30+01:00
categories = ["coding"]
tags = ["python","game","cpp","linux","desktop","laptop","rust","ai","minimax","negamax","alpha-beta-pruning","random","pyglet","tilemap","2d-graphics","algorithm","search","terminal","shell","argparse","object-orientated","functional","gui","hud","animation","sprite","procedural-generation","physics","collision-detection","c++","coding","programming","project"]
description = "A list of the little games I've made"
toc = true
cover = "img/covers/73.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## N-in-a-row
{{< cal 2017-03-11 >}}
<i class="fab fa-github meta-icon sub-icon sub-icon"></i> https://github.com/eidoom/python-n-in-a-row
<i class="fab fa-python meta-icon sub-icon sub-icon"></i>
<i class="fa fa-terminal meta-icon sub-icon sub-icon"></i>

The first game worth mention.
Play a generalisation of [noughts and crosses](https://en.wikipedia.org/wiki/Tic-tac-toe) in the terminal where the board sides and victory row length can be any size.
Optionally turn on gravity for a similar generalisation of [four-in-a-row](https://en.wikipedia.org/wiki/Four-in-a-row).

I played around with the AI mostly here, [starting](https://github.com/eidoom/python-n-in-a-row/blob/6cff18fc989f97b9c0ad8354277ac1b77c6a2a5b/main.py#L450) with a standard [minimax](https://en.wikipedia.org/wiki/Minimax) algorithm, then [improving](https://github.com/eidoom/python-n-in-a-row/blob/6cff18fc989f97b9c0ad8354277ac1b77c6a2a5b/main.py#L489) it with [alpha-beta pruning](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning) and [streamlining](https://github.com/eidoom/python-n-in-a-row/blob/6cff18fc989f97b9c0ad8354277ac1b77c6a2a5b/main.py#L175) to fail-soft [negamax](https://en.wikipedia.org/wiki/Negamax).
I used [recursion](https://en.wikipedia.org/wiki/Recursion_(computer_science)) to express the logic elegantly.
All the algorithms are in the [main file](https://github.com/eidoom/python-n-in-a-row/blob/master/main.py), with the unused ones commented out at the end.
I found [Chess Programming Wiki](https://www.chessprogramming.org/Main_Page) to be a great resource on the algorithms, see [iterative search](https://www.chessprogramming.org/Iterative_Search), [minimax](https://www.chessprogramming.org/Minimax), [alpha-beta](https://www.chessprogramming.org/Alpha-Beta), [fail-soft](https://www.chessprogramming.org/Fail-Soft), and [negamax](https://www.chessprogramming.org/Negamax).

## WORLd
{{< cal 2019-01-18 >}}
<i class="fab fa-github meta-icon sub-icon sub-icon"></i> https://github.com/eidoom/flavour-anomalies-game
<i class="fab fa-python meta-icon sub-icon sub-icon"></i>
<i class="fa fa-terminal meta-icon sub-icon sub-icon"></i>

A terminal-based [walking simulator](https://en.wikipedia.org/wiki/Adventure_game#Walking_simulators).
The map is [procedurally generated](https://en.wikipedia.org/wiki/Procedural_generation) each time the game is run.

## Minor Planets
{{< cal 2019-03-11 >}}
<i class="fab fa-github meta-icon sub-icon"></i> https://github.com/eidoom/learning-pyglet
<i class="fab fa-python meta-icon sub-icon"></i>
<i class="fa fa-image meta-icon sub-icon"></i>

This was a project to learn [`pyglet`](https://en.wikipedia.org/wiki/Pyglet).
I followed the [`pyglet` tutorial](https://pyglet.readthedocs.io/en/latest/programming_guide/examplegame.html) to make an [Asteroids](https://en.wikipedia.org/wiki/Asteroids_(video_game)) clone.

An attempt to [implement](https://github.com/eidoom/learning-pyglet/blob/4a09e411a0817124a8b488624520fc6ecdd5e718/game/physicalobject.py#L102) more realistic physics in the form of [Newtonian elastic collisions](https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects) led to some very entertaining asteroid behaviour.
It's likely due to my treatment of [hitboxes](https://en.wikipedia.org/wiki/Collision_detection) and "decay" of asteroids into child asteroids when they're shot.

## WORLd 2
{{< cal 2019-03-27 >}}
<i class="fab fa-github meta-icon sub-icon"></i> https://github.com/eidoom/learning-tilemaps
<i class="fab fa-python meta-icon sub-icon"></i> 
<i class="fa fa-image meta-icon sub-icon"></i>

Having [learnt `pyglet`]({{< ref "#minor-planets" >}}), I revisited the idea of [WORLd]({{< ref "#world" >}}) with proper 2D graphics.
I built the world as a [tilemap](https://en.wikipedia.org/wiki/Tile-based_video_game) and dropped [sprites](https://en.wikipedia.org/wiki/Sprite_(computer_graphics)) on top of this for objects and characters.
I also built a [GUI](https://en.wikipedia.org/wiki/Graphical_user_interface): a [HUD](https://en.wikipedia.org/wiki/Heads-up_display_(video_games)) and a main menu.
Simple image sequences make for charming animations of the stick-figure protagonist.
The game includes [foes](https://en.wikipedia.org/wiki/Non-player_character) which the player must vanquish using their magical abilities.

## Plantus
{{< cal 2019-07-20 >}}
<i class="fab fa-github meta-icon sub-icon"></i> https://github.com/eidoom/plant-game
<i class="fab fa-python meta-icon sub-icon"></i> 
<i class="fa fa-image meta-icon sub-icon"></i>

A simple parody [clicker game](https://en.wikipedia.org/wiki/Incremental_game).

## New World
{{< cal 2019-12-03 >}}
<i class="fab fa-github meta-icon sub-icon"></i> https://github.com/eidoom/new-world
<i class="fab fa-cuttlefish meta-icon sub-icon"></i> 
<i class="fa fa-terminal meta-icon sub-icon"></i>

An [interactive story](https://en.wikipedia.org/wiki/Interactive_fiction) in the terminal, written to learn more C++.
Only Chapter One is done.

## Cellular automatum
{{< cal 2020-03-06 >}}
<i class="fab fa-gitlab meta-icon sub-icon"></i> https://gitlab.com/eidoom/cellular-automata
<i class="fab fa-cuttlefish meta-icon sub-icon"></i> 
<i class="fa fa-image meta-icon sub-icon"></i>

A [zero-player game](https://en.wikipedia.org/wiki/Zero-player_game) is still a game, right?
See [this post]({{< ref "cellular-automata" >}}).

## Trying Rust
{{< cal 2020-03-06 >}}
<i class="fab fa-gitlab meta-icon sub-icon"></i> https://gitlab.com/eidoom/trying-rust
<i class="fab fa-rust meta-icon sub-icon"></i> 
<i class="fa fa-terminal meta-icon sub-icon"></i>

A very barebones text-based game.
More about learning Rust than making a game.
See [this post]({{< ref "learning-rust" >}}).

+++
title = "Virtual Galton board"
date = 2021-04-04T20:10:58+01:00
categories = ["simulation","web"]
tags = ["physics","outreach","public-engagement","science-communication","html","css","javascript","rollup","typescript","sass","canvas","static-website","gitlab-ci","gitlab-pages","gitlab","physics-engine","gravity","force","position","velocity","speed","acceleration","collisions","collision-detection","velocity-verlet","numerical-integration","newton","classical-mechanics","project","elastic-collisions","dynamics","kinetics","2d"]
description = "Duplicating a physics outreach exhibit on the web"
toc = true
draft = false
cover = "img/covers/55.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

At the university institute where I am doing my PhD, we have a [Galton board](https://www.modellinginvisible.org/exhibits/) {{< wiki "Galton board" >}}  (aka bean machine, quincunx) outreach exhibit.
Since we can't see anyone in person during lockdown, last year I made a virtual version of the exhibit.
The source is at {{< gl galton-board-web >}} and the web app is live [here](https://eidoom.gitlab.io/galton-board-web/).

<!-- <iframe src="https://eidoom.gitlab.io/galton-board-web" class="window"></iframe> -->

## The spiel

The demonstration shows how sampling from a discrete binomial distribution tends to a continuous normal distribution with enough samples.
The binomial distribution arises if we consider that a ball has equal probability of falling to the left or right of each pin that it encounters, building up (unnormalised) probabilities given by Pascal's triangle.
This represents the background or noise of a scientific measurement, like when measuring cross-sections of high energy collisions at particle colliders, we cannot avoid measuring all the decay products that are irrelevant (as well as the one we want) since they come together in a big mess.

We can then add barriers into the triangular pin grid to change the shape of the collected balls at the bottom.
The barriers represent the thing we want to probe in a real experiment and the change of shape of the resulting histogram is the signal we want to measure.
In real life such measurements, we can't see the object of interest-it could be a particular interaction of gluons in a proton-and we must determine its properties from what we can measure, which is the histogram we see at the bottom of the Galton board.

The experiment shows that to better resolve the signal from the background, we need a high number of samples of each ("better statistics").
To figure out the struture which cannot be directly probed, we have to make precise predictions with theoretical models to compare to the experimental results.
In particle physics, this is called phenomenology.

## Web technology

I've somewhat failed in seeing the thing get used but I learnt a lot about web development along the way.

I initially wrote it in [VanillaJS](http://vanilla-js.com/), using [MDN Web Docs](https://developer.mozilla.org/en-US/) heavily since I'd never used JavaScript before.
The animation is drawn with the performant [`canvas`](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API).
I used [Chart.js](https://www.chartjs.org/) for the histogram.
I bundled the static web app with [rollup](https://rollupjs.org/guide/en/).
Later, I switched to [TypeScript](https://www.typescriptlang.org/) to see what all the hype was about (static typing does make debugging easier) and tried [Sass](https://sass-lang.com/) (which I like) for the styling, using Rollup to "compile".
The colour scheme is that of the university, before you think I came up with it.

## Simulation physics

The simulation interaction physics is entirely [elastic collisions](https://en.wikipedia.org/wiki/Elastic_collision#Two-dimensional_collision_with_two_moving_objects) between balls and obstacles (the sides, the barriers, and the pins).
Balls do not bounce off each other; the principles of the exhibit are independent of particle self-interaction.
The system is evolved by numerical integration of the equations of motions using the [velocity Verlet](https://en.wikipedia.org/wiki/Verlet_integration#Velocity_Verlet) algorithm.

## Unforeseen consequences

I made an exact scale replica of the physical Galton board for the web version.
I found that it wasn't great at producing a perfect normal distribution in the large sampling limit.
I suspect that this is because the balls fall too easily through the pins, putting the experiment in a low scattering regime in which the assumption of a ball's path having binomial statistics fails.
I added another mode where the ratio of the ball's size to the gaps between pins was increased.
This puts us in the high scattering regime and produces lovely normal distributions.

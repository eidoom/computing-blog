+++
title = "A server in the cloud"
date = 2022-05-03T20:15:17+02:00
categories = ["self-host"]
tags = ["server","cdist","cloud","fedora","debian","debian-11","fedora-36","ddns","nginx","prosody","xmpp","tls","ssl","duckdns","lets-encrypt","radicale","reverse-proxy","https","dynamic-dns","caldav","carddav","calendar","journal","diary","memos","contacts","address-book","infrastructure","hosting","backend","firewalld","stun","turn","nsd","http","debian-bullseye","wireguard","vpn","eturnal","ovh","mumble","murmur","unbound","dns","nameserver","fail2ban","bash","shell","terminal","firewall","security","hardening","dos","ddos","converse.js","omemo","certbot","cloud-server","home"]
description = "With running my home server now being unfeasible, I replaced it with a remote server"
toc = true
draft = false
cover = "img/covers/24.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

DEPRECATED: I've gone back to [hosting at home]({{< ref "server-install" >}}) using a light Debian installation without `cdist` or containers. It's [accessible]({{< ref "ipv6" >}}) over the Internet via IPv6.

## Purpose

Things I'll run on this server:

* Nginx reverse proxy, similar to [previous setup]({{< ref "caldav/#nginx" >}})
* I'll use DuckDNS dynamic DNS (DDNS) [again]({{< ref "caldav/#duckdns" >}})
* And Let's Encrypt TLS [again]({{< ref "caldav/#tls" >}})
* I'd like to run a [Prosody XMPP server](https://prosody.im/), possibly with [converse.js](https://conversejs.org/)
* I'll run Radicale [again]({{< ref "caldav/#radicale" >}}) as my Cal/CardDAV server, possibly with [InfCloud](https://inf-it.com/open-source/clients/infcloud/)
* [Wireguard VPN]({{< ref "wireguard" >}})
* Maybe I'll manage my passwords with [`pass`](https://www.passwordstore.org/)
* I'll use a software configuration management tool to set it all up: [`cdist`](https://www.cdi.st/)

## Server

I would prefer to run my own hardware, but my living situation just now makes that awkward.
Instead, I rent a remote server.
I use a VPS (virtual private server) rather than a dedicated server because this is a modest use case.

### Host

I chose [OVH VPS](https://www.ovhcloud.com/en-gb/vps/) because it was cheap.
I went for the Starter tier:

Hardware|Amount
---|---
Processor|1 vCore 
Memory|2 GB 
Storage|20 GB SSD NVMe 
Bandwidth|100 Mbit/s 

I chose Debian 11 as the OS.
I chose the "Western Europe, France, Gravelines (GRA)" location.

### Setup

SSH in with default account
```shell
ssh-copy-id <default>@<address>
ssh <default>@<address>
```

Change root password
```shell
sudo su -
passwd
```

Allow root login
```shell
sudo vim /etc/ssh/sshd_config
```
```vim
...
PermitRootLogin yes
...
```

```shell
sudo systemctl restart sshd
exit
```

SSH in as `root` to make sure it worked
```shell
ssh-copy-id root@<address>
ssh root@<address>
```

Soon we'll disallow password logins (see [`__conf_ssh`]({{< ref "#__conf_ssh" >}})), so I did `ssh-copy-id` on anther local machine too, so I won't lose access if my primary local machine dies.

### Security

OVH has a page of [basic security recommendations](https://docs.ovh.com/ie/en/vps/tips-for-securing-a-vps/).
I take some precautions with [`__conf_ssh`]({{< ref "#__conf_ssh" >}}), [`__firewalld`]({{< ref "#__firewalld" >}}) and [`__fail2ban`]({{< ref "#__fail2ban" >}}).

## Software configuration management tool

> {{< cal 2023-02-16 >}}
>
> Note to self: I no longer use `cdist` on this server and have recently been configuring software manually.
> This was because `cdist` was too cumbersome when I wanted to quickly try out new things.
> Of course, I'll regret this when there comes a time that I want to migrate to another cloud service.
> Next time, since my needs are quite basic, I may try writing my own idempotent installation script.

### cdist

> I considered [Ansible](https://www.ansible.com/)
> {{< wiki "Ansible_(software)" >}}
> {{< ghubn ansible ansible >}}
> {{< docs "https://docs.ansible.com/" >}}
> {{< pack "https://packages.fedoraproject.org/pkgs/ansible/ansible/" >}}
> ```shell
> sudo dnf install ansible
> ```
> first, but ended up going with `cdist` as it was closer to the shell experience I was used to.

[`cdist`](https://www.cdi.st/)
{{< wiki Cdist >}}
{{< git "https://code.ungleich.ch/ungleich-public/cdist.git" >}}
{{< docs "https://www.cdi.st/manual/latest/" >}}
{{< pypi "cdist" >}}
{{< pl "https://packages.fedoraproject.org/pkgs/cdist/cdist/" >}}
is a software configuration management tool.
We run it on a local machine and it configures the remote server over `ssh` according to our `manifest`, which uses `type`s to modularise the configuration.

My main gripe with `cdist` concerns security.
I really don't like that it requires `root` `ssh`.
Note that `cdist config` can be used with a custom `ssh` port (ie. not port 22) by configuring the remote in `~/.ssh/config`.

> {{< cal "2022-06-11" >}}
>
> `python3.10` and the latest stable release `cdist6.9.8` are not compatible.
> We can use `python3.9` {{< pl "https://packages.fedoraproject.org/pkgs/python3.9/python3.9/" >}} to use the `pip` version
> ```shell
> sudo dnf install python3.9
> python3.9 -m ensurepip
> pip3.9 install --user cdist
> cdist -h
> ```
> or use the `git` version of `cdist` as I do below.
> Note that `6.9.8` does not have the builtin `__letsencrypt_cert` type that I use.

Install it from `git` source with

```shell
cd ~/git
git clone https://code.ungleich.ch/ungleich-public/cdist.git
cd cdist
echo "export PATH=\$PATH:$(pwd -P)/bin" # add to shell rc
./bin/cdist-build-helper version
make install-user
```

### Types

I have made myself some `cdist` `type`s at {{< gl cloud-infrastructure >}}.
Install them with
```shell
cd ~/git
git clone git@gitlab.com:eidoom/cloud-infrastructure.git
cd cloud-infrastructure
./install.bash
```
These include:

#### `__conf_ssh`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__conf_ssh" >}}
Ensure `~/.ssh` exists with correct permissions and is populated with a key pair for `root` and `user`, add an authorised public key, and configure `/etc/ssh/sshd_config` with a custom SSH port for a layer of security by obscurity, no password logins (only public keys), a maximum of three login attempts, to only accept `ed25519` public keys, no X11 forwarding, and allow `root` logins by public key (for `cdist`).

#### `__duckdns`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__duckdns" >}} Sets up a script to provide the machine's IP address to the DDNS provider DuckDNS. If the `--dynamic` option is given, also set up a cronjob to update the IP address given to the DDNS provider every five minutes.

#### `__eturnal`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__eturnal" >}} Set up [Eturnal](https://eturnal.net/) {{< wl STUN >}}/{{< wl TURN Traversal_Using_Relays_around_NAT >}} server, installed from [`eturnal` `extrepo`](https://eturnal.net/documentation/code/readme.html#installation). Logs in `/var/log/eturnal/eturnal.log`

> {{< cal 2023-02-16 >}}
>
> Originally set up to be used with [Prosody]({{< ref "#__prosody" >}}), now used with [Synapse]({{< ref "matrix-synapse" >}}).

#### `__fail2ban`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__fail2ban" >}} Set up [Fail2ban](https://www.fail2ban.org/wiki/index.php/Main_Page) {{< wiki Fail2ban >}} {{< ghub fail2ban fail2ban >}} {{< arch "Fail2ban" >}} {{< pl "https://packages.debian.org/stable/fail2ban" >}} to block brute force attacks on `ssh` (in aggressive mode since OVH seems to get {{< wl "DoSed" "Denial-of-service_attack" >}} often), [`nginx`]({{< ref "#__reverse_proxy" >}}), and [`murmur`]({{< ref "#__mumble" >}}).

#### `__firewalld`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__firewalld" >}} Sets up [`firewalld`](https://firewalld.org/) {{< wiki firewalld >}} {{< arch firewalld >}} {{< ghub firewalld firewalld >}} with the `http` and `https` ports open on the `--interface`.

#### `__init_misc`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__init_misc" >}} Install `rsync`.

#### `__init_user`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__init_user" >}} Sets up a `user` using `bash` shell with a home directory `/home/user/`.

#### `__mumble`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__mumble" >}} Set up Murmur server {{< pl "https://packages.debian.org/stable/mumble-server" >}} for [Mumble](https://www.mumble.info/) {{< wiki "Mumble_(software)" >}} {{< ghub mumble-voip mumble >}}.

#### `__nsd`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__nsd" >}} [NSD authoritative nameserver]({{< ref "new-dns#nsd" >}}) to manage subdomains.

> {{< cal 2023-02-16 >}}
>
> I'm no longer using this.
> ```shell
> sudo systemctl stop nsd
> sudo systemctl disable nsd
> ```

#### `__prosody_wrapper`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__prosody_wrapper" >}} Wrapper for [`__prosody`]({{< ref "#__prosody" >}}) and [`__eturnal`]({{< ref "#__eturnal" >}}). For XMPP clients, I use [Conversations](https://conversations.im/) {{< pl "https://f-droid.org/en/packages/eu.siacs.conversations/" >}} on Android and [Dino](https://dino.im/) {{< pl "https://packages.fedoraproject.org/pkgs/dino/dino/" >}} on Linux. I have friends and family on [Snikket](https://snikket.org/) for [Android](https://play.google.com/store/apps/details?id=org.snikket.android) and [iOS](https://apps.apple.com/us/app/snikket/id1545164189).

> {{< cal 2023-02-16 >}}
>
> Deprecated in favour of [Matrix on Synapse]({{< ref "matrix-synapse" >}}).

#### `__prosody`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__prosody" >}} Set up [Prosody](https://prosody.im/) {{< wiki "Prosody_(software)" >}} {{< pl "https://packages.debian.org/stable/prosody" >}} [XMPP](https://xmpp.org/) {{< wiki XMPP >}} server linked to Eturnal server from [`__eturnal`]({{< ref "#__eturnal" >}}) and with Prosody plugins for [XMPP compatibility](https://compliance.conversations.im/server/eidoom.duckdns.org/) and [converse.js](https://conversejs.org/) with [OMEMO encryption](https://conversations.im/omemo/) {{< wiki OMEMO >}} support. Logs in `/var/log/prosody/prosody.log`. See also [`__prosody_wrapper`]({{< ref "#__prosody_wrapper" >}}).

> {{< cal 2023-02-16 >}}
>
> I'm no longer using this.
> ```shell
> sudo systemctl stop prosody
> sudo systemctl disable prosody
> sudo apt remove prosody
> sudo rm /etc/apt/sources.list.d/extrepo_prosody.sources
> ```

#### `__radicale`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__radicale" >}} Installs `radicale` from `pip3` and sets it up to run as a `systemd` service. Also puts the collections in a `git` repository (with address set by the `--repo` option) which commits and pushes to a `git` remote at midnight every day.

#### `__reverse_proxy`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__reverse_proxy" >}} Sets up an `nginx` reverse proxy using Let's Encrypt for TLS (installed by [`__tls_services`]({{< ref "#__tls_services" >}})). Redirects `http` to `https`. Has a landing page at the root address. Serves [`radicale`]({{< ref "#__radicale" >}}) and [`converse`]({{< ref "#__prosody" >}}). Runs as a `systemd` service. Consider similar: [`__nginx`](https://www.cdi.st/manual/contrib/man7/cdist-type__nginx.html) by [cdist-contrib](https://www.cdi.st/manual/contrib/index.html).

#### `__tls_services`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__tls_services" >}} Wrapper for all services using TLS: [`__reverse_proxy`]({{< ref "#__reverse_proxy" >}}), [`__prosody_wrapper`]({{< ref "#__prosody_wrapper" >}}), [`__mumble`]({{< ref "#__mumble" >}}). Installs TLS certificate from Let's Encrypt with [`__letsencrypt_cert`](https://www.cdi.st/manual/latest/man7/cdist-type__letsencrypt_cert.html) in [`webroot`](https://eff-certbot.readthedocs.io/en/stable/using.html?highlight=deploy-hook#webroot) authentication mode using a [`http-01`]("https://datatracker.ietf.org/doc/html/rfc8555#section-8.3) challenge.

> {{< cal 2023-02-16 >}}
>
> Deprecated in favour of [`__tls_wildcard_duckdns`]({{< ref "#__tls_wildcard_duckdns" >}}).

#### `__tls_wildcard_duckdns`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__tls_wildcard_duckdns" >}} Uses [`certbot`](https://certbot.eff.org/) {{< pl "https://packages.debian.org/stable/certbot" >}} to obtain certificates from Let's Encrypt by [`manual`](https://eff-certbot.readthedocs.io/en/stable/using.html?highlight=deploy-hook#manual) authentication using a [`dns-01`](https://datatracker.ietf.org/doc/html/rfc8555#section-8.4) challenge. An [authentication hook script](https://eff-certbot.readthedocs.io/en/stable/using.html?highlight=deploy-hook#hooks) provides automatic renewals by [setting up the DNS TXT challenge](https://www.duckdns.org/spec.jsp) like [this](https://gitlab.com/eidoom/cloud-infrastructure/-/blob/02282a7218ff5cc9b57656205b6f4476a18d7925/type/__tls_wildcard_duckdns/manifest#L39) ([alternative](https://github.com/infinityofspace/certbot_dns_duckdns)).

#### `__unbound`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__unbound" >}} Set up [Unbound recursive nameserver]({{< ref "dns-unbound#unbound" >}}). I use this as my nameserver on the VPN.

#### `__update`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__update" >}} Updates all packages.

#### `__wireguard`

{{< src "https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__wireguard" >}} Set up [Wireguard VPN]({{< ref "vpn-wireguard" >}}).


### Manifest

I put them together in a private initial manifest.
It resides at {{< gl cloud-infrastructure-private >}}, although you need to be me to access this.
I similarly install it as
```shell
cd ~/git
git clone git@gitlab.com:eidoom/cloud-infrastructure-private.git
cd cloud-infrastructure-private
./install.bash
```

The initial manifest looks like (hiding my information)
```shell
~/.cdist/manifest/init
```
```vim
export CDIST_ORDER_DEPENDENCY=1

__init_misc

__init_user "$user"

__conf_ssh ssh \
    --user "$user" \
    --pubkey "$ssh_pubkey" \
    --port "$ssh_port"

__duckdns dns \
    --user "$user" \
    --domain "$domain" \
    --token "$token"

# actually need to now manually copy public keys to `git`
# remote before can use `__radicale` in `__tls_services`

__firewalld firewall \
    --interface "$interface" \
    --ssh_port "$ssh_port"

__radicale caldav \
    --user "$user" \
    --repo "$radicale_repo" \
    --radicale_user "$radicale_user" \
    --radicale_password "$radicale_password"

__nsd authoritative_nameserver \
    --port "$nsd_port" \
    --admin "$admin" \
    --ip_address "$ip_address" \
    --domain "$domain"

__unbound recursive_nameserver

__tls_services lets_encrypt \
    --domain "$domain" \
    --email "$email" \
    --webroot "$webroot" \
    --prosody_admin "$admin" \
    --turn_port "$turn_port" \
    --mumble_password "$mumble_password" \
    --mumble_port "$mumble_port" \
    --mumble_welcome_text "$mumble_welcome_text" \
    --mumble_register_name "$mumble_register_name" \
    --omemo_path "$omemo_path" \
    --omemo_address "$omemo_address"

__wireguard vpn \
    --address "$vpn_address" \
    --port "$vpn_port"

__fail2ban secure \
    --private_addresses "$vpn_addresses"
```

Run it with
```shell
cdist config -v -i ~/.cdist/manifest/init <target>
```
where `<target>` is defined in `~/.ssh/config` with custom `ssh` port.

## Cal/CardDAV

I'm using `radicale`, but I'm considering switching to [`xandikos`](https://www.xandikos.org/)
{{< ghubn jelmer xandikos >}}
{{< pypi "xandikos" >}}
{{< pl "https://packages.debian.org/stable/xandikos" >}}
.
[Here](https://vdirsyncer.pimutils.org/en/stable/tutorials/radicale.html) is the prompt, including [better standard compatibility](https://en.wikipedia.org/wiki/Comparison_of_CalDAV_and_CardDAV_implementations) and wider client support.
However, [it doesn't seem quite so convenient to use](https://blog.firedrake.org/archive/2019/07/Xandikos_Calendar_Server_on_Debian_10.html).

> {{< cal 2022-06-14 >}}
>
> [Xandikos doc site](https://xandikos.org/docs/) down but can build locally from the repo.

> {{< cal 2022-10-22 >}}
>
> So far, `radicale` works and I've stuck with it.

### Cleaning up contacts
When I was compiling all my contacts together, I ended up with inconsistent notation in my `vcf` contact files and duplicate entries of numbers under contacts, for example with and without the country code.
I set up a wee tool {{< gl contact-cleaner >}} to take care of this.
It uses `vdirsyncer` {{< ghubn pimutils vdirsyncer >}} {{< docs "https://vdirsyncer.pimutils.org/en/stable/" >}} to [sync files with the CalDAV server](https://gitlab.com/eidoom/contact-cleaner/-/blob/main/sync.py), `khard` {{< ghubn lucc khard >}} {{< docs "https://khard.readthedocs.io/en/latest/" >}} to [pretty print contacts to the terminal](https://gitlab.com/eidoom/contact-cleaner/-/blob/main/print.py), and a simple [Python script](https://gitlab.com/eidoom/contact-cleaner/-/blob/main/clean.py) to clean the contacts.

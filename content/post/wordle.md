+++
title = "Wordle fun"
date = 2022-10-22T16:04:17+02:00
categories = ["data"]
tags = ["project","python","game","reimplementation","python3","wordle","data","analysis","data-analysis","coding","programming"]
description = "I couldn't not"
toc = true
draft = false
cover = "img/covers/116.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I joined in the Wordle fun by creating a [solver](https://gitlab.com/eidoom/wordle-fun/-/blob/main/oracle.py), which shows you possible words for a given game state and ranks them by various simple algorithms, and [terminal implementation of the game](https://gitlab.com/eidoom/wordle-fun/-/blob/main/play.py) at {{< gl wordle-fun >}}.

+++
title = "Yet another iteration of the stories blog"
date = 2023-05-31T14:15:37+02:00
categories = ["web"]
tags = ["js","javascript","html","css","svelte","sveltekit","sqlite3","sqlite","sql","better-sqlite3","npm","node","nodejs","nginx","argon2","authentication","sessions","password","user","website","web","dynamic-website","merriweather-font","db","database","material-icons","salt","hash","markdown","markdown-it","vite"]
description = "Migrating to a dynamic website setup"
toc = true
cover = "img/covers/125.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking concept art fantasy'-'borders'"
+++

Moving to the third stage of its life, I made [yet]({{< ref "zola" >}}) [another]({{< ref "stories" >}}) version of my stories blog at {{< gl stories2 >}}.
This time, I keep the data in a SQLite database, which can be interacted with from the website by the admin user.
It's build with the SvelteKit framework.

It's served [here](https://stories.eidoom.duckdns.org/), but I make no promises of URL permanence.

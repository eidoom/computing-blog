+++
title = "Facebook messages analytics"
date = 2020-07-08T18:04:16+01:00
edit = 2021-04-05
categories = ["data"]
tags = ["mini-project","facebook","python","plot","chart","graph","linux","fedora","desktop","laptop","pandas","matplotlib","wordcloud","nltk","data","pypi","dnf","data-analysis"]
description = "Playing with the data of my Facebook messages"
toc = true
cover = "img/covers/74.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I discovered that you can [download all of your messages from Facebook](https://gitlab.com/eidoom/facebook-messages-analytics#getting-the-information).
I though it would be fun to make some graphs using this data, so made a [project](https://gitlab.com/eidoom/facebook-messages-analytics) for it.

I put together a Python script with the help of some [Python packages](https://gitlab.com/eidoom/facebook-messages-analytics#dependancies) to get the data out in a usable format and process it into a
* wordcloud
* histogram of words used by number of letters in the word

This involved using [`nltk`](https://en.wikipedia.org/wiki/Natural_Language_Toolkit) to chuck out [boring words](https://en.wikipedia.org/wiki/Stop_words) from the data, [`pandas`](https://en.wikipedia.org/wiki/Pandas_%28software%29) to handle statistics, [`matplotlib`](https://en.wikipedia.org/wiki/Matplotlib) as the plotting library, and [`wordcloud`](https://github.com/amueller/word_cloud) to generate the wordclouds.

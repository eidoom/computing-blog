+++
title = "Installing Nvidia drivers on Fedora"
date = "2019-11-09T14:12:40Z"
edit = 2020-04-29
tags = ["linux","desktop","install","drivers","dkms","fedora","nvidia","graphics","geforce","gtx-1080-ti","geforce-10-series","pascal","gpu","rpmfusion","negativo17"]
categories = ["maintenance","driver"]
description = "How to get the proprietary drivers working"
toc = true
cover = "img/covers/103.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## DEPRECATED
{{< cal "2021-11-25" >}}

[I switched package from Negativo17 to RPMFusion.]({{< ref "linux-video#update-on-drivers" >}})

## The market

I have an Nvidia GeForce GTX 1080 Ti in my desktop, which runs [Fedora](https://getfedora.org/) 31.
Using the open source [Nouveau](https://nouveau.freedesktop.org/wiki/) drivers was easy, just install and go.
In search of optimal performance and power management, I decided to install the proprietary drivers.
The packaging options were to use [RPMFusion](https://rpmfusion.org/Howto/NVIDIA), [Negativo17](https://negativo17.org/nvidia-driver/), or the [official Nvidia](https://www.nvidia.com/en-us/drivers/unix/) binaries.
Getting that working was a finicky one indeed.
Be warned that changing from one of these options to another can require a system reinstall. 
Here's the solution I found using Negativo17 which worked with version 440.36.

> {{< cal 2022-10-31 >}}
>
> Another reason to use the proprietary drivers is that Nouveau does not support CUDA or OpenCL.

## Negativo17

### Installation

Start by removing any existing Nvidia drivers from the system

```shell
sudo dnf remove '*nvidia*'
```

although I'd recommend starting from a fresh OS install.

Install [DKMS](https://en.wikipedia.org/wiki/Dynamic_Kernel_Module_Support) and dependencies to handle the Nvidia driver as a kernal module `nvidia`

```shell
sudo dnf install dkms kernel-devel
```

DKMS will automatically rebuild the `nvidia` module when the Linux kernel is updated.

Add the Negativo17 Nvidia driver repository with 

```shell
sudo dnf config-manager --add-repo=https://negativo17.org/repos/fedora-nvidia.repo
```

and install the drivers with

```shell
sudo dnf install nvidia-driver dkms-nvidia nvidia-driver-libs.i686 nvidia-settings
```

Check that the driver install has disabled Nouveau by appending `rd.driver.blacklist=nouveau` to `GRUB_CMDLINE_LINUX` in `/etc/default/grub`.

```shell
sudo vi /etc/default/grub 
```
```vim
GRUB_TIMEOUT=15
GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
GRUB_DEFAULT=saved
GRUB_DISABLE_SUBMENU=true
GRUB_TERMINAL_OUTPUT="console"
GRUB_CMDLINE_LINUX="quiet rd.driver.blacklist=nouveau"
GRUB_DISABLE_RECOVERY="true"
GRUB_ENABLE_BLSCFG=true
```

If any changes are made to `etc/default/grub`, then [regenerate the main configuration file](https://wiki.archlinux.org/index.php/GRUB#Generate_the_main_configuration_file) with (for an [EFI system](https://fedoraproject.org/wiki/GRUB_2#Create_a_GRUB_2_configuration))

```shell
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```

Check the driver install was successful with: 

```shell
dkms status
```
```shell
...
nvidia, 440.44, 5.4.8-200.fc31.x86_64, x86_64: installed
...
```
which shows `dkms` is reporting `nvidia` as installed, and check the driver is in use with:

```shell
lspci -v | grep VGA -A 10
```
```shell
01:00.0 VGA compatible controller: NVIDIA Corporation GP102 [GeForce GTX 1080 Ti] (rev a1) (prog-if 00 [VGA controller])
        Subsystem: NVIDIA Corporation Device 120f
        Flags: bus master, fast devsel, latency 0, IRQ 137
        Memory at ee000000 (32-bit, non-prefetchable) [size=16M]
        Memory at d0000000 (64-bit, prefetchable) [size=256M]
        Memory at e0000000 (64-bit, prefetchable) [size=32M]
        I/O ports at e000 [size=128]
        [virtual] Expansion ROM at 000c0000 [disabled] [size=128K]
        Capabilities: <access denied>
        Kernel driver in use: nvidia
        Kernel modules: nouveau, nvidia_drm, nvidia
```

We see that the kernel driver in use is `nvidia`, as wanted.

That's it!
I didn't install CUDA, see [here](https://negativo17.org/nvidia-driver/) for instructions.

If there are any problems, I found that [manually invoking DKMS](https://wiki.archlinux.org/index.php/Dynamic_Kernel_Module_Support#Usage) fixed mine

```shell
sudo dkms autoinstall
```

### Packages in detail

What were all those packaged I installed?

* `nvidia-driver` is obvious
* `nvidia-settings` is also self-explanatory
* I chose the kernal module package `dkms-nvidia` over the alternative `kmod-nvidia` since it has less build overhead, so has lighter and faster operation
* The 32 bit libraries (on a 64 bit system) `nvidia-driver-libs.i686` are required to use Steam

### Updates
{{< cal "20 Dec 2019" >}}

I recently ran into some issues while updating from version 440.36 to 440.44.
This was at the same time as a kernel update, although I don't know if that was related.
I ran 
```shell
sudo dnf update -y
``` 
as usual to update, noticing that `nvidia-driver` and `kernel` were in the update list.
I powered down the computer without looking at the messages from the update, which may have been a useful thing to do.
Regardless, I suspected `nvidia-driver` as the culprit when, on the next system start, only a blank screen with a blinking underline at the top right was displayed after the bootloader.

To get terminal access, I changed tty with `Ctrl`+`Alt`+`F2`.
If that doesn't work, I follow [this]({{< ref "broken" >}}).
Checking 
```shell
sudo dkms status
``` 
showed something had gone wrong, as it failed on looking for the `dkms.conf` for the old version of the `nvidia` module, 440.36, which had been removed by the update. 
I found the new module version number with
```shell
sudo dnf info nvidia-driver | grep Version
```
and built it for the new kernel with 
```shell
sudo dkms install nvidia/440.44
```
Removing the old module version for all kernel versions with 
```shell
sudo dkms remove nvidia/440.36 --all
``` 
failed due to the missing `dkms.conf`, so I removed it manually with 
```shell
sudo rm -rf /var/lib/dkms/nvidia/440.36
```

Now the DKMS status showed only the newest version installed
```shell
sudo dkms status
```
```vim
...
nvidia, 440.44, 5.3.16-300.fc31.x86_64, x86_64: installed
...
```
Everything then worked as normal on `sudo reboot`.

{{< cal "25 Jan 2020" >}}

Same problem again, on a kernel update only this time.
This time,
```shell
sudo dkms status
```
didn't show a version for the new kernel, 5.4.13-201, but 
```shell
sudo dkms install nvidia/440.44
```
wouldn't proceed because apparently the files for the new kernel already existed.
I did
```shell
sudo dkms remove nvidia/440.36 --all
sudo rm -rf /var/lib/dkms/nvidia/440.44
sudo dkms install nvidia/440.44
```
to fix it.
I'm not sure what the source of the problem is, particularly since this hasn't been the first kernel update since the last problem.

{{< cal "2021-11-25" >}}

See [this]({{< ref "linux-video#update-on-drivers" >}}) for details on me giving up on the Negativo17 packaging of the proprietary drivers in favour of RPMFusion.

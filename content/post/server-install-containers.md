+++
title = "A fresh home server II: containers"
date = 2020-06-05T12:16:08+01:00
categories = ["home","operating-system","software"]
tags = ["debian","server","install","docker","virtualisation","apt","debian-testing","lan","linux","update","samba","nfs","pihole","docker-compose","environment","container","nginx","reverse-proxy","transmission-bittorrent","plex","jellyfin","automate","calibre","calibre-web","jackett","sonarr","radarr","lidarr","linuxserver.io","rclone","cloud","cloud-storage","google-drive","backup","beets","self-host","file-server","h5ai","bittorrent","apache","guacamole","cli","gui","terminal","bash","shell","alias","ad-block","home-server","debian-buster","debian-10","port-forwarding","*arr"]
description = "Using containers to set up software on the server"
toc = true
cover = "img/covers/81.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

*This post is the second in a duology about setting up my home server again from scratch.
[Part one]({{< ref server-install >}}) is about setting up the server's operating system.
This, part two, is about using containers to run software on the server.*

## Installation
I'll use the `testing` versions of [`docker`](https://packages.debian.org/testing/docker.io) and [`docker-compose`](https://packages.debian.org/testing/docker-compose)
```shell
sudo apt install -t testing docker.io docker-compose
```
I added my user to the `docker` group so that I could run `docker` without `sudo`
```shell
sudo usermod -aG docker <user>
```
## Configuration
Although tailored for their specific Docker images, I found [this documentation](https://docs.linuxserver.io/) a useful brief introduction.

I set up the configuration for `docker-compose`
```shell
mkdir ~/dkr
cd ~/dkr
vi docker-compose.yml
```
starting the `yaml` with
```vim
version: "3.7"
services
```
Version 3.7 was the latest supported by my system, so I used that.

Then I added in the following premade services:

<table class="borderless">
<tbody>
<tr>
<td><a href={{< ref "#nfs" >}}><i class="fas fa-folder"></i></a></td>
<td><a href={{< ref "#nfs" >}}> NFS</a></td>
<td>file share with Linux clients</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#samba" >}}><i class="fas fa-folder"></i></a></td>
<td><a href={{< ref "#samba" >}}> Samba</a></td>
<td>file share with Windows clients</td>
</tr>
<tr>
<td><a href={{< ref "#reverse-proxy" >}}><i class="fas fa-server"></i></a></td>
<td><a href={{< ref "#reverse-proxy" >}}> Nginx reverse proxy</a></td>
<td>provides URLs for all the services</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#pi-hole" >}}><i class="fas fa-server"></i></a></td>
<td><a href={{< ref "#pi-hole" >}}> Pi-hole</a></td>
<td>an <a href="https://en.wikipedia.org/wiki/DNS_sinkhole">ad-blocking</a> <a href="https://en.wikipedia.org/wiki/Name_server#Caching_name_server">caching</a> <a href="https://en.wikipedia.org/wiki/Domain_Name_System">DNS</a> server</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#transmission" >}}><i class="fas fa-download"></i></a></td>
<td><a href={{< ref "#transmission" >}}> Transmission</a></td>
<td>BitTorrent client</td>
</tr>
<tr>
<td><a href={{< ref "#jackett" >}}><i class="fas fa-download"></i></a></td>
<td><a href={{< ref "#jackett" >}}> Jackett</a></td>
<td>tracker API support</td>
</tr>
<tr>
<td><a href={{< ref "#sonarr" >}}><i class="fas fa-tv"></i></a></td>
<td><a href={{< ref "#sonarr" >}}> Sonarr</a></td>
<td>television series library manager</td>
</tr>
<tr>
<td><a href={{< ref "#radarr" >}}><i class="fas fa-film"></i></a></td>
<td><a href={{< ref "#radarr" >}}> Radarr</a></td>
<td>film library manager</td>
</tr>
<tr>
<td><a href={{< ref "#lidarr" >}}><i class="fas fa-music"></i></a></td>
<td><a href={{< ref "#lidarr" >}}> Lidarr</a></td>
<td>music library manager</td>
</tr>
<tr>
<td><a href={{< ref "#readarr" >}}><i class="fas fa-book"></i></a></td>
<td><a href={{< ref "#readarr" >}}> Readarr</a></td>
<td>ebook library manager</td>
</tr>
<tr>
<td><a href={{< ref "#headphones" >}}><i class="fas fa-music"></i></a></td>
<td><a href={{< ref "#headphones" >}}> Headphones</td>
<td>music library manager</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#beets" >}}><i class="fas fa-music"></i></a></td>
<td><a href={{< ref "#beets" >}}> Beets</a></td>
<td>CLI music library manager</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#calibre" >}}><i class="fas fa-book"></i></a></td>
<td><a href={{< ref "#calibre" >}}> Calibre</a></td>
<td>ebook library manager</td>
</tr>
<tr>
<td><a href={{< ref "#calibre-web" >}}><i class="fas fa-book"></i></a></td>
<td><a href={{< ref "#calibre-web" >}}> Calibre-Web</a></td>
<td>web interface for ebook library</td>
</tr>
<tr>
<td><a href={{< ref "#plex" >}}><i class="fas fa-photo-video"></i></a></td>
<td><a href={{< ref "#plex" >}}> Plex</a></td>
<td>media player</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#jellyfin" >}}><i class="fas fa-photo-video"></i></a></td>
<td><a href={{< ref "#jellyfin" >}}> Jellyfin</a></td>
<td>media player</td>
</tr>
<tr>
<td><a href={{< ref "#h5ai" >}}><i class="fas fa-folder"></i></a></td>
<td><a href={{< ref "#h5ai" >}}> h5ai</a></td>
<td>web file browser</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#heimdall" >}}><i class="fas fa-columns"></i></a></td>
<td><a href={{< ref "#heimdall" >}}> Heimdall</td>
<td>dashboard</td>
<td><i class="fas fa-trash"></i></td>
</tr>
<tr>
<td><a href={{< ref "#rclone" >}}><i class="fas fa-cloud"></i></a></td>
<td><a href={{< ref "#rclone" >}}> Rclone</a></td>
<td>cloud sync</td>
</tr>
<tr>
<td><a href={{< ref "#ubooquity" >}}><i class="fas fa-book"></i></a></td>
<td><a href={{< ref "#ubooquity" >}}> Ubooquity</td>
<td>ebook reader</td>
<td><i class="fas fa-trash"></i></td>
</tr>
</tbody>
</table>

<!-- * [MPD]() https://hub.docker.com/r/vimagick/mpd ? -->
<!-- * https://hub.docker.com/r/sameersbn/squid -->
<!-- * add TLS! letsencrypt: discuss on new post -->
Follow the links to the sections below to see the docker-compose configuration snippet for each service.

I discovered the container images on [Docker Hub](https://hub.docker.com/), choosing them by testing the top few most popular and actively maintained images for each service and keeping whichever I got on with the best.

> Not all services from this list have survived my recent pruning.
> I have marked discarded items above with <i class="fas fa-trash"></i>.
> Further discussion on this below.

### NFS
I use [NFS](https://en.wikipedia.org/wiki/Network_File_System) to serve files from the server to Linux machines on the LAN.

In the server's previous iteration, [NFS was installed by `apt` and ran natively]({{< ref "nfs" >}}).
This time, I used the `docker` image [itsthenetwork/nfs-server-alpine](https://hub.docker.com/r/itsthenetwork/nfs-server-alpine) {{< ghub "sjiveson" "nfs-server-alpine" >}}.
```vim
  nfs-server:
    image: itsthenetwork/nfs-server-alpine
    container_name: nfs-server
    ports:
      - "2049:2049/tcp"
    privileged: true
    volumes:
      - /media/pool/data:/data
    environment:
      - "SHARED_DIRECTORY=/data"
      - "PERMITTED=192.168.1.*"
      - "TZ=Europe/London"
    depends_on:
      - "pihole"
    restart: always
```
This creates a writable share accessible by clients at `192.168.1.*`.
[Mount on clients]({{< ref "nfs#client" >}}) with
```shell
sudo mount HOSTNAME:/ mnt
```

> {{< cal 2021-08-24 >}} <i class="fas fa-trash"></i>
>
> I found this wasn't as stable as before so went back to the [native setup]({{< ref "nfs" >}}).

### Samba
I use [Samba](https://www.samba.org/) {{< git "https://git.samba.org/" >}} {{< wiki "Samba_(software)" >}} to share media files on the server with the desktop I use for [home cinema]({{< ref "home-cinema" >}}).

I used the image [dperson/samba](https://hub.docker.com/r/dperson/samba) {{< ghub "dperson" "samba" >}}.
```vim
  samba-server:
    image: dperson/samba
    container_name: samba-server
    ports:
      - "137:137/udp"
      - "138:138/udp"
      - "139:139/tcp"
      - "445:445/tcp"
    environment:
      TZ: 'Europe/London'
      USERID: 1000
      GROUPID: 1000
      NMBD: 'True'
      WORKGROUP: 'WORKGROUP'
      RECYCLE: 'False'
      USER: 'ryan;<password>;1000;ryan;1000'
      SHARE: 'data;/mount;yes;no;no;ryan;none;;all the data'
    volumes:
      - /media/pool/data:/mount
    depends_on:
      - "pihole"
    restart: always
```
The `SHARE` [configuration](https://github.com/dperson/samba/wiki/Using-Docker-Compose) is `<name>;<path>;[browsable];[read_only];[guest];<users>;<admins>;<writelist>;<comment>`.
Note that `1000` is the user ID of the normal user on my server.
So, this creates a writable share for user `ryan`.
The share should be visible in the Windows network screen, or accessible by typing `\\ryanserver\data` in the file explorer address bar.

### Reverse proxy
I used [Nginx](https://en.wikipedia.org/wiki/Nginx) to run a [reverse proxy server](https://en.wikipedia.org/wiki/Reverse_proxy) (see [this](https://docs.nginx.com/nginx/admin-guide/web-server/reverse-proxy/)).
I opted for the [official Nginx image](https://hub.docker.com/_/nginx), using the version based on [Alpine Linux](https://en.wikipedia.org/wiki/Alpine_Linux).
```vim
  nginx-proxy:
    image: nginx:alpine
    container_name: nginx-proxy
    environment:
      TZ: 'Europe/London'
    ports:
      - "80:80"
      - "443:443"
    volumes:
      - ./nginx-proxy/conf.d:/etc/nginx/conf.d
      - ./nginx-proxy/network_internal.conf:/etc/nginx/network_internal.conf:ro
    depends_on:
      - "pihole"
    restart: always
```
I used the [docker-gen](https://hub.docker.com/r/jwilder/docker-gen/) image {{< ghub "jwilder" "docker-gen" >}} to automate configuration of the reverse proxy.
```vim
  docker-gen:
    image: jwilder/docker-gen
    container_name: docker-gen
    command: -notify-sighup nginx-proxy -watch /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf
    environment:
      TZ: 'Europe/London'
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./docker-gen:/etc/docker-gen/templates:ro
      - ./nginx-proxy/conf.d:/etc/nginx/conf.d
    depends_on:
      - "pihole"
      - "nginx-proxy"
    restart: always
```
`docker-gen` requires a template; I used the one provided for [reverse proxy servers](https://raw.githubusercontent.com/jwilder/docker-gen/master/templates/nginx.tmpl).
```shell
mkdir -p ~/dkr/docker-gen
cd ~/dkr/docker-gen
curl -o nginx.tmpl https://raw.githubusercontent.com/jwilder/docker-gen/master/templates/nginx.tmpl
```
Now `docker-gen` will scrape the metadata in `docker-compose.yml` to generate the configuration: services will have `environment` variables `VIRTUAL_HOST` and `VIRTUAL_PORT` for this.
[`pihole`]({{< ref "#pi-hole" >}})'s DNS will be configured to serve these addresses using the `PROXY_LOCATION` metadata field.

I also [restricted access to the LAN by IP address](https://github.com/nginx-proxy/nginx-proxy#internet-vs-local-network-access)
```shell
mkdir -p ~/dkr/nginx-proxy
vi ~/dkr/nginx-proxy/network_internal.conf
```
```vim
# These networks are considered "internal"
allow 127.0.0.0/8;
allow 10.0.0.0/8;
allow 192.168.0.0/16;
allow 172.16.0.0/12;

# Traffic from all other networks will be rejected
deny all;
```
> <i class="fas fa-trash"></i> {{< cal "2021-10-10" >}}
>
> This was replaced by [a new HTTPS reverse proxy]({{< ref "caldav#reverse-proxy" >}}) to which [I moved the services on this page]({{< ref "proxy" >}}).

### Pi-hole
I used [`pihole`](https://pi-hole.net/) {{< ghub "pi-hole" "pi-hole" >}} {{< wiki "Pi-hole" >}} [previously]({{< ref "home-network-ad-blocking-with-pi-hole" >}}).
To containerise, I used the official `pihole` container, [pihole/pihole](https://hub.docker.com/r/pihole/pihole) {{< ghub "pi-hole" "docker-pi-hole" >}}.

I prepared persistent directories and files
```shell
mkdir -p ~/dkr/pihole
touch ~/dkr/pihole/pihole.log
```
I had to disable the DNS stub resolver that `systemd-resolved` uses by default to [free up port 53 for `pihole`](https://github.com/pi-hole/docker-pi-hole#installing-on-ubuntu)
```shell
sudo sed -r -i.backup 's/#?DNSStubListener=yes/DNSStubListener=no/g' /etc/systemd/resolved.conf
sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
sudo systemctl restart systemd-resolved.service
```
I used [this example](https://github.com/pi-hole/docker-pi-hole/blob/master/docker-compose-jwilder-proxy.yml) to write this `docker-compose` snippet such that `pihole`'s DNS would work with the reverse proxy server
```vim
  pihole:
    container_name: pihole
    image: pihole/pihole
    ports:
      - "53:53/tcp"
      - "53:53/udp"
      - "67:67/udp"
      - "8053:80/tcp"
      - "8052:443/tcp"
    environment:
      TZ: 'Europe/London'
      WEBPASSWORD: '<password>'
      PROXY_LOCATION: pihole
      VIRTUAL_HOST: pihole.ryanserver.lan
      VIRTUAL_PORT: 80
      NETWORK_ACCESS: internal
    volumes:
      - './pihole/pihole:/etc/pihole'
      - './pihole/dnsmasq.d:/etc/dnsmasq.d'
      - './pihole/pihole.log:/var/log/pihole.log'
    dns:
      - 127.0.0.1
      - 1.1.1.1
    extra_hosts:
      - 'ryanserver.lan:192.168.1.2'
      - 'pihole pihole.ryanserver.lan:192.168.1.2'
      - 'transmission transmission.ryanserver.lan:192.168.1.2'
      - 'sonarr sonarr.ryanserver.lan:192.168.1.2'
      - 'radarr radarr.ryanserver.lan:192.168.1.2'
      - 'lidarr lidarr.ryanserver.lan:192.168.1.2'
      - 'readarr readarr.ryanserver.lan:192.168.1.2'
      - 'beets beets.ryanserver.lan:192.168.1.2'
      - 'jackett jackett.ryanserver.lan:192.168.1.2'
      - 'calibre calibre.ryanserver.lan:192.168.1.2'
      - 'calibre-web-books books.calibre-web.ryanserver.lan:192.168.1.2'
      - 'calibre-web-comics comics.calibre-web.ryanserver.lan:192.168.1.2'
      - 'calibre-web-textbooks textbooks.calibre-web.ryanserver.lan:192.168.1.2'
      - 'calibre-web-magazines magazines.calibre-web.ryanserver.lan:192.168.1.2'
      - 'jellyfin jellyfin.ryanserver.lan:192.168.1.2'
      - 'h5ai h5ai.ryanserver.lan:192.168.1.2'
    restart: always
```
The custom addresses for the DNS are registered under `extra_hosts`.
When the container's running, the [local network must be configured to use the new DNS server]({{< ref "home-network-ad-blocking-with-pi-hole#configuring-the-network" >}}).

The `pihole` interface is accessible at http://pihole.ryanserver.lan.
Get the password with
```shell
docker-compose logs pihole | grep "Assigning random password"
```
> <i class="fas fa-trash"></i> {{< cal "2021-10-10" >}}
>
> I replaced the Docker image on the server with a [dedicated RPi]({{< ref "rpi-dns" >}}), then replaced that Pi-hole with [a custom setup using NSD and Unbound on the RPi]({{< ref "new-dns" >}}).

### Transmission
I use [Transmission](https://transmissionbt.com/) {{< ghub "transmission" "transmission" >}} {{< wiki "Transmission_(BitTorrent_client" >}} as my BitTorrent client.

I used the [linuxserver/transmission](https://hub.docker.com/r/linuxserver/transmission) image {{< ghub "linuxserver" "docker-transmission" >}} {{< docs "https://docs.linuxserver.io/images/docker-transmission" >}}.

I added it to `docker-compose`
```vim
  transmission:
  image: linuxserver/transmission
    container_name: transmission
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - PROXY_LOCATION=transmission
      - VIRTUAL_HOST=transmission.ryanserver.lan
      - VIRTUAL_PORT=9091
      - NETWORK_ACCESS=internal
    volumes:
      - ./transmission:/config
      - /media/disk-3a/data/torrents/downloads:/downloads
      - /media/pool/data/torrents/watch:/watch
      - /media/pool/data/games-dl:/games
      - /media/disk-3a/data/music:/music
    ports:
      - 9091:9091/tcp
      - 51413:51413/tcp
      - 51413:51413/udp
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always
```
I configured it as [before]({{< ref "transmission#transmission" >}}) and also [before]({{< ref "address-transmission-web-client-using-hostname" >}}), but now with
```shell
vi ~/dkr/transmission/settings.json
```
```vim
...
    "rpc-host-whitelist": "transmission.ryanserver.lan",
...
    "rpc-whitelist": "127.0.0.1,172.18.0.*",
...
```
for the new URL and to whitelist the proxy server's IP address, which is dynamic on the docker subnet..

### Jackett
I used the [linuxserver/jackett](https://hub.docker.com/r/linuxserver/jackett) image {{< ghub "linuxserver" "docker-jackett" >}} {{< docs "https://docs.linuxserver.io/images/docker-jackett" >}} for [Jackett](https://github.com/Jackett/Jackett).
```vim
  jackett:
    image: linuxserver/jackett
    container_name: jackett
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - AUTO_UPDATE=true
      - PROXY_LOCATION=jackett
      - VIRTUAL_HOST=jackett.ryanserver.lan
      - VIRTUAL_PORT=9117
      - NETWORK_ACCESS=internal
    volumes:
      - ./jackett/config:/config
      - ./jackett/blackhole:/downloads
    ports:
      - 9117:9117
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always
```
> {{< cal "2021-10-10" >}}
>
> I noticed there's now [Prowlarr](https://prowlarr.com/), but Jackett works fine so I won't change anything.

### Sonarr
I used the [linuxserver/sonarr](https://hub.docker.com/r/linuxserver/sonarr) image {{< ghub "linuxserver" "docker-sonarr" >}} {{< docs "https://docs.linuxserver.io/images/docker-sonarr" >}} for [Sonarr](https://sonarr.tv/) {{< ghub "Sonarr" "Sonarr" >}}.
```vim
  sonarr:
    image: linuxserver/sonarr
    container_name: sonarr
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=022
      - PROXY_LOCATION=sonarr
      - VIRTUAL_HOST=sonarr.ryanserver.lan
      - VIRTUAL_PORT=8989
      - NETWORK_ACCESS=internal
    volumes:
      - ./sonarr:/config
      - /media/pool/data/series:/tv
      - /media/disk-3a/data/torrents/downloads:/downloads
    ports:
      - 8989:8989
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
      - "jackett"
      - "transmission"
    restart: always
```

### Radarr
I used the [linuxserver/radarr](https://hub.docker.com/r/linuxserver/radarr) image {{< ghub "linuxserver" "docker-radarr" >}} {{< docs "https://docs.linuxserver.io/images/docker-radarr" >}} for [Radarr](https://radarr.video/) {{< ghub "Radarr" "Radarr" >}}.
```vim
  radarr:
    image: linuxserver/radarr
    container_name: radarr
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=022
      - PROXY_LOCATION=radarr
      - VIRTUAL_HOST=radarr.ryanserver.lan
      - VIRTUAL_PORT=7878
      - NETWORK_ACCESS=internal
    volumes:
      - ./radarr:/config
      - /media/pool/data/film:/movies
      - /media/disk-3a/data/torrents/downloads:/downloads
    ports:
      - 7878:7878
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
      - "jackett"
      - "transmission"
    restart: always
```

### Lidarr
I used the [linuxserver/lidarr](https://hub.docker.com/r/linuxserver/lidarr) image {{< ghub "linuxserver" "docker-lidarr" >}} {{< docs "https://docs.linuxserver.io/images/docker-lidarr" >}} for [Lidarr](https://lidarr.audio/) {{< ghub "lidarr" "Lidarr" >}}.
```vim
  lidarr:
    image: linuxserver/lidarr
    container_name: lidarr
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=022
      - PROXY_LOCATION=lidarr
      - VIRTUAL_HOST=lidarr.ryanserver.lan
      - VIRTUAL_PORT=8686
      - NETWORK_ACCESS=internal
    volumes:
      - /media/disk-3a/data/meta/lidarr:/config
      - /media/disk-3a/data/music:/music
      - /media/disk-3a/data/torrents/downloads:/downloads
    ports:
      - 8686:8686
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
      - "jackett"
      - "transmission"
    restart: unless-stopped
```
The cover art folder in `/config` can get very large, so I put it on a data disk rather than the SSD.

I don't keep this container running all the time as, with a large library, the scheduled maintenance operations are pretty hard on the disk.

### Readarr
For ebooks, there's [Readarr](https://readarr.com/) {{< ghub Readarr Readarr >}}.
I used [hotio/readarr](https://hub.docker.com/r/hotio/readarr) {{< ghub hotio readarr >}} {{< docs "https://hotio.dev/containers/readarr/" >}}.
```vim
  readarr:
    container_name: readarr
    image: hotio/readarr:nightly
    ports:
      - "8787:8787"
    environment:
      - PUID=1000
      - PGID=1000
      - UMASK=002
      - TZ=Europe/London
      - PROXY_LOCATION=readarr
      - VIRTUAL_HOST=readarr.ryanserver.lan
      - VIRTUAL_PORT=8787
      - NETWORK_ACCESS=internal
    volumes:
      - ./readarr:/config
      - /media/disk-3a/data/cloud/gdrive-sync/library:/data/library
      - /media/disk-3a/data/torrents/downloads:/downloads
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
      - "jackett"
      - "transmission"
    restart: unless-stopped
```

I had to `docker exec -it readarr bash` and `chown hotio data`.

### Headphones
I also tried the [Lidarr]({{< ref "#lidarr" >}}) alternative [Headphones](https://github.com/rembo10/headphones) with the [linuxserver/headphones](https://hub.docker.com/r/linuxserver/headphones/) image {{< ghub "linuxserver" "docker-headphones" >}} {{< docs "https://docs.linuxserver.io/images/docker-headphones" >}}.
```vim
  headphones:
    image: linuxserver/headphones
    container_name: headphones
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=022
      - PROXY_LOCATION=headphones
      - VIRTUAL_HOST=headphones.ryanserver.lan
      - VIRTUAL_PORT=8181
      - NETWORK_ACCESS=internal
    volumes:
      - ./headphones:/config
      - /media/disk-3a/data/music:/music
      - /media/disk-3a/data/torrents/downloads:/downloads
    ports:
      - 8181:8181
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
      - "jackett"
      - "transmission"
    restart: unless-stopped
```

> <i class="fas fa-trash"></i>
>
> After trialing, I retired Headphones in favour of [Lidarr]({{< ref "#lidarr" >}}).

### Beets
For managing music libraries from the command line, [`beets`](http://beets.io/) {{< ghub "beetbox" "beets" >}} is king.
I [previously used it natively]({{< ref "nerdifying-music-library-management-with-beets" >}}).
For `docker`, I used the [linuxserver/beets](https://hub.docker.com/r/linuxserver/beets) image {{< ghub "linuxserver" "docker-beets" >}} {{< ext "https://blog.linuxserver.io/2016/10/08/managing-your-music-collection-with-beets/" >}} {{< docs "https://docs.linuxserver.io/images/docker-beets" >}}.
```vim
  beets:
    image: linuxserver/beets
    container_name: beets
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - PROXY_LOCATION=beets
      - VIRTUAL_HOST=beets.ryanserver.lan
      - VIRTUAL_PORT=8337
      - NETWORK_ACCESS=internal
    volumes:
      - ./beets:/config
      - /media/disk-3a/data/music:/music
      - /media/disk-3a/data/torrents/downloads:/downloads
    ports:
      - 8337:8337
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: unless-stopped
```
Use `beets` with
```shell
docker-compose -f ~/dkr/docker-compose.yml exec beets beet <command>
```
or the [alias]({{< ref "#aliases" >}})
```shell
beets <command>
```

> <i class="fas fa-trash"></i>
>
> I still maintain `beets` is the best command line tool for managing the metadata of music library files.
> However, I canned it and just use the web GUI provided by [Lidarr]({{< ref "#lidarr" >}}).

### Calibre
I have a few ebook libraries managed by [`calibre`](https://calibre-ebook.com/) {{< ghub "kovidgoyal" "calibre" >}} {{< wiki "Calibre_%28software%29" >}}.
There's a separate library for novels and nonfiction books, textbooks, comics, and magazines.
I use [Calibre Web]({{< ref "#calibre-web" >}}) to provide a nice web interface; while I use it for most things, it doesn't do everything.
Sometimes, vanilla `calibre` is required, either via the CLI (when `ssh`'d into the server), or the desktop GUI.
For example,
* adding ebooks on the server disks
* adding a folder containing multiple formats of the same ebook
* [emailing ebooks to ebook readers](https://manual.calibre-ebook.com/faq.html#i-cannot-send-emails-using-calibre) (although I failed to get that to work anyway)

I installed the [linuxserver/calibre](https://hub.docker.com/r/linuxserver/calibre) image {{< ghub "linuxserver" "docker-calibre" >}} {{< docs "https://docs.linuxserver.io/images/docker-calibre" >}}.
It provides remote access to the desktop application in the browser through a [Guacamole server](https://guacamole.apache.org/), which is pretty neat.
```vim
  calibre:
    image: linuxserver/calibre
    container_name: calibre
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=002
      - PROXY_LOCATION=calibre
      - VIRTUAL_HOST=calibre.ryanserver.lan
      - VIRTUAL_PORT=8080
      - NETWORK_ACCESS=internal
    volumes:
      - /media/disk-3a/data/cloud/gdrive-sync:/data
      - ./calibre:/config
      - /media/disk-3a/data/torrents/downloads/complete:/new
    ports:
      - 8080:8080
      - 8081:8081
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: unless-stopped
```

> <i class="fas fa-bug"></i> Access to Guacamole does not seem to work using the reverse proxied URL, although it does work with the IP address at port 8080.
> Perhaps requires [configuration](https://guacamole.apache.org/doc/gug/proxying-guacamole.html#proxying-with-nginx).

The [Calibre content server](https://manual.calibre-ebook.com/server.html) can be started (and set to automatically start) from Guacamole at `Preferences > Sharing > Sharing over the net > Main` and is on port 8081.

[Manage](https://manual.calibre-ebook.com/generated/en/calibredb.html) `calibre` in the terminal with
```shell
docker-compose -f ~/dkr/docker-compose.yml exec calibre calibredb <options>
```
or [alias]({{< ref "#aliases" >}})
```shell
calibredb <options>
```
and similarly for [other commands](https://manual.calibre-ebook.com/generated/en/cli-index.html) via alias
```shell
calibre <command> <options>
```
For example, to [add](https://manual.calibre-ebook.com/generated/en/calibredb.html#add) novels to `calibre`,
```shell
calibredb --library-path=/data/library add new/<file1> new/<file2>
```

Changes from `calibre` CLI and GUI show up immediately in [Calibre Web]({{< ref "#calibre-web" >}}).
However, changes made via the Calibre Web interface seem to take some time to propagate to the vanilla `calibre` GUI.
Editing the same `calibre` database at similar times with the two interfaces is not safe!

### Calibre-Web
I installed [Calibre-Web](https://github.com/janeczku/calibre-web) to access and manage my [`calibre`]({{< ref "#calibre" >}}) ebook libraries, using the image [linuxserver/calibre-web](https://hub.docker.com/r/linuxserver/calibre-web) {{< ghub "linuxserver" "docker-calibre-web" >}} {{< docs "https://docs.linuxserver.io/images/docker-calibre-web" >}}.
I spun up a container for each library.
```vim
  calibre-web-books:
    image: linuxserver/calibre-web
    container_name: calibre-web-books
    environment:
      - PUID=1000
      - PGID=1000
      - UMASK=002
      - TZ=Europe/London
      - DOCKER_MODS=linuxserver/calibre-web:calibre
      - PROXY_LOCATION=calibre-web-books
      - VIRTUAL_HOST=books.calibre-web.ryanserver.lan
      - VIRTUAL_PORT=8083
      - NETWORK_ACCESS=internal
    volumes:
      - ./calibre-web:/config
      - /media/disk-3a/data/cloud/gdrive-sync/library:/books
    ports:
      - 8083:8083
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always

  calibre-web-comics:
    image: linuxserver/calibre-web
    container_name: calibre-web-comics
    environment:
      - PUID=1000
      - PGID=1000
      - UMASK=002
      - TZ=Europe/London
      - DOCKER_MODS=linuxserver/calibre-web:calibre
      - PROXY_LOCATION=calibre-web-comics
      - VIRTUAL_HOST=comics.calibre-web.ryanserver.lan
      - VIRTUAL_PORT=8083
      - NETWORK_ACCESS=internal
    volumes:
      - ./calibre-web-comics:/config
      - /media/disk-3a/data/cloud/gdrive-sync/comics:/books
    ports:
      - 8084:8083
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always

  calibre-web-textbooks:
    image: linuxserver/calibre-web
    container_name: calibre-web-textbooks
    environment:
      - PUID=1000
      - PGID=1000
      - UMASK=002
      - TZ=Europe/London
      - DOCKER_MODS=linuxserver/calibre-web:calibre
      - PROXY_LOCATION=calibre-web-textbooks
      - VIRTUAL_HOST=textbooks.calibre-web.ryanserver.lan
      - VIRTUAL_PORT=8083
      - NETWORK_ACCESS=internal
    volumes:
      - ./calibre-web-textbooks:/config
      - /media/disk-3a/data/cloud/gdrive-sync/textbooks:/books
    ports:
      - 8085:8083
    depends_on:
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always

  calibre-web-magazines:
    image: linuxserver/calibre-web
    container_name: calibre-web-magazines
    environment:
      - PUID=1000
      - PGID=1000
      - UMASK=002
      - TZ=Europe/London
      - DOCKER_MODS=linuxserver/calibre-web:calibre
      - PROXY_LOCATION=calibre-web-magazines
      - VIRTUAL_HOST=magazines.calibre-web.ryanserver.lan
      - VIRTUAL_PORT=8083
      - NETWORK_ACCESS=internal
    volumes:
      - ./calibre-web-magazines:/config
      - /media/disk-3a/data/cloud/gdrive-sync/magazines:/books
    ports:
      - 8086:8083
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always
```
#### Initialisation
For the initial [set up](https://github.com/linuxserver/docker-calibre-web#application-setup) of each instance, navigate to `http://*.calibre-web.ryanserver.lan`, enter `/books` in the `Library Configuration` field, and use `admin` and `admin123` as the username and password to login.

#### Usage
Calibre Web provides access to the libraries so that:
* ebooks of supported format (including `epub`, `cbr`, and `pdf`, but not `djvu` or `mobi`) can be read in the browser ([enable](https://github.com/janeczku/calibre-web/issues/1231#issuecomment-592141528) for `<user>` in `Admin > User > <user> > Allow eBook Viewer`)
* ebooks can be downloaded (enable for `<user>` in `Admin > User > <user> > Allow Downloads`)
* metadata of ebooks can be edited (enable for `<user>` in `Admin > User > <user> > Allow Edit`; access under `Edit Metadata` in the `Book Details` screen for an ebook)
* new books can be uploaded (enable in `Admin > Configuration > Edit Basic Configuration > Feature Configuration > Enable Uploads`, then for a `<user>` with `Admin > User > <user> > Allow Uploads`)
* ebooks can be converted to a different format (enable by writing `/usr/bin/ebook-convert` in `Admin > Configuration > Edit Basic Configuration > External Binaries > Path to Calibre E-Book Converter`; access under `Edit Metadata` in the `Book Details` screen for an ebook)

#### Proxy configuration
To allow uploads of larger files, [the reverse proxy must be edited](https://github.com/janeczku/calibre-web/issues/452#issuecomment-373672757).
Add these lines near the bottom of `~/dkr/docker-gen/nginx.tmpl` under `server`
```vim
client_body_in_file_only clean;
client_body_buffer_size 32K;

client_max_body_size 500M;

sendfile on;
send_timeout 300s;
```

#### Tranferring ebooks to ereaders
To manage the tranfer of ebooks to ebook readers via USB, the normal Calibre desktop application must be used.
On Linux clients, I mount the files using [NFS]({{< ref "#nfs" >}}), then add the libraries in the desktop application.
It's the same on Windows, except you have to use [Samba]({{< ref "#samba" >}}) and the share must be mounted using the ["Map network drive" option](https://en.wikipedia.org/wiki/Drive_mapping).

#### Ebook format conversion woes
If format conversion [fails without an error message](https://github.com/janeczku/calibre-web/issues/952), it's probably to do with file permissions.
Try
```shell
sudo chown <user> <directory of the ebook>
```

### Plex
I prefer the libre [Jellyfin]({{< ref "#jellyfin" >}}) over [Plex](https://en.wikipedia.org/wiki/Plex_(software)), but I still need [Plex to provide local music for Alexa]({{< ref "alexa-local-music" >}}).

I used the [official image](https://hub.docker.com/r/plexinc/pms-docker).
```vim
  plex:
    image: plexinc/pms-docker
    container_name: plex
    environment:
      - PLEX_UID=1000
      - PLEX_GID=1000
      - TZ=Europe/London
      - ADVERTISE_IP=http://192.168.1.2:32400
      - HOSTNAME=ryanserver
      - PROXY_LOCATION=plex
      - VIRTUAL_HOST=plex.ryanserver.lan
      - VIRTUAL_PORT=32400
      - NETWORK_ACCESS=internal
    volumes:
      - ./plex:/config
      - /media/disk-3a/data/music:/music
      - /media/pool/data/series:/series
      - /media/pool/data/film:/film
    devices:
      - /dev/dri:/dev/dri
    ports:
      - 32400:32400/tcp
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: unless-stopped
```
Plex is in the default `docker` network mode, [`bridge`](https://docs.docker.com/network/bridge/), so it can't automatically enable internet access.
This is required to use the [Plex skill for Alexa]({{< ref "alexa-local-music" >}}).
This can be done manually by [setting the public port](https://support.plex.tv/articles/200931138-troubleshooting-remote-access/?utm_campaign=Plex%20Apps&utm_medium=Plex%20Web&utm_source=Plex%20Apps) for Plex at `Settings > Remote Access > Manually specify public port`, then port forwarding on the router.
For my [Sagemcom router from TalkTalk](https://community.talktalk.co.uk/t5/Articles/Set-up-port-forwarding/ta-p/2205382), the menu is at `Advanced Settings > Access Control > Port Forwarding`.
I made a new rule and set:
* Protocol: `TCP`
* External host: `*`
* Internal host: `192.168.1.2`
* External Port: `<same as set in the Plex interface>`
* Internal Port: `32400`

> <i class="fas fa-trash"></i>
>
> I deprecated Plex and use [Jellyfin]({{< ref "#jellyfin" >}}) exclusively now.
> It means I can't use [Alexa for VUI music]({{< ref "alexa-local-music" >}}), but I'm OK with that.

### Jellyfin
I used the [linuxserver/jellyfin](https://hub.docker.com/r/linuxserver/jellyfin) image {{< ghub "linuxserver" "docker-jellyfin" >}} {{< docs "https://docs.linuxserver.io/images/docker-jellyfin" >}} for [Jellyfin](https://jellyfin.org/) {{< ghub "jellyfin" "jellyfin" >}}.
```vim
  jellyfin:
    image: linuxserver/jellyfin
    container_name: jellyfin
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - UMASK_SET=022
      - PROXY_LOCATION=jellyfin
      - VIRTUAL_HOST=jellyfin.ryanserver.lan
      - VIRTUAL_PORT=8096
      - NETWORK_ACCESS=internal
    volumes:
      - ./jellyfin:/config
      - /media/pool/data/series:/data/tvshows
      - /media/pool/data/film:/data/movies
      - /media/disk-3a/data/music:/data/music
    ports:
      - 8096:8096
    devices:
      - /dev/dri:/dev/dri
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always
```
As well as in the browser at <http://jellyfin.ryanserver.lan/>, Jellyfin can be accessed on mobile with the free app from [F-Droid](https://f-droid.org/en/packages/org.jellyfin.mobile/) or the [Play Store](https://play.google.com/store/apps/details?id=org.jellyfin.mobile&hl=en_US).

> After updating Jellyfin today, I found myself unable to access the web interface.
> The address showed a page that said only `Forbidden`.
> The [solution](https://github.com/jellyfin/jellyfin/issues/3397#issuecomment-650271397) was to open up `~/dkr/jellyfin/system.xml` and ensure remote access was enabled with the line `<EnableRemoteAccess>true</EnableRemoteAccess>`.

### h5ai

I added [h5ai](https://larsjung.de/h5ai/) {{< ghub "lrsjng" "h5ai" >}} for browsing files in a web browser.
I used the [awesometic/h5ai](https://hub.docker.com/r/awesometic/h5ai/dockerfile) image {{< ghub "awesometic" "docker-h5ai" >}}.
```vim
  h5ai:
    image: awesometic/h5ai
    container_name: h5ai
    environment:
      - TZ=Europe/London
      - PROXY_LOCATION=h5ai
      - VIRTUAL_HOST=h5ai.ryanserver.lan
      - VIRTUAL_PORT=80
      - NETWORK_ACCESS=internal
    volumes:
      - /media/pool/data:/h5ai
      - ./h5ai:/config
    ports:
      - 8054:80
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: always
```
Loading directories at <http://h5ai.ryanserver.lan/> was [torturously slow](https://github.com/lrsjng/h5ai/issues/572), so I disabled showing directory sizes with
```shell
sudo vi ~/dkr/h5ai/h5ai/_h5ai/private/conf/options.json
```
```vim
...
    "foldersize": {
        "enabled": false,
...
```
> <i class="fas fa-trash"></i> {{< cal "2021-10-10" >}}
>
> I really like the aesthetic of h5ai, but I deprecated it in favour of [simply using the `autoindex` Nginx module]({{< ref "proxy#file-browser" >}}).

### Heimdall
I tried [Heimdall](https://heimdall.site/) {{< ghub "linuxserver" "Heimdall" >}} as a dashboard, using the official image [linuxserver/heimdall](https://hub.docker.com/r/linuxserver/heimdall/) {{< ghub "linuxserver" "docker-heimdall" >}}, {{< docs "https://docs.linuxserver.io/images/docker-heimdall)" >}}.
```vim
  heimdall:
    image: linuxserver/heimdall
    container_name: heimdall
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - PROXY_LOCATION=heimdall
      - VIRTUAL_HOST=heimdall.ryanserver.lan
      - VIRTUAL_PORT=80
      - NETWORK_ACCESS=internal
    volumes:
      - ./heimdall:/config
    ports:
      - 8099:80
    depends_on:
      - "pihole"
      - "nginx-proxy"
      - "docker-gen"
    restart: unless-stopped
```

> <i class="fas fa-trash"></i> I have removed this since I never use it.
> A dashboard is not really necessary when all services can be accessed with a human-readable URL thanks to the [reverse proxy server]({{< ref "#reverse-proxy" >}}).

### Ubooquity
I tried [Ubooquity](https://vaemendis.net/ubooquity/) as a web-based ebook reader for the ebook libraries on my server, using the [linuxserver/ubooquity](https://hub.docker.com/r/linuxserver/ubooquity) image {{< ghub "linuxserver" "docker-ubooquity" >}} {{< docs "https://docs.linuxserver.io/images/docker-ubooquity" >}}.
```vim
  ubooquity:
    image: linuxserver/ubooquity
    container_name: ubooquity
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - MAXMEM=1024
      - PROXY_LOCATION=ubooquity
      - VIRTUAL_HOST=ubooquity.ryanserver.lan
      - VIRTUAL_PORT=2202
      - NETWORK_ACCESS=internal
    volumes:
      - ./ubooquity:/config
      - /media/pool/data/cloud/gdrive-sync/library:/books/books
      - /media/pool/data/cloud/gdrive-sync/textbooks:/books/textbooks
      - /media/pool/data/cloud/gdrive-sync/magazines:/books/magazines
      - /media/pool/data/cloud/gdrive-sync/comics:/books/comics
      - /media/pool/data/cloud/gdrive-sync/library:/comics/books
      - /media/pool/data/cloud/gdrive-sync/textbooks:/comics/textbooks
      - /media/pool/data/cloud/gdrive-sync/magazines:/comics/magazines
      - /media/pool/data/cloud/gdrive-sync/comics:/comics/comics
    ports:
      - 2202:2202
      - 2203:2203
    restart: always
```
The admin portal is accessible at <http://ryanserver.lan:2203/ubooquity/admin> and the libraries at <http://ubooquity.ryanserver.lan/ubooquity>.

I'm not sure if I enjoy the experience of using this software, so I may not keep it.

> <i class="fas fa-trash"></i> I didn't like it, so it's out.
> [calibre-web]({{< ref "#calibre-web" >}}) provides a superior interface for reading ebooks in the browser.

### Rclone
I installed [`rclone`](https://rclone.org/) {{< ghub "rclone" "rclone" >}} using the [official image](https://hub.docker.com/r/rclone/rclone).
I use this program to synchronise local directories with their cloud backups.

This replaces [`drive`]({{< ref "gdrive" >}}) as my CLI client for Google Drive.
Looking for a more performant solution, I considered some [alternative](https://github.com/gdrive-org/gdrive) [projects](https://github.com/nurdtechie98/drive-cli), but settled on `rclone`.
Compared to `drive`, it is a larger, more mature and more actively developed project, and I find it synchronises faster.
It also provides a unified interface to many different cloud storage providers, which could come in useful in the future.
```vim
  rclone:
    image: rclone/rclone
    container_name: rclone
    stdin_open: true
    tty: true
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
    volumes:
      - ./rclone:/config/rclone 
      - /media/disk-3a/data/cloud:/data
```
`rclone` does not run as a continuous service.
Instead, a container is spun up on usage
```shell
docker-compose -f ~/dkr/docker-compose.yml run --rm rclone <command>
```
where the `<command>` is an [`rclone` command](https://rclone.org/commands/), like `listremotes`.
That's a rather tedious command, so I set up some [aliases]({{< ref "#aliases" >}}).
The local directories for cloud synchronisation are visible to the container as `/data/*`.

Using the aliases, I can
* Set up a new remote interactively
    ```shell
    rclone config
    ```
    It was straightforward for [Drive](https://rclone.org/drive/).
    I set up a [personal `client_id`](https://rclone.org/drive/#making-your-own-client-id) for this.
* Update remote `gdrive-sync` (with progress)
    ```shell
    rclone sync -P /data/gdrive-sync gdrive-sync:/
    ```
    or, using the custom command,
    ```shell
    rcsr gdrive-sync
    ```
* Update local `gdrive-sync` (with progress)
    ```shell
    rclone sync -P gdrive-sync:/ /data/gdrive-sync
    ```
    or, using the custom command,
    ```shell
    rcsl gdrive-sync
    ```
* The `sync` commands can also be made for specific folders or files using a second argument (recommended as large syncs are slow and increase change of accidental file changes)
    ```shell
    rcsl gdrive-sync path/inside/gdrive-sync
    ```
* Use `--dry-run` or `-i` (interactive) to avoid accidentally deleting files! Aliases are interactive (and do not accept flags). Use `rcslf` and `rcsrf` for non-interactive (unrecommended).

## Management
### Commands
Commands which I've found useful so far include:
* All the containers specified in the yaml are started as daemons with
    ```shell
    docker-compose up -d
    ```
    or in the foreground for quick debugging by omitting the `-d`.
    Note that this command must be executed in the same directory as the `docker-compose.yml` file, or with `-f "<path/to/docker-compose.yml>"`.
    This command should also be used to apply edits to the yaml.
* I can see which are running with
    ```shell
    docker-compose ps
    ```
* I can stop a container
    ```shell
    docker-compose stop <container name>
    ```
* Or I can stop all `docker-compose` containers with
    ```shell
    docker-compose stop
    ```
* All stopped containers are removed (their files are deleted) with
    ```shell
    docker container prune
    ```
* Similarly for old versions of images, which are not automatically removed on update,
    ```shell
    docker image prune
    ```
* Update all `docker-compose` container images with
    ```shell
    docker-compose pull
    docker-compose up -d
    ```
* View all the recent logs with
    ```shell
    docker-compose logs -t --tail=50
    ```
    and optionally name a container at the end of that command to see only its logs.
* Access the shell of `<container>`
    ```shell
    docker exec -it <container> bash
    ```

### Aliases
I put together some aliases for using `docker-compose`
The Debian default `~/.bashrc` sources `~/.bash_aliases`, so I put them there.
```vim
alias dcp='docker-compose -f $HOME/dkr/docker-compose.yml'
alias dcpe='vi $HOME/dkr/docker-compose.yml'
alias dcpp='dcp pull'
alias dcpu='dcp up -d --remove-orphans'
alias dimp='docker image prune'
alias dcu='dcpp && dcpu && dimp -f'
alias dcpl='dcp logs -tf --tail="50"'
alias dcps='dcp stop'
alias dcpr='dcp restart'
```
I also made some aliases for [`rclone`]({{< ref "#rclone" >}})
```vim
alias rclone='dcp run --rm rclone'
# rclone sync local
function rcsl(){
    rclone sync -P $1:/$2 /data/$1/$2
}
# rclone sync remote
function rcsr(){
    rclone sync -P /data/$1/$2 $1:/$2
}
```
Note that I had to [use `bash` functions to make "aliases" that take arguments](https://stackoverflow.com/a/7131683/5922871).

[`beets`]({{< ref "#beets" >}}) also got an alias
```vim
alias beets='dcp exec beets beet'
```

And [`calibre`]({{< ref "#calibre" >}})
```vim
alias calibre='dcp exec calibre'
alias calibredb='calibre calibredb'
```

I put these aliases in a remote repo at {{< gl home-server-aliases >}}.
The up-to-date container aliases are [here](https://gitlab.com/eidoom/home-server-aliases/-/blob/master/.bash_aliases) along with some other useful aliases.

### Automation
Most containers will automatically start on boot after the initial `docker-compose up -d` since they have a [restart policy](https://docs.docker.com/config/containers/start-containers-automatically/#use-a-restart-policy) defined in the `docker-compose.yml`
```vim
    restart: always
```
The others, which I use infrequently, have
```vim
    restart: unless-stopped
```
which means I will manaully stop (`dcps <name>`) and start them (`dcpu <name>`), and they won't automatically start with `docker` if I've stopped them.
This is to lessen system usage.

<!-- I [automated startup]({{< ref "daemon" >}}) of `docker-compose` on boot with `systemd`. -->
<!-- I based my service file on [this example](https://stackoverflow.com/a/48066454/5922871). -->
<!-- It uses a [`oneshot`](https://stackoverflow.com/a/39050387/5922871) type with [timeout](https://www.freedesktop.org/software/systemd/man/systemd.service.html#TimeoutStartSec=): -->
<!-- ```shell -->
<!-- sudo vi /etc/systemd/system/docker-compose.service -->
<!-- ``` -->
<!-- ```vim -->
<!-- [Unit] -->
<!-- Description=Docker Compose --> 
<!-- Requires=docker.service -->
<!-- After=docker.service -->

<!-- [Service] -->
<!-- User=ryan -->
<!-- Group=ryan -->

<!-- ExecStart=docker-compose -f "home/ryan/dkr/docker-compose.yml" up -d -->
<!-- ExecStop=docker-compose down -->

<!-- Type=oneshot -->
<!-- RemainAfterExit=yes -->
<!-- TimeoutStartSec=0 -->

<!-- [Install] -->
<!-- WantedBy=multi-user.target -->
<!-- ``` -->
<!-- The service runs on boot when enabled -->
<!-- ```shell -->
<!-- sudo systemctl enable docker-compose.service -->
<!-- ``` -->

+++
title = "OCR with Tesseract"
date = 2021-06-13T17:31:03+01:00
categories = ["software"]
tags = ["ocr","documents","linux","fedora","fedora-34","laptop","desktop"]
description = "Optical character recognition from the Linux command line"
toc = true
draft = false
cover = "img/covers/49.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Using the Tesseract OCR engine {{< wiki "Tesseract (software)" >}} {{< ghub tesseract-ocr tesseract >}} {{< docs "https://tesseract-ocr.github.io/" >}} from the command line on Fedora 34 couldn't be easier.
Simply install
```shell
sudo dnf install tesseract
```
then run
```shell
tesseract <input document or image> <output filename>
```
to produce `<output filename>.txt` with the text.

+++
title = "Boids"
date = 2020-12-05T09:31:26Z
edit = 2021-04-04
categories = ["simulation"]
tags = ["cpp","meson","graphics","sfml","2d","simulation","emergence","gcc","linux","game","c++","project","dynamics"]
description = "Bird-oid objects, like fish, you know"
toc = true
draft = false
cover = "img/covers/66.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I love the bulk motion of flocks of birds in the sky, or shoals of fish in the sea.
Members of these groups are referred to as [boids](https://en.wikipedia.org/wiki/Boids).
There have been attempts to describe and simulate this natural phenomenon as [emergent](https://en.wikipedia.org/wiki/Emergence) behaviour: apparent complexity deriving from a small set of simple rules.
Emergence is a fascinating and satisfying aspect of my field, physics, and it's just as compelling to see it in biology.
Well, I guess [we already know]({{< ref "cellular-automata" >}}) I like this stuff.
The [seminal paper](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.103.7187&rep=rep1&type=pdf) is quite lovely.

These simulations have aesthetically pleasing results, so I decided to have a go myself.
I programmed in [C++](https://en.wikipedia.org/wiki/C%2B%2B) and tried out the [Meson build system](https://en.wikipedia.org/wiki/Meson_(software)) since I fancied using [Ninja](https://en.wikipedia.org/wiki/Ninja_(build_system)) rather than [Make](https://en.wikipedia.org/wiki/Make_(software)) as usual.
The repo is [here](https://gitlab.com/eidoom/boids/).

There's still work to be done!

<video width="100%" autoplay loop muted>
  <source src="../../vid/boids.mp4" type="video/mp4">
</video>

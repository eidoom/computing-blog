+++
title = "Trying Alacritty"
date = 2020-10-01T10:44:07+01:00
edit = 2021-07-12
categories = ["environment-core"]
tags = ["terminal","gpu","environment","shell","zsh","fedora","install","configure","linux","tmux","neovim","tty","alacritty","kitty"]
description = "Trying a new terminal emulator---with GPU-acceleration, really?"
toc = true
cover = "img/covers/10.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

On the basis of its ([controversial](https://news.ycombinator.com/item?id=24016977)) claimed superlative performance, I tried out [`alacritty`](https://github.com/alacritty/alacritty) {{< wiki Alacritty >}} {{< arch "Alacritty" >}} as an alternative terminal emulator to my [current]({{< ref "install-fedora#terminal" >}}) GNOME terminal.
It's GPU-accelerated, written in [Rust]({{< ref "learning-rust" >}}), and the [default](https://github.com/swaywm/sway/blob/c150177a94e1b3df3710d1b2b3db1d59fb396685/config.in#L17) terminal for [Sway](https://swaywm.org/), so I was intrigued.

## Installation

`alacritty` is available on Fedora 32's default repos.
However, to get a slightly newer version, I used [a `copr`](https://copr.fedorainfracloud.org/coprs/pschyska/alacritty/)
```shell
sudo dnf copr enable pschyska/alacritty
sudo dnf install alacritty
```

> {{< cal 2021-07-12 >}}
>
> I've just noticed on Fedora 34 that the system repositories now provide the latest Alacritty  as `alacritty` and this `copr` is deprecated.

## Configuration

I made the [configuration file](https://gitlab.com/eidoom/dotfiles-alacritty/-/raw/master/alacritty.yml) at

```shell
mkdir -p ~/.config/alacritty
vi ~/.config/alacritty/alacritty.yml
```

and pasted in the template from [releases](https://github.com/alacritty/alacritty/releases).
I [set the font](https://github.com/alacritty/alacritty/wiki/Changing-the-default-font) to [hack]({{< ref "font-code" >}}) and the colours to solarized using [alacritty's template](https://github.com/alacritty/alacritty/wiki/Color-schemes#solarized).

I also had to set `TERM="xterm-256color"` in [my `.zshrc`]({{< ref "shell" >}}) for the colours to work in [`nvim`]({{< ref "neovim#colours" >}}) inside [`tmux`]({{< ref "tmux#colours" >}}).

My `alacritty` configuration is available [here](https://gitlab.com/eidoom/dotfiles-alacritty).

## Conclusion

So far, I haven't noticed any speed differences in ordinary terminal usage (inside a `tmux` session) to GNOME terminal.

## Outlook

* Here's the [thread](https://news.ycombinator.com/item?id=24643008) that sparked off this whole post.
* I'm also interested in testing contender [`kitty`](https://sw.kovidgoyal.net/kitty/) {{< ghub "kovidgoyal" "kitty" >}}, from the author of [`calibre`]({{< ref "server-install#calibre" >}}), as a replacement for `alacritty+tmux`.
It's also GPU-accelerated, using OpenGL, and conforms to the `*tty` naming convention.
* Another GPU-accelerated terminal emulator with multiplexing built in is [wezterm](https://wezfurlong.org/wezterm/) {{< ghub "wez" "wezterm" >}}, discussion [here](https://news.ycombinator.com/item?id=26633708).

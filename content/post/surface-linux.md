+++
title = "Linux on a Surface"
date = 2020-08-26T18:09:17+01:00
edit = 2021-04-05
categories = ["environment-core"]
tags = ["surface","surface-pro","surface-pro-4","windows","linux","fedora","fedora-32","windows-10","install","manjaro","arch","dual-boot","partitioning","grub","systemd","systemd-boot","refind","bootloader","boot-manager","debian","uefi","swap","swapfile","kernel","custom-kernel","firmware","drivers","wayland","touchscreen","stylus","surface-pen","manjaro-architect","wacom","n-trig","rtl8152","firefox","lts","distro","gnome","tablet","pen-computer","xournal","xournal++","krita","eog","dd"]
description = "Installing Linux on a Surface Pro 4"
toc = true
cover = "img/covers/32.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

In this post, I recorded my attempts to put together an almost fully functional Linux installation on a Surface Pro 4, using the [`surface-linux`](https://github.com/linux-surface/linux-surface) kernel.
The almost is because there is currently no [support](https://github.com/linux-surface/linux-surface/wiki/Supported-Devices-and-Features#feature-matrix) for the camera.
The primary aim is for the touch and stylus experience to be the same as on Windows.

While I did manage to achieve this goal (for limited time periods, at least), unfortunately I did not manage to get anything as stable as I wanted.

{{< figure alt="manjaro" src="../../img/surface-linux/manjaro.jpg" caption="Manjaro running on the Surface">}}

## Introducing the Surface
I use a [Microsoft Surface Pro 4](https://en.wikipedia.org/wiki/Surface_Pro_4) as a writing device and for reading PDFs.
I bought it back in Jan 2017 as an upgrade to a [ThinkPad X220 Tablet](http://www.thinkwiki.org/wiki/Category:X220_Tablet).

I have the [i5](https://en.wikichip.org/wiki/intel/core_i5/i5-6300u) model with 4 GB of RAM.
It's a little light on the RAM, but sufficient for my current uses.
The processor is overkill, if anything, but I used to make more use of it when it was my only laptop-like device.

If I need to use a keyboard, I connect an [Obins Anne Pro](https://computing-blog.netlify.app/post/split-keyboard/#mechanical-keyboards) by bluetooth.

I use rechargeable AAAA batteries for the [Surface Pen](https://en.wikipedia.org/wiki/Surface_Pen).
It was quite difficult to source rechargeables in this form factor, or even to find information about it.
Nonetheless, there seem to be a small number of [NiCd](https://en.wikipedia.org/wiki/Nickel%E2%80%93cadmium_battery) and [NiMH](https://en.wikipedia.org/wiki/Nickel%E2%80%93metal_hydride_battery) AAAA batteries available.
NiMH is the superior technology, and also has a lower environmental impact in its disposal, so the choice was clear.
I ended up going with [these](https://smile.amazon.co.uk/dp/B071FSM8Q4/ref=pe_3187911_189395841_TE_dp_1) Odec batteries as they had the best price when I was shopping (Nov 2018).
I use [this](https://smile.amazon.co.uk/dp/B073P63NSV/ref=pe_3187911_189395841_TE_dp_3) DuraPro charger to charge them.
I have only had trouble with this set up once, when a battery warped on the charger; I'm not sure if this was due to the charger or the battery being of poor quality. In any case, I'm careful to not allow the batteries to overcharge now.

Recently, I managed to damage the screen, so had to [remove]({{< ref "surface-screen-remove" >}}) and [replace]({{< ref "surface-screen-replacement" >}}) it.

## Preparing to migrating to Linux
For a long time, I've wanted to use Linux on the Surface.
Initially, while I could install Linux, there was no driver support for the touchscreen and pen, so I wouldn't be able to use its main features.
Since then, however, the [`surface-linux`](https://github.com/linux-surface/linux-surface) project has provided this suppport.
Finding this, I immediately set about [installing](https://github.com/linux-surface/linux-surface/wiki/Installation-and-Setup#installation) Linux on the Surface.

### Preparing the Surface
In Windows, I held `Shift` and selected *Restart* in the *Start Menu* to get into the UEFI.
(Alternatively, access the UEFI by holding `Volume Up` and `Power` when the tablet is off.)
There, I disabled *Secure Boot* and put *USB Storage* at top of the *Boot Device Order* list.
I then rebooted into Windows.

### Dual boot
I'll keep the Windows partition for installing firmware updates and in case I find Linux to be unstable on the Surface.
The tools used in the following can be found by searching for them from the Windows *Start Menu*.

Since I'll be migrating my work to the Linux partition anyway, I did a factory reset of Windows using *Reset this PC*. 
This put me on [version 2004](https://docs.microsoft.com/en-us/windows/whats-new/whats-new-windows-10-version-2004).

I [disabled fast start-up and hibernate](https://wiki.archlinux.org/index.php/Dual_boot_with_Windows#Fast_Startup_and_hibernation) at `Control Panel > Hardware and Sound > Power Options > System Settings`.

### Making some room for Linux
#### Attempt 1
I tried to use *Disk Management* to shrink the Windows partition.
Apparently there were immovable files hanging around, meaning I could only shrink the parition by 231 MB.
That wasn't really going to cut it.
I needed a tool that was a bit smarter than Windows Disk Management.

I installed a tool I've used before on Windows for this very reason, [MiniTool Partition Wizard](https://www.partitionwizard.com/free-partition-manager.html), only to find I still couldn't resize the volume because it was BitLocker encrytped.
While Disk Management agreed with this assessment, Control Panel did not.
The solution was to enable and then immediately disable BitLocker on the volume in the Control Panel.

With access granted, I asked Partition Wizard for the partition to be shrunk to 50 GB, leaving the rest unallocated.
It restarts the device into a shell script to do this; I made sure the Surface was plugged in at the wall to avoid an inconveniently-timed flat battery.

This did not work, reporting an error with the file system.
The shell also started garbling its letters, which was worrying.
Tiring of these problems, I decided to try a different method.

#### Attempt 2
I decided to reinstall Windows manually so I could set the partition size before installation.

The local *Reset my PC* I'd used previously did not offer these options, but there was also a cloud version of the *Reset* that I thought might allow this since it downloads some kind of installer.
However, I was not able to test this as *Reset* no longer worked: apparently Windows could no longer see the recovery partition.
I guess the [previous attempt]({{< ref "#attempt-1" >}}) is the culprit for this.

Usually I'd download the [installation image](https://www.microsoft.com/en-us/software-download/windows10ISO) and flash a USB stick, but I don't have a sufficiently large USB stick to hand for the 5 GB image;
thanks to the pandemic lockdown, many of my things are locked away in my office, which I can't access.

It seems that from Windows, one can visit [this page](https://www.microsoft.com/en-us/software-download/windows10) to download an installation tool, bypassing the need for a USB stick, so that's what I tried.
Unfortunately, it did not show the partitioning menu, so was unsuccessful.

#### Attempt 3
Having reinstalled Windows again, I tried Partition Wizard again to see if the file system error had been fixed.
It had.
I resized the Windows partition to 50 GB. 
I also deleted the recovery partitions; since recovery images can be downloaded anyway, there was no need to keep a local recovery parition.
Finally, there was some space to install Linux.

## Installing Linux
`surface-linux` supports Debian-, Arch- and Fedora-based distributions.
Very nice, I thought, I'll go with Fedora since that's what [I'm using on my laptop and desktop these days](../../tags/fedora).

Spoiler: I ran into some issues with Fedora and ended up trying Arch-based [Manjaro]({{< ref "#manjaro" >}}) instead.
Manjaro wasn't perfect either, but was better.

### Fedora

#### Installation medium
I prepared a USB flash drive as an installtion medium as [before]({{< ref "install-fedora#preparing-the-flash-install-medium" >}}) with
```shell
https://download.fedoraproject.org/pub/fedora/linux/releases/32/Workstation/x86_64/iso/Fedora-Workstation-Live-x86_64-32-1.6.iso
sudo dd if=Fedora-Workstation-Live-x86_64-32-1.6.iso of=/dev/sdX bs=8M status=progress oflag=direct
```
#### Initial installation
I used a USB 3 hub to attach to the Surface a mouse, a keyboard, and the USB stick that I previously loaded with the Fedora installation image.
I rebooted the Surface, which put me in the Fedora installation menu.
I installed Fedora, configuring partitioning like [before]({{< ref "install-fedora#partitioning" >}}).
When the installation was complete, I rebooted into the new system and ran an update
```shell
sudo dnf update -y
sudo reboot # for kernel update
```

I noticed that there is a touch keyboard icon on the bottom right of the screen in GRUB.
However, sometimes when I try to use it, it crashes the computer.
Unfortunately, it appears to be unstable.

It also turned out that the Anne Pro (connected via USB) did not work for GRUB, so I had to use a different keyboard.
Even then, it did not always recognise the other keyboard unless I physically disconnected and reconnected the USB cable.

#### Patching the kernel
To support the touchscreen and pen, I had to [patch the kernel](https://github.com/linux-surface/linux-surface/wiki/Installation-and-Setup#surface-kernel-installation).

I added the `surface-linux` Fedora repository with
```shell
sudo dnf config-manager --add-repo=https://pkg.surfacelinux.com/fedora/linux-surface.repo
```
and installed the required pacakges with
```shell
sudo dnf install -y kernel-surface surface-firmware surface-secureboot iptsd
sudo dnf install -y --allowerasing libwacom-surface
sudo systemctl enable iptsd.service
```
The custom kernel was the default choice in GRUB after this, so I didn't have to change that.

I rebooted into the linux-surface kernel and checked it had worked:
```shell
$ uname -a
```
show a kernel with `surface` in its name, so I was on the custom kernel.

#### Swap
I configured a 4 [GiB]({{< ref "jargon#binary" >}}) swap file
```shell
sudo dd if=/dev/zero of=/swapfile bs=1M count=4096 status=progress
sudo chmod 600 /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
```
and added it to the `fstab`
```shell
echo "/swapfile none swap defaults 0 0" | sudo tee -a /etc/fstab
```
then [tested]({{< ref "server-install#mounting-data-disks" >}}) the new `fstab` configuration
```shell
sudo umount -av
sudo mount -av
```

#### Firmware
Fedora comes installed with the Linux proprietary firmware package and the Intel CPU microcode firmware package, so I didn't have to install anything extra.
I made sure the proprietary firmware was installed with
```shell
sudo dnf install linux-firmware
```
It was.

#### Hardware clock
I set the linux clock to `localtime` to match the Windows clock to avoid hardware lcok time issues resulting from the dual boot
```shell
sudo timedatectl set-local-rtc 1
sudo hwclock --systohc --localtime
```

#### Wayland
I ensured the desktop session was using Wayland like [this]({{< ref "xps-install#xorg" >}}).
It returned `Type=wayland`. Success.

#### Testing the pen
I installed [xournal++](https://xournalpp.github.io/) {{< ghub "xournalpp" "xournalpp" >}} and [Krita](https://krita.org/en/) {{< glsh "https://invent.kde.org/graphics/krita" >}}
```shell
sudo dnf install xournalpp krita
```
to test out the pen.
I found it worked nicely.

#### Testing the touchscreen
I couldn't get the touchscreen to work.
Then I read the [manual](https://github.com/linux-surface/linux-surface/wiki/Installation-and-Setup#touch-controls) more slowly and realised that was because `surface-linux` currently doesn't support multitouch and pen as I wanted on Fedora.

The [current state of affairs](https://www.reddit.com/r/SurfaceLinux/comments/iaj6ex/multitouch_support_merged_into_linuxsurface_581/) is that `surface-linux` supports full multitouch and stylus usage on kernel versions `>=5.8` via `iptsd`. That's great for Debian- and Arch-derived distros, for which `surface-linux` has compatible pre-built releases.
However, Fedora currently only reaches `v5.7`, for which the driver (for `5.4 <=` version `<= 5.7`) only does single-touch mode or stylus mode---not both at the same time.

The kernel update's probably coming [soon](https://www.reddit.com/r/SurfaceLinux/comments/iglh02/when_will_come_the_58_kernel_to_fedora/), but, for now, time to jump distro-ship.

> The very day after this, Fedora got a 5.8 `linux-surface` kernel [release](https://github.com/linux-surface/linux-surface/releases/tag/fedora-32-5.8.5-1).
> I'm happy I tried out Manjaro anyway because I liked it a lot.

### Manjaro
I've used Arch Linux in the past (on that X220t) and have fond memories of the experience and how much I learnt from it.
However, I also feel worried to use it as a daily driver;
I shouldn't really regularly spend time fixing my operating system after updates on my work machine.

Enter [Manjaro](https://manjaro.org/) {{< wiki "Manjaro" >}} {{< ext "https://distrowatch.com/table.php?distribution=manjaro" >}}: Arch with a stability buffer (and an installer).

I downloaded the [minimal GNOME version](https://manjaro.org/downloads/official/gnome/), using the torrent option and letting it seed to be kind to their servers, and wrote it to a USB stick:

```shell
sudo dd if=manjaro-gnome-20.0.3-minimal-200606-linux56.iso of=/dev/sda bs=8M status=progress oflag=direct
```

I plugged that into the Surface along with the keyboard and mouse and attempted to install Manjaro with its graphical installer, replacing the Fedora partition.
I had some difficulties with this.
The first time, the live system booted, but the installer wouldn't open.
The second time, the installer started, but then hung at 95% during "misc postinstall configurations".
After that, I switched to the custom installer, [Manjaro Architect](https://manjaro.org/downloads/official/architect/).

#### Installing with Architect
For the [Architect installation](https://wiki.manjaro.org/index.php?title=Installation_with_Manjaro_Architect), I first tried to provide internet via ethernet through a `rtl8153` USB adapter.
It couldn't make a connection---driver issues?
It was fine on wifi.

I set up Manjaro to use the existing Linux partition, formatted as `ext4` and mounted with default settings (which included an [optimisation](https://tldp.org/LDP/solrhe/Securing-Optimizing-Linux-RH-Edition-v1.3/chap6sec73.html) `noatime`).
I chose to use a swapfile of size equal to the RAM size (apparently this failed, so I later did this manually as [before]({{< ref "#swap" >}})).
I didn't reformat the UEFI partition.
I set the UEFI mountpoint to `/boot` to support `systemd-boot`.
For the [mirrorlist]({{< ref "#mirrors" >}}), I used `Edit Pacman Mirror Configuration` to set `Method = Random`, then did `Rank Mirrors by Speed` and chose all UK mirrors.
Then I went back and set `Method = Rank`, ran the ranking again, and chose the best two.

I included `base-devel` for [AUR](https://aur.archlinux.org/) usage and chose the `linux58` kernel; I'll install the `surface-linux` 5.8 kernel afterwards.
I chose GNOME for the desktop environment.
For additional packages, I chose `linux-firmware` and `intel-ucode`.
I opted for the light `systemd-boot` as bootloader.
I generated the `fstab` with device UUIDs.
I chose `localtime` clock for dual-boot compatibility.
I gifted my user with `zsh` as the default shell.

I rebooted into the new system and did an update:
```shell
sudo pacman -Syu
```
Everything looked fine, except `systemd-boot` didn't show a menu, going straight into Manjaro.
(I discovered that the UEFI can be accessed with `sudo systemctl reboot --firmware-setup`.)
I edited the bootloader configuration to show the boot menu by adding `timeout 5` to `/boot/efi/loader/loader.conf`.

#### Mirrors

Note that [mirrors](https://wiki.manjaro.org/index.php?title=Manjaro_Mirrors) ([Archwiki](https://wiki.archlinux.org/index.php/Mirrors)) can be optimised on the running system with
```shell
sudo pacman-mirrors --fasttrack && sudo pacman -Syyu
```
This takes a while but makes managing mirrors effortless.

#### Adding surface-linux
To add the `surface-linux` [repo](https://wiki.archlinux.org/index.php/Unofficial_user_repositories), I first [added their key](https://wiki.archlinux.org/index.php/Pacman/Package_signing#Adding_unofficial_keys) with:
```shell
wget -qO - https://raw.githubusercontent.com/linux-surface/linux-surface/master/pkg/keys/surface.asc | sudo pacman-key --add -
sudo pacman-key --finger 56C464BAAC421453
sudo pacman-key --lsign-key 56C464BAAC421453
```
Then I added the [entry](https://wiki.archlinux.org/index.php/Pacman#Repositories_and_mirrors):
```shell
echo "\n[linux-surface]\nServer = https://pkg.surfacelinux.com/arch/" | sudo tee -a /etc/pacman.conf
```

Next I installed the `surface-linux` packages:
```shell
sudo pacman -S linux-surface-headers linux-surface surface-ipts-firmware iptsd
sudo systemctl enable iptsd.service
```

#### Boot manager
`systemd-boot` didn't pick up on the new kernel and I had difficulty adding it manually, so I switched to the boot manager [rEFind](https://wiki.archlinux.org/index.php/rEFInd): 
```shell
sudo pacman -S refind
sudo bootctl remove # uninstall systemd-boot from EFI partition
sudo refind-install
```
I enabled touch screen support in the boot manager by [setting](http://www.rodsbooks.com/refind/configfile.html#adjusting) `enable_touch` in the [configuration file](https://wiki.archlinux.org/index.php/rEFInd#Configuration) `/boot/efi/EFI/refind/refind.conf`.
Surprisingly, this worked, providing a very nice boot manager for a tablet.

However, I must have messed something up because the system ended up not being able to boot any of the kernels.
I solved this by booting into Manjaro Architect and using the System Rescue utility in `setup` to install GRUB as the default bootloader.
I [enabled the menu](https://wiki.archlinux.org/index.php/GRUB/Tips_and_tricks#Hidden_menu) by setting `GRUB_TIMEOUT_STYLE=countdown` in `/etc/default/grub` then [regenerating the configuration file](https://wiki.archlinux.org/index.php/GRUB#Generate_the_main_configuration_file) with 
```shell
sudo grub-mkconfig -o /boot/grub/grub.cfg
```
The default settings have a nice Manjaro theme and use the last selected entry as the default entry each time.

#### libwacom
The other `surface-linux` packages are on the AUR. I used the [AUR helper](https://wiki.archlinux.org/index.php/AUR_helpers) [`yay`](https://github.com/Jguer/yay).
I first [imported the GPG key](https://aur.archlinux.org/packages/libwacom-surface/#comment-761722):
```shell
gpg --keyserver hkps://keyserver.ubuntu.com --recv-key E23B7E70B467F0BF
```
then installed the [patched](https://github.com/linux-surface/linux-surface/wiki/Utilities-and-Packages#libwacom-patched) [`libwacom-surface`](https://aur.archlinux.org/packages/libwacom-surface/):
```shell
yay -S libwacom-surface
```
which replaced [`libwacom`](https://linuxwacom.github.io/) {{< ghub "linuxwacom" "libwacom" >}}.

#### Ethernet driver
I tried to install [drivers](https://aur.archlinux.org/packages/r8152-dkms/) for the `rtl8152` USB to ethernet adapter:
```shell
sudo pacman -S linux58-headers
yay -S r8152-dkms
```
This failed for the `surface-linux` kernel, unfortunately.

#### Configuring for touch
I installed Wayland:
```shell
sudo pacman -S wayland
```
and enabled it in `/etc/gdm/custom.conf` by setting `WaylandEnable=true`.
I restarted `gdm` to apply this setting:
```shell
sudo systemctl restart gdm
```

I installed Firefox and enabled touch support:
```shell
sudo pacman -S firefox
echo "MOZ_USE_XINPUT2=1" | sudo tee -a /etc/environment
```

#### Some useful packages

I [again]({{< ref "#testing-the-pen" >}}) installed xournal++ and Krita, for digital writing and painting respectively.
I also installed Eye of Gnome for an image viewer that supports multitouch gestures.
```shell
sudo pacman -S xournalpp krita eog
```

#### Review

After all that, the touchscreen was unusably buggy.
I had to give up with the `>=5.8` drivers.

There remained one option: the old drivers (`<=5.3`). 

#### LTS kernel
The original `ipts` driver is supported on the LTS kernel (`4.19`), so I installed that:
```shell
sudo pacman -S linux-surface-lts-headers linux-surface-lts
```
and rebooted into it.

{{< figure alt="writing" src="../../img/surface-linux/write.jpg" caption="Testing the pen with xournal++">}}

This demonstrated a working touchscreen and stylus.
It's still not stable: it's crashing just a little too regularly.
Sadly, unless I've messed up something up, it doesn't seem that Manjaro is totally stable on the Surface either.

## Conclusion
Perhaps the Surface would be more stable on Debian (my distro of choice for stable [servers]({{< ref "server-install" >}})).
Or maybe if I'm patient things will improve on Manjaro.
The `5.8` drivers are very recent and development seems highly active just now.
I'll wait it out for the time being and see if `5.8` starts working on Manjaro in the near future, in the meanwhile using the LTS kernel.

<!-- I'll try Debian out when I have time if Manjaro is a flop. -->
<!-- I've downloaded the latest GNOME image including non-free firmware from [here](https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/amd64/bt-hybrid/) in preparation. -->

<!-- ## Secure Boot -->
<!-- I reenabled Secure Boot -->
<!-- I'll do this if and when everything else is working -->
<!-- https://github.com/linux-surface/linux-surface/wiki/Secure-Boot -->

+++
title = "Fluid"
date = 2021-04-04T16:57:05+01:00
categories = ["simulation"]
tags = ["cpp","fluid","navier-stokes","opengl","physics","graphics","2d","glad","glfw","game","linux","gcc","c++","project"]
description = "Numerically simulating (almost) Navier-Stokes to visualise a 2d fluid"
toc = true
draft = false
cover = "img/covers/56.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

As it turns out, solving {{< wl "Navier-Stokes" >}} numerically is indeed difficult.
I made a WIP 2d simulation at {{< gl fluid >}}.
I followed the well-known and lucid [Real-Time Fluid Dynamics for Games](https://www.dgp.toronto.edu/public_user/stam/reality/Research/pdf/GDC03.pdf).

I took this opportunity to learn about graphics programming.
I used [OpenGL](https://www.opengl.org/) {{< wiki OpenGL >}}, following [this popular guide](https://learnopengl.com/).
Loading is handled by {{< gh Dav1dde glad >}}, and windowing by [GLFW](https://www.glfw.org/) {{< wiki GLFW >}} {{< ghub "glfw" "glfw" >}}.
I borrowed the matplotlib colourmap viridis from [here](https://github.com/BIDS/colormap/blob/master/colormaps.py).

Here's some turbulence:

<video width="100%" autoplay loop muted>
  <source src="../../vid/turb.webm" type="video/webm">
</video>

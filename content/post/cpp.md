+++
title = "C++ and tools"
date = "2019-11-12T12:52:57Z"
tags = ["cpp","c++","software-development","fedora","fedora-34","laptop","desktop","server","open-source","software-language","static-typing","multi-paradigm","object-oriented-programming","oop","programming","coding","iso","compiler","standard","standard-library","header-files","source-files","source-code","code","macros","c++20","package-manager","conan","vcpkg","build2","spack","buckaroo","hunter","software-network","gcc","g++","clang","llvm","clang++","debugging","gdb","cli","tui","gnu","libtool","ldd","nm","objdump","c++flit","file","build","compile","build-system","make","makefile","autotools","cmake","meson","profiling","gprof","valgrind","callgrind","memcheck","static-analysis","clang-check","cpp-check","c++11","c++14","c++17","cppreference"]
categories = ["coding"]
description = ""
draft=false
toc = true
cover = "img/covers/23.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

Beware: this article is (fittingly) a work in progress!

## The language

[C++](https://isocpp.org/) {{< wiki "C++" >}} {{< docs "https://en.cppreference.com" >}} {{< arch "C%2B%2B" >}} is a [multi-paradigm]({{< ref "jargon#paradigms" >}}) [statically-typed]({{< ref "jargon#type-checking" >}}) general-purpose programming language.
Its specification is published by the ISO as versioned standards and these are implemented as compiled languages by several vendors.
They include the core language and a {{< wl "standard library" "C++ Standard Library" >}}.

C++ source code includes:
* headers files
    * `.hpp`
    * contain the declarations; define interfaces
    * note that template functions are implemented in the header file, or in a separate file that is included in the header with `#include "FILE.ipp"`
* source files
    * `.cpp`
    * contain the implementations

Files are imported with pre-processor macros `#include "FILE.SUFFIX"` and libraries with `#include <LIBRARY>`.
`C++20` introduced [modules](https://en.cppreference.com/w/cpp/language/modules), although [current compiler support is only partial](https://en.cppreference.com/w/cpp/compiler_support/20).

A C++ program must also include a `main` function, which is where execution starts:
```c++
int main(int argc, char* argv[]) {
    ...
}
```
The command line arguments are provided through its parameters.

While [many C++ libraries exist](https://en.cppreference.com/w/cpp/links/libs), there is no official package manager.
Packages can be installed manually, through the system package manager if supported, or by one of the various C++ community package managers.
These perform some subset of the duties of downloading packages, installing packages, building projects, and tracking package dependencies.
Here's an [incomplete](https://hackingcpp.com/cpp/tools/package_managers.html) [list](https://caiorss.github.io/C-Cpp-Notes/package-managers.html):
* [Conan](https://conan.io/) {{< ghub conan-io conan >}}
* [vcpkg](https://vcpkg.io/en/index.html) {{< ghub microsoft vcpkg >}}
* [build2](https://build2.org/) {{< git "https://git.build2.org/cgit/build2" >}}
* [spack](https://spack.io/) {{< ghub spack spack >}}
* [Buckaroo](https://buckaroo.pm/) {{< ghub LoopPerfect buckaroo >}}
* [Software Network](https://software-network.org/) {{< ghub SoftwareNetwork sw >}}
* [Hunter](https://hunter.readthedocs.io/en/latest/) {{< ghub cpp-pm hunter >}}

For some examples of my C++ projects, see
* {{< gl cellular-automata >}} {{< il cellular-automata >}}
* {{< gl boids >}} {{< il boids >}}
* {{< gl fluid >}} {{< il fluid >}}

I also keep a directory of short test scripts where I figure out how features work at
* <https://gitlab.com/jetdynamics/njet-tools/-/tree/master/cpp-tests>

## Compilers

There are two main compilers:
* `g++` from [GCC](https://gcc.gnu.org/) {{< wiki "GNU_Compiler_Collection" >}} {{< arch "GNU_Compiler_Collection" >}}
    ```shell
    sudo dnf install gcc-c++
    ```
* `clang++` from [Clang](https://clang.llvm.org/) {{< wiki "Clang" >}} {{< arch "clang" >}}
    ```shell
    sudo dnf install clang
    ```

## Debugging tools

Compile with `-g` for debug info.

This section is rather sparse, but maybe can provide the first step in finding an appropriate debugging tool.

### gdb

Install `gdb` {{< docs "https://sourceware.org/gdb/current/onlinedocs/gdb/" >}} with
```shell
sudo dnf install gdb
```

Run as
```shell
gdb <object>
```
then ([cheatsheet](https://darkdust.net/files/GDB%20Cheat%20Sheet.pdf)):
* `run`/`r` run `<object>`
* `backtrace`/`bt` print all __stack frames__ in __call stack__
* `break <location>`/`b` to add __breakpoint__ at line number or function name
* `s`/`step` step going into functions - step into
* `n`/`next` step without going into functions -next line
* `c`/`continue` to next `b`

#### TUI mode
Start with
```shell
gdb --tui <binary>
```
or use in a session with
```gdb
(gdb) tui enable
```
Use
```gdb
refresh
```
when the output is garbled.

#### With Libtool
To use `gdb` with stuff compiled with [`GNU Libtool`]({{< ref "libtool" >}}),
```shell
libtool --mode=execute gdb --args ./path/to/compiled/file
# libtool --mode=execute gdb -tui --args ./path/to/compiled/file # if you want TUI mode
(gdb) run
(gdb) refresh
```

### ldd
Install
```shell
sudo dnf install glibc-common
```
to debug linking with [`ldd`](http://man7.org/linux/man-pages/man1/ldd.1.html).
`man ldd` is useful.

### nm
Install
```shell
sudo dnf install binutils
```
to list symbols from object files with
```shell
nm --demangle
```
Again, `man nm` is your friend.

### objdump
We can disassemble with
```shell
objdump -d --demangle <name>.o
```

### c++filt
Another demangler (not in the Fedora repos):
```shell
g++ -S -c <name>.cpp -o <name>.S
cat <name>.S | c++filt > demangled
```

### file
We can also install
```shell
sudo dnf install file
```
to use [`file`](https://linux.die.net/man/1/file) to debug linking.

## Build systems

There are many build systems to manage the compilation and installation of C++ codebases.
Working with build systems is generally the most painful part of any programming project.

### Make
`make` is often enough for simple codes.
```shell
sudo dnf install make
```
Here's an [example usage](https://gitlab.com/jetdynamics/njet-tools/-/blob/master/cpp-tests/pointers/Makefile).
Get started with {{< gl cpp-minimal-template >}}.

### Autotools
The Autotools suite is widely used in older projects.
```shell
sudo dnf install autoconf automake libtool
```
For an example usage, see [C++lular automata]({{< ref cellular-automata >}}).

### CMake
[CMake](https://cmake.org/) {{< glsh "https://gitlab.kitware.com/cmake/cmake" >}} is quite popular.
```shell
sudo dnf install cmake
```

### Meson
[Meson](https://mesonbuild.com/) is a newer option which seems quite nice.
```shell
sudo dnf install meson
```
For an example usage, see [Boids]({{< ref boids >}}).

## Profiling

### gprof
```shell
sudo dnf install binutils
```

Compile with `-p -pg`, link with `-pg`
```shell
./time
gprof time
```

### valgrind
```shell
sudo dnf install valgrind kcachegrind
```
* https://valgrind.org/
* https://sourceware.org/git/?p=valgrind.git

#### callgrind

```shell
valgrind --tool=callgrind ./time
kcachegrind callgrind.out.NUMBER
```

#### memcheck

```shell
valgrind -s --leak-check=full --show-leak-kinds=all --track-origins=yes ./time
```

## Static analysis

{{< wl "Static program analysis" >}}

### clang-check

clang-check https://clang.llvm.org/docs/ClangCheck.html
```shell
sudo dnf install clang-tools-extra
```

### cppcheck

[cppcheck](http://cppcheck.net) {{< ghub danmar cppcheck >}}

laptop Fedora 34
```shell
sudo dnf install cppcheck
```

Or

* Install
```shell
cd ~/git
git clone git@github.com:danmar/cppcheck.git
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$CPPCHECK_LOCAL -DFILESDIR=$CPPCHECK_LOCAL/share -DUSE_MATCHCOMPILER=ON -DHAVE_RULES=ON ..
cmake -j 64 --build .
cmake --install .
```
* Use
```shell
cppcheck --enable=all --suppress=unusedFunction --language=c++ -j 64 *
```

## Writing C++
[keywords](https://en.cppreference.com/w/cpp/keyword) C++11
`enum`


### Type aliases
`typedef`
`using`

### Virtual
`virtual`
`final`
`override`
`=0` pure

### Const
`const`
`constexpr`
`static`

### Pointers and References
`_*`
`*_`
`const *_`
`const _*`
`&`
`nullptr`

* [References](https://gitlab.com/jetdynamics/njet-tools/-/blob/master/cpp-tests/references/test.cpp)
* [Pointers](https://gitlab.com/jetdynamics/njet-tools/-/blob/master/cpp-tests/pointers/test.cpp)

### Types

#### Fundamental

https://en.cppreference.com/w/cpp/language/types

`char`

`int`
`long`
`unsigned`
`short`

`float`
`double`

`void`

#### Standard library

`#include <string>`
`std::string`

`cstdlib` `cstddef`
On `std::size_t` ...

### Assignment

`double number = 1.`
`double number { 1. }`

`std::vector<double> numbers({1., 2.})`
`std::vector<double> numbers {{1., 2.}}`
`std::vector<double> numbers = {1., 2.}`

https://en.cppreference.com/w/cpp/language/implicit_conversion

### Casting types
https://en.cppreference.com/w/cpp/language/explicit_cast

`double number{1.};`
`int(number)`
`(int)number`
`static_cast<int>(number)`

`dynamic_cast<int>(number)`
`static_cast<int>(number)`
`reinterpret_cast<int>(number)`

### Printing

`#include <iostream>`
`std::cout`
`std::endl` vs `'\n'`

### Logic
https://en.cppreference.com/w/cpp/language/operator_alternative
`and` `&&`
`or` `||`
`not` `!`

`==`
`!=`

`false`
`true`

### Bit twiddling
?
`|=` `or_eq`
`&=` `and_eq`

`|` `bitor`
`&` `bitand`
`^` `xor`

### Classes

`class`
`struct`

`public`
`protected`
`private`

`friend`

`explicit`

`this`

members
`.`
`->`

### Control flow
`for`
`if`
`while`
https://en.cppreference.com/w/cpp/language/do
`do {} while {}`
`continue`
`break`

`switch () { case 1: {break;} default: {} }`

### Namespace
`namespace`
important!

### Templates
...

### Misc
* Never use macros
* `std::shared_ptr`
* `std::array`

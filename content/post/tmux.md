+++
title = "Configuring tmux"
date = "2019-11-12T11:43:49Z"
edit = 2020-01-26
tags = ["environment","terminal-multiplexer","linux","terminal","install","shell","server","desktop","laptop","fedora","tmux"]
categories = ["environment-core"]
description = "My tmux configuration"
toc = true
cover = "img/covers/88.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## DEPRECATED
{{< cal "2022-04-10" >}}

Now I use [this much lighter setup](https://gitlab.com/eidoom/dotfiles-tmux-2).

## Introduction

To have multiple windows and panes in my terminal, I use the terminal multiplexer [`tmux`](https://github.com/tmux/tmux/wiki).
My full configuration and install scripts are [here](https://gitlab.com/eidoom/dotfiles-tmux).
Installation of system pacakges is specific to Fedora and tested on Fedora 31.
In this post, I discuss the formation of these dotfiles.

## Installation

I installed `tmux`  with

```shell
sudo dnf install tmux
```

## Configuration

The `tmux` configuration is stored at `~/.tmux.conf`.
I initially customised it as

```shell
vi ~/.tmux.conf
```

```shell
# tmux configuration file

# Set zsh as default shell
set-option -g default-shell /bin/zsh

# Indexing to match the keyboard
set-option -g base-index 1

# Automatically set window title to last part of current path
set-window-option -g automatic-rename on
set-option -g automatic-rename-format '#{b:pane_current_path}'

# Mouse mode
set-option -g mouse on

# Alt-arrow without prefix key to switch panes
bind -n M-Left select-pane -L
bind -n M-Down select-pane -D 
bind -n M-Up select-pane -U
bind -n M-Right select-pane -R

# Shift-arrow without prefix key to switch windows
bind -n S-Left  previous-window
bind -n S-Right next-window

# Reload tmux config
bind r source-file ~/.tmux.conf
bind R source ~/.tmux.conf
```

I leave the prefix as the default, `Ctrl`+`b`, for local sessions.
I use the `screen` default of `Crtl`+`a` for `ssh` sessions, so on headless machines set

```shell
echo "unbind C-b\nset-option -g prefix C-a" >> ~/.tmux.conf
```

My configuration for headless machines can be found [here](https://gitlab.com/eidoom/dotfiles-tmux-headless-light).
This configuration means that remote `tmux` sessions can be controlled independently when they are nested within local `tmux` sessions.

## Plugins

I use the `tmux` package manager [`tpm`](https://github.com/tmux-plugins/tpm).
I installed it by 

```shell
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
```

then adding the following to the end of `~/.tmux.conf`

```shell
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-pain-control'
set -g @plugin 'tmux-plugins/tmux-prefix-highlight'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-yank'

# Initialize tmux plugin manager (keep this line at the very bottom of tmux.conf!)
run -b '~/.tmux/plugins/tpm/tpm'
```

and reloading `tmux` with `prefix`+`R`.

This includes a few starter plugins:

-   [`sensible`](https://github.com/tmux-plugins/tmux-sensible) gives some... sensible defaults.
-   [`pain-control`](https://github.com/tmux-plugins/tmux-pain-control) provides some sane pane controls.
-   [`prefix-highlight`](https://github.com/tmux-plugins/tmux-prefix-highlight) changes the colour of the leftmost section of the infobar at the bottom when `prefix` is engaged.
-   [`yank`](https://github.com/tmux-plugins/tmux-yank) makes copying easier. 
-   [`copycat`](https://github.com/tmux-plugins/tmux-copycat) provides searches.

New plugins are loaded with `prefix`+`I`.

## Persistence

I use [`resurrect`](https://github.com/tmux-plugins/tmux-resurrect) to remember my `tmux` configuration over reboots.
The restore/save is manually performed by `prefix`+`Ctrl`+`r`/`s`.
[`continuum`](https://github.com/tmux-plugins/tmux-continuum) automates the process so I don't need to manually save and restore sessions.
It saves the session automatically and can autorestore with

```shell
echo -e "set -g @continuum-restore 'on'\n$(cat ~/.tmux.conf)" >! ~/.tmux.conf
```

where the fancy echo is to prepend the line to the file since `tpm` must be initialised at the end of the file.
The `-e` just [allows us to use the newline character](https://ss64.com/bash/echo.html) `\n`.
These packages are installed by adding the following to `~/.tmux.conf`

```vim
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
```

and reloading with `prefix`+`I` then `prefix`+`Ctrl`+`r`.

## Powerline

I installed [`powerline`](https://github.com/powerline/powerline) for `tmux` with

```shell
sudo dnf install tmux-powerline
echo -e 'source "/usr/share/tmux/powerline.conf"\n$(cat ~/.tmux.conf)' >! ~/.tmux.conf
```

<!--
```
sudo npm install -g jsonlint
mkdir ~/.config/powerline/
```

and configured it as

```shell
$ vi ~/.config/powerline/config.json
```

I checked the configuration with

```shell
jsonlint ~/.config/powerline/config.json
powerline-lint
```
-->

## Colours

To use `truecolor`, I set up `tmux` as follows.
`zprezto` (see [shell configuration]({{< ref "shell" >}})) sets the `TERM` variable

```shell
echo $TERM
```

```shell
xterm-256color
```

I enabled `truecolor` in `tmux` with

```shell
echo -e 'set-option -ga terminal-overrides ",xterm-256color:Tc"\n$(~/.tmux.conf)' >> ~/.tmux.conf
```

I confirmed this worked by restarting `tmux` and running

```shell
tmux info | grep Tc
```

```shell
203: Tc: (flag) true
```

## Copying

I use `Shift`+ `highlight with mouse cursor` then `Ctrl`+`Shift`+`c` to copy and `Ctrl`+`Shift`+`v` to paste in a `tmux` session.
This configuration also supports yanking between [`vi`]({{< ref "neovim" >}}) sessions in different `tmux` panes thanks to the `yank` plugin.

## Result

Now `tmux` looks like

{{< figure alt="image of tmux" src="../../img/tmux.webp" >}}

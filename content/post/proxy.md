+++
title = "Putting the new proxy to use"
date = 2021-10-03T11:57:15+01:00
categories = ["self-host"]
tags = ["nginx","reverse-proxy","self-host","hosting","infrastructure","backend","calibre-web","transmission-bittorrent","jellyfin","autoindex","*arr","server","bittorrent","home-server"]
description = "Adding more services to my public Nginx reverse proxy server"
toc = true
force_toc = true
draft = false
cover = "img/covers/41.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

DEPRECATED: see [new setup]({{< ref "cloud" >}}).

[Having set up a public HTTPS reverse proxy]({{< ref "caldav#reverse-proxy" >}}) to provide a [Radicale]({{< ref caldav >}}) interface on my [home server]({{< ref "server-install" >}}), I put some of my [other home services]({{< ref "server-install-containers" >}}) on there too by adding the following entries to
```
/etc/nginx/sites-available/reverse_proxy_https.conf
```
Nginx can be reloaded with
```shell
sudo systemctl restart nginx
```
The error log can be accessed for debugging at
```shell
sudo vi /var/log/nginx/error.log
```
and the access log at
```shell
sudo vi /var/log/nginx/access.log
```
Older log entries are in `*.log.1`, `*.log.2.gz`, `*.log.3.gz`, and so on.
## Calibre-Web

I host [some ebook libraries with Calibre-Web interfaces]({{< ref "server-install-containers#calibre-web" >}}).
For each, I disabled the guest user by unchecking `Admin > Configuration > Edit Basic Configuration > Feature Configuration > Enable Anonymous Browsing` and set up users and passwords at `Admin > Users` in the web GUI then [added them to the proxy](https://github.com/janeczku/calibre-web/wiki/Setup-Reverse-Proxy#nginx) with
```shell
sudo vi /etc/nginx/sites-available/reverse_proxy_https.conf
```
```vim
...
    location /books/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8083/;
        proxy_set_header  X-Script-Name      /books;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }

    location /comics/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8084/;
        proxy_set_header  X-Script-Name      /comics;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }

    location /textbooks/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8085/;
        proxy_set_header  X-Script-Name      /textbooks;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }

    location /magazines/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8086/;
        proxy_set_header  X-Script-Name      /magazines;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
...
```
To allow uploading large files to Calibre-Web, I increased the allowed upload size with
```shell
server {
    ...
    client_max_body_size 1G;
    ...
}
```

## Transmission

The `transmission` {{< wl "RPC" "Remote procedure call" >}} web interface username and password are set in `settings.json` with [`rpc-username` and `rps-password`](https://github.com/transmission/transmission/wiki/Editing-Configuration-Files#rpc) (`transmission` overwrites the password with its hash).
Remember that you have to stop `transmission` before editing `settings.json` as `transmission` reads this file on startup and overwrites it while running.

Since I'm [running `tranmission` on `docker`]({{< ref "server-install-containers#transmission" >}}), I didn't edit `settings.json` manually but instead managed the user by setting `USER` and `PASS` under `environment` for the `transmission` entry in my `docker-compose` configuration.

I added `transmission` to the proxy server as
```vim
    location /transmission/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:9091/transmission/;
        proxy_set_header  X-Script-Name      /transmission;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
```

## Jellyfin

I host a [Jellyfin]({{< ref "server-install-containers/#jellyfin" >}}) instance.
In the web portal, set `Menu > Admin > Dashboard > Advanced > Networking > Server Address Settings > Base URL` to `/jellyfin` and made Jellyfin [available](https://jellyfin.org/docs/general/networking/nginx.html#nginx-with-subpath-exampleorgjellyfin) through the proxy with
```vim
    location /jellyfin/ {
        proxy_pass http://127.0.0.1:8096/jellyfin/;

        proxy_pass_request_headers on;

        proxy_set_header Host $host;

        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $http_host;

        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $http_connection;

        proxy_buffering off;
    }
```
I also manage users in the web GUI.

## Jackett

For [Jackett]({{< ref "server-install-containers/#jackett" >}}), I set an `Admin password` and the `Base path override` to `/jackett` in the `Jackett Configuration` area of the web portal.
I added a [proxy entry](https://github.com/Jackett/Jackett/wiki/Reverse-Proxy)
```vim
    location /jackett/ {
        proxy_pass         http://127.0.0.1:9117;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade $http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_cache_bypass $http_upgrade;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_set_header   X-Forwarded-Host $http_host;
    }
```

## *arr

For each of the *arrs {{< docs "https://wiki.servarr.com/" >}}, in its web portal,
* I set up `Forms (Login Page)` authentication at `Settings > General > Security > Authentication` with a `Username` and `Password`
* I changed the base URL at `Settings > General > Host > URL Base` to `/*arr`.

I made the following entries in the reverse proxy:
* [Sonarr]({{< ref "server-install-containers/#sonarr" >}})
```vim
    location /sonarr/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8989/sonarr/;
        proxy_set_header  X-Script-Name      /sonarr;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
```
* [Radarr]({{< ref "server-install-containers/#radarr" >}})
```vim
    location /radarr/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:7878/radarr/;
        proxy_set_header  X-Script-Name      /radarr;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
```
* [Lidarr]({{< ref "server-install-containers/#lidarr" >}})
```vim
    location /lidarr/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8686/lidarr/;
        proxy_set_header  X-Script-Name      /lidarr;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
```
* [Readarr]({{< ref "server-install-containers/#readarr" >}})
```vim
    location /readarr/ {
        proxy_bind        $server_addr;
        proxy_pass        http://127.0.0.1:8787/readarr/;
        proxy_set_header  X-Script-Name      /readarr;
        proxy_set_header  X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header  Host               $http_host;
        proxy_set_header  X-Scheme           $scheme;
    }
```

## File browser
I also added a simple file browser using [the Nginx autoindex module](https://nginx.org/en/docs/http/ngx_http_autoindex_module.html).

I [set up](https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/) [basic authentication](https://nginx.org/en/docs/http/ngx_http_auth_basic_module.html) using `bcrypt` hashing for higher security (cf [Radicale]({{< ref "caldav#radicale" >}}) authentication),
```shell
sudo apt install apache2-utils python3-bcrypt
sudo htpasswd -B -c /etc/apache2/.htpasswd <user>
```
and added the entry
```vim
    location /files/ {
        autoindex on;
        autoindex_exact_size off;

        auth_basic "Files";
        auth_basic_user_file /etc/apache2/.htpasswd;
    }
```
then made my data available here
```shell
ln -s /media/pool/data/ ~/www/files
```
Note that this basic authentication method is only secure because we're using HTTPS to encode everything in transit.

If streaming files to, for example, mpv, authentication can be passed in the url like
```
mpv https://<user>:<password>@<domain>/<path>
```
Not using URL-sensitive characters in the password makes this easier.

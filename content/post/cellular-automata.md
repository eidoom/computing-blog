+++
title = "C++lular automata"
date = 2020-07-08T14:35:07+01:00
edit = 2021-04-05
categories = ["simulation"]
tags = ["cpp","autotools","gnu","automake","autoconf","libtool","gcc","make","sfml","game","ffmpeg","html","front-end","avc","h.264","mkv","mp4","rpmfusion","fedora","cellular-automata","conways-game-of-life","physics","maths","qft","hep","linux","emergence","c++","project"]
description = "Conway's Game of Life in C++"
toc = true
draft = false
cover = "img/covers/77.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I have been working on writing [Conway's Game of Life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) (_Life_) in C++. 
The source is [here](https://gitlab.com/eidoom/cellular-automata/).
The project started as a bit of fun to learn more about C++.
I later [autoconfiscated](http://www.catb.org/~esr/jargon/html/A/autoconfiscate.html) the project ([commit](https://gitlab.com/eidoom/cellular-automata/-/commit/2b03ef666464193c0450d6511faf5a8304dccc77)) to learn how to use the [GNU Autotools suite](https://en.wikipedia.org/wiki/GNU_Autotools), including publishing [releases](https://gitlab.com/eidoom/cellular-automata/-/releases).
I also upgraded the terminal graphics to proper 2D graphics using [SFML](https://en.wikipedia.org/wiki/Simple_and_Fast_Multimedia_Library) ([commit](https://gitlab.com/eidoom/cellular-automata/-/commit/09029afdcc00fe54ef11cbd92f35601c9f1dab26)).

I was interested to find that _Life_, while certainly the most famous, is not the only (interesting) cellular automaton (CA).
There is a family of [_Life_-like CA](https://en.wikipedia.org/wiki/Life-like_cellular_automaton) classified by their "born" and "survive" rules, `B3/S23` in the case of _Life_, and these are just a subset of all [CA](https://en.wikipedia.org/wiki/Cellular_automaton).
It is a fascinating field of mathematics.
I heard at a [recent conference](https://conference.ippp.dur.ac.uk/event/906/contributions/4869/) that CA are even being applied to study the foundations of quantum field theory in high energy physics.

<video width="100%" autoplay loop muted>
  <source src="../../vid/game-of-life.mp4" type="video/mp4">
</video>

### Aside
I recorded this little video of the program running using [SimpleScreenRecorder](https://en.wikipedia.org/wiki/SimpleScreenRecorder).
On Fedora, it's included in [`rpmfusion-free`](install-fedora/#rpmfusion) as `simplescreenrecorder`.
I used the [H.264 compression standard](https://en.wikipedia.org/wiki/Advanced_Video_Coding) (not to be confused with one of its implementations, the [x264 encoder](https://en.wikipedia.org/wiki/X264)) and [mp4 container](https://en.wikipedia.org/wiki/MPEG-4_Part_14) since this combination is the [most widely supported format by browsers](https://www.freecodecamp.org/news/video-formats-for-the-web/).
I initially recorded to an [mkv container](https://en.wikipedia.org/wiki/Matroska), before realising this was unsupported by browsers, but `ffmpeg` (also from `rpmfusion-free`) can easily be used to convert the container:
```shell
ffmpeg -i game-of-life.mkv -c copy game-of-life.mp4
```
The [element](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/video) uses [`autoplay`, `loop` and  `muted`](https://stackoverflow.com/a/53258723/5922871) attributes ([source](https://gitlab.com/eidoom/computing-blog/-/blob/master/content/post/cellular-automata.md)).
`muted` is required for `autoplay` videos by some browsers (makes sense).

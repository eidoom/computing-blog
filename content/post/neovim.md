+++
title = "Configuring Neovim"
date = "2019-12-05T11:12:27Z"
edit = 2021-06-28
tags = ["environment","linux","install","terminal","text-editor","desktop","server","laptop","fedora","solarized","vim","vi","neovim","cpp","python","javascript","html","css","json","rust","markdown","typescript","shell","bash","zsh","vim-bootstrap","vim-plug","go","syntax-highlighting","plugins","customise","powerline","comments","indentation","autocompletion","multiple-cursors","yankstack","scripts","julia","c++","nvim","configure"]
categories = ["environment-core"]
description = "Setting up a new flavour of Vi"
toc = true
cover = "img/covers/4.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## DEPRECATED
{{< cal 2021-06-05 >}}

See [new setup]({{< ref "nvim2" >}}).

## Introduction

I've been using [`vim`](https://www.vim.org/) {{< ghub vim vim >}} {{< wiki "Vim (text editor)" >}} {{< arch "Vim" >}} as one of my go-to text editors for some time.
I decided to replace it with [Neovim](https://neovim.io/) {{< ghub neovim neovim >}} {{< wiki "Vim_(text_editor)#Neovim" >}} {{< arch "Neovim" >}}, or `nvim`, and configure it from scratch using [`vim-bootstrap`](https://github.com/editor-bootstrap/vim-bootstrap).
`vim-bootstrap` uses [`vim-plug`](https://github.com/junegunn/vim-plug) as the `(n)vim` package manager, which is what I was using previously with `vim`.
I've used this on my desktop and laptop, which both run Fedora 31 [edit: now updated through to 34].

A cheatsheet for using this configuration is [here]({{< ref "vim-cheatsheet" >}}).

The up-to-date configuration and install scripts initially developed in this post can be found [here](https://gitlab.com/eidoom/dotfiles-nvim).

## Installation

I installed `nvim` and prerequisites for packages and `vim-bootstrap`

```shell
sudo dnf install ncurses-devel git ctags-etags curl neovim python3-neovim python3-pyflakes python3-jedi python3-flake8 ripgrep npm golang
sudo npm install -g neovim
```

Since Go is used, GOPATH and PATH must be configured appropriately.
My configuration is detailed [here]({{< ref "go" >}}).

## Change default

I set `nvim` as the default terminal text editor and aliased calls to `vi` and `vim`

```shell
echo "EDITOR=nvim\nVISUAL=nvim\nalias vi=nvim\nalias vim=nvim" >> ~/.zshrc
```

## Initial configuration

`nvim` settings are at `~/.config/nvim/init.vim`.
I used `vim-bootstrap` to generate the initial configuration

```shell
go get github.com/avelino/vim-bootstrap
cd $GOPATH/src/github.com/avelino/vim-bootstrap
go build
mkdir -p ~/.config/nvim
vim-bootstrap -langs=c,html,javascript,python -editor=nvim >! ~/.config/nvim/init.vim
```

## Plugin manager

`vim-bootstrap` sets up `vim-plug` as the `nvim` package manager.
`vim-plug` will install all packages in the new configuration file the first time `nvim` is run.
Control with `:Plug*`.

## Plugins

Here's a noncomplete list of the plugins in use.
Plugins which are not part of `vim-bootstrap` are marked as <i class="fas fa-plus-circle"></i>.
My favourites are also highlighted with <i class="fas fa-heart"></i>.
-   [`vim-commentary`](https://github.com/tpope/vim-commentary) automates commenting with `gc*` <i class="fas fa-heart"></i>.
    * [Unsupported filetypes can have comments defined manually](https://github.com/tpope/vim-commentary#faq), as [I have done]({{< ref "#customisation" >}}) in my `local_init.vim`. [View available filetypes](https://vi.stackexchange.com/a/5782) with `:setfiletype `+`Tab`.
-   [`vim-multiple-cursors`](https://github.com/terryma/vim-multiple-cursors) gives Sublime-style cursors with `Ctrl`+`n` <i class="fas fa-plus-circle"></i>.
    * I [ended up]({{< ref "#bugs" >}}) deleting this <i class="fas fa-trash"></i>.
-   [`vim-airline`](https://github.com/vim-airline/vim-airline) gives a powerline statusbar at the bottom and buffers line at the top.
-   [`indentLine`](https://github.com/Yggdroot/indentLine) shows faint lines for code indentation levels.
-   [`jedi-vim`](https://github.com/davidhalter/jedi-vim) provides Python autocompletion.
-   [`vim-yankstack`](https://github.com/maxbrunsfeld/vim-yankstack) allows cycling through recent yanks on pasting with `Alt`+`p` <i class="fas fa-plus-circle"></i> <i class="fas fa-heart"></i>.

## Customisation

When using `vim-bootstrap`, custom settings are in `~/.config/nvim/local_init.vim`.
I edited it to include the following

```shell
vi ~/.config/nvim/local_init.vim
```

```shell
# Enable mouse
set mouse=a

# Stop any hiding for Markdown
autocmd FileType markdown let g:indentLine_setConceal=0

# Make the powerline use the solarized colour scheme
let g:airline_theme='solarized'

# Get fancy powerline symbols
let g:airline_powerline_fonts=1

# Stop csv syntax highlighting for Sherpa *.dat runcards
au BufRead,BufNewFile *.dat set filetype=text

# Use TeX syntax highlighting for TeX class files
au BufRead,BufNewFile *.cls set filetype=tex

# Recognise custom make files - required for using tabs; spaces break make
au BufRead,BufNewFile Makefile.* set filetype=make

# HTML comments in Markdown
autocmd FileType markdown setlocal commentstring=<!-- %s -->

# Disable autoclosing of brackets, etc.
let loaded_delimitMate = 1
```

When setting filetype rules, it can be useful to see what Vim thinks the current filetype is with `:set filetype`.

Custom plugins are defined in `~/.config/nvim/local_bundles.vim`.
I added

```shell
vi ~/.config/nvim/local_bundles.vim
```

```vim
Plug 'terryma/vim-multiple-cursors'
Plug 'maxbrunsfeld/vim-yankstack'
```

## Colours

I use the [solarised colour scheme](https://github.com/altercation/solarized) for my terminal.
I use `nvim` in `tmux`, which is set up to use `truecolor` as described [here]({{< ref "tmux" >}}).
This `tmux` configuration is required for the following.

To get a `truecolor` `solarized` colour palette working in `nvim`, I used [NeoSolarized](https://github.com/iCyMind/NeoSolarized).
I added it as a `vim-plug` plugin by adding

```shell
echo "Plug 'iCyMind/NeoSolarized'" >> ~/.config/nvim/local_bundles.vim
```

then running `:PlugInstall` in `nvim`.
I enabled `truecolor` in `nvim` with

```shell
echo "set termguicolors" >> ~/.config/nvim/local_init.vim
```

and set the colour palette with

```shell
echo "colorscheme NeoSolarized" >> ~/.config/nvim/local_init.vim
```

Following [this](https://github.com/iCyMind/NeoSolarized#truecolor-test), I tested that it was working by opening a terminal in `nvim` by `:terminal` and running

```shell
awk 'BEGIN{
    s="/\\/\\/\\/\\/\\"; s=s s s s s s s s;
    for (colnum = 0; colnum<77; colnum++) {
        r = 255-(colnum*255/76);
        g = (colnum*510/76);
        b = (colnum*255/76);
        if (g>255) g = 510-g;
        printf "\033[48;2;%d;%d;%dm", r,g,b;
        printf "\033[38;2;%d;%d;%dm", 255-r,255-g,255-b;
        printf "%s\033[0m", substr(s,colnum+1,1);
    }
    printf "\n";
}'
```

which shows a colour bar of smoothly changing colour in `truecolor` and discrete colour blocks elsewise.

The light and dark themes can be switched between with `:set background=dark` and `:set background=light`.

## Result

Now `nvim` looks like:

{{< figure alt="image of nvim" src="../../img/nvim.webp" caption="Writing this article in customised Neovim">}}

## Addenda

### Bugs
{{< cal 2019-12-15 >}}

Having used this for a while, I've noticed that certain mistypes while using `vim-multiple-cursors` makes `nvim` freeze.
My current solution is to only hit the right keys... 

{{< cal 2019-01-02 >}}

Also, the use of `Ctrl`+`n` for multiple cursors conflicts with Jedi autocompletion in Python.

{{< cal 2020-04-22 >}}

I got the error
```
E138: All /home/USER/.local/share/nvim/shada/main.shada.tmp.X files exist, cannot write ShaDa file!
```
when trying to save a file.
It's mentioned [in this issue](https://github.com/neovim/neovim/issues/11955) but unaddressed.
My solution was to manually delete [these temporary files](https://neovim.io/doc/user/starting.html#shada)
```shell
rm -f /home/USER/.local/share/nvim/shada/main.shada.tmp.*
```

{{< cal 2021-02-13 >}}

I ended up removing `vim-multiple-cursors` since we can do that with [`:%s/` and the like]({{< ref "vim-cheatsheet#search-and-replace" >}}) anyway.

### Autoformatting
{{< cal 2019-12-23 >}} {{< cal 2020-07-07 >}} {{< cal 2020-12-05 >}} {{< cal 2021-06-28 >}} {{< cal 2022-03-08 >}}

I used the package [vim-autoformat](https://github.com/Chiel92/vim-autoformat) to allow the use of external coding formatters within `nvim`, with the exception of a dedicated plugin for Julia. The formatters I used were

| Language   | Formatter                                                                        |
| ---------- | ---------------------------------------------------------------------------------|
| C++        | [clang-format](http://clang.llvm.org/docs/ClangFormat.html)                      |
| Python     | [black](https://github.com/psf/black)                                            |
| JavaScript | [js-beautify](https://github.com/einars/js-beautify)                             |
| HTML       | [html-beautify](https://github.com/einars/js-beautify)                           |
| CSS        | [css-beautify](https://github.com/einars/js-beautify)                            |
| TypeScript | [typescript-formatter](https://github.com/vvakame/typescript-formatter)          |
| Rust       | [rustfmt](https://github.com/nrc/rustfmt)                                        |
| Markdown   | [remark](https://github.com/remarkjs/remark)                                     |
| JSON       | [js-beautify](https://github.com/einars/js-beautify)                             |
| Shell      | [shfmt](https://github.com/mvdan/sh)                                             |
| Julia      | [JuliaFormatter.jl](https://github.com/domluna/JuliaFormatter.jl) *              |
| LaTeX      | [latexindent.pl](https://github.com/cmhughes/latexindent.pl)                     |

<!-- JSON|[fixjson](https://github.com/rhysd/fixjson) -->

I installed these with

```shell
sudo dnf install clang-tools-extra python3-black rustfmt texlive-latexindent
sudo npm install -g remark-cli js-beautify typescript-formatter
go get -u mvdan.cc/sh/cmd/shfmt
echo "Plug 'Chiel92/vim-autoformat'" >> ~/.config/nvim/local_bundles.vim
```

and, for Julia (* which doesn't use vim-autoformat, but the asynchronous [JuliaFormatter.vim](https://github.com/kdheepak/JuliaFormatter.vim)), after [installing]({{< ref "julia" >}}) `julia` and `julia-devel`,
```shell
echo "Plug 'kdheepak/JuliaFormatter.vim'" >> ~/.config/nvim/local_bundles.vim
nvim> :PlugInstall
cd ~/.config/nvim/plugged/JuliaFormatter.vim
julia --project scripts/packagecompiler.jl
```
This precompiles `JuliaFormatter` using `PackageCompiler` to speed up the plugin initialisation time.

Then `>>~/.config/nvim/local_init.vim`
```vim
let g:JuliaFormatter_use_sysimage=1
let g:JuliaFormatter_always_launch_server=1
autocmd FileType julia :command! Au JuliaFormatterFormat<CR>
```

In `nvim`, `:PlugInstall` then use with `:Autoformat` or `:Au` for short.

## Summary

See the full [install script](https://gitlab.com/eidoom/dotfiles-nvim/blob/master/install.sh), [custom configuration](https://gitlab.com/eidoom/dotfiles-nvim/blob/master/.config/nvim/local_init.vim) and [custom plugins list](https://gitlab.com/eidoom/dotfiles-nvim/blob/master/.config/nvim/local_bundles.vim) in [this repo](https://gitlab.com/eidoom/dotfiles-nvim).

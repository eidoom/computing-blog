+++
title = "Choosing the next book to read"
date = 2023-05-31T16:49:52+02:00
categories = ["data"]
tags = ["mini-project","scrape","bs4","python3","python","json","beautiful-soup","beautiful-soup-4","analyse","data","analytics","table","pipenv"]
description = "Always an exciting prospect"
toc = true
cover = "img/covers/130.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking, concept art, dramatic, verdant, fantasy'-'borders writing signature'"
+++

I wrote a quick script to score speculative fiction novels by recent awards at {{< gl speculative-awards >}}.

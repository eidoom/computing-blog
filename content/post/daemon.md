+++
title = "Autostarting services with systemd"
date = "2019-12-02T16:23:53Z"
edit = 2020-01-21
tags = ["linux","server","systemd","debian","automate","environment","autostart","service","daemon"]
categories = ["environment-core"]
description = "Using systemd to start custom service daemons on system start"
toc = true
cover = "img/covers/99.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

There are sufficiently many services running on my Debian home server now that starting them all manually after I restart the machine has become tedious, as infrequently as it occurs. 
[Systemd](https://wiki.archlinux.org/title/Systemd) [daemons](https://en.wikipedia.org/wiki/Daemon_(computing)) can be used to automate this.

## Case study: beets web

One such service is [`beets web`]({{< ref "nerdifying-music-library-management-with-beets" >}}).
To make it run on system start-up, [simply](https://superuser.com/a/1403699) [create](https://wiki.archlinux.org/title/Systemd#Writing_unit_files) a `.service` file in `/etc/systemd/system/`

```shell
sudo vim /etc/systemd/system/beetsweb.service
```

```vim
[Unit]
Description=Beets web daemon
After=network.target

[Service]
User=ryan
Group=ryan

ExecStart=python3 -m beets web

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

To check that it works, manually start the daemon with

```shell
sudo systemctl start beetsweb
```

If the `.service` file needs changing, the configuration should be reloaded with

```shell
sudo systemctl restart beetsweb
```

Once it's ready to use, activate running on startup with

```shell
sudo systemctl enable beetsweb
```

## Another [example](https://man.archlinux.org/man/systemd.service.5#EXAMPLES)

I manage my ebook library with Calibre. To autostart the Calibre content server (with web interface)

```shell
sudo vim calibre-server.service
```

```vim
[Unit]
Description=Calibre server daemon
After=network.target

[Service]
User=ryan
Group=ryan

ExecStart=calibre-server --enable-local-write --listen-on=0.0.0.0 --port=9000 /home/ryan/gdrive-sync/library

Type=simple
TimeoutStopSec=20
KillMode=process
Restart=on-failure

[Install]
WantedBy=multi-user.target
```

## SELinux

{{< cal "2023-08-24" >}}

While setting up a unit file on Rocky Linux 8, I came across an [SELinux hiccup](https://unix.stackexchange.com/a/573790).
To set the correct security context file label,

```shell
sudo restorecon <unit-file>.service
```

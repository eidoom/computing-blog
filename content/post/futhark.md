+++
title = "Using Futhark to write code for GPUs"
date = 2022-10-31T10:25:49+01:00
categories = ["coding"]
tags = ["nvidia","data-parallel","data","gpu","heterogeneous","heterogeneous-computing","opencl","cuda","futhark","declarative","functional","programming","f36","fedora-36","fedora","linux","desktop","server","laptop"]
description = "High performance parallel execution from declarative code"
cover = "img/covers/118.avif"
coveralt = "stable-diffusion-2:'a lost city post-apocalypse nature reclaiming buildings epic dramatic vivid detailed'+RealESRGAN_x4plus+ImageMagick"
+++

## Futhark

[Futhark](https://futhark-lang.org/) {{< wiki "Futhark (programming language)" >}} {{< ghubn "diku-dk/futhark" >}} is a language that allows you to declaratively write code for data parallel execution on a GPU.

I'm playing with it at {{< gl trying-futhark >}}.
Below I'll detail how to set up a system with an Nvidia GPU to compile and run Futhark code for OpenCL.

### Installation

We can [install](https://futhark.readthedocs.io/en/stable/installation.html#installing-from-a-precompiled-snapshot) from a [precompiled binary](https://futhark-lang.org/releases/)
```shell
cd ~/tar
VERSION=0.22.2
wget https://futhark-lang.org/releases/futhark-$VERSION-linux-x86_64.tar.xz
tar xf futhark-$VERSION-linux-x86_64.tar.xz
cd futhark-$VERSION-linux-x86_64/
PREFIX=$LOCAL/futhark make install
echo "export PATH=\$PATH:$LOCAL/futhark/bin" >> ~/.profile
```

## Nvidia drivers

[As discussed before]({{< ref "linux-video#update-on-drivers" >}}), Fedora 36 comes with the open-source Nvidia driver Nouveau,
```shell
xorg-x11-drv-nouveau
```
but that doesn't support CUDA or OpenCL.

We need to install the proprietary Nvidia driver.
It's packaged by [RPM Fusion]({{< ref "install-fedora#rpmfusion" >}}).
```shell
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install xorg-x11-drv-nvidia-cuda
sudo reboot  # builds kernel module on boot
```

The base driver package is `akmod-nvidia`, which builds the kernel module.

We can use
```shell
nvidia-smi
```
to monitor usage.

I'm trying this on [my laptop, on which I've wrestled with Nvidia drivers before on previous iterations of the OS installation]({{< ref "xps-install#graphics-card" >}}) and [upgrades]({{< ref "fedora-upgrade" >}}).
You can launch any graphical program on the discrete GPU like in this example,
```shell
switcherooctl launch glxgears
```

When Fedora is starting, you may receive the message, 

> NVIDIA kernel module missing. Falling back to nouveau.

This may be because of [Secure Boot](https://rpmfusion.org/Howto/Secure%20Boot).
It can be solved by disabling Secure Boot or following the instructions at `/usr/share/doc/akmods/README.secureboot`.

> If CUDA is not working, just restart.
> You've probably run a kernel update and now `akmod-nvidia` needs the reboot.
> You can always test if CUDA is available by running `nvidia-smi`.

## OpenCL

To target [OpenCL](https://www.khronos.org/opencl/) {{< wiki OpenCL >}} we need to [install](https://futhark.readthedocs.io/en/stable/installation.html#using-opencl-or-cuda)
```shell
sudo dnf install ocl-icd-devel mesa-libOpenCL-devel
```

`clinfo` to check.

## CUDA
To get the [CUDA](https://developer.nvidia.com/cuda-zone) {{< wiki CUDA >}} headers, which are unfortunately not packaged by RPM Fusion, we need to [install the CUDA Toolkit](https://rpmfusion.org/Howto/CUDA#CUDA_Toolkit).
I haven't tried this.

## Building a program

```shell
futhark opencl <name>.fut
./<name>
```

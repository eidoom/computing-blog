+++
title = "Nix package manager"
date = 2021-09-03T14:09:05+01:00
categories = ["environment-core"]
tags = ["nix","package-manager","linux","server","desktop","laptop","install","update","environment","rga"]
description = "Another solution for dependency hell"
toc = true
draft = false
cover = "img/covers/42.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[Nix](https://nixos.org/) {{< ghubn "NixOS/nix" >}} {{< docs "https://nixos.wiki/wiki/Nix" >}} {{< wiki "Nix package manager" >}} is a system package manager which tracks the dependencies of each package independently, making multiple versions of packages available by managing them in separate directories.
## Install Nix
Install the insecure way with
```shell
curl -L https://nixos.org/nix/install | sh
```
## Install a package
For example, to install [`rga`]({{< ref rga >}}), first remove the existing installation
```shell
rm ~/.local/bin/rga*
```
then install from `nixpkgs`, the main Nix package repository, with
```shell
nix-env -iA nixpkgs.ripgrep-all
```
I had hoped using the latest version would fix [this bug that I reported](https://github.com/phiresky/ripgrep-all/issues/115), but it still errors.
## Update packages
Update the `nixpkgs` channel with
```shell
nix-channel --update nixpkgs
```
and update installed packages with
```shell
nix-env -u
```
## Package queries
List installed packages with
```shell
nix-env -q --installed
```
Search for a package with
```shell
nix-env -qaP <package name>
```


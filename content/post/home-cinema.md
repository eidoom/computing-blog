+++
title = "Home cinema"
date = 2020-01-02T18:00:33Z
edit = 2021-06-12
categories = ["video"]
tags = ["windows","windows-10","madvr","svp","mpc-hc","ffdshow","lav-filters","media","video","audio","install","configure","chocolatey"]
description = "My stack for home cinema"
toc = true
cover = "img/covers/90.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[12 Jun 2021 DEPRECATED]({{< ref "linux-video" >}})

## Synopsis

We'll use:

* [Windows 10](https://en.wikipedia.org/wiki/Windows_10) as the operating system <i class="far fa-frown"></i>
* [Chocolatey](https://chocolatey.org/), or `choco`, as the package manager
* [Media Player Classic - Home Cinema](https://mpc-hc.org/), or MPC-HC, as the video player
* [Madshi Video Renderer](http://www.madvr.com/), or MadVR, as the video renderer
* [SmoothVideo Project](https://www.svp-team.com/), or SVP, as the framerate enhancement software
* [K-Lite Codec Pack](https://www.codecguide.com/about_kl.htm) to manage the codecs
* [LAV filters](https://github.com/Nevcairiel/LAVFilters) for decoding audio and video streams
* [ffdshow](https://en.wikipedia.org/wiki/Ffdshow) to perform some tricks
* [Unifed Remote](https://www.unifiedremote.com/) for remote control

## Installation

### Chocolatey

Just because we're on Windows does not mean we have to be truly filthy; there exists a package manager, [`choco`](https://chocolatey.org/).
It is [installed](https://chocolatey.org/docs/install#more-install-options) by running the following command in a `cmd.exe` shell with administrative privileges

```shell
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"
```

### K-Lite Codec Pack

Everything we need, with the exception of SVP, is contained in the [K-Lite Codec Pack](https://www.codecguide.com/about_kl.htm).
It has a `choco` [package](https://chocolatey.org/packages/k-litecodecpackmega) which can be installed with

```shell
choco install k-litecodecpackmega
```

### SVP

I dislike viewing at low frame rates, in particular finding that fast action scenes and panning shots induce motion sickness.
I use the [SmoothVideo Project software SVP4](https://www.svp-team.com/) to increase the refresh rate of the video to that of the output device through [frame interpolation](https://en.wikipedia.org/wiki/Motion_interpolation).
I watch on an [Optoma HD141x projector](https://www.projectorcentral.com/Optoma-HD141X.htm), which has a refresh rate of 60 Hz.
Source material is almost always in 24 fps, in which case SVP increases the framerate by a factor of 5 / 2.

This is not free software on Windows, although a free version exists for Linux.
You can get it [here](https://www.svp-team.com/get/).

SVP can also manage install and updating of MadVR, MPC-HC, LAV filters and ffdshow filters.
However, K-Lite Codec Pack is better at keeping up to date, so is a better choice for this.

## Configuration

I'll cover the setup of the tools then show how they all plug into MPC-HC at the end.
The codec configuration dialogues are most easily accessed through MPC-HC.

### MadVR

For full disclosure, MadVR is total overkill.
It is avaricious with system resources, uses electricity with abandon, and, in particular, highly possessive of GPUs.
It does some neat [post-processing](https://en.wikipedia.org/wiki/Video_post-processing), but I'm sure I wouldn't notice if you switched it all off.
I only use it because I've got the hardware lying around anyway---I'm not fond of multitasking, so don't game while watching videos.

Everything is left to the default settings except the following.

{{< figure src="../../img/video/madvr-artifact_removal.webp" alt="madVR settings > processing > artifact removal" caption="Artifact removal dialog" position="center" >}}
I switched on some options for [compression artifact](https://en.wikipedia.org/wiki/Compression_artifact) removal.

{{< figure src="../../img/video/madvr-image_enhancements.webp" alt="madVR settings > processing > image enhancements" caption="Image enhancement dialog" position="center" >}}
I enabled some sharpening and post-processing artifact removal.

{{< figure src="../../img/video/madvr-chroma_upscaling.webp" alt="madVR settings > processing > chroma upscaling" caption="Chroma upscaling dialog" position="center" >}}
Videos are encoded with their colour (or chroma) information at a reduced resolution.
This is called subsampling and is further discussed [here]({{< ref "exporting-photographs-for-web-with-gimp" >}}).
MadVR upscales the chroma channels back to full resolution.
The `super-xbr` algorithm does this with high sharpness and low artifacting, if the dialog plot is to be believed, but without the crippling performance demands of its competitor in quality, `NGU`.

{{< figure src="../../img/video/madvr-image_upscaling.webp" alt="madVR settings > processing > image upscaling" caption="Image upscaling dialog" position="center" >}}
Why have I included image upscaling when I watch 1080p source material on a 1080p projector?
I also occasionally watch things on my desktop monitor, which has resolution 2560x1600.

{{< figure src="../../img/video/madvr-dithering.webp" alt="madVR settings > processing > dithering" caption="Dithering dialog" position="center" >}}
[Error diffusion](https://en.wikipedia.org/wiki/Error_diffusion) is supposed to be the best [dithering](https://en.wikipedia.org/wiki/Dither) technique.

### SVP

SVP is set to use my graphics card for hardware acceleration and configured as
{{< figure src="../../img/video/svp.webp" alt="SVP configuration" caption="SVP control panel" position="center">}}

The downside of frame interpolation is that it can produce artifacting.
This is most pronounced around fast-moving foreground objects in front of a complex-textured background.
Enabling `Medium` level `Artifacts masking` reduces this, but it is still visible in extreme cases.

### Audio codecs
I use an old receiver/amplifier, the [Sony STR-DB940](https://www.sony.co.uk/electronics/support/audio-components-receivers-amplifiers/str-db940).
It's hooked up to some loudspeakers in a [5.1 channel surround sound](https://en.wikipedia.org/wiki/5.1_surround_sound) configuration.
I connect it to my computer digitally via an [optical audio cable, or TOSLINK](https://en.wikipedia.org/wiki/TOSLINK).

> [This cable](https://www.ebay.co.uk/itm/20m-LONG-TOSlink-Optical-Digital-Cable-Audio-Lead-PREMIUM-CHROME-RANGE/372380254045?ssPageName=STRK%3AMEBIDX%3AIT&_trksid=p2057872.m2749.l2649) is 20 metres long since the computer and receiver are in different rooms.
> I had decided not to use analogue signals at that distance for signal degredation reasons --- not to mention that would mean _three_ long stereo cables.
> The obvious solution is to use a single HDMI cable for audio and video, passing it through the receiver before the projector.
> However, my receiver is too aged to support HDMI, so optical audio cable it is.
> I had worried that 20 metres would be too long for an optical audio cable due to attenuation of the light signal, but it works flawlessly.
> It is connected directly to the optical audio output of the motherboard of my computer, which is a [Gigabyte Z170X-UD3](https://www.gigabyte.com/Motherboard/GA-Z170X-UD3-rev-10#ov).
 
The receiver can decode [Dolby Digital (AC3)](https://en.wikipedia.org/wiki/Dolby_Digital) and [DTS](https://en.wikipedia.org/wiki/DTS_(sound_system)) multichannel audio formats.
If the media source (usually [Blu-rays](https://en.wikipedia.org/wiki/Blu-ray)) is in one of these formats, I pass on the stream directly to the receiver, called bitstreaming or pass-through.
This is controlled by `LAV audio decoder`, configured as
{{< figure src="../../img/video/lav-audio.webp" alt="ffdshow audio decoder configuration > Output" caption="LAV audio decoder options dialog" position="center ">}}
Modern media sources often use newer multichannel audio formats such as Dolby Digital Plus (E-AC3), Dolby TrueHD, and DTS-HD.
Since the receiver doesn't support the decoding of these newer formats, I decode unsupported streams on my computer and re-encode in a custom output format supported by the receiver (AC3).
`LAV audio decoder` does the decoding then passes stream to `ffdshow audio processor` to encode the output, which is configured as
{{< figure src="../../img/video/ffdshow-audio.webp" alt="ffdshow audio processor configuration > Output" caption="ffdshow audio processor output configuration dialog" position="center ">}}

### LAV video decoder

The video decoder is set up to use hardware acceleration on my graphics card as
{{< figure src="../../img/video/lav-video-decoder.webp" alt="LAV Video Decoder > Video Settings" caption="LAV video decoder options dialog" position="center ">}}

### MPC-HC

I use MPC-HC, or more accurately its [fork by clsid2](https://github.com/clsid2/mpc-hc), as my video player.

It's configured to use MadVR as the video renderer
{{< figure src="../../img/video/mpc-hc-output.webp" alt="MPC-HC > Options > Playback > Output" caption="MPC-HC playback options" position="center ">}}
The `External Filters` connect in some of the above configurations:

* `ffdshow raw video filter` [for SVP](https://www.svp-team.com/wiki/SVP:MPC-HC)
* `ffdshow audio processor` for audio output encoding

They are set up as 
{{< figure src="../../img/video/mpc-hc-external-filters.webp" alt="MPC-HC > Options > External Filters" caption="MPC-HC external filters options" position="center ">}}
with both set to `Prefer`.

Everything else is left on default.

## Remote control

I use [Unifed Remote](https://www.unifiedremote.com/) to provide a remote interface to the system.
I installed the server on my desktop and use the [app](https://play.google.com/store/apps/details?id=com.Relmtech.RemotePaid) (paid) on my Android phone to remote control video playback.

## Future projects

Ideally I'd like to move everything in my "video stack" over to [libre software](https://www.gnu.org/philosophy/philosophy.html).

> {{< cal "12 Jun 2021" >}}
>
> [And I did!]({{< ref "linux-video" >}})

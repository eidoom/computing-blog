+++
title = "Numeric representations"
date = 2020-07-10T16:12:35+01:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
cover = "img/covers/117.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

https://en.wikipedia.org/wiki/Floating-point_arithmetic
https://en.wikipedia.org/wiki/Single-precision_floating-point_format
https://en.wikipedia.org/wiki/Decimal32_floating-point_format
https://docs.python.org/3.8/tutorial/floatingpoint.html
https://en.cppreference.com/w/cpp/language/types

QD https://www.davidhbailey.com/dhbsoftware/

compare to https://en.m.wikipedia.org/wiki/Unum_(number_format)#Unum_III
https://gitlab.com/eidoom/posits
`p32` 10x faster than `f32` for summing 0.1 test.

Fixed-point

Rationals

Integers

Finite fields

+++
title = "Element messenger"
date = 2021-01-13
categories = ["software"]
tags = ["fedora","linux","desktop","laptop","install","communication","video-call","environment","flatpak","element","matrix","slack","voip","mobile","windows","chocolatey","apple","google","play-store","f-droid","app-store","flathub","browser","web-app","element-x"]
description = "It's time to break down those [garden walls](https://en.wikipedia.org/wiki/Closed_platform)"
toc = true
cover = "img/covers/63.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

The [messenger app](https://en.wikipedia.org/wiki/Messaging_apps) choice that ticks all my boxes is [Element](https://element.io/) {{< ext "https://matrix.org/ecosystem/clients/element/" >}}, except that I have yet to convince everyone to migrate to it.
Element is a polished frontend for [Matrix](https://matrix.org/): a [libre](https://en.wikipedia.org/wiki/Free_software), [federated](https://en.wikipedia.org/wiki/Federation_(information_technology)), and [secure](https://en.wikipedia.org/wiki/End-to-end_encryption) communication network.
It is one of [many options](https://www.matrix.org/clients/) for Matrix clients.

Unfortunately, as of 2021, it's not 100% there yet but it is usable.
I occasionally see bugs or annoyances cropping up. While one-on-one video is fine, if you want to have a group call or screen-share, you have to use the [Jitsu](https://meet.jit.si/) conference widget, which is a bit weird.

They have a [blog](https://element.io/blog/) and [Twitter feed](https://twitter.com/element_hq).

## Desktop
It has a desktop app {{< ghub vector-im element-desktop >}} {{< ext "https://www.matrix.org/docs/projects/client/element" >}}.
However, it's just an Element wrapper around the [web version]({{< ref "#web" >}}), so we might as well just use that.
### Linux
I've it used on Fedora 32--34. It can be installed with
* either `dnf` from the [`copr`](https://fedoraproject.org/wiki/Category:Copr) [taw/element](https://copr.fedorainfracloud.org/coprs/taw/element/)
    ```shell
    sudo dnf copr enable taw/element
    sudo dnf install element
    ```
* or [`flatpak`]({{< ref "software-deploy#flatpak" >}}) from [`flathub`](https://flathub.org/apps/details/im.riot.Riot) (generally updates to the latest release faster)
    ```shell
    flatpak install flathub im.riot.Riot
    ```
    Note that Element was [formerly called Riot](https://element.io/blog/welcome-to-element/).

> There are alternative Matrix client apps for Linux desktop, for example [Fractal](https://wiki.gnome.org/Apps/Fractal) {{< glsh "https://gitlab.gnome.org/GNOME/fractal" >}} {{< ext "https://www.matrix.org/docs/projects/client/fractal" >}} which can be installed with
> ```shell
> flatpak install flathub org.gnome.Fractal
> ```

### Windows
On Windows 10, we can use [Chocolatey](https://chocolatey.org/packages/element-desktop)
```shell
choco install element-desktop
```

## Mobile
### Android
On mobile, there is an Android app {{< ghub vector-im element-android >}} that can be downloaded from
* [F-Droid](https://f-droid.org/en/packages/im.vector.app/) <i class="fas fa-thumbs-up"></i>
* or [Google Play Store](https://play.google.com/store/apps/details?id=im.vector.app) <i class="fas fa-thumbs-down"></i>

> {{< cal "2024-10-11" >}}
>
> There's a [v2](https://element.io/blog/element-x-ignition/) app called [Element X](https://element.io/app-for-productivity) {{< ghub element-hq element-x-android >}} {{< ext "https://matrix.org/ecosystem/clients/element-x/" >}}.
> Since the [new tech](https://element.io/blog/we-have-lift-off-element-x-call-and-server-suite-are-ready/) (notably sliding sync) is now enabled and included in Synapse, it seems Element X is ready for production.
>
> * [F-Droid](https://f-droid.org/packages/io.element.android.x/) (lags behind on updates)
> * [Play Store](https://play.google.com/store/apps/details?id=io.element.android.x&hl=en)
>
> Note that with [microG]({{< ref fp3 >}}), device registration and cloud messaging must be enabled for Element X notifications <i class="fas fa-frown"></i>.

### iOS
There's an iOS app {{< ghub vector-im element-ios >}} on the [Apple App Store](https://apps.apple.com/us/app/element-messenger/id1083446067).

> And Element X {{< ghub element-hq element-x-ios >}} on the [App Store](https://apps.apple.com/us/app/element-x-secure-chat-call/id1631335820).

## Web

There is also a web app {{< ghub vector-im element-web >}} {{< ext "https://www.matrix.org/docs/projects/client/element" >}} for use [in the browser](https://app.element.io/).

> An alternative client for the web is the lighter [Hydrogen](https://hydrogen.element.io) {{< ghub vector-im hydrogen-web >}} {{< ext "https://www.matrix.org/docs/projects/client/hydrogen" >}}.

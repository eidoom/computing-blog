+++
title = "HHD"
date = 2020-01-19T13:32:27Z
categories = ["maintenance"]
tags = ["hard-disks","parted","fstab","smart","ext4","gpt"]
description = ""
draft = true
toc = true
+++

# HHD

## SMART

```shell
sudo apt install smartmontools
```

https://wiki.archlinux.org/index.php/S.M.A.R.T.
https://www.howtoforge.com/checking-hard-disk-sanity-with-smartmontools-debian-ubuntu#-using-smartd

```shell
sudo vi /etc/default/smartmontools
```
```vim
start_smartd=yes
smartd_opts="--interval=86400"
```

```shell
sudo vi /etc/smartd.conf
```
```vim
DEVICESCAN -m root -M exec /usr/share/smartmontools/smartd-runner
```

see with `mail`
reverted changes, untested

## fstab - adding new data drive

https://wiki.archlinux.org/index.php/Fstab
https://wiki.archlinux.org/index.php/Parted
```shell
sudo parted /dev/sde mklabel gpt
sudo parted /dev/sde mkpart primary ext4 0% 100%
sudo mkfs.ext4 /dev/sde1
```

https://btrfs.wiki.kernel.org/index.php/Status
https://wiki.archlinux.org/index.php/ZFS
https://wiki.archlinux.org/index.php/Btrfs
https://wiki.archlinux.org/index.php/File_systems

```shell
sudo mkdir /media/<mountpoint>
lsblk -f
sudo vi /etc/fstab
UUID=<UUSD>       /media/<mountpoint>              ext4    defaults                0       2
```

```shell
sudo mount -a
```

```shell
cd /media/<mountpoint>
mkdir data
sudo chown <user>:<user> data
cd data
```

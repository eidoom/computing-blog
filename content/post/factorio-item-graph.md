+++
title = "Factorio item graph"
date = 2020-07-08T17:11:43+01:00
edit = 2021-04-05
categories = ["web"]
tags = ["factorio","mini-project","javascript","npm","yarn","mermaid","static-website","graph","gitlab-pages","continuous-deployment","front-end","chart","plot","markup","gitlab","gitlab-ci"]
description = "Building a flowchart of the Factorio production tree"
toc = true
cover = "img/covers/75.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

[Factorio](https://www.factorio.com/) {{< wiki "Factorio" >}} has got to be my favourite game ranked by mechanics.
I love the sprawling item production dependency tree and thought it would be fun to see it as a graph.
I decided to make it on a webpage generated from data in a [human-readable markup format](https://en.wikipedia.org/wiki/Lightweight_markup_language).
Using [Yarn](https://en.wikipedia.org/wiki/Npm_(software)#Alternatives) ([deja vu]({{< ref "font-awesome-icons" >}})) as the package manager, I found the JavaScript package [`mermaid`](https://www.npmjs.com/package/mermaid) {{< ghub "mermaid-js" "mermaid" >}}, which does exactly this, and used it to create a [flowchart](https://mermaid-js.github.io/mermaid/#/flowchart) {{< src "https://gitlab.com/eidoom/factorio-item-graph/-/blob/master/public/index.js" >}}.
I borrowed the images from the [wiki](https://wiki.factorio.com/Main_Page).

While it's sadly not shown by my Git history, putting the graph source together in `mermaid`'s syntax involved a lot of regex replacements using Linux command line tools.
Then a whole bunch more when I decided to change something.
And repeat...

The webpage is hosted on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/).
To set that up, I simply [moved](https://gitlab.com/eidoom/factorio-item-graph/-/commit/ca52a4ff1b20af52c06c8294db72a9ad19bc0aa3) the webpage files into a folder `public` and [wrote](https://about.gitlab.com/blog/2016/04/07/gitlab-pages-setup/#add-gitlab-ci) a [`.gitlab-ci.yml`](https://gitlab.com/eidoom/factorio-item-graph/-/blob/master/.gitlab-ci.yml) file.
Adding the `yml` really is the only thing you need to do to get a page at `https://<username>.gitlab.io/<project name>`.

The full source is [here](https://gitlab.com/eidoom/factorio-item-graph) and the item graph is live [here](https://eidoom.gitlab.io/factorio-item-graph/).
~~The result is a pretty terrible representation of information, but~~ it perfectly captures the emotion of the subject.

> {{< cal "2024-03-29" >}}
>
> Mermaid's new experimental [renderer](https://mermaid.js.org/syntax/flowchart.html#renderer) `elk` does a much better job of laying out the nodes for readability.
> It's still pretty crazy though.

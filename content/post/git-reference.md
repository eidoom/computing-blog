+++
title = "Git cheatsheet"
date = 2019-12-22T10:22:18Z
edit = 2021-06-14
categories = ["cheatsheet"]
tags = ["version-control","linux","git","server","desktop","laptop","shell","terminal","environment"]
description = "A reference for Git commands I use frequently"
toc = true
cover = "img/covers/95.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Aliases

I use zsh configured by [prezto](https://github.com/sorin-ionescu/prezto) with the [git module](https://github.com/sorin-ionescu/prezto/tree/master/modules/git) enabled, which gives some useful [aliases](https://github.com/sorin-ionescu/prezto/blob/master/modules/git/alias.zsh), and I added [some of my own](https://gitlab.com/eidoom/dotfiles-zsh/-/blob/master/.zshrc).
The aliases I use are
```vim
alias g='git'
alias gb='git branch'
alias gbc='git checkout -b'
alias gcam='git commit --all --message'
alias gcm='git commit --message'
alias gg='git grep'
alias gia='git add'
alias gl='git log --topo-order --pretty=format:"%C(bold)Commit:%C(reset) %C(green)%H%C(red)%d%n%C(bold)Author:%C(reset) %C(cyan)%an <%ae>%n%C(bold)Date:%C(reset)   %C(blue)%ai (%ar)%C(reset)%n%+B"'
alias glg='git log --topo-order --all --graph --pretty=format:"%C(green)%h%C(reset) %s%C(red)%d%C(reset)%n"'
alias gd='git ls-files'
alias gfc='git clone'
alias gp='git push'
alias gws='git status --ignore-submodules="none" --short'
alias gwS='git status --ignore-submodules="none"'
alias gco='git checkout'
alias gpl='git pull'
```

## Add
### Add FILE to index

{{< docs "https://git-scm.com/docs/git-add" >}}
Alias `gia FILE` or 

```shell
git add FILE
```
to stage file for commit, _i.e._ changes to file will be included in next commit.

### Unstage FILE

{{< docs "https://git-scm.com/docs/git-reset" >}}
To undo a `git add FILE` without changing the file itself,
```shell
git reset FILE
```

### Unstage all

Undo all `git add`s with changing any files
```shell
git reset
```

## Branch

### List branches

{{< docs "https://git-scm.com/docs/git-branch" >}}
Alias `gb` or

```shell
git branch
```

### Make new branch and move to it

{{< docs "https://git-scm.com/docs/git-checkout" >}}
Alias `gbc BRANCH` or

```shell
git checkout -b BRANCH
```

### Copy FILE over from BRANCH

```shell
git checkout BRANCH -- FILE
```

### Checkout BRANCH from REMOTE

```shell
git checkout -t REMOTE/BRANCH
```
Note: doesn't work on shallow repos.

### Merge branch FEATURE into branch MASTER

```shell
git checkout MASTER
git merge FEATURE
```

### Merge branch NEW into OLD with all conflicts resolved in favour of NEW

To effectively [overwrite OLD with NEW](https://stackoverflow.com/a/3364506),
```shell
git checkout OLD
git merge -X theirs REMOTE/NEW
```
As usual, REMOTE is optional if the branches are on the same REMOTE.

If files were deleted in NEW, they will have to be manually resolved with
```shell
git rm DELETED_FILES
```

## Cherry-pick

{{< docs "https://git-scm.com/docs/git-cherry-pick" >}}
Apply a commit from one branch to another with
```shell
git cherry-pick <commit hash>
```

## Commit
{{< docs "https://git-scm.com/docs/git-commit" >}}

### Commit files in index

Alias `gcm "MESSAGE"` or

```shell
git commit -m "MESSAGE"
```

### Commit all tracked files

Alias `gcam "MESSAGE"` or

```shell
git commit -am "MESSAGE"
```

### Undo last local commit without changing files

```shell
git reset HEAD~1 --soft
```

## Clone
{{< docs "https://git-scm.com/docs/git-clone" >}}

### Shallow clone

```shell
git clone --depth 1 URL
```

to download only the most recent revision

### Clone with submodules

```shell
git clone --recurse-submodules -j8 URL
```
where the `-j8` flag means up to 8 submodules are downloaded simultaneously.

### Clone specific BRANCH

```shell
git clone -b BRANCH URL
```

### Unshallow a repo

```shell
git fetch --unshallow --verbose
git config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git fetch origin --verbose
```

## Date

### Get creation date of FILE

```shell
git log --diff-filter=A --follow --format=%aD -- FILE | tail -1
```
returns the dates that the file was first added to the `git` repository.
Note that this doesn't work on shallow repositories.

### Get last modification date of FILE

```shell
git log -1 --pretty="format:%ci" -- FILE
```
gives the date of the last commit with that file.

### Get date of initial commit

```shell
git log --reverse --pretty=format:%as | head -1
```

## Delete

### Delete FILE in Git but keep locally

{{< docs "https://git-scm.com/docs/git-rm" >}}
```shell
git rm --cached FILE
```

or similar alias `gix` which is also recursive.

### Delete all untracked directories and files

{{< docs "https://git-scm.com/docs/git-clean" >}}
First do a dry run with `-n`, [then](https://linuxize.com/post/how-to-remove-untracked-files-in-git/)
```shell
git clean -d -f
```

## Diff
{{< docs "https://git-scm.com/docs/git-diff" >}}

### Diff changed FILE against indexed/committed version

```shell
git diff -- FILE
```

Note that the `--` stops the string `FILE` matching the name of a branch.

### Diff against FILE version on BRANCH (on REMOTE)

```shell
git diff (REMOTE/)BRANCH -- FILE
```

### Diff current branch against BRANCH and show files only

```shell
git diff BRANCH --name-status
```

## Fork

To fork a project, we can pull the upstream repo and name this remote appropriately
```shell
git clone <url of upstream>
cd <directory>
git remote rename origin upstream
```
then make a remote repository for the fork and point the `origin` [remote]({{< ref "#remote" >}}) of the local repo to it
```shell
git remote add origin <url of fork>
```
and push the content to the fork
```shell
git push -u origin <main branch>
```
Later, we can pull changes from upstream with
```shell
git pull upstream <main branch>
```

## LFS

See [this post]({{< ref "git-lfs" >}}) for information about large files.

## List
{{< docs "https://git-scm.com/docs/git-ls-files" >}}

### List files

Alias `gd` or 

```shell
git ls-files
```

## Log

{{< docs "https://git-scm.com/docs/git-log" >}}

### Show graph of commits on branches

I use alias `glg` to see a graphical log with hashes and commit messages, or full command

```shell
git log --topo-order --all --graph --pretty=format:"%C(green)%h%C(reset) %s%C(red)%d%C(reset)%n"
```

## Push
{{< docs "https://git-scm.com/docs/git-push" >}}

### Push to remote

Alias `gp` or

```shell
git push
```

## Rename
{{< docs "https://git-scm.com/docs/git-mv" >}}

### Rename file

```shell
git mv OLD_NAME NEW_NAME
```

## Remote
{{< docs "https://git-scm.com/docs/git-remote" >}}

### Add remote

We can have multiple remote repos on a songle local repo.
To add a new remote,

```shell
git remote add <remote name> <remote repo url>
git remote update
```
If the new remote is on the local machine, `<remote repo url>` can also be the file system path to it.

### Change remote

This is useful, for example, when changing a remote from HTTPS to SSH protocol.
For example, for remote `origin`,

```shell
git remote set-url origin URL
```

## Revert

Note that `git checkout` is aliased to `gco`.

### Revert FILE

#### With uncommitted changes

```shell
git checkout -- FILE
```

or 
{{< docs "https://git-scm.com/docs/git-restore" >}}

```shell
git restore -- FILE
```

#### To previous commit

Find HASH of desired commit using 

```shell
git log -- FILE
```

and revert by

```shell
git checkout HASH -- FILE
```

#### To version on BRANCH (on REMOTE)

```shell
git checkout (REMOTE/)BRANCH -- FILE
```

### Revert repository 

The following `reset`s can be forced pushed to the remote repository with
```shell
git push --force
```
However, this is dangerous as it changes history so should not be used without due caution!

#### To last commit

If repository contains uncommitted edits, use
```shell
git reset --hard
```

#### Undo last commit

To undo last commit,
```shell
git reset --hard HEAD~1
```

#### Back to HASH commit

```shell
git reset --hard HASH
```

### Revert only HASH commit

To create a new commit reverting changes from a previous commit,
```shell
git revert HASH
```
where `HASH` is the hash of the commit to revert.
This is safe as history is unchanged.

## Search
{{< docs "https://git-scm.com/docs/git-grep" >}}

### Search files

Alias `gg SEARCH` or

```shell
git grep SEARCH
```
Use flag `-i` for a case-insensitive search.

## Status
{{< docs "https://git-scm.com/docs/git-status" >}}

### Show status

Alias `gwS` or similar

```shell
git status
```

### Show concise status

Alias `gws` or similar

```shell
git status --short
```

## Submodule
{{< docs "https://git-scm.com/docs/git-submodule" >}}

### Initialise/update all submodules

Alias `gSI` (serial download) or, for parallel download,

```shell
git submodule update --recursive --init --jobs=8
```

### Integrate changes from upstream subprojects

```shell
git submodule update --recursive --remote --init --jobs=8
```

### Add non-empty git repo with remote URL as a new submodule

This also works if URL has an existing local repo in the directory of the superproject.
```shell
git submodule add URL <optional path>
```

### Delete submodule at PATH

```shell
git submodule deinit -- PATH
vi .gitmodules  # and remove submodule entry
git rm -f PATH
```

### Push changes on submodule with detached head

`git submodule update --remote` detaches the head of submodule repos.
To get back on BRANCH,

```shell
git checkout BRANCH
```

If I have changed FILES and I want to keep the changes

```shell
git add FILES
git commit -m "MESSAGE"
git checkout BRANCH  # HASH of detached commit will be shown
git pull
git merge HASH
git push
```

+++
title = "Pre-commit with pre-commit"
date = 2021-06-20T10:20:28+01:00
edit = 2021-06-28
categories = ["coding"]
tags = ["continuous-integration","pre-commit","git-hooks","git","vcs","version-control","python","yaml","install","configure","autoformat","lint","podman","docker"]
description = "Git hooks for effective continuous integration"
toc = true
draft = false
cover = "img/covers/48.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

My PhD work often involves working on small software projects collaboratively.
I appreciate the methodology of {{< wl "continuous integration" >}} (CI), that is, merging changes sufficiently frequently so as to avoid [integration hell](https://wiki.c2.com/?IntegrationHell).
Using a {{< wl "version control system" >}} like Git is pretty much a given as part of this workflow, but that's not where the fun ends.

{{< wl Linters "lint (software)" >}} provide cheap and quick {{< wl "static program anaysis" >}} that can pick up on bugs (more semicolons, anyone?).
They should be used before committing---in fact, this should be enforced...

A codebase should use correct and consistent styling on every line.
This is not just aesthetic: it's for readability.
Moreover commits from different authors must have consistent styling since there's nothing less useful than merges full of whitespace changes hiding that one breaking line edit.
It should have been two commits (each git commit should only do one thing) but I'd rather avoid the situation in the first place.

## Git hooks

Git offers a neat way to help us here with [Git hooks](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) {{< docs "https://git-scm.com/docs/githooks" >}} {{< ext "https://githooks.com/" >}}.
These are custom scripts that run at set points of the Git workflow, both client-side and server-side.
We can use the `pre-commit` hook to run linting and autoformatting before a commit is allowed.
If after entering
```shell
git commit -m "add bad code"
```
---which triggers the hook---the lint fails, then the commit fails.
If it passes, the autoformatter does its thing and the commit succeeds.
Wonderful.

We can set this up by
```shell
cp .git/hooks/pre-commit.sample .git/hooks/pre-commit
vi .git/hooks/pre-commit
```
and editing to our heart's content.
However, this `pre-commit` script is local: it isn't automatically shared with your other machines or colleagues.
Unacceptable.

An easy fix is to change the location of the hook scripts and add them to the Git repo
```shell
mkdir .githooks
mv .git/hooks/pre-commit .githooks/pre-commit
git config core.hooksPath .githooks
echo -e "#!/usr/bin/env bash\ngit config core.hooksPath .githooks" > init.sh
chmod +x init.sh
git add .githooks/pre-commit init.sh
```
Now on other machines, after installing dependencies like linters, we can run `./init.sh` and we're all set up.
It's actually rather a neat solution.

It does seem redundant for everyone to write the same/similar checking scripts for their projects though.
If only someone else had done the hard work already and shared it...

## Pre-commit

Enter [pre-commit](https://pre-commit.com/).
Hurrah for [laziness](https://proxy.c2.com/cgi/wiki?AgainstBoredomEvenTheGodsStruggleInVain)!
"But we're already using `pre-commit`," you cry.
Well yes, but I'm introducing the brilliantly christened program pre-commit now, not `pre-commit` the hook.

Pre-commit manages your `pre-commit` scripts for you.
We simply install it and configure it
```shell
pip install --user pre-commit
pre-commit sample-config > .pre-commit-config.yaml
vi .pre-commit-config.yaml
```
which takes the form of adding appropriate [plugins](https://pre-commit.com/hooks.html): git repos containing prewritten hook scripts.
Then we initialise pre-commit for our local repo with
```shell
pre-commit install
```
and we're done!
Pre-commit will run on the next Git commit.

The plugins can be updated with
```shell
pre-commit autoupdate
```
which edits their `.pre-commit-config.yaml` `rev` entry, and also manually invoked at any time with
```shell
pre-commit run --all-files
```

## An attempt to run anywhere

Unfortunately, there's one last hurdle: environment.
Sigh.
Those lovely linters and fabulous formatters must be installed before you can run them.
We could add some dependency installation to an `init.sh` but managing system compatibility soon gets out of hand: it's easier to just ask people to install these dependencies themselves.

One way to get around this is for the hooks to use OCI containers.
I set up some hooks at {{< gl pre-commit-hooks-cpp >}} for C++ CI like this.
I used GitLab CD (continuous deployment) to [automatically build](https://gitlab.com/eidoom/pre-commit-hooks-cpp/-/blob/main/.gitlab-ci.yml) the images, storing them on the [GitLab Container Registry](https://gitlab.com/eidoom/pre-commit-hooks-cpp/container_registry), and define the hooks in a [`.pre-commit-hooks.yaml`](https://gitlab.com/eidoom/pre-commit-hooks-cpp/-/blob/main/.pre-commit-hooks.yaml).
I use these hooks to run `pre-commit` on {{< gl cpp-ci >}} but ran into some troubles: 
* `clang-check` [needs](https://clang.llvm.org/docs/HowToSetupToolingForLLVM.html) a [compilation database](https://clang.llvm.org/docs/JSONCompilationDatabase.html), which can be made with CMake or [Bear](https://github.com/rizsotto/Bear) if using other build systems, but then we need to do extra work to make it available in the container.
    One could generate the `compile_commands.json` in the native environment and pass it by a call like
    ```shell
    podman run --rm -v $PWD:/src:ro,Z --workdir /src \
        registry.gitlab.com/eidoom/pre-commit-hooks-cpp clang-check main.cpp
    ```
    but then the paths are wrong for execution in the container.
    Alternatively we could generate the database in the container, but then we're not build system agnostic.
    Maybe it's best not to use containers for this one?
* I tried to use the containerised hooks [on the remote](https://gitlab.com/eidoom/cpp-ci/-/blob/main/.gitlab-ci.yml), but it fails.
    Possibly something to do with nested Docker instances?

This was mainly an exercise to learn how to write hooks as there already exist hooks like this, for example {{< gl "daverona" "pre-commit/cpp" >}}.
I put together an example project using some (not containerised) hooks at {{< gl cpp-ci-2 >}}.

Another solution is that pre-commit can handle Python virtual environments including `pip` package dependencies.


## In the wild

I used pre-commit for our project {{< gl JosephPB n3jet_diphoton >}}.
I included an [`init.sh`](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/init.sh) to install pre-commit and initialise it.
I [configured](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/.pre-commit-config.yaml) with four plugins:

* Python formatter [`black`](https://github.com/psf/black) {{< docs "https://black.readthedocs.io/en/stable/integrations/source_version_control.html" >}}
    * Configured with [`pyproject.py`](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/pyproject.toml)
* Python linter [`flake8`](https://github.com/pycqa/flake8) {{< docs "https://flake8.pycqa.org/en/latest/user/using-hooks.html" >}}
    * Configured with [`.flake8`](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/.flake8)
* YAML formatter [`yamlfmt`](https://github.com/mmlb/yamlfmt) ([hook](https://github.com/jumanjihouse/pre-commit-hook-yamlfmt))
* YAML linter [`yamllint`](https://github.com/adrienverge/yamllint) {{< docs "https://yamllint.readthedocs.io/en/stable/integration.html" >}}
    * Configured with [`.yamllint.yaml`](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/.yamllint.yaml)

I set up some [remote CI](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/.gitlab-ci.yml) to check syntax and formatting server-side too.
This is useful to catch commits that weren't checked locally.
It includes using `clang-format`, configured at [`.clang-format`](https://gitlab.com/JosephPB/n3jet_diphoton/-/blob/main/.clang-format), for `C++` formatting.
I packaged `clang-format` in an OCI container at {{< gl clang-format-container >}} and provided on [Docker hub](https://hub.docker.com/r/eidoom/clang-format) for this.
I did this before setting up pre-commit, otherwise I'd have just used pre-commit again here by invoking it in a container.

## The bottom line

At the end of the day, even if the automatic environment handling goes well, we still have to rely on others to remember to run a single command once.
That's actually quite a big ask.
Sadly, there's no getting around that while respecting personal development environments, which we should.
We can always apply some positive feedback by appending a congratulatory note to commit messages on good machines with the `prepare-commit-msg` hook.
This also opens up the possibility to shame those without the postscript, hehe.

+++
title = "Alt math"
date = 2022-09-07T18:58:15+02:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
+++

https://www.sagemath.org/

Mathics
https://mathics.org/
https://mathics-development-guide.readthedocs.io/en/latest/index.html
Python 3.10 (Pyston, PyPy?)
```shell
dnfi python3-devel libsq3-devel python3-setuptools lapack-devel llvm-devel asymptote ghostscript texlive-xetex mariadb-connector-c-devel
python3.10 -m venv ~/venv/mathics
source ~/venvs/mathics/bin/activate 
pip install -U pip
pip install cython
pip install Mathics-omnibus[full]
mathicsserver
```

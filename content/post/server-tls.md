+++
title = "Server tls"
date = 2020-06-27T13:35:11+01:00
categories = [""]
tags = [""]
description = ""
toc = true
draft = true
+++

https://en.wikipedia.org/wiki/Transport_Layer_Security

## Fail
Extend:
```vim
  docker-gen:
    image: jwilder/docker-gen
    container_name: docker-gen
    command: -notify-sighup nginx-proxy -watch /etc/docker-gen/templates/nginx.tmpl /etc/nginx/conf.d/default.conf
    environment:
      TZ: 'Europe/London'
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./docker-gen:/etc/docker-gen/templates:ro
      - ./nginx-proxy/conf.d:/etc/nginx/conf.d
    labels:
      - "com.github.jrcs.letsencrypt_nginx_proxy_companion.docker_gen"
    depends_on:
      - "pihole"
      - "nginx-proxy"
    restart: always
```
Change:
```vim
  nginx-proxy:
    image: linuxserver/letsencrypt
    container_name: nginx-proxy
    cap_add:
      - NET_ADMIN
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=Europe/London
      - URL=ryanserver.lan
      - SUBDOMAINS=pihole,h5ai
      - VALIDATION=http
      - EMAIL=<email>
      - STAGING=true
    volumes:
      - ./letsencrypt-letsencrypt:/config
      - ./letsencrypt-nginx:/etc/nginx/conf.d
    ports:
      - 443:443
      - 80:80
    depends_on:
      - "pihole"
    restart: always
```

## Fail
https://blog.ippon.tech/set-up-a-reverse-proxy-nginx-and-docker-gen-bonus-lets-encrypt/
https://github.com/nginx-proxy/nginx-proxy
```vim
  nginx-proxy:
    container_name: nginx-proxy
    image: jwilder/nginx-proxy:alpine
    ports:
      - "80:80"
      - "443:443"
    environment:
      TZ: 'Europe/London'
      DEFAULT_HOST: pihole.ryanserver.lan
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock:ro
      - ./nginx-proxy/network_internal.conf:/etc/nginx/network_internal.conf:ro
      - ./letsencrypt/vhost.d:/etc/nginx/vhost.d
      - ./letsencrypt/certs:/etc/nginx/certs:ro
      - ./letsencrypt/html:/usr/share/nginx/html
    depends_on:
      - "pihole"
    labels:
      com.github.jrcs.letsencrypt_nginx_proxy_companion.nginx_proxy: "true"
    restart: always
```
https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion
https://hub.docker.com/r/jrcs/letsencrypt-nginx-proxy-companion
https://docs.linuxserver.io/images/docker-letsencrypt
```vim
  letsencrypt:
    container_name: letsencrypt
    image: jrcs/letsencrypt-nginx-proxy-companion
    environment:
      TZ: 'Europe/London'
      DEFAULT_EMAIL: '<email>'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./letsencrypt/vhost.d:/etc/nginx/vhost.d
      - ./letsencrypt/certs:/etc/nginx/certs:rw
      - ./letsencrypt/html:/usr/share/nginx/html
    depends_on:
      - "pihole"
      - "nginx-proxy"
    restart: always
```
```vim
      LETSENCRYPT_HOST: <name>.ryanserver.lan
```

## See
https://github.com/nginx-proxy/docker-letsencrypt-nginx-proxy-companion/blob/master/docs/Docker-Compose.md


+++
title = "Advent of Code 2017 with Lua"
date = 2022-10-22T16:02:07+02:00
categories = ["advent-of-code"]
tags = ["advent-of-code-2017","lua","luajit","coding","programming","jit","interpreted","bytecode","aoc","aoc17"]
description = "Trying out the fastest interpreted language"
toc = true
draft = false
cover = "img/covers/111.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++
I started trying Lua by solving some of Advent of Code 2017 at {{< gl aoc17 >}}.

+++
title = "New ereader: Kobo Clara HD"
date = 2021-08-16T16:00:47+01:00
categories = ["ereader"]
tags = ["eink","epub","calibre","kobo","kobo-clara-hd","epaper","ebook","kindle","kindle-paperwhite"]
description = "Comments and tweaks"
toc = true
draft = false
cover = "img/covers/46.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

As much as I find the resultant proliferation of the letter "e" prefix distasteful[^1], I love using {{< wl epaper "Electronic paper" >}} devices to read ebooks as I read *a lot* and like to do so on the move.

## My past ereaders

My first {{< wl "ereader" "e-reader" >}} {{< ext "https://www.ethicalconsumer.org/technology/shopping-guide/tablets-e-readers" >}} was the Amazon Kindle 4 (2011) {{< wiki "Amazon_Kindle#Kindle_4" >}} {{< ext "https://wiki.mobileread.com/wiki/Amazon_Kindle_4" >}}, which was handed down to me from the family.
I upgraded to the Kindle Paperwhite 3 (2015) {{< wiki "Amazon_Kindle#Kindle_Paperwhite_(third_generation)" >}} {{< ext "https://wiki.mobileread.com/wiki/Kindle_paperwhite_3" >}} for its 300 {{< wl PPI "Pixel density" >}} frontlit touchscreen {{< wiki frontlight >}} {{< ext "https://wiki.mobileread.com/wiki/Frontlight" >}}, returning the 4 (212 PPI, no frontlight or touch) to the family, and later moved to the Paperwhite 4 (2018) {{< wiki "Amazon_Kindle#Kindle_Paperwhite_(fourth_generation)" >}} {{< ext "https://wiki.mobileread.com/wiki/Kindle_paperwhite_4" >}} (waterproof), handing down the Paperwhite 3.
Once again, there's need for another hand-me-down, so the Paperwhite 4 is going off and I'm getting myself a new ereader.
The difference this time is that I'm not getting an Amazon product (hurrah!) and the market is finally such that I won't even need to break the bank to go non-Kindle.

## Kobo Clara HD: initial impressions

Enter the [Kobo Clara HD](https://uk.kobobooks.com/products/kobo-clara-hd) {{< wiki "Kobo_eReader#Kobo_Clara_HD" >}}.
I bought one secondhand.
It's almost identical to the Paperwhite 3. 
It has the same size of screen, 6", but is smaller in the hand thanks to a thinner bezel.
The power button is, again, located on the bottom, but this time it's harder to accidentally press when reading, thank goodness.
The OS (version 4.28.18220 {{< ext "https://wiki.mobileread.com/wiki/Kobo_Firmware_Releases" >}}) is very similar, although with better UX and you don't have to pay to remove adverts.
One new feature is that the colour temperature of the Kobo's frontlight can be adjusted.
This is to reduce the possible but unproven effect of disruption of circadian rhythm by exposure to blue light at night {{< wiki "Biological_effects_of_high-energy_visible_light" >}} {{< ext "https://wiki.mobileread.com/wiki/Blue-light_exposure" >}}.
It also adds [ePub](https://www.w3.org/publishing/epub32/) {{< wiki EPUB >}} {{< ext "https://wiki.mobileread.com/wiki/EPUB" >}} support.
When I close the (unofficial) cover case, it even goes to sleep automatically.
I do miss the easy translate on highlight feature though.

## Identity grumbles

When I first switched it on, I had to sign into an account, which was a poor show.
I'd rather have had the option, and for it to be the default, to not sign into anything in order to use the device.
If you later sign out, it resets the Kobo and you have to sign in again to use it again.
So, we must resort to a [hack](https://www.yingtongli.me/blog/2018/07/30/kobo-rego.html).

## Adding books

Adding books to the device (or {{< wl sideloading >}}, as {{< wl "walled gardens" "Closed platform" >}} like to call it) is a breeze with [`calibre`]({{< ref calibre >}}), which automatically detects everything and treats it appropriately.

It's possible to set up automatic syncing between a Calibre library and the Kobo [through Calibre-Web](https://github.com/janeczku/calibre-web/wiki/Kobo-Integration), which is pretty exciting.
[I use Calibre-Web]({{< ref "server-install-containers#calibre-web" >}}) so I may set that up some time.

## Customising the reading experience

So far I'm enjoying the font {{< wl Georgia "Georgia (typeface)" >}}, which is included as a default font.
I had to reduce the default font size, line spacing, and margins to more sensible values.

Annoying little page numbers were popping up on the right side of the screen but I found that they can be disabled under `Settings > Reading settings > Page appearance > Show Adobe EPUB page numbers`.
 
## Sleep image woes

When I put it to sleep, it shows an fnac logo---undesirable, to say the least.
The good people at [mobileread](https://www.mobileread.com/) set me straight though.
I connected the Kobo to my laptop via USB (shame it's Micro not C), enabled the connection on the Kobo, and on the laptop navigated to [`.kobo/affiliate.conf`](https://wiki.mobileread.com/wiki/Affiliate.conf), which for me is
```shell
vi /run/media/ryan/KOBOeReader/.kobo/affiliate.conf
```
where I set `affiliate=`.
Now it shows the cover of the book I'm reading instead, using the image from the metadata database.

However, it seems that the device syncs with the affiliate every week, and in doing so, this gets reset.
The Mobileread forums would suggest this is determined by the serial number of the hardware device.
The process also burns through about 50% battery!
[A solution](https://www.mobileread.com/forums/showpost.php?p=4059481&postcount=43) is to open [`Kobo ereader.conf`](https://wiki.mobileread.com/wiki/Kobo_Configuration_Options)
```shell
vi /run/media/ryan/KOBOeReader/.kobo/Kobo/Kobo\ ereader.conf
```
and spoof the last sync date to ten years in the future with
```vim
LastAffiliateSyncTime=@Variant(\0\0\0\x10\0%\x94h\x3!>i\x2)
```
where the value is [some kind of serialised Qt date](https://www.mobileread.com/forums/showpost.php?p=4133349&postcount=58).

This doesn't protect us from resets during firmware updates though.
My ultimate solution was also to not connect the Kobo to wifi, thus stopping all automatic updates.
The battery life is pretty great now.

I'd advise others to **not** buy their Kobo through affiliate fnac to avoid this annoyance, although when you're buying second hand that's not so easy.

[^1]: Don't worry, I'll at least make sure there aren't a bunch of redundant hyphens flying around with them.

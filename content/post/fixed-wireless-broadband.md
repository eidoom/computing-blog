+++
title = "Fixed wireless broadband"
date = 2023-10-14T11:18:40+01:00
categories = ["network"]
tags = ["iperf","ping","dsl","adsl","bandwidth","latency","internet","isp","wisp","upload","download","ethernet","wireless","wifi","cell","cellular","mobile","nomadic","broadband","map","4g","lte","aerial","antenna","mimo","ca","radio","rf","sma","coaxial","router","modem","network","wan","lan","cpe","home","telephone","landline","twisted-pair","copper","sim","signal","reception","rssi","rsrp","sinr","rsrq","db","dbm","mhz","mbps","packet","transmission","receiver","mast","transmitter","external","external-antenna","band","radiation","poynting","huawei","ee","data","connection","wire","cable","fibre","fttx","server","remote","cloud","debian","fedora","rural","firewalld","tcp","port","debian-12","bookworm","fedora-38","sender","wap","access","test","benchmark"]
description = "Leaping forward 20 years of internet bandwidth in rural UK"
toc = true
draft = false
cover = "img/covers/mountain-rf.webp"
coveralt = "Signal is always best at the top of the mountains, but we live at the bottom"
+++

My current residence is in a rural area of the UK.
Good for some things, bad for others.

## Landline

Internet access is available by {{< wl DSL >}} on the telephone line.
The local exchange is in a village around four kilometres away as the crow flies, but with wild hills in between.
The connection is copper {{< wl "twisted pair" >}} from there.
There is a {{< wl "fibre-to-the-cabinet" "Fiber_to_the_x" >}} {{< wl box "Serving_area_interface" >}} on the other side of the road, but we never got included in that neighbourhood infrastructure upgrade.

### Bandwidth

Let's see what bit rate the internet connection boasts.
We'll use [`iperf3`](https://software.es.net/iperf/) {{< pl "https://iperf.fr/" >}} {{< pl "https://en.wikipedia.org/wiki/Iperf" >}} {{< pl "https://packages.debian.org/stable/iperf3" >}} {{< pl "https://packages.fedoraproject.org/pkgs/iperf3/iperf3/" >}} {{< pl "https://wiki.archlinux.org/title/Benchmarking#iperf" >}} with my [cloud server]({{< ref "cloud" >}}) at `<remote hostname>` as the remote.

We open port 5201 for TCP traffic and install and run the `iperf3` server on the remote server (Debian 12),
```shell
sudo apt install iperf3
sudo firewall-cmd --zone=public --add-port 5201/tcp
iperf3 -s
```
First, ensure nobody is stealing {{< wl bandwidth "Bandwidth_(computing)" >}}!
Then, on a local machine, we install and run the `iperf3` client to get the upload statistics, choosing a transmission time of one minute,
```shell
sudo dnf install iperf3
iperf3 -c <remote hostname> -t 60
```
```shell
Connecting to host <remote hostname>, port 5201                                                                      
[  5] local <local ip> port <local port> connected to <remote ip> port 5201                                               
[ ID] Interval           Transfer     Bitrate         Retr  Cwnd                                                      
...
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-60.00  sec  10.1 MBytes  1.41 Mbits/sec  311             sender
[  5]   0.00-62.94  sec  7.00 MBytes   933 Kbits/sec                  receiver

iperf Done.
```
**Upload** bandwidth result: **1.41 [Mb]({{< ref "jargon#denary" >}})/s** (1.34 [Mib]({{< ref "jargon#binary" >}})/s).

Similarly in reverse mode for download bandwidth,
```shell
iperf3 -c <remote hostname> -t 60 -R
```
```shell
Connecting to host <remote hostname>, port 5201                                                                      
Reverse mode, remote host <remote hostname> is sending                                                               
[  5] local <local ip> port <local port> connected to <remote ip> port 5201                                               
[ ID] Interval           Transfer     Bitrate
...
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bitrate         Retr
[  5]   0.00-60.07  sec  25.4 MBytes  3.55 Mbits/sec   42             sender
[  5]   0.00-60.00  sec  25.2 MBytes  3.52 Mbits/sec                  receiver

iperf Done.
```
**Download** bandwidth result: **3.52 Mb/s** (3.36 Mib/s).

To clean up, we shut down the `iperf3` server and close the port,
```shell
sudo firewall-cmd --zone=public --remove-port 5201/tcp
```

### Ping

What about {{< wl latency "Latency_(engineering)" >}}?
After checking that the lines are quiet,

```shell
ping -c 10 <remote hostname>
```
```shell
PING <remote hostname> (<remote ip>) 56(84) bytes of data.
64 bytes from <remote address> (<remote ip>): icmp_seq=1 ttl=48 time=32.9 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=2 ttl=48 time=33.7 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=3 ttl=48 time=33.0 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=4 ttl=48 time=33.6 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=5 ttl=48 time=33.2 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=6 ttl=48 time=33.2 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=7 ttl=48 time=33.5 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=8 ttl=48 time=32.7 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=9 ttl=48 time=33.4 ms
64 bytes from <remote address> (<remote ip>): icmp_seq=10 ttl=48 time=33.2 ms

--- <remote hostname> ping statistics ---
10 packets transmitted, 10 received, 0% packet loss, time 9012ms
rtt min/avg/max/mdev = 32.749/33.243/33.686/0.289 ms
```

Result: **33 ms**.

### USO

Hold on, this bandwidth violates the [Universal Service Obligation for broadband](https://www.ofcom.org.uk/phones-telecoms-and-internet/advice-for-consumers/broadband-uso-need-to-know).
On phoning, I was recommended to use {{< wl "fixed wireless broadband" "Fixed_wireless#Fixed_wireless_broadband" >}}.

## Wireless

Now onto {{< wl "mobile networks" "Cellular_network" >}}.
I ordered some budget second-hand equipment on eBay.

### Transmitter

To evaluate available masts,

* [CellMapper](https://www.cellmapper.net/) shows the location of and information about masts and cells. You can select different providers. Extremely useful!
* Check for {{< wl "line of sight" "Line-of-sight_propagation" >}} with an elevation map tool, for example,
    * [Solwise](https://www.solwise.co.uk/wireless-elevationtool.html) (Google satellite view).
    * [RRUK](https://radioreferenceuk.co.uk/losmap.php) (OpenStreetMaps, can set antenna heights).
* [OpenRouteService](https://maps.openrouteservice.org) is useful for measure distances and bearings.
* Test signal with a mobile phone or cellular router with internal antennas, along with any SIM cards available.

The closest mast is occulted by an intervening hill sadly, but we do have line of sight with another mast about 8km away.
It has {{< wl LTE "LTE_(telecommunication)" >}}/{{< wl 4G >}} cells supporting {{< wl bands "LTE_frequency_bands" >}}:

* band 3 (B3, {{< wl FDD "Duplex_(telecommunications)#Frequency-division_duplexing" >}}, 1800 MHz) with bandwidth 20 MHz.
* band 20 (B20, FDD, 800 MHz) with bandwidth 5 MHz.

Tests indicate an external antenna is a must.

### Receiver

I got a [Poynting XPOL-2](https://poynting.tech/antennas/xpol-2/) (A-XPOL-0002-V2) {{< wl antenna "Antenna_(radio)" >}}.
It has two cross-polarised elements, supporting 2x2 <abbr title="Multiple-Input and Multiple-Output">MIMO</abbr>[^mimo].
It is a {{< wl "directional antenna" >}}.
The {{< wl gain "Gain_(antenna)" >}} is advertised as ~7 dBi at 800 MHz and ~6.5 dBi at 1800 MHz.
The {{< wl "radiation pattern" >}} at 800 MHz shows +- ~10 degrees before gain starts dropping, and at 1800 MHz a generous +- ~30 degrees.
It has 5 metres of twin {{< wl "coaxial cable" >}} attached terminating in {{< wl "SMA connectors" "SMA_connector" >}}.
It seems to [perform]({{< ref "#signal" >}}) fine.

Other (more expensive) options included {{< wl LPDA "Log-periodic antenna" >}} or {{< wl parabolic "Parabolic_antenna" >}} antennas.

### Modem

I got a Huawei B525s-23a LTE {{< wl CPE "Customer-premises_equipment" >}} router.
I didn't actually want the Wi-Fi but I couldn't find any appropriate only-modems for sale.
It has four gigabit ethernet ports, an RJ11 socket for the telephone, and two SMA sockets for external antennas.
It [is](https://kenstechtips.com/index.php/huawei-broadband-routers#Huawei_B525) [LTE Category 6](https://www.cablefree.net/wirelesstechnology/4glte/lte-ue-category-class-definitions/).
It supports B3+B3 intra-band contiguous {{< wl "carrier aggregation" >}} (CA)[^ca], which seems a good match for my cell.
I'm quite happy with it overall, although the web interface can be a bit slow to respond.

> {{< cal 2023-11-27 >}}
> 
> The Huawei LTE routers seem to have an undocumented API.
> Thankfully, {{< ghn "Salamek/huawei-lte-api" >}} exists.
> I put together a little dashboard at {{< gl "huawei-lte-dashboard" >}}.

### Mounting

We fixed the backbone (aluminium pole) of a scrapped {{< wl "collinear antenna array" >}} to the side of the house and mounted the antenna on it.

{{< figure src="../../img/aerial.avif" alt="antenna" caption="Mounted antenna" >}}

The 5m coaxial cable goes through a hole in the wall to the router, which sits in the loft and is connected to the LAN (including distributed wireless access points) via ethernet, as well as to some cordless telephones.

I have to be careful now because the WiFi connection can be the bottleneck in internet access depending on where I'm standing, something that's never happened before here!
Don't worry, everything important has an ethernet connection.

### Contract

We switched from an EE landline broadband contract to an EE SIM only contract with unlimited data and speed, such that the termination fee for the former was waivered, since EE operate from our in-sight mast.

### Performance

Over the weekend, I kept an eye on the new connection.

#### Signal

We are at the edge of the cell so signal strength is not ideal, but it does seem quite clean.
Readings vary but an instantaneous [measurement](https://www.signalbooster.com/blogs/news/acronyms-rsrp-rssi-rsrq-sinr) on the router gives:

<abbr title="Received Signal Strength Indication">RSSI</abbr> {{< wiki "Received signal strength indicator" >}} (<abbr title="decibel-milliwatts">dBm</abbr> {{< wiki "DBm" >}})|<abbr title="Reference Signal Received Power">RSRP</abbr> {{< wiki "Reference_Signal_Received_Power" >}} (dBm) |<abbr title="Signal-to-Interference-plus-Noise Ratio">SINR</abbr> {{< wiki "Signal-to-interference-plus-noise_ratio" >}} (<abbr title="decibel">dB</abbr> {{< wiki Decibel >}})|<abbr title="Reference Signal Received Quality">RSRQ</abbr> (dB)
---|---|---|---
<span style="color: orange">-67</span>|<span style="color: orange">-96</span>|<span style="color: green">20</span>|<span style="color: green">-9</span>

The router settles into using B3, sadly without CA, when I allow only B3 and B20 in the settings.
Otherwise, it appears to attempt CA on bands I'm not receiving.

#### Bandwidth

However, the connection speed does not seem to be limited by this.
Bandwidth varies a lot, mostly likely due to the mast traffic.
It it generally around **30 Mb/s download** in busy hours, roughly a ten-fold improvement over the landline!
It doesn't drop below around 20 Mb/s and peaks even in excess of 100 Mb/s in quiet hours.

Upload bandwidth is more symmetric than with the landline too.

#### Latency

I was worried about increased latency, but while the ping has roughly doubled and it's *much* more variable, it's not too bad:

```shell
--- <remote hostname> ping statistics ---
60 packets transmitted, 60 received, 0% packet loss, time 59068ms
rtt min/avg/max/mdev = 43.034/59.049/135.133/18.272 ms
```

The other worry, packet loss, doesn't seem to be a problem either.

#### Summary

Overall, a success!

[^mimo]: 2x2 <abbr title="Multiple-Input and Multiple-Output">MIMO</abbr> {{< wiki MIMO >}}: two simultaneous channels (datalink on carrier wave) on both uplink and downlink, differentiated by two orthogonally (linearly) polarised antennas.

[^ca]: CA: two simultaneous channels differentiated by their frequencies.

+++
title = "Upgrading Fedora"
date = 2020-04-06T10:51:51+01:00
edit = 2021-05-10
categories = ["maintenance","operating-system"]
tags = ["environment","fedora","laptop","desktop","server","snap","hugo","python","nvidia","drivers","update","upgrade"]
description = "Upgrading Fedora to a new release version"
toc = true
cover = "img/covers/82.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Fedora 31: plain sailing

{{< pl "https://fedoramagazine.org/announcing-fedora-31/" >}}

Yesterday, I [upgraded the version of Fedora](https://docs.fedoraproject.org/en-US/quick-docs/dnf-system-upgrade/) on my secondary desktop from 29 to 31.
I have also updated my ThinkPad laptop from 30 to 31.
Both experiences were painless; I simply did
```shell
sudo dnf upgrade --refresh
sudo reboot
sudo dnf install dnf-plugin-system-upgrade
sudo dnf system-upgrade download --refresh --releasever=31
sudo dnf system-upgrade reboot
```

## Fedora 32: rougher waters
{{< cal 2020-05-03 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-32/" >}}

On the upgrade from 31 to 32, my laptops upgraded without incident using the same commands as above, but with the appropriate release version
```shell
sudo dnf upgrade --refresh
sudo reboot
sudo dnf install dnf-plugin-system-upgrade
sudo dnf system-upgrade download --refresh --releasever=32
sudo dnf system-upgrade reboot
```
However, my desktop ran into a missing-dependency problem
```shell
Error: 
 Problem: package python2-beautifulsoup4-4.9.0-1.fc31.noarch requires python2-lxml, but none of the providers can be installed
   - python2-lxml-4.4.0-1.fc31.x86_64 does not belong to a distupgrade repository
   -   - problem with installed package python2-beautifulsoup4-4.9.0-1.fc31.noarch
```
This is because [Python 2 has been removed from Fedora in 32](https://fedoraproject.org/wiki/Releases/32/ChangeSet#Retire_Python_2) since it's [EOL](https://www.python.org/doc/sunset-python-2/).
I fixed the conflict by [allowing the updater to remove problematic packages](https://ask.fedoraproject.org/t/dnf-upgrade-from-31-to-32-fails/6703/2).
This is achieved by adding the `--allowerasing` switch to the `system-upgrade` command
```shell
sudo dnf system-upgrade download --refresh --releasever=32 --allowerasing
```
I also had to manually install the [Nvidia driver]({{< ref "nvidia-drivers-on-fedora" >}}) DMKS module after the update.
I got terminal access via the [by-now-familiar method]({{< ref "broken" >}}), found the driver version with
```shell
dnf info nvidia-driver | grep Version
```
and ran
```shell
sudo dkms install nvidia/<version>
```
then reboot.

## Fedora 33
{{< cal 2021-01-13 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-33/" >}}

On the laptops, this update was painless.
The only thing I had to change was to [switch]({{< ref "install-fedora#update" >}}) to Hugo on [Snap]({{< ref "software-deploy#snap" >}}) because [the Copr repo I was previously using]({{< ref "install-fedora#hugo" >}}) wasn't updated for 33.
```shell
sudo dnf config-manager --set-disabled copr:copr.fedorainfracloud.org:daftaupe:hugo
sudo dnf remove hugo
sudo snap install hugo
```
On the desktop, I had to manually installed the Nvidia driver [again]({{< ref "#fedora-32-rougher-waters" >}}).

## Fedora 34
{{< cal 2021-05-10 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-34/" >}}

I got the error
```shell
Error:
 Problem: rdma-core-34.0-1.fc33.i686 has inferior architecture
  - rdma-core-34.0-1.fc33.x86_64 does not belong to a distupgrade repository
  - problem with installed package rdma-core-34.0-1.fc33.i686
(try to add '--skip-broken' to skip uninstallable packages)
```
so I uninstalled that package with its dependants
```shell
sudo dnf remove rdma-core.i686
```
Next, I received an insufficient disk space error [so]({{< ref "utilities#disk-usage-diet" >}}) consulted [`baobab`](https://wiki.gnome.org/Apps/DiskUsageAnalyzer) and ran
```shell
sudo flakpak remove --unused
sudo dnf autoremove
```
Then the upgrade proceeded.

I had to migrate to [Tray Icons: Reloaded](https://extensions.gnome.org/extension/2890/tray-icons-reloaded/) for tray icon support in GNOME 40.

## Fedora 35
{{< cal 2021-11-05 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-35/" >}}

This time,
```shell
Error: 
 Problem: package tomahawk-libs-0.8.4-23.fc31.x86_64 requires libquazip.so.1()(64bit), but none of the providers can be installed
  - quazip-0.7.6-10.fc34.x86_64 does not belong to a distupgrade repository
  - problem with installed package tomahawk-libs-0.8.4-23.fc31.x86_64
```
so
```shell
sudo dnf remove tomahawk-libs
```
then proceed as usual.

## Fedora 36
{{< cal 2022-06-06 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-36/" >}}

No problems.

## Fedora 37
{{< cal 2022-11-23 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-37/" >}}

Python is upgraded from 3.10 to 3.11.
I uninstalled (including `rm -r ~/.config/nvim/plugged`) then reinstalled [Neovim and its plugins]({{< ref "nvim2" >}}) and [associated `pip` libraries](https://gitlab.com/eidoom/dotfiles-nvim-2#formatters) to migrate everything to Python 3.11.

On [sys]({{< ref "linux-video#update-on-drivers" >}})t[ems]({{< ref "futhark#nvidia-drivers" >}}) where I use RPM Fusion Nvidia drivers, I had to uninstall and reinstall `akmod-nvidia` to force it to recompile the kernel module.

## Fedora 38
{{< cal 2023-04-24 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-38/" >}}

Again, the `akmod-nvidia` problem, grr.
I forced re-compilation with

```shell
akmodsbuild /usr/src/akmods/nvidia-kmod.latest
sudo dnf remove kmod-nvidia-<kernel version> # uninstall
sudo dnf install kmod-nvidia-<kernel version> # reinstall from custom build in current directory
sudo reboot
```

on one system, but it still didn't work on another with the latest kernel `6.2.11-300.fc38.x86_64` so I just used the previous kernel `6.2.11-200.fc38.x86_64` until the next kernel update ({{< cal "2023-04-26" >}}: `6.2.12-300.fc38.x64_64`).

## Fedora 39

{{< cal 2023-11-09 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-linux-39/" >}}

Only had to manually re-install the Nvidia driver as [above]({{< ref "#fedora-38" >}}).

Be aware Python is upgraded to 3.12 in this release.

One annoyance is that `wl-clipboard` {{< ghubn "bugaevc/wl-clipboard" >}} version (2.2.0) {{< pl "https://packages.fedoraproject.org/pkgs/wl-clipboard/wl-clipboard/" >}} has a bug that intermittently interrupts yanking in [`(n)vim`]({{< ref "nvim2" >}}) when using `set clipboard=unnamedplus`.
In an attempt to fix this, I tried version 2.2.1, which I installed from `rawhide`,
```shell
sudo dnf install fedora-repos-rawhide
sudo dnf upgrade --enablerepo=rawhide wl-clipboard
```

## Fedora 40

{{< cal 2024-04-30 >}}

<br>

{{< pl "https://fedoramagazine.org/announcing-fedora-linux-40/" >}}

Somehow broke the Solarised colour scheme {{< ghub iCyMind NeoSolarized >}} for syntax highlighting in [Neovim]({{< ref nvim2 >}}).
Potential fix would be to use one of the modern schemes like {{< ghn "maxmx03/solarized.nvim" >}} which support [TreeSitter](https://tree-sitter.github.io/tree-sitter/) {{< ghubn "tree-sitter/tree-sitter" >}} (through {{< ghn "nvim-treesitter/nvim-treesitter" >}}).
Feels like it's maybe time to start a fresh nvim configuration with this in mind.
Also presents an opportunity to use Lua for [`init.lua`](https://neovim.io/doc/user/lua-guide.html#lua-guide-config) instead of VimScript (`init.vim`) while I'm at it.

> {{< cal "2024-08-12" >}}
>
> Done: {{< gl dotfiles-nvim-lua >}}.

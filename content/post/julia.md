+++
title = "Advent of Code 2020 with Julia"
date = 2020-12-05T09:22:58Z
edit = 2021-04-05
categories = ["advent-of-code"]
tags = ["advent-of-code-2020","julia","aoc","aoc20"]
description = "Something new for AoC20"
toc = true
cover = "img/covers/68.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I decided to try out [Julia](https://julialang.org/) {{< wiki "Julia_%28programming_language%29" >}} for [Advent of Code](../../tags/advent-of-code) this year.
The repo's at {{< gl aoc20 >}} (and {{< gh aoc20-mirror >}} using [mirroring](https://docs.gitlab.com/ee/user/project/repository/repository_mirroring.html#setting-up-a-push-mirror-from-gitlab-to-github)), with a solution for every day and some notes in the readme.

I installed the latest version of Julia on Fedora 32/33 with
```shell
sudo dnf copr enable nalimilan/julia
sudo dnf install julia julia-devel
```
and [set up my nvim for it]({{< ref "neovim#autoformatting" >}}).

Got 50 stars!

+++
title = "Animals by forebrain neurons"
date = 2023-05-31T14:17:57+02:00
categories = ["data"]
tags = ["mini-project","python","matplotlib","bs4","scrape","wikipedia","beautiful-soup","beautiful-soup-4","json","python3","html","table","plot","graph","chart","bar-chart"]
description = "Plotting the ladder of sentience?"
toc = true
cover = "img/covers/127.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking concept art fantasy'-'borders'"
+++

I found it quite interesting to graph the number of forebrain neurons (as a proxy for intelligence) measured in animals using the scripts at {{< gl forebrain-neurons >}}.

+++
title = "Anti-hour"
date = 2023-02-16T11:10:16+01:00
categories = ["domotics"]
tags = ["api","electricity","electric","utilities","home","smarthome","home-automation","monitoring","data","analytics","analysis","js","svelte","sveltekit","css","html","charts.css","static-site","static-website","web","website","webpage","mvp.css","vite","javascript","nginx","json","node","nodejs","npm","python","make","pipenv","pip","matplotlib","numpy","requests","python3","python3.11","datetime","histogram","2d-histogram","correlation","plot","graph","chart","matrix","matrix-nio","pdm"]
description = "Monitoring home energy consumption"
toc = true
cover = "img/covers/126.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking concept art fantasy'-'borders'"
+++

I'm very impressed with how {{< wl "Octopus Energy" >}} offers an Agile tariff that is determined by market prices on a half-hourly basis (set a day ahead).
It incentivises customers to spread their usage to non-peak slots, promoting a more efficient use of the national energy infrastructure.

I'm also impressed by how they make available all the pricing and personal usage data through an [API](https://developer.octopus.energy/docs/api/).
I made:
* {{< gl anti-hour >}} ([non-permanent live link](https://anti-hour.eidoom.duckdns.org/)) to show upcoming prices.
* {{< gl electric >}} to do some analysis of past electricity usage.
* {{< gl matrix-bot-dynamic-energy-prices >}} is a [Matrix]({{< ref "/tags/matrix" >}}) bot gives you updates on upcoming prices.

*Anti-hour* refers to the auspicious but evanescent slots where the cost is actually negative.

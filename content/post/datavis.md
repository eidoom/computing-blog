+++
title = "Interactive data visualisation"
date = 2021-04-04T21:08:03+01:00
edit = 2021-04-05
categories = ["web","data"]
tags = ["javascript","d3.js","css","html","data","visualisation","benchmark","performance","maths","haskell","c","cpp","c++","rust","go","java","nim","pi","fortran","python","julia","compiler","declarative","imperative","gcc","clang","llvm","rollup","gitlab-ci","gitlab-pages","gitlab","bash","shell","linux","cargo","pip","pypy","cpython","toml","json","bit-shifting","functional","d","lua","luajit","dlang","gdc","ldc","clang++","g++","procedural","js","datavis","project"]
description = "Trying out d3.js"
toc = true
draft = false
cover = "img/covers/25.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I gave a talk at my code club recently about making [interactive data visualisations](https://eidoom.gitlab.io/datavis/).
I diverged slightly also into how I generated data for this.

## Generating some data

### Language benchmarks

After [doing Advent of Code 2020 in new language Julia]({{< ref "julia" >}}), I was interested in the relative number-crunching performance of languages.
I wanted to know:
* was there much difference between languages with optimising compilers?
* how did JIT compare to AOT?
* how much worse were interpreted languages really?

Such benchmarks are of course not representative of the real-world, but I thought it was a fun project to pursue anyway.

### π à la Chudnovsky

I decided to benchmark an algorithm that generated the digits of π at {{< gl pi >}}.
I read up on it and got carried away with the {{< wl "Chudnovsky algorithm" >}} ([here](https://gitlab.com/eidoom/pi/-/tree/master/chudnovsky)), excited because it had been used to set previous and the current world record on the number of digits of π.
It was really a terrible candidate for what I wanted though because it requires large number support, so benchmarking it is really testing the library providing these types, not the language.

### Leibniz

A better choice is the simple {{< wl "Leibniz formula for π" >}} ([here](https://gitlab.com/eidoom/pi/-/tree/master/leibniz)).
It's a bad algorithm for π, converging very slowly, so a good benchmark.
It also doesn't need anything but normal `float`s.
I used {{< wl "64-bit floats" "Double-precision floating-point format" >}} (`double`s).

### Languages

Without any strong guiding principle, I wrote implementations for the Leibniz algorithm in:

* [C](http://www.open-std.org/jtc1/sc22/wg14/) ([GCC](https://gcc.gnu.org/), [CLang](https://clang.llvm.org/))
* [C++]({{< ref "cpp" >}}) (g++, clang++)
* [Fortran](https://fortran-lang.org/) (gfortran)
* [Go](https://golang.org/)
* [Haskell](https://www.haskell.org/) ([GHC](https://www.haskell.org/ghc/))
* [Java](https://java.com/en/) ([OpenJDK](https://openjdk.java.net/))
* [JavaScript](https://www.ecma-international.org/technical-committees/tc39/) ([Node](https://nodejs.org/en/))
* [Julia](https://julialang.org/)
* [Nim](https://nim-lang.org/)
* [Python](https://www.python.org/) (CPython, [Pypy](https://www.pypy.org/))
* [Rust](https://www.rust-lang.org/)
* [Lua](https://www.lua.org/) (Lua, [LuaJIT](https://luajit.org/luajit.html))
* [D](https://dlang.org/) ([GDC](https://wiki.dlang.org/GDC), [LDC](https://wiki.dlang.org/LDC))

Multiparadigm languages had [imperative]({{< ref "jargon#imperative" >}}) and [declarative]({{< ref "jargon#declarative" >}}) implementations.
Compiled languages with more than one major compiler implementation were tested with those as listed above.
The implementations were timed for a number of different series sizes.

It's a very simple algorithm, but demonstrates plenty of basic language features:

* (inline) ternary conditional statement
* reading command line argument
* formatting output/strings
* type casting
* for loop/map/reduce
* functions
* error handling
* arithmetic
* using standard libraries
* variable declaration/initialisation/definition {{< sto "https://stackoverflow.com/q/23345554" >}}
* printing to the console
* types

### Struture

I wanted to have all benchmarking information (except the source code files themselves) in one place.
I opted to write a [TOML](https://toml.io/en/) file [here](https://gitlab.com/eidoom/pi/-/blob/master/leibniz/bench.toml) for this, using nested tables to reduce redundant information entry.
I found [`toml-cli`](https://lib.rs/crates/toml-cli) (installed with `cargo`) useful to read the data from the command line, formatting the output with [`jq`](https://stedolan.github.io/jq/) like `toml get bench.toml "languages[0]" | jq`, although introducing the nested layout meant I had to use numeric indices to select nested options, which is not very useful.

I wrote a little Python script [here](https://gitlab.com/eidoom/pi/-/blob/master/leibniz/bench.py) to use this configuration to compile the appropriate programs (by generating a `Makefile` and running it) and do all the benchmarking.
It used [`toml`](https://pypi.org/project/toml/) installed with `pip` to read the configuration.
I found [`subprocess`](https://docs.python.org/3/library/subprocess.html) a useful way to call shell commands from Python.
Timing was done with [`time.perf_counter()`](https://docs.python.org/3/library/time.html?highlight=perf_counter#time.perf_counter).
The results with metadata were stored in a JSON using [`json`](https://docs.python.org/3/library/json.html).

### Bit shifting

Most of the time is spent in the loop doing arithmetic.
Each term in the series (ie. each iteration of the loop) comes with a plus sign if it's an even iteration or an odd sign for odd iterations.
In maths, we'd say `pow(-1, i)` (using `C` notation), but this is not a particularly efficient way to sign the term by parity on a computer.
A better version is `i % 2 ? -1 : 1`.

I thought that since this conditional statement introduced branching to the loop, using an alternative without branching would speed things up.
I did some bit twiddling and came up with the optimised `1 - ((n & 1) << 1)`.
I was surprised to find it didn't make a noticable difference to any of the contestants.
I guess if any optimisation is going on under the hood, that's one thing that gets done for you.

## Visualisation

The source for my visualisation is at {{< gl datavis >}} and the web page is live [here](https://eidoom.gitlab.io/datavis/).

### Getting started

I learned how to use [d3.js](https://d3js.org/) to put this together.
It's a low-level plotting library and has a much steeper learning curve than the likes of `matplotlib` on Python.
Their official up-to-date (v6) tutorials are annoyingly on some site called Observable, which involves far too much Observable-specific code.
I therefore immediately gave up with that, but found that most other guides were out of date.
I found [this gallery](https://www.d3-graph-gallery.com/) of examples with nice accompanying discussion of data visualisation, so learnt from that.
Some of it is dated, so I also referred to the [d3.js API documentation](https://github.com/d3/d3/blob/master/API.md) as my other primary resource.

I used [Rollup](https://rollupjs.org/) to bundle the static web app.

### d3.js impressions

Once you get your head around it, the library is very nice to work with.
It uses a powerful declarative style to map data to visuals.
The core of this is demonstrated by the [`join` operation](https://github.com/d3/d3-selection/blob/v2.0.0/README.md#joining-data).
A selection of elements (which could be empty) is associated with some data, then they are `join`ed by

* `enter`: data which is not yet associated with an element has a new element created for it
* `update`: data which was in the previous data set and also in the new data set is kept
* `exit`: elements associated with data that was in the previous data set but not the current are discarded

Operations like setting element attributes (just using `CSS`) or animation can be applied on any of these branches, and on the resultant set of surviving elements.
It's a great way to code up a dynamic graph.
See it in action in [this source](https://gitlab.com/eidoom/datavis/-/blob/master/src/line_plot.js#L5).

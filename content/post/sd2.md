+++
title = "Stable(r) Diffusion v2"
date = 2022-11-24T16:00:31+01:00
categories = ["machine-learning"]
tags = ["ai-art","art","stable-diffusion","gpu","conda","server","python","miniconda","git","image","picture","pytorch","numpy","cuda","nvidia","ml","ai","real-esrgan","upscaling","machine-learning","artificial-intelligence","diffusion-model","desktop","linux","torch","data","git","github","huggingface","venv","virtual-environment"]
description = "Looking again at text-to-image and image upscaling"
toc = true
cover = "img/covers/119.avif"
coveralt = "stable-diffusion-2:'a lost city hidden in the mountains epic colorful dynamic dramatic detailed'+RealESRGAN_x4plus+ImageMagick"
+++

I tried the [second version](https://stability.ai/blog/stable-diffusion-v2-release) of [Stable Diffusion]({{< ref "stable-diffusion" >}}).

## Installation

Clone the [Stable Diffusion v2 project repository](https://github.com/Stability-AI/stablediffusion)
```shell
cd <project root>
git clone https://github.com/Stability-AI/stablediffusion.git
```

[With `conda` available]({{< ref "stable-diffusion#installation" >}}),
* install from scratch (recommended)
    ```shell
    cd <project root>/stablediffusion
    conda env create -f environment.yaml
    ```
* [update existing `ldm` environment](https://github.com/Stability-AI/stablediffusion#requirements)
    ```shell
    cd <project root>/stablediffusion
    conda activate ldm
    conda install pytorch==1.12.1 torchvision==0.13.1 -c pytorch
    python3 -m pip install transformers==4.19.2 diffusers invisible-watermark open_clip_torch
    python3 -m pip install -e .
    ```

Clone the models for Stable Diffusion v2 text-to-image [`stable-diffusion-2`](https://huggingface.co/stabilityai/stable-diffusion-2) and x4 image upscaling [`stable-diffusion-x4-upscaler`](https://huggingface.co/stabilityai/stable-diffusion-x4-upscaler)
```shell
cd <project root>
git clone https://huggingface.co/stabilityai/stable-diffusion-2
git clone https://huggingface.co/stabilityai/stable-diffusion-x4-upscaler
```

For upscaling, we'll also need
```shell
conda activate ldm
python3 -m pip install gradio
```

One can also install [`xformers`](https://github.com/facebookresearch/xformers) for improved performance.
The installation didn't immediately work for me and I was too lazy to pursue it though.
<!--
```shell
git clone --recurse-submodules https://github.com/facebookresearch/xformers.git
```
-->

I had to move the cache for [`pip`](https://pip.pypa.io/en/stable/topics/caching/) and [`huggingface`](https://huggingface.co/docs/datasets/cache#cache-directory) onto a scratch drive because they got very large
```vim
export PIP_CACHE_DIR=/scratch/cache/pip
export HF_DATASETS_CACHE=/scratch/cache/huggingface
```

## Usage

### Text-to-image

[Generate images with](https://github.com/Stability-AI/stablediffusion#text-to-image)
```shell
cd <project root>/stablediffusion
conda activate ldm
CUDA_VISIBLE_DEVICES=0 \
    python3 scripts/txt2img.py \
    --ckpt ../stable-diffusion-2/768-v-ema.ckpt 
    --config configs/stable-diffusion/v2-inference-v.yaml \
    --outdir ../output/cuda0/ \
    --n_iter 1 \
    --n_samples 1 \
    --steps 50 \
    --H 768 \
    --W 768 \
    --prompt "<prompt>"
```

Some comments:
* I avoid overwrites between CUDA devices by setting a different output folder
* 100 steps seems good

Memory usage for a single sample:
Width|Height|VRAM (GB)
---|---|---
512|512|10
1408|704|30

We can preview images by setting up an [SSH tunnel](https://wiki.archlinux.org/title/OpenSSH#Forwarding_other_ports) and running a simple web server with a directory listing
```shell
local $ ssh -L 8000:localhost:8000 <remote host address>
remote $ cd <project root>/output
remote $ python3 -m http.server
```
then opening <http://localhost:8000> in the local browser.

### Image upscaling

Setup SSH tunnel
```shell
ssh -L 7860:localhost:7860 <remote host address>
```

[Upscale images with](https://github.com/Stability-AI/stablediffusion#image-upscaling-with-stable-diffusion)
```shell
cd <project root>/stablediffusion
conda activate ldm
python3 scripts/gradio/superresolution.py \
    configs/stable-diffusion/x4-upscaling.yaml \
    ../stable-diffusion-x4-upscaler/x4-upscaler-ema.ckpt
```
and access at <http://localhost:7860/>.

Be aware that it requires a lot of VRAM.
It even OOM crashed a 32GB GPU.
Perhaps it's a bug?
Or because I didn't install `xformers`?

## Example

{{< figure src="/img/ai/test.avif" caption="Prompt: `scifi concept art`" >}}

I [upscaled with Real-ESRGAN]({{< ref "stable-diffusion#upscaling" >}}) because of the memory problems with the Stable Diffusion upscaler.

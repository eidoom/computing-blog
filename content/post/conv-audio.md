+++
title = "Converting between audio formats"
date = 2021-02-24T14:53:44Z
edit = 2021-02-13
categories = ["audio"]
tags = ["audio","ffmpeg","flac","parallel","alac","m4a","ogg","opus","vorbis","codec","compression","lossless","lossy","format","encode","software","sound","music"]
description = "Using `ffmpeg` to work with audio file formats"
toc = true
draft = false
cover = "img/covers/59.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Lossless conversion
For example, to convert from [ALAC](https://macosforge.github.io/alac/) {{< wiki Apple_Lossless >}} to [FLAC](https://xiph.org/flac/) {{< wiki FLAC >}},
```shell
ffmpeg -i <input file>.m4a <output file>.flac
```
Any options for the input and output files come before the file names, as stated in `ffmpeg --help`
```shell
ffmpeg [options] [[infile options] -i infile]... {[outfile options] outfile}...
```
Generic audio options are detailed [here](https://ffmpeg.org/ffmpeg.html#Audio-Options) and codec-specific options [here](https://ffmpeg.org/ffmpeg-codecs.html).
The output audio codec is automatically determined by the suffix of the output file name or explicitly set with, for example, `-acodec flac`.
[For FLAC](https://ffmpeg.org/ffmpeg-codecs.html#flac-2), we could increase the default compression level of `5` with`-compression_level 12`.

## Parallelise

Bringing this together with [GNU parallel](https://www.gnu.org/software/parallel/) {{< wiki GNU_parallel >}} to parallelise the operation over all `m4a` files in a directory, we get
```shell
ls *.m4a | parallel ffmpeg -acodec alac -i {} -acodec flac -compression_level 12 {.}.flac
```

## Lossy conversion
The most advanced open format for lossy audio is [Opus](https://opus-codec.org/) {{< wiki "Opus_%28audio_format%29" >}}, although [Vorbis](https://xiph.org/vorbis/) {{< wiki Vorbis >}} can be superior for compatibility.

To convert from FLAC to Opus, we can call
```shell
ffmpeg -i <input file>.m4a -acodec libopus <output file>.flac
```
This uses the [libopus encoder](https://ffmpeg.org/ffmpeg-codecs.html#libopus-1) rather than the [ffmpeg native Opus encoder](https://ffmpeg.org/ffmpeg-codecs.html#opus) because `libopus` is better.
The default output options are `-vbr on -compression_level 10 -application audio`, which gives the best audio quality for music.

The option `-application voip` is optimised for speech.
In that case, we might also want to set the number of channels with `-ac 1` and the bitrate with `-b 16k`, for example.
(I used to use [this](https://github.com/eidoom/linux-on-windows-scripts/blob/master/compress-recordings) to reencode my audio recording of lectures during my Master's.)

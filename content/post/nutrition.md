+++
title = "Nutritional information"
date = 2023-05-31T14:19:23+02:00
categories = ["data"]
tags = ["mini-project","python","python3","sql","sqlite","make","matplotlib","data","analysis","analytics","visualisation","datavis","database","sqlite3","plot","chart","graph","radar-chart","spider-chart","bar-chart","parallel-coordinates","numpy","venv","virtual-environment","mpltern","ternary-chart","markdown","pandoc","json","pipenv","openfoodfacts","api","fooddata-central","db","barcode","ean-13","ean-8","1d-barcode","linear-barcode","python-imageio","zbar","pyzbar"]
description = "Having a look at the nutritional content of foods"
toc = true
cover = "img/covers/128.webp"
coveralt = "A1111:+'epic landscape, natural, rugged, breathtaking concept art dramatic verdant'-'borders'"
+++

I explored the [USDoA FoodData Central](https://fdc.nal.usda.gov/) database, importing data into an SQLite database and plotting various quantities, at {{< gl nutrition >}}.
Later, I also looked into scanning food item barcodes with a webcam and querying a public database API, [OpenFoodFacts](https://world.openfoodfacts.org/), for associated data.

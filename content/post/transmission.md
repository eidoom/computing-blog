+++
title = "Transmission configuration"
date = 2020-01-19T13:44:32Z
edit = 2020-02-10
categories = ["home"]
tags = ["linux","bittorrent","server","automate","transmission-bittorrent","lan","network","debian","umask","daemon","apt","systemd","install","configure","debian-buster","debian-10","home-server","port-forwarding"]
description = "Configuring Transmission on my home server"
toc = true
cover = "img/covers/86.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I run [Transmission](https://transmissionbt.com/) {{< pl "https://github.com/transmission/transmission" >}} {{< pl "https://en.wikipedia.org/wiki/Transmission_(BitTorrent_client)" >}} {{< pl "https://wiki.archlinux.org/index.php/Transmission" >}} {{< pl "https://packages.debian.org/stable/transmission" >}} {{< pl "https://wiki.debian.org/Transmission" >}} on my Debian home server and access it through a web interface.

## Daemon

I installed the [Transmission daemon](https://packages.debian.org/stable/transmission-daemon) with

```shell
sudo apt install transmission-daemon
```

This enables a [daemon]({{< ref "daemon" >}}) under user `debian-transmission` configured at `/lib/systemd/system/transmission-daemon.service`.

### Permissions

The file permissions can be handled by the choice of owner.

#### Use own user

The service can be customised by running

```shell
sudo systemctl edit transmission-daemon.service
```

I added here

```vim
[Serivce]
User=<username>
UMask=002
```

to run the daemon under my personal user account `<username>` to simplify file permissions.
I also set the umask so that files are readable and executable by everyone.
These settings are stored at `/etc/systemd/system/transmission-daemon.service.d/override.conf`.

#### Separate user

Alternatively, we can use the default from the Debian package: a separate user called `debian-transmission`.
We still set the service `UMask` as above but not the `User`.
We can make a folder `<downloads>`
```shell
mkdir <downloads>
```
that belongs to `debian-transmission`
```shell
sudo chown debian-transmission <downloads>
sudo chgrp debian-transmission <downloads>
```
but make it accessible (`rwx`) to `<username>` by adding them to the `debian-transmission` group
```shell
sudo usermod -a -G debian-transmission <username>
```
and giving write permission to the group
```shell
sudo chmod g+w <downloads>
```
Note we have to log out and back in to the shell to effect the user permissions changes.

## Transmission

Before changing the [configuration](https://wiki.debian.org/Transmission#Configuration), we need to stop the daemon otherwise changes will be overwritten.

```shell
sudo systemctl stop transmission-daemon
```

Open up the settings with either

* for own user
    ```shell
    vim ~/.config/transmission-daemon/settings.json
    ```
* for separate user
    ```shell
    sudo vim /etc/transmission-daemon/settings.json
    ```

and

* I set the web UI username and password,
    ```json
    "rpc-password": "<plain text password>",
    ...
    "rpc-username": "<username>",
    ```
    where the password is automatically replaced by its hash on reload
* I set the path for completed downloads
    ```json
    "download-dir": "<path to directory for completed downloads>",
    ```
* I set the queue size
    ```json
    "download-queue-size": 10,
    ```
* I set a folder for incomplete downloads
    ```json
    "incomplete-dir": "<path to directory for incomplete downloads>",
    "incomplete-dir-enabled": true,
    ```
* I enabled access to the web interface over lan on the `001` subnet (not required if using a [proxy]({{< ref "address-transmission-web-client-using-hostname#proxy" >}}))
    ```json
    "rpc-whitelist": "127.0.0.1,192.168.1.*",
    ```
* I [set permissions](https://askubuntu.com/a/157471) of downloaded files such that [means users who are not the owner or in the group of the file have no write permissions](https://wiki.archlinux.org/index.php/Umask#Meaning_of_the_mode_mask) (don't forget to set this in the [service file]({{< ref "#permissions" >}}) too)
    ```json
    "umask": 2,
    ```
* I also [set the address of the web interface]({{< ref "address-transmission-web-client-using-hostname#configuration" >}})
* Further details of the configuration options can be found [here](https://github.com/transmission/transmission/wiki/Editing-Configuration-Files)

Then start up the daemon again.
```shell
sudo systemctl start transmission-daemon
```

## Port forwarding

I also added a port forwarding rule to my router for Transmission.
I simply set a TCP protocol rule to any external IP address from my server IP address with the port used by Transmission.

## Buffers

We can [increase](https://unix.stackexchange.com/a/520626) the system network buffer sizes on Debian to support the defaults requested by transmission.

> If we [already]({{< ref "dns-unbound#configuration" >}}) [edited]({{< ref "rpi-dns#unbound" >}}) `rmem_max` for Unbound, ensure the configurations don't conflict.

Let's set 16 MiB read/receive and 4 MiB write/send,
```shell
sudo vim /etc/sysctl.d/buffer.conf
```
```vim
net.core.rmem_max = 16777216
net.core.wmem_max = 4194304
```
and apply,
```shell
sudo sysctl -p
```

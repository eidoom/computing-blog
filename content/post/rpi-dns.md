+++
title = "Home DNS resolution on a RasPi"
date = 2021-08-21T15:56:57+01:00
categories = ["dns"]
tags = ["raspberry-pi","raspi","rpi","unbound","pihole","self-host","dns","debian-11","debian-bullseye","linux","arm","server","debian-buster","debian-10","headless","dd","home","network","lan","debian-testing","networking","static-ip","ipv4"]
description = "Moving the DNS resolver from the server to a dedicated Raspberry Pi"
toc = true
draft = false
cover = "img/covers/20.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

*DEPRECATED: [I stopped using Pi-hole, replacing it with a blocklist on Unbound and NSD as the authoritative local nameserver.]({{< ref "new-dns" >}})*

I have decided to move [my self-hosted DNS resolver]({{< ref "server-install-containers#pi-hole" >}}) onto a dedicated headless Raspberry Pi.
This is to avoid DNS resolution downtime when I restart my server for kernel updates or when I'm just messing around on there.
The setup is [Pi-hole]({{< ref home-network-ad-blocking-with-pi-hole >}}) for content-blocking (and caching) with [Unbound]({{< ref unbound >}}) as the recursive (and validating and caching) DNS server.

## Hardware

I bought a second-hand [Raspberry Pi 2 Model B](https://www.raspberrypi.org/products/raspberry-pi-2-model-b/) {{< wiki "Raspberry_Pi" >}}.
The B models have an ethernet port and the earlier models are preferrable as they have a [lower power consumption](https://www.raspberrypi.org/documentation/computers/raspberry-pi.html#typical-power-requirements) {{< ext "https://www.raspberrypi-spy.co.uk/2018/11/raspberry-pi-power-consumption-data/" >}} while providing sufficient computational ability for this application (and are cheaper).

The [recommended PSU current capacity](https://www.raspberrypi.org/documentation/computers/raspberry-pi.html#power-supply) for this model is 1.8A.
[Phone chargers can be insufficient to meet the rapidly changing current demands of attached peripherals](https://www.raspberrypi.org/documentation/computers/getting-started.html#setting-up-your-raspberry-pi), but I'm not using any so I'll reuse a humble old phone charger (5V 2A).

The Pi uses a {{< wl "microSD card" SD_card >}} as its storage medium.
Mine came with a 64GB card.

[Wikipedia has a nice comparison table of all the different models and versions.](https://en.wikipedia.org/wiki/Raspberry_Pi#Simplified_Model_B_Changelog)

## Operating system

Installing an OS on the SD card for the Pi is [most easily](https://raspi.debian.net/what-is-image/) done by flashing a pre-prepared image.

Raspberry Pi offers [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/) {{< wiki Raspberry_Pi_OS >}}, but I don't see why I should use it over a plain Debian image.
At 445MB, it's bloated as far as I'm concerned.
~~Furthermore, Raspberry Pi OS (formerly Raspian) has an {{< wl "unresolved controversy" "Raspberry_Pi_OS#Microsoft_repository_controversy" >}} that puts me off.~~ (Now resolved.)

Luckily, the [Debian wiki](https://wiki.debian.org/RaspberryPi) pointed me to a [Debian project](https://raspi.debian.net/) which provides Pi-compatible [images](https://raspi.debian.net/tested-images/) {{< glsh "https://salsa.debian.org/raspi-team/image-specs" >}}.
It's unofficial, probably since the images include the [required `non-free` blob](https://wiki.debian.org/RaspberryPi#Booting_via_a_binary_blob).
[Note](https://wiki.debian.org/RaspberryPi#Raspberry_Pi_1_.28A.2C_B.2C_A.2B-.2C_B.2B-.2C_Zero.2C_Zero_W.29) [that](https://raspi.debian.net/faq/) the Pi 0 and Pi 1 models use a CPU that is not targeted by Debian, so while `armel` architecture Debian images exist for them, they are much slower due to the use of emulated floaing points---Raspberry Pi OS is admittedly preferable here.
All other models do not suffer this problem, with the Pi 2 on `armhf` and later models on `arm64` all using hard floats.

## Setup attempt one: *bullseye* (Pi-hole unsupported)

### Flashing

On my laptop, I downloaded the appropriate tested *bullseye* (Debian 11, latest release) image (297 [MiB]({{< ref "jargon#binary" >}}), that's more like it)
```shell
wget https://raspi.debian.net/verified/20210717_raspi_2_bullseye.img.xz
```
and verified the file
```shell
$ wget https://raspi.debian.net/verified/20210717_raspi_2_bullseye.xz.sha256.asc
$ sha256sum --check 20210717_raspi_2_bullseye.xz.sha256.asc
20210717_raspi_2_bullseye.img.xz: OK
```
I inserted the SD card into the laptop and identified it with `lsblk` as `/dev/sda` before flashing the image
```shell
xzcat 20210717_raspi_2_bullseye.img.xz | sudo dd of=/dev/sda bs=64k oflag=dsync status=progress
```

### Security

Before removing the SD card from the laptop, I mounted the image and put the SSH public keys of the machines which should have access to the Pi into `/home/authorized_keys`.

I then plugged the SD card into the Pi and connected a monitor via HDMI, a keyboard via USB, ethernet, and power via microUSB.

> I could actually have done all this remotely from the get-go, as explained by [FAQ 4](https://raspi.debian.net/faq/).
> Unfortunately, I didn't notice that until after.

The image has a passwordless `root` by default, so I logged in as `root` to immediately set a `root` password
```shell
passwd
```
and disabled password SSH access in [the usual manner]({{< ref "server-install#ssh" >}})
```shell
sed -i "s/^#PasswordAuthentication yes$/PasswordAuthentication no/g" /etc/ssh/sshd_config
```
I updated the system
```shell
apt update
apt upgrade -y
```
I [made a user and set their password](https://wiki.archlinux.org/title/Users_and_groups#Example_adding_a_user)
```shell
useradd -m ryan
passwd ryan
```
I allowed SSH access by the desired machines
```shell
mkdir /home/ryan/.ssh
chown ryan:ryan /home/ryan/.ssh
chown ryan:ryan /home/authorized_keys
mv /home/authorized_keys /home/ryan/.ssh
```

### Network

Networking is nice and simple; it's managed with the `networking` service.
Since we're not using wireless here, I disabled `wpa_supplicant`
```shell
systemctl disable wpa_supplicant
systemctl stop wpa_supplicant
```
I [set](https://wiki.debian.org/NetworkConfiguration#Configuring_the_interface_manually) [up](https://www.debian.org/doc/manuals/debian-handbook/sect.network-config.en.html) a static IP address
```
vi /etc/network/interfaces.d/eth0
```
```vim
auto eth0
iface eth0 inet static
  address 192.168.1.3/24
  gateway 192.168.1.1
```
and restarted the network to apply the settings
```shell
ifdown eth0
ifup eth0
```
([Don't use](https://wiki.debian.org/NetworkConfiguration#Starting_and_Stopping_Interfaces) `systemctl restart networking` as it may not restart all interfaces.)
I confirmed the new settings with `ip a` and tested with `ping -c 1 192.168.1.2`.

> Turns out setting a static IP address can be skipped because the `pihole` installer will do it for us.

Now everything was ready to set up the Pi in its new home and go remote.

### Unbound

I set up Unbound very similarly to [before]({{< ref unbound >}}): I `ssh`ed into the Pi and
```shell
su
apt install unbound dnsutils
systemctl stop unbound-resolvconf
systemctl disable unbound-resolvconf
echo "net.core.rmem_max=1048576" > /etc/sysctl.d/unbound.conf
vi /etc/unbound/unbound.conf.d/pi-hole.conf
```
```vim
server:
    # If no logfile is specified, syslog is used
    # logfile: "/var/log/unbound/unbound.log"
    verbosity: 0

    interface: 127.0.0.1

    port: 5335

    do-ip4: yes
    do-ip6: no

    do-udp: yes
    do-tcp: yes

    # You want to leave this to no unless you have *native* IPv6. With 6to4 and
    # Terredo tunnels your web browser should favor IPv4 for the same reasons
    prefer-ip6: no

    # Trust glue only if it is within the server's authority
    harden-glue: yes

    # Require DNSSEC data for trust-anchored zones, if such data is absent, the zone becomes BOGUS
    harden-dnssec-stripped: yes

    # Don't use Capitalization randomization as it known to cause DNSSEC issues sometimes
    # see https://discourse.pi-hole.net/t/unbound-stubby-or-dnscrypt-proxy/9378 for further details
    use-caps-for-id: no

    # Reduce EDNS reassembly buffer size.
    # Suggested by the unbound man page to reduce fragmentation reassembly problems
    edns-buffer-size: 1472

    # Perform prefetching of close to expired message cache entries
    # This only applies to domains that have been frequently queried
    prefetch: yes

    # One thread should be sufficient, can be increased on beefy machines. In reality for most users running on small networks or on a single machine, it should be unnecessary to seek performance enhancement by increasing num-threads above 1.
    num-threads: 1

    # Ensure kernel buffer is large enough to not lose messages in traffic spikes
    so-rcvbuf: 1m

    # Ensure privacy of local IP ranges
    private-address: 192.168.0.0/16
    private-address: 169.254.0.0/16
    private-address: 172.16.0.0/12
    private-address: 10.0.0.0/8
    private-address: fd00::/8
    private-address: fe80::/10
```
```shell
systemctl restart unbound
```
and did the [tests]({{< ref "dns-unbound#testing" >}}).

### Pi-hole

Installing Pi-hole was, again, not dissimilar to [before]({{< ref home-network-ad-blocking-with-pi-hole >}}) (although more dissimilar to the [container version]({{< ref "server-install-containers#pi-hole" >}})), which is to say, very easy.

To get Pi-hole on the Pi, which doesn't have `wget` or `curl` installed (or even `bash-completion`, as I keep discovering) and which I'll keep as neat as possible, I downloaded the installer onto my laptop and copied it over with
```shell
wget -O basic-install.sh https://install.pi-hole.net
scp basic-install.sh 192.168.1.3:
```

> This was superfluous as `pi-hole` installs `wget` anyway.

On the Pi,
```shell
chmod +x basic-install.sh
su
PIHOLE_SKIP_OS_CHECK=true ./basic-install.sh
```
The OS check skip is required because Pi-hole hasn't caught up with *bullseye* yet.
Probably should have read the manual, this is going to cause pain.

In the configuration TUI, I set `127.0.0.1#5335` (our local `unbound` service) as the upstream DNS provider and everything else to the default.

After `pihole` was installed, the `pihole-FTL` service failed because `lighttpd` failed to start
```shell
$ systemctl status lighttpd
...
Aug 21 21:06:12 rpi2-20210717 systemd[1]: Starting Lighttpd Daemon...
Aug 21 21:06:13 rpi2-20210717 lighttpd[11337]: 2021-08-21 21:06:12: configfile.c.461) Warning: "mod_compress" is DEPRECATED and has been replaced with "mod_deflate".  A future release of lighttpd 1.4.x will not contain mod_compress and lighttpd may fail to start up
Aug 21 21:06:13 rpi2-20210717 lighttpd[11337]: 2021-08-21 21:06:12: plugin.c.195) dlopen() failed for: /usr/lib/lighttpd/mod_deflate.so /usr/lib/lighttpd/mod_deflate.so: cannot open shared object file: No such file or directory
Aug 21 21:06:13 rpi2-20210717 lighttpd[11337]: 2021-08-21 21:06:12: server.c.1238) loading plugins finally failed
Aug 21 21:06:13 rpi2-20210717 systemd[1]: lighttpd.service: Control process exited, code=exited, status=255/EXCEPTION
Aug 21 21:06:13 rpi2-20210717 systemd[1]: lighttpd.service: Failed with result 'exit-code'.
Aug 21 21:06:13 rpi2-20210717 systemd[1]: Failed to start Lighttpd Daemon.
```
This is because *bullseye* uses a version where `mod_compress` has been removed.
The [solution](https://forum.armbian.com/topic/14880-short-info-pihole-on-unsupported-os/) is
```shell
sed -r -i.backup 's/mod_compress/mod_deflate/g' /etc/lighttpd/lighttpd.conf
apt install lighttpd-mod-deflate
systemctl start lighttpd
systemctl start pihole-FTL
```
But `pihole` still had another problem.
At this point, I decided it would be more sensible to just use a supported OS version for `pihole`.

Note that if restarting at any point while things are broken, name resolution problems can be encountered.
After installing `unbound`, the system is not configured to look at port 5335 so won't see the service.
Also note that after installing `pihole`, `dhcpcd` will take over writing `/etc/resolv.conf`.
A temporary alternative nameserver address can be set with
```shell
echo "nameserver <address>" > /etc/resolv.conf.head
systemctl restart dhcpcd
```
or, better yet, just temporarily comment out the `port: 5335` line in the `unbound` configuration at `/etc/unbound/unbound.conf.d/pi-hole.conf` and `systemctl restart unbound`.

## Setup attempt two: *buster* (Pi-hole supported)

This time, we'll use *buster* (307 MiB image) since Pi-hole supports that release.
Things proceeded much as before, apart from:

* I had to install `ca-certificates` [from]({{< ref "server-install#installing-selectively-from-testing" >}}) *bullseye*.
* `raspi3-firmware` has been renamed as `raspi-firmware`, which I had to manually apply
    ```shell
    apt install raspi-firmware # also uninstalls raspi3-firmware
    systemctl restart rpi-reconfigure-raspi-firmware
    ```

I found that `/usr/sbin/` wasn't on my `PATH`, so did
```shell
echo 'PATH="/usr/sbin:$PATH"' >> ~/.profile
```

I [set up `sudo`]({{< ref "server-install#sudo" >}}) for my user this time, set `bash` as the login shell with `chsh -s /bin/bash`, and [did](https://superuser.com/a/136653)
```shell
echo "127.0.0.1 $HOSTNAME" | sudo tee -a /etc/hosts
```
to [fix](https://askubuntu.com/q/790233) the harmless but irksome
```shell
sudo: unable to resolve host <hostname>: Name or service not known
```
error.

I noticed `pihole restartdns` tries to use deprecated `service`, so I used `systemctl restart pihole-FTL` instead to restart.

Pi-hole can be updated with `pihole -up`.

I can access the web interface at <http://192.168.1.3/admin> or <http://pi.hole/admin>.

Local DNS mappings can be entered on the web portal at `Local DNS > DNS Records` or at `/etc/pihole/custom.list`.
{{< wl "CNAME records" "CNAME_record" >}} for the home server's reverse proxy subdomains can be entered at `Local DNS > CNAME Records`.
Sadly these are put straight into a SQLite `db` so there's no text file to edit.

## Demoting the home server's DNS service

I kept the Pi-hole container and `unbound` on the home server, but changed the DNS entries on the router so that the Pi was the primary nameserver and the home server was the second nameserver.

## Outlook

This was a slightly unsatisfying experience as I didn't get `pihole` on the latest version of Debian as I wanted, instead using the old stable release which had some annoying quirks.
Added to this, I'm sure `pihole` will get its *bullseye* release within a matter of days of me doing this.
I also realised that `pihole` is extremely invasive of the host system.
One way to solve both of these problems would have been to use the `docker` version like I did on the server for some time, although the reason that I didn't do this is because I wanted a minimal setup for running on the light hardware.

I am now considering whether it would be better to kick `pihole` and just use `unbound` by itself.
It [can](https://www.bentasker.co.uk/documentation/linux/279-unbound-adding-custom-dns-records) [be](https://www.asrivas.me/blog/configuring-unbound-as-a-local-dns-server/) set up as a DNS blackhole.
I could even continue using the [Pi-hole default blocklist](https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts) if I reformatted it with `regex`.
Resolution of local machines and CNAMEs [could](https://lists.nlnetlabs.nl/pipermail/unbound-users/2009-March/000509.html) be handled by running an authorative DNS server like [NSD](https://www.nlnetlabs.nl/projects/nsd/about/) {{< wiki NSD >}}.

## Appendices

We could use the builder tool for the Debian images to configure and build our own image.
Then we could have a custom image with everything we want ready to go, which would be very nice for re-deployments.

### (Not) building an image on Fedora

```shell
sudo dnf install -y dosfstools qemu qemu-user-static debootstrap time kpartx bmap-tools
cd ~/git
git clone git@gitlab.com:larswirzenius/vmdb2.git
git clone --recursive https://salsa.debian.org/raspi-team/image-specs.git
cd image-specs
sed -i "s/vmdb2/\/home\/ryan\/git\/vmdb2\/vmdb2/g" Makefile
sed -i "s/qemu-debootstrap/debootstrap/g" raspi_master.yaml
sudo make raspi_4_buster.img
```
```shell
cat raspi_master.yaml | \
sed "s/__FIRMWARE_PKG__/raspi3-firmware/" | \
sed "s/__RELEASE__/buster/" |\
sed "s/__SECURITY_SUITE__/buster\/updates/" |\
sed "s/__FIX_FIRMWARE_PKG_NAME__/sed -i s\/raspi-firmware\/raspi3-firmware\/ \/etc\/systemd\/system\/rpi-reconfigure-raspi-firmware.service/" |\
grep -v '__EXTRA_SHELL_CMDS__' > raspi_base_buster.yaml
cat raspi_base_buster.yaml | sed "s/__ARCH__/arm64/" | \
sed "s#raspi3-firmware#raspi-firmware/buster-backports#" | \
sed "s#apt-get update#echo 'APT::Default-Release \"buster\";' > /etc/apt/apt.conf\n      apt-get update#" | \
sed "s#cmdline.txt#cmdline.txt\n      sed -i 's/cma=64M //' /boot/firmware/cmdline.txt\n      sed -i 's/cma=\\\$CMA //' /etc/kernel/postinst.d/z50-raspi-firmware#" | \
sed "s/__LINUX_IMAGE__/linux-image-arm64\/buster-backports/" | \
sed "s/__EXTRA_PKGS__/- firmware-brcm80211\/buster-backports/" | \
sed "s/__DTB__/\\/usr\\/lib\\/linux-image-*-arm64\\/broadcom\\/bcm*rpi*.dtb/" |\
sed "s/__SERIAL_CONSOLE__/ttyS1,115200/" |\
sed "s/__OTHER_APT_ENABLE__/deb http:\/\/deb.debian.org\/debian\/ buster-backports main contrib non-free # raspi 4 needs a kernel and raspi-firmware newer than buster's/" |\
sed "s/__HOST__/rpi4/" |\
grep -v '__EXTRA_SHELL_CMDS__' > raspi_4_buster.yaml
touch raspi_4_buster.log
time nice  /home/ryan/git/vmdb2/vmdb2 --verbose --rootfs-tarball=raspi_4_buster.tar.gz --output=raspi_4_buster.img raspi_4_buster.yaml --log raspi_4_buster.log
Load spec file raspi_4_buster.yaml
Exec: ['dpkg', '--print-architecture']
Exec: ['qemu-img', 'create', '-f', 'raw', 'raspi_4_buster.img', '1500M']
Exec: ['parted', '-s', 'raspi_4_buster.img', 'mklabel', 'msdos']
Exec: ['parted', '-m', 'raspi_4_buster.img', 'print']
Exec: ['parted', '-s', 'raspi_4_buster.img', '--', 'mkpart', 'primary', 'fat32', '4MiB', '20%']
Exec: ['parted', '-m', 'raspi_4_buster.img', 'print']
Exec: ['parted', '-m', 'raspi_4_buster.img', 'print']
Exec: ['parted', '-s', 'raspi_4_buster.img', '--', 'mkpart', 'primary', 'ext2', '20%', '100%']
Exec: ['parted', '-m', 'raspi_4_buster.img', 'print']
Exec: ['kpartx', '-asv', 'raspi_4_buster.img']
remembering /dev/mapper/loop22p1 as /boot
remembering /dev/mapper/loop22p2 as /
Exec: ['/sbin/mkfs', '-t', 'vfat', '-n', 'RASPIFIRM', '/dev/mapper/loop22p1']
Exec: ['/sbin/mkfs', '-t', 'ext4', '-L', 'RASPIROOT', '/dev/mapper/loop22p2']
Exec: ['mount', '/dev/mapper/loop22p2', '/tmp/tmpv35wzo40']
Exec: ['mount', '/dev/mapper/loop22p1', '/tmp/tmpv35wzo40/.//boot/firmware']
Exec: ['debootstrap', '--arch', 'arm64', '--variant', '-', '--components', 'main,contrib,non-free', 'buster', '/tmp/tmpv35wzo40', 'http://deb.debian.org/debian']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'apt-get', 'update']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'sh', '-ec', 'echo \'APT::Default-Release "buster";\' > /etc/apt/apt.conf\napt-get update']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'apt-get', 'update']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'apt-get', '-y', '--no-show-progress', '', 'install', 'eatmydata']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'eatmydata', 'apt-get', 'update']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'eatmydata', 'apt-get', '-y', '--no-show-progress', '', 'install', 'ca-certificates', 'dosfstools', 'iw', 'parted', 'ssh', 'wpasupplicant', 'raspi-firmware/buster-backports', 'linux-image-arm64/buster-backports', 'firmware-brcm80211/buster-backports']
Exec: ['chroot', '/tmp/tmpv35wzo40', 'apt-get', 'clean']
tag /boot mounted /tmp/tmpv35wzo40/.//boot/firmware cached True
tag / mounted /tmp/tmpv35wzo40 cached True
caching rootdir /tmp/tmpv35wzo40
caching relative ['.', './/boot/firmware']
Exec: ['tar', '--one-file-system', '-C', '/tmp/tmpv35wzo40', '-caf', 'raspi_4_buster.tar.gz', '.', './/boot/firmware']
Exec: ['sh', '-ec', 'echo "rpi4-$(date +%Y%m%d)" > "${ROOT?}/etc/hostname"\n\n# Allow root logins locally with no password\nsed -i \'s,root:[^:]*:,root::,\' "${ROOT?}/etc/shadow"\n\ninstall -m 644 -o root -g root rootfs/etc/fstab "${ROOT?}/etc/fstab"\n\ninstall -m 644 -o root -g root rootfs/etc/network/interfaces.d/eth0 "${ROOT?}/etc/network/interfaces.d/eth0"\ninstall -m 600 -o root -g root rootfs/etc/network/interfaces.d/wlan0 "${ROOT?}/etc/network/interfaces.d/wlan0"\n\ninstall -m 755 -o root -g root rootfs/usr/local/sbin/rpi-set-sysconf "${ROOT?}/usr/local/sbin/rpi-set-sysconf"\ninstall -m 644 -o root -g root rootfs/etc/systemd/system/rpi-set-sysconf.service "${ROOT?}/etc/systemd/system/"\ninstall -m 644 -o root -g root rootfs/boot/firmware/sysconf.txt "${ROOT?}/boot/firmware/sysconf.txt"\nmkdir -p "${ROOT?}/etc/systemd/system/basic.target.requires/"\nln -s /etc/systemd/system/rpi-set-sysconf.service "${ROOT?}/etc/systemd/system/basic.target.requires/rpi-set-sysconf.service"\n\n# Resize script is now in the initrd for first boot; no need to ship it.\nrm -f "${ROOT?}/etc/initramfs-tools/hooks/rpi-resizerootfs"\nrm -f "${ROOT?}/etc/initramfs-tools/scripts/local-bottom/rpi-resizerootfs"\n\ninstall -m 644 -o root -g root rootfs/etc/systemd/system/rpi-reconfigure-raspi-firmware.service "${ROOT?}/etc/systemd/system/"\nmkdir -p "${ROOT?}/etc/systemd/system/multi-user.target.requires/"\nln -s /etc/systemd/system/rpi-reconfigure-raspi-firmware.service "${ROOT?}/etc/systemd/system/multi-user.target.requires/rpi-reconfigure-raspi-firmware.service"\nsed -i s/raspi-firmware/raspi-firmware/buster-backports/ /etc/systemd/system/rpi-reconfigure-raspi-firmware.service\n\ninstall -m 644 -o root -g root rootfs/etc/systemd/system/rpi-generate-ssh-host-keys.service "${ROOT?}/etc/systemd/system/"\nln -s /etc/systemd/system/rpi-generate-ssh-host-keys.service "${ROOT?}/etc/systemd/system/multi-user.target.requires/rpi-generate-ssh-host-keys.service"\nrm -f "${ROOT?}"/etc/ssh/ssh_host_*_key*']
ERROR: Program failed: 1
ERROR: RuncmdError('Program failed: 1')
Something went wrong, cleaning up!
Exec: ['kpartx', '-dsv', 'raspi_4_buster.img']
Command exited with non-zero status 1
502.11user 63.34system 9:25.53elapsed 99%CPU (0avgtext+0avgdata 107892maxresident)k
671160inputs+3819896outputs (590major+13162193minor)pagefaults 0swaps
make: *** [Makefile:139: raspi_4_buster.img] Error 1
```
Hmm. Looks like it'd take some work to get this to function on Fedora. Could use Debian on Docker or my home server.

### Customising the image

To [fork]({{< ref "git-reference#fork" >}}) the project, I created a new repo {{< gl image-specs >}} then
```shell
cd ~/git
git clone --recursive https://salsa.debian.org/raspi-team/image-specs.git
cd image-specs
git remote rename origin upstream
git remote add origin git@gitlab.com:eidoom/image-specs.git
git push -u origin master
```
I can update the fork with `git pull upstream master`.

I played around with this a little, but ended up abandoning it for now.

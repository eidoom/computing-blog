+++
title = "Which font for coding?"
date = 2020-01-13T10:32:23Z
categories = ["environment-core"]
tags = ["environment","linux","fedora","laptop","desktop","font","terminal","text-editor","hack-font","dejavu-font","inconsolata-font","fira-font","droid-font","source-code-pro-font","ubuntu-font"]
description = "Choosing a terminal font"
toc = true
cover = "img/covers/87.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

I found a [nice site](https://app.programmingfonts.org/) for comparing programming fonts, which motivated me to find and choose a favourite font to use in [GNOME terminal](https://wiki.gnome.org/Apps/Terminal) {{< wiki GNOME_Terminal >}} {{< glsh "https://gitlab.gnome.org/GNOME/gnome-terminal" >}}.

## Testing

I drew up a shortlist using this site, then also tested the fonts in the terminal.
I used my laptops, one with a [1920x1080 14" screen]({{< ref "install-fedora#hardware" >}}) and the other with a [3840x2160 15.6" screen]({{< ref "xps-install" >}}), both running Fedora. 
In order of decreasing (very rough and fluid) preference, I tabulated the tested fonts, recording:

* name of the font with link to testing site
* year of release
* name of font in the GNOME Terminal fonts dialogue on Fedora 37
* font size used
* name of Fedora 37 `dnf` package that provides the font

Where no Fedora package existed in the standard repositories, I included a link for installation.
To install fonts manually, get the `<font>.ttf` or preferably `<font>.oft` font file from the link, [then](https://docs.fedoraproject.org/en-US/quick-docs/fonts/#user-fonts--command-line)
```shell
mkdir -p ~/.local/share/fonts/<font-family-name>
cp <font-family-name>-*.(oft|ttf) ~/.local/share/fonts/<font-family-name>
fc-cache -v
```
then restart the terminal.

Fonts are set in GNOME terminal as [shown here]({{< ref "install-fedora#font" >}}).

## Results

I prefer sans serif, no ligature fonts.

Font|Year|Name|Size|Package
---|---|---|---|---
[Hack](https://app.programmingfonts.org/#hack)|2015|Hack|9|`source-foundry-hack-fonts`
[JetBrains Mono](https://app.programmingfonts.org/#jetbrainsmono)|2020|JetBrains Mono NL|9|`jetbrains-mono-nl-fonts`[^nl]
[Sometype Mono](https://app.programmingfonts.org/#sometype-mono)|2018|||[Manual](https://monospacedfont.com/)
[Overpass Mono](https://app.programmingfonts.org/#overpass)|2015|Overpass Mono|9|`overpass-mono-fonts`
[Bront DejaVu Sans Mono](https://app.programmingfonts.org/#bront-dejavu)|2015|DejaVu Sans Mono - Bront Bront|9|[GitHub](https://github.com/chrismwendt/bront)
RedHat Mono {{< ghubn "RedHatOfficial/RedHatFont" >}}||Red Hat Mono|9|`redhat-mono-fonts`
[DejaVu Mono](https://app.programmingfonts.org/#dejavu)|2004|DejaVu Sans Mono|9|`dejavu-sans-mono-fonts`
[Source Code Pro](https://app.programmingfonts.org/#source-code-pro)|2012|Source Code Pro|9 |`adobe-source-code-pro-fonts`
[Inconsolata-g](https://app.programmingfonts.org/#inconsolata-g)|2009|Inconsolata-g g|9|[Manual](https://leonardo-m.livejournal.com/77079.html) 
[Inconsolata](https://app.programmingfonts.org/#inconsolata)|2001|Inconsolata|11|`levien-inconsolata-fonts`
[Borg Sans Mono](https://app.programmingfonts.org/#borg-sans-mono)|2016|||[GitHub](https://github.com/marnen/borg-sans-mono)
[Bront Ubuntu Mono](https://app.programmingfonts.org/#bront-ubuntu)|2015|Ubuntu Mono - Bront Regular|11|[GitHub](https://github.com/chrismwendt/bront)
[Fira Code](https://app.programmingfonts.org/#firacode)|2014|Fira Code|9|`fira-code-fonts`
[Roboto Mono](https://app.programmingfonts.org/#roboto)|2015|Roboto Mono|9|`google-roboto-mono-fonts`
[Noto Sans](https://app.programmingfonts.org/#noto)|2012|[^noto]|9|`google-noto-sans-mono-fonts`
[Droid Sans](https://app.programmingfonts.org/#droid-sans)|2006|Droid Sans Mono|9|`google-droid-sans-mono-fonts`


### Winner

The winner was [Hack](http://sourcefoundry.org/hack/) {{< ghubn "source-foundry/Hack" >}}.
Install on Fedora 37 with
```shell
sudo dnf install source-foundry-hack-fonts
```

> Older versions of Fedora did not package the Hack font.
> To install in this case, either
> * get the [latest release](https://github.com/source-foundry/Hack/releases)
>     ```shell
>     mkdir -p ~/tar/hack
>     cd ~/tar/hack
>     wget https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.tar.xz
>     tar xf Hack-v3.003-ttf.tar.xz
>     ```
>     and install as described above,
> * or use this [Copr](https://fedoraproject.org/wiki/Category:Copr) [repository](https://copr.fedorainfracloud.org/coprs/zawertun/hack-fonts/) with
>     ```shell
>     sudo dnf copr enable zawertun/hack-fonts
>     sudo dnf install hack-fonts
>     ```

<!-- https://app.programmingfonts.org/#borg-sans-mono -->

[^nl]: no ligatures version
[^noto]: not appearing?

+++
title = "Hosting services on IPv6"
date = 2024-09-28T22:27:25+01:00
categories = ["self-host"]
tags = ["ipv6","certbot","tls","ssl","http","https","server","debian-12","debian-bookworm","debian","ddns","duckdns","nginx","lets-encrypt","proxy","reverse-proxy","dynamic-dns","hosting","infrastructure","ipv4","backend","admin","traffic-rules","router","openwrt","firewall","ip-address","dhcp","dhcpv6","lan","wan","nat","dig","luci","nc","curl","shell","sh","domain","subdomain","home","home-server"]
description = "Setting up public access to my home server again"
toc = true
draft = false
cover = "img/covers/duck.webp"
coveralt = "SDXL1"
+++

We have a new ISP with FTTH replacing [cellular]({{< ref "fixed-wireless-broadband" >}}) - quite the upgrade.
They're using IPv4 {{< wl CGNAT >}} so unfortunately we don't get our own WAN IPv4 address, but we do get our own WAN IPv6 address[^private].
Naturally, I'd like to make use of that.

[^private]: The language here can get ambiguous: I could say a private address to mean it is only mine, or I could say a public address to mean my machine is publically accessible on it...

> By the way, if [self-hosting a nameserver]({{< ref "/tags/dns" >}}), ensure `do-ip6: yes` on `unbound` or similar for others.


## WAN IP address

We can check our WAN IP address by querying a service like `myip.opendns.com` with the tool `dig`.

### IPv4

For {{< wl IPv4 "IPv4#Addressing" >}}, to get the WAN address corresponding to our LAN,

```shell
dig -4 A +short myip.opendns.com @resolver1.opendns.com
```

To check if that address is behind an ISP CGNAT, we can use `traceroute`,

```shell
dig -4 A +short myip.opendns.com @resolver1.opendns.com | xargs traceroute
```

If we get more than 1 hop, we're out of luck.

### IPv6

{{< wl IPv6 "IPv6_address" >}} addresses are generally managed quite differently to IPv4 addresses.

IPv4 address exhaustion means LANs use their own address space which sits behind a NAT with respect to the WAN address space.
More recently, CGNATs have added a further intermediate nesting to this Matryoshka address space.

IPv6 addresses, having 128 bits (~3x10^38 addresses) versus the 32 bits (~4x10^9 addresses) of IPv4, (currently) don't need NATs so are usually allocated globally (as the internet was intended).
The ISP provides a routing prefix (this is called prefix delegation), which is combined with an interface identifier (suffix) to generate the full address by manual or automatic assignment on the machine or router.
There are two main methods of suffix allocation:
* *stateful* configuration:
a LAN DHCPv6 server is configured for static leases with IPv6 suffixes (my preferred method).
* *stateless* configuration (also called SLAAC for StateLess Address AutoConfiguration):
the address suffix is automatically derived from the machine's MAC address.
This is discouraged if one has privacy and tracking concerns.

It's also normal to allocate multiple IPv6 addresses per machine, including a `fe80::/10` {{< wl "link-local address" >}}.

View a machine's IPv6 addresses with

```shell
ip a | grep inet6
```

We can check our WAN IPv6 address for a specific machine with

```shell
dig -6 AAAA +short myip.opendns.com @resolver1.opendns.com
```

Note that the ISP's routing prefix may be dynamic.
This is common practice on private/home/non-commercial connections for privacy reasons in some countries.

## Public access

With IPv4, we'd add a port forwarding rule to the router's NAT.
For IPv6, there is no NAT so we just need to [add rules](https://saudiqbal.github.io/IPv6/ipv6-home-server-with-dynamic-prefix-for-vpn-web-server-rdp-and-firewall-setup-guide.html) to the router's firewall.
If the machine is running a firewall, that will need the ports opened too.

### Static lease

First, we give the machine hosting public services a static `IPv6-Suffix` lease on the DHCPv6 server.
On the router:

* For [OpenWrt]({{< ref "openwrt-services" >}}) LuCI, that's at `Network` > `DHCP and DNS` > `Static Leases`.
* On proprietary routers, something like `Network` > `LAN Network` > `DHCP Server` > `DHCP Static Associations`.

Make sure to set the DUID and a lease time of 5 minutes for flexibility.
On the Debian machine, we just [enable IPv6 DHCP on the network interface]({{< ref "server-install#networking" >}}); this will prioritise stateful addresses by default for outgoing traffic.

It has happened to me that the machine got stuck on a stateless address for some reason.
To resolve this, I rebooted the router then ran `sudo systemctl restart networking` on the machine.

### Traffic rules

We need to add `accept` traffic rules to the router's firewall for IPv6 traffic over the relevant protocol from WAN to LAN on the relevant destination ports.

* On OpenWrt LuCI, it's at `Network` > `Firewall` > `Traffic Rules`.
    We can't set the destination IP addresses if the IPv6 prefix is dynamic; in which case, either leave it blank to allow all destination IPs or [add rules to static suffix leases](https://superuser.com/a/1507356).
* Proprietary router: `Network` > `Firewall` > `Rules`.

### Check

#### LAN

Can check if host machine ports are accessible over LAN (ie. its firewall is correctly configured) from another LAN machine with

```shell
nc -vz <IPv6 address> <port>
```

#### WAN

We can check over WAN using remote hosts we have access to (and that support IPv6) or some online port checking tool service like [this](https://port.tools/port-checker-ipv6/).

## Dynamic DNS

I tried a few free dynamic DNS services.
I have used DuckDNS for several years but the service has significantly degraded over that time, with lots of downtime, so an alternative is in order.
Realistically, the correct answer is a paid-for service.

### DuckDNS

Free.
[DuckDNS](https://www.duckdns.org/) offers all the features I like but is **unreliable**.
Features include wildcard DNS records, which aren't as "neat" as explicit CNAME records, but appeal to my laziness, and easy DNS-01 validation for TLS.

#### Domain

For a dynamic DNS service, we can use DuckDNS [like]({{< ref "cloud#__duckdns" >}}) [before]({{< ref "caldav#duckdns" >}}) but with IPv6 address (which must be explicitly given unlike with IPv4):

`/home/<user>/duckdns/duck.sh`

```shell
#!/usr/bin/env sh

domains=<subdomains>
token=<token>
ip="$(dig -6 AAAA +short myip.opendns.com @resolver1.opendns.com)"

log=~/duckdns/duck.log

echo url="https://www.duckdns.org/update?domains=$domains&token=$token&ipv6=$ip&verbose=true" \
    | curl -k -o $log -K -

echo >> $log
date >> $log
```

#### TLS

We'll use `certbot` for TLS on DuckDNS, similar to [previous]({{< ref "caldav#tls" >}}) [setups]({{< ref "cloud#__tls_wildcard_duckdns" >}}).
First we install it, which also sets up automatic renewal,

```shell
sudo apt install certbot
```

We set up a manual authentication script for DuckDNS,

`/etc/letsencrypt/duckdns.sh`
```shell
#!/usr/bin/env sh

domains=<duckdns subdomains>
token=<token>

curl "https://www.duckdns.org/update?domains=$domains&token=$token&txt=$CERTBOT_VALIDATION&verbose=true" \
    -o /etc/letsencrypt/duckdns.log
```

Then we use it to register a wildcard TLS certificate,

```shell
sudo chmod +x /etc/letsencrypt/duckdns.sh
sudo certbot certonly \
	--agree-tos \
	--cert-name "star.<domain>" \
	--domain "*.<domain>" \
	--email "<email>" \
	--non-interactive \
	--manual \
	--preferred-challenges dns \
	--manual-auth-hook "/etc/letsencrypt/duckdns.sh" \
	--keep-until-expiring \
	--deploy-hook "systemctl restart nginx.service"
```

### FreeDNS

[FreeDNS](https://freedns.afraid.org/)'s free tier doesn't offer wildcard DNS records.
However, we get 50 subdomains, so can add CNAME records for (sub)subdomains, or can point to our own nameserver with an NS record and handle it there.
The TLS situation is untested.

#### Domain

It's [easy]({{< ref "caldav#freedns" >}}) to set up a subdomain on one of the domains available on [FreeDNS](https://freedns.afraid.org).
The [API](https://freedns.afraid.org/dynamic/v2/) to update the IPv6 address uses a randomised token for each entry and automatically detects the address,
```shell
curl https://v6.sync.afraid.org/u/<token>/
```
This can be automated in the [usual fashion]({{< ref "caldav#duckdns" >}}).

However, wildcard DNS is not offered on the free tier.

#### TLS

Unfortunately, automatic DNS validation on FreeDNS does not seem to be supported, either by [`acme.sh`](https://github.com/acmesh-official/acme.sh/wiki/dnsapi#15-use-freedns) or [manually](https://freedns.afraid.org/faq/#17) (note that `data_id` can be found in the url of the subdomain edit entry).
Therefore, we have to do HTTP validation (on every (sub)subdomain), or possibly host a TXT record for validation on our own nameserver (named by an NS record on FreeDNS).

### ClouDNS

[ClouDNS](https://www.cloudns.net)'s free tier doesn't give API access for creating TXT records for TLS DNS challenges.

#### Domain

[First](https://www.cloudns.net/wiki/article/364/), create an account and zone.
Add an AAAA record, which can be [wildcard](https://www.cloudns.net/wiki/article/190/) by setting the host to `*`.
[Then](https://www.cloudns.net/wiki/article/36/), activate dynamic DNS on the record, which provides an API URL with a token for that record.
The IP address on the record can be updated with

```shell
curl https://ipv6.cloudns.net/api/dynamicURL/?q=<token>
```
which returns `OK` if OK.

#### TLS

Automated DNS validation is [not](https://www.cloudns.net/wiki/article/448/) supported on the free tier.

### NoIP

[NoIP](https://www.noip.com/) doesn't offer free wildcards.

## Reverse proxy

We configure Nginx to serve various services.
These include:

* static sites served by Nginx,
* reverse proxies for other web servers (like [Radicale]({{< ref "caldav" >}})),
* and [Nginx file browsers]({{< ref "proxy#file-browser" >}}).

We use two domains,

* one accessible publically over the WAN via IPv6 with TLS,
* and another private one accessible only in the LAN via IPv4 or IPv6 without encryption.

Services run on different subdomains, using one server block per service.
The subdomains are given appropriate DNS entries in their relevant nameserver.
There is also a default server block to return `403 Forbidden` to requests to unregistered addresses.
The server blocks take the forms illustrated below:

`/etc/nginx/sites-available/mask`
```nginx
# attempts to access addresses (IPv4/6) with no
# virtual hosts get a 403 Forbidden response

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    server_name _;

    return 403;
}

server {
    listen 443 default_server;
    listen [::]:443 default_server;

    server_name _;

    ssl_certificate /etc/letsencrypt/live/<name>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<name>/privkey.pem;

    return 403;
}
```

`/etc/nginx/sites-available/public`
```nginx
# WAN accessible over IPv6 only with TLS

## example of static site with TLS optionally available

### HTTP
server {
    listen [::]:80;

    server_name <wan domain> www.<wan domain>;

    root /var/www/public;

    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

### HTTPS
server {
    listen [::]:443;

    server_name <wan domain> www.<wan domain>;

    ssl_certificate /etc/letsencrypt/live/<name>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<name>/privkey.pem;

    root /var/www/public;

    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

## example of reverse proxy with mandatory TLS

### redirect HTTP to HTTPS
server {
    listen [::]:80;
    
    server_name <subdomain>.<wan domain>;

    location / {
        return 301 https://$host$request_uri;
    }
}

### reverse proxy on HTTPS 
server {
    listen [::]:443;

    server_name <subdomain>.<wan domain>;

    ssl_certificate /etc/letsencrypt/live/<name>/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/<name>/privkey.pem;

    location / {
        proxy_pass http://127.0.0.1:<port>/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

`/etc/nginx/sites-available/private`
```nginx
# LAN accessible over IPv4/6 without TLS

# example of static website
server {
    listen 80;
    listen [::]:80;

    server_name <lan domain> www.<lan domain>;

    root /var/www/private;

    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}

# example of file browser
server {
    listen 80;
    listen [::]:80;

    server_name files.<lan domain>;

    root <path>;

    location / {
        autoindex on;
        autoindex_exact_size off;
    }
}

# example of reverse proxy
server {
    listen 80;
    listen [::]:80;

    server_name <subdomain>.<lan domain>;

    location / {
        proxy_pass http://127.0.0.1:<port>/;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Prefix /;
    }
}
```

These are enabled in the usual fashion,

```shell
cd /etc/nginx/sites-enabled
sudo ln -s ../sites-available/<name> .
sudo systemctl restart nginx
```

+++
title = "Stories v2"
date = 2022-10-22T15:57:02+02:00
categories = ["web"]
tags = ["project","svelte.js","svelte","rollup","rollup.js","html","css","js","javascript","python","ci","gitlab-ci","ui","ux","interactive","static-website","website","markdown","pip","npm","pyyaml","mistletoe","strictyaml","ruamel.yaml","vanilla.js","mpa","spa","webpages"]
description = "A rewrite of Musings"
toc = true
draft = false
cover = "img/covers/114.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I made a more interactive version of [my stories blog]({{< ref zola >}}).
You can find it [here](https://eidoom.gitlab.io/stories), with source {{< gl stories >}}.

The new website is a so-called multi-page application (MPA), like what [Astro](https://astro.build/) does (I didn't know about Astro when I was writing it, otherwise I would have used it).
Basically, it is a static website, but has rich interactivity through Javascript.
Only the interactive parts of pages are client-side hydrated templates, everything else is plain HTML.

It went through several iterations.

* My [first attempt](https://gitlab.com/eidoom/stories/-/tree/custom) had minimal library and framework dependency.
    I processed Markdown content with Python and injected it into a Javascript file, which did rendering and single-page application (SPA) routing in Vanilla JS.
    This was more of a pedagogical exercise than anything else.
* In [next version](https://gitlab.com/eidoom/stories/-/tree/sveltekit), I did everything in Javascript, using the SvelteKit framework.
    However, I didn't enjoy doing the "backend", by which I mean processing Markdown files to JSON and generating the tags and so on, in Javascript.
* I tried a [hybrid approach](https://gitlab.com/eidoom/stories/-/tree/hybrid), where content was processed in Python and dumped as JSON for SvelteKit to use to build the website.
    I found myself fighting against SvelteKit here, which wants to be a full-stack framework solution when I only want a frontend UI library.
* I settled on a [hybrid method](https://gitlab.com/eidoom/stories) with Python data processing and plain Svelte for the frontend with Rollup as a bundler.
    The "backend" with Python is easy for me, and I enjoy using Svelte for the static frontend.

I also reclaimed some of my old data from Goodreads with the scripts at {{< gl convert-goodreads >}}.

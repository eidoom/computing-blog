+++
title = "Running services on OpenWrt"
date = 2021-04-28T22:16:54+01:00
edit = 2021-04-30
categories = ["network"]
tags = ["network","lan","router","openwrt","samba","ad-block","fdisk","gdisk","curl","blocklist","dns","flash","firmware","hard-disk","ethernet","wifi","nfs","nas","backup","luci","lede","uci","ash","embedded","ext4","fstab","usb","apple","mac","ipad","reuse","sata","dropbear","ssh","oom","home"]
description = "File sharing and ad-blocking on the router"
toc = true
draft = false
cover = "img/covers/11.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I just added some services to my parents' router to block ads on the local network and provide network attached storage for a family photographs local backup solution.

I set up the local network at my parents' house while it was getting built a few years ago.
The house has well insulated walls which are not friendly to wireless range, so this included running some ethernet cables through the walls and loft to a couple of old routers repurposed as extra wireless access points/ethernet switches.
I also buried an external ethernet line to an outbuilding.
Overall, it came out as around 200 metres of wiring behind the wall ports.
It was quite awkward to do, but I can recall it fondly now having done the work.

## Router hardware
The central hub of this network is a [Netgear WNDR3700v2](https://wikidevi.wi-cat.ru/Netgear_WNDR3700v2) {{< ext "https://openwrt.org/toh/hwdata/netgear/netgear_wndr3700_v2" >}} ([stats]({{< ref "wap#netgear-wndr3700v2-router" >}})), kindly gifted by a stranger graduate of my undergrad uni.
I run [OpenWrt](https://openwrt.org/) {{< wiki OpenWrt >}} {{< ext "https://openwrt.org/toh/netgear/wndr3700" >}} on it with the [LuCI web interface](https://openwrt.org/docs/guide-user/luci/start).

## Upgrade
To [upgrade](https://openwrt.org/docs/guide-quick-start/sysupgrade.luci) OpenWrt, I downloaded the latest stable Sysupgrade image from the [OpenWrt website](https://firmware-selector.openwrt.org/?version=19.07.7&target=ath79%2Fgeneric&id=netgear_wndr3700v2) for my router (19.07.7), then uploaded the image to the router through LuCI at `System > Backup/Flash Firmware (Flash Operations) > Actions > Flash new firmware image`, checked the hash, and flashed.

> {{< cal 2023-11-27 >}}
>
> I did a successful clean upgrade directly to [23.05.2](https://openwrt.org/releases/23.05/notes-23.05.2).

## Storage devices

### Hardware interface
The router had a [USB 2.0](https://en.wikipedia.org/wiki/USB#USB_2.0) port for attaching a storage device.
While this old interface was glacially slow, it sufficed for our needs.
I fitted a 3.5" SATA mechanical hard disk in an external USB enclosure and connected it up to the router.

### Software
To get this [working with OpenWrt](https://openwrt.org/docs/guide-user/storage/usb-drives), I set it up on the command line with shell [`ash`](https://en.wikipedia.org/wiki/Almquist_shell) via `ssh`, which is served by [Dropbear](https://matt.ucc.asn.au/dropbear/dropbear.html) {{< ext "https://openwrt.org/docs/guide-user/base-system/dropbear" >}} {{< wiki "Dropbear (software)" >}}.

> {{< cal 2023-11-27 >}}
>
> It appears only to support `rsa` keys, while I usually use `ed15519`.
> I created an `rsa` key specially for this and configured the ssh connection,
> ```shell
> ssh-keygen
> vim ~/.ssh/config
> ```
> ```vim
> ...
> Host openwrt
>     Hostname <address>
>     User root
>     IdentityFile ~/.ssh/id_rsa
>     PubkeyAcceptedAlgorithms +ssh-rsa
>     HostkeyAlgorithms +ssh-rsa
> ```
> then could
> ```shell
> ssh openwrt
> ```

I installed the necessary software with
```shell
opkg update
opkg install kmod-usb-storage kmod-usb-storage-uas usbutils block-mount gdisk e2fsprogs kmod-fs-ext4
```
then confirmed the USB drivers were doing their thing
```shell
root@OpenWrt:~# lsusb -t
/:  Bus 02.Port 1: Dev 1, Class=root_hub, Driver=ohci-platform/2p, 12M
/:  Bus 01.Port 1: Dev 1, Class=root_hub, Driver=ehci-platform/2p, 480M
    |__ Port 1: Dev 2, If 0, Class=Mass Storage, Driver=usb-storage, 480M
```
and confirmed the hard disk was detected
```shell
root@OpenWrt:~# ls -l /dev/sd*
brw-------    1 root     root        8,   0 Apr 28 20:15 /dev/sda
brw-------    1 root     root        8,   1 Apr 28 20:15 /dev/sda1
```
### Partition
Next I interactively formatted the disk with `gdisk /dev/sda` {{< arch "GPT_fdisk" >}}, using 
* `o` to create a new empty [GPT](https://en.wikipedia.org/wiki/GUID_Partition_Table),
* `n` to add a new partition,
* and `w` to write the changes to the disk

### Format
I then formatted the new partition as `ext4`
```shell
mkfs.ext4 /dev/sda1
```
### fstab
All that was left to do was to set up automounting by editing [`fstab`](https://en.wikipedia.org/wiki/Fstab).
OpenWrt provides [tooling](https://openwrt.org/docs/guide-user/base-system/uci) to make this a breeze.
I also enabled file system checking on boot.
```shell
block detect | uci import fstab
uci set fstab.@mount[-1].enabled='1'
uci set fstab.@global[0].check_fs='1'
uci commit fstab
```
then reboot.

> Changes to `fstab` can be applied without rebooting by running
> ```shell
> /etc/init.d/fstab boot
> ```

### Check
I checked everything was in order by accessing the mounted drive
```shell
ls -l /mnt/sda1
```
and checking its info
```shell
root@OpenWrt:~# block info
...
/dev/sda1: UUID="dfa823c1-43b8-4078-a0eb-6720040ef4fd" VERSION="1.0" MOUNT="/mnt/sda1" TYPE="ext4"
```

## Services

### Limitations on embedded hardware
When installing packages on embedded hardware, one must keep in mind the limitations of the root storage and the memory.
According to `df -h`, my router had 11.5 [MiB]({{< ref "jargon#binary" >}}) of root storage, which saw 72% usage after the following installations.
`free` reported 58.1 MiB of RAM, with 79% used during a spot check.

### Idling hard disk
For more economical running of the hard disk, I set it up to [idle when not in use](https://openwrt.org/docs/guide-user/storage/usb-drives#optionalidle_spindown_timeout_on_disks_for_nas_usage).
I installed the hard disk idle LuCI app
```shell
opkg update
opkg install luci-app-hd-idle
```
and reboot then configured the service in the web interface at `Services > HDD Idle`.

### Ad-blocking
OpenWrt services can be installed for [ad-blocking](https://openwrt.org/docs/guide-user/services/ad-blocking).
I opted for the package [adblock](https://github.com/openwrt/packages/blob/master/net/adblock/files/README.md), a domain blocker (lighter but similar to [`pihole`]({{< ref "pihole" >}})), installed by
```shell
opkg update
opkg install curl adblock luci-app-adblock
```
then rebooting.
I used `curl` as the download utility to support downloading blocklists over HTTPS.

The service can be configured in LuCI at `Services > Adblock`.
I did not change the default settings, fearing adding any further blocklist sources would [OOM](https://en.wikipedia.org/wiki/Out_of_memory) the router.

### Samba
There are plenty of options for [supporting network attached storage in OpenWrt](https://openwrt.org/docs/guide-user/services/nas/start).
I initially set up [NFS](https://openwrt.org/docs/guide-user/services/nas/nfs.server), but then had to ditch it when I had difficulties setting up user friendly access on Mum's old Mac.
I settled on [Samba](https://openwrt.org/docs/guide-user/services/nas/cifs.server).

I installed Samba with
```shell
opkg update
opkg install samba4-server luci-app-samba
```
then rebooted and configured in LuCI at `Services > Network Shares` for the attached hard disk to be shared.

#### Clients
The Samba shared then popped up in Finder on the old Mac and was accessible through some app on the iPad.

On [Dad's laptop]({{< ref "x220t" >}}), I had to install the packages [`cifs-utils`](https://packages.debian.org/buster/cifs-utils) and [`gvfs-backends`](https://packages.debian.org/buster/gvfs-backends) to [access the Samba share](https://wiki.archlinux.org/index.php/Thunar#Using_Thunar_to_browse_remote_locations) through xfce's [Thunar](https://en.wikipedia.org/wiki/Thunar)
```shell
apt install cifs-utils gvfs-backends
```

## Conclusion
All in all, I'm highly impressed with what OpenWrt can do with this old router!

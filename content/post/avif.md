+++
title = "AVIF images for the web"
date = 2022-10-02T19:50:09+02:00
categories = ["web"]
tags = ["avif","gimp","imagemagick","image","flatpak","flathub","av1","format","file-format","compression","lossy-compression","fedora","fedora-36","git","install","responsive","html","bash","dnf","autotools","photograph","picture","xcf","desktop","laptop","image-processing","linux","jpg","jpeg","png","webp"]
description = "Small and responsive images"
toc = true
draft = false
cover = "img/covers/28.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## AVIF

[AVIF]({{< ref "exporting-photographs-for-web-with-gimp#format" >}}) is an image file format using AV1 compression.
It is currently the most size-efficient choice of image format for lossy compression and is supported by modern browsers.

## GIMP

I like to use [GIMP]({{< ref "exporting-photographs-for-web-with-gimp#gimp" >}}) to edit my images.
[GIMP has supported AVIF since version 2.10.22](https://www.gimp.org/news/2020/10/07/gimp-2-10-22-released/).
Unfortunately, the Fedora 36 package for GIMP is currently not compiled with AVIF support.

However, we can use the [`flatpak`]({{< ref "software-deploy#flatpak" >}}) version of [GIMP from Flathub](https://flathub.org/apps/details/org.gimp.GIMP), which is compiled with AVIF support.
Install with
```shell
flatpak install flathub org.gimp.GIMP
flatpak run org.gimp.GIMP
```
Then we can export images in the AVIF format.

## ImageMagick

I like to use [ImageMagick]({{< ref "exporting-photographs-for-web-with-gimp#imagemagick" >}}) for command line image manipulation.

Fedora 36 currently provides ImageMagick version 6 {{< pl "https://packages.fedoraproject.org/pkgs/ImageMagick/ImageMagick/" >}}, which does not support AVIF.

I built ImageMagick version 7 on Fedora 36 from the latest Git source with AVIF support, as well as support for PNG (which also requires XML), JPEG, and WebP formats.
```shell
sudo dnf install -y libxml2-devel libpng-devel libjpeg-turbo-devel libwebp-devel
sudo dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install -y libheif-devel
cd ~/git
git clone https://github.com/ImageMagick/ImageMagick.git
cd ImageMagick/
mkdir build
cd build
../configure
make -j`nproc`
sudo make install
```
We can convert, for example, PNG images to AVIF with
```shell
for f in *.png; do
    magick $f -quality 50 ${f%.png}.avif
done
```

## Responsive images

This is unrelated to the image format in use.

We can use [responsive images](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images) to provide an image with several resolution options.
The browser chooses the best one depending on the circumstances.
The HTML syntax is like
```html
<img srcset="image-3840w.avif 3840w, image-1920w.avif 1920w">
```

I generated images of different resolutions with ImageMagick [like this](https://gitlab.com/eidoom/recipes/-/blob/main/photo.bash)
```shell
for w in 3840 1920 960 480; do
	magick $input -resize $w -quality 25 static/img/$output-w$w.avif
done
```

I have a demonstration of responsive images [here](https://eidoom.gitlab.io/web-images) ([source](https://gitlab.com/eidoom/web-images)).

+++
title = "Software packaging and deployment systems"
date = 2021-01-13
categories = ["environment-core"]
tags = ["linux","desktop","server","laptop","fedora","flatpak","snap","install","environment","nix","package-manager"]
description = "Apps for all distributions"
toc = true
cover = "img/covers/13.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## A new channel for packages

For software which is unavailable or out-of-date in the official Fedora repositories (or in [the RPMFusion repos]({{< ref "#rpmfusion" >}}) or [coprs](https://copr.fedorainfracloud.org/)), often the easiest[^1] way to obtain it is through a software packaging and deployment system.
This modern distro-agnostic channel for software distribution is gaining popularity and, I suspect, will only become more ubiquitous---although there's also a lot of resistance in the community against it.

## Flatpak

Fedora comes with [Flatpak](https://flatpak.org/) {{< wiki Flatpak >}} {{< arch "Flatpak" >}} installed by default.
To get it up and running, it must be linked to a remote repository.
[Flathub](https://flathub.org/home) is the most popular store.
[Enable it](https://flatpak.org/setup/Fedora/) with
```shell
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
```
The interface is intuitive, with `flatpak install <repository (optional)> <package>`, `flatpak search ...`, `flatpak remove ...`, `flatpak list`, and `flatpak update`.
`flatpak remove --unused` can also be handy to free up disk space.

## Snap

Another choice is Canonical's [Snap](https://snapcraft.io/) {{< wiki "Snap_(package_manager)" >}} {{< arch "Snap" >}}.
It's [installed](https://snapcraft.io/install/authy/fedora) with
```shell
sudo dnf install snapd
sudo reboot
```
To enable compatibility with older [classic snaps](https://wiki.archlinux.org/index.php/Snap#Classic_snaps), 
```shell
sudo ln -s /var/lib/snapd/snap /snap
```
The interface is very similar to `flatpak`, except you update with `snap refresh` and it doesn't have a CLI `autoremove` ([alternatives](https://askubuntu.com/questions/1036633/how-to-remove-disabled-unused-snap-packages-with-a-single-line-of-command)).

## Which to use?

Flatpak is the superior philosophical choice as it can work through any remote, while Snap only supports distribution through [Canonical's](https://en.wikipedia.org/wiki/Canonical_(company)) store.
However, in practice I find the software support of Snap for development tools to be better.
So, I use both: Flatpak when available, Snap otherwise.

[Nix](https://en.wikipedia.org/wiki/Nix_package_manager) is another option I'd like to explore some time.

> {{< cal "2021-10-15" >}}
>
> I did so [here]({{< ref "nix" >}}).

[^1]: I.e. when you can't be bothered to compile the source code and keep it up to date yourself.

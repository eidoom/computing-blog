+++
title = "Building Android for markw (FAIL)"
date = 2020-09-06T07:49:25+01:00
edit = 2021-04-05
categories = ["smartphone"]
tags = ["duct-tape","phone","mobile","custom-rom","android","kernel","lineageos","build","xiaomi","redmi-4-prime","redmi-4-pro","fedora","environment","operating-system"]
description = "Compiling LineageOS for the officially unsupported Xiaomi Redmi 4 Prime"
toc = true
cover = "img/covers/69.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

I currently use [custom ROMs on my phone]({{< ref "custom-rom" >}}) which are maintained by kind internet strangers.
This phone, the Xiaomi Redmi 4 Prime or `markw`, is not officially supported by [the big custom Android distribution projects](https://en.wikipedia.org/wiki/List_of_custom_Android_distributions), but there is support from [smaller](https://forum.xda-developers.com/redmi-4-prime/development/rom-havoc-os-v2-5-pie-t3931286) [distributions](https://download.mokeedev.com/markw.html) and [unofficial](https://store.kde.org/p/1330188/) [builds](https://forum.xda-developers.com/redmi-4-prime/development/rom-crdroid-6-0-t4020259) on the [4PDA](https://4pda.ru/forum/index.php?showtopic=794653) and [XDA](https://forum.xda-developers.com/redmi-4-prime/development) forums.

I decided to try compiling Android myself to see what the process entailed, going with [LineageOS](https://www.lineageos.org/) {{< ghub "LineageOS" "android" >}} {{< wiki LineageOS >}}, which I'll refer to as LOS, as the distribution due to its overwhelming popularity.
I'll use the latest release, `17.1`.

## Documentation
I started by perusing the [AOSP documentation](https://source.android.com/setup) and a [LOS build guide for a similar phone with the same processor](https://wiki.lineageos.org/devices/mido/build) (the `mido`/Xiaomi Redmi Note 4X, which shares the `msm8953`/Qualcomm Snapdragon 625).
It seems building AOSP or LOS is very easy for a supported device, and developing support for a new device is well beyond my capabilities.
Therefore, I will attempt to piece together other people's open-source work to build a custom LOS for `markw`.

## Finding source code
The source for this custom Android distribution is comprised of four pieces:
* LOS source
* device specific configuration (`device`)
* kernel (`kernel`)
* proprietary blobs (`vendor`)

The LOS source is [easily obtained]({{< ref "#los-source" >}}), agnostic of phone model, and I'm not going to edit it for this project.
The latter three components are device specific and there exists no official channel providing them for LOS 17.1.
Xiaomi released [their kernel](https://github.com/MiCode/Xiaomi_Kernel_OpenSource/tree/markw-m-oss), but that's out of date now.

I had a look at [the GitHub repos of the author](https://github.com/redispade?tab=repositories) of [the ROM I'm currently using](https://forum.xda-developers.com/redmi-4-prime/development/rom-crdroid-6-0-t4020259), an unofficial build of [crDroid](https://github.com/crdroidandroid/android), which is based on LOS.
I also searched GitHub for [`markw`](https://github.com/search?o=desc&q=android_kernel_xiaomi_markw&s=updated&type=Repositories) and [`msm8953`](https://github.com/search?q=android_kernel_xiaomi_msm8953) kernels.
All this led me to [the repos of another developer](https://github.com/ShihabZzz?tab=repositories) who had recently released a [LOS build](https://4pda.ru/forum/index.php?showtopic=794653&st=27800#entry94491466), [ShihabZzz](https://github.com/ShihabZzz).
There, I found sources for [`device`](https://github.com/ShihabZzz/android_device_xiaomi_markw/tree/lineage-17.1), [`kernel`](https://github.com/ShihabZzz/android_kernel_xiaomi_markw/tree/lineage-17.1), and [`vendor`](https://github.com/ShihabZzz/proprietary_vendor_xiaomi_markw/tree/ten).

<!-- ## Other finds -->
<!-- SonicBSV https://web.telegram.org/#/im?p=@CustomRomsBySonicBSV -->
<!-- https://github.com/SonicBSV/android_device_xiaomi_markw -->
<!-- https://github.com/SonicBSV/android_kernel_xiaomi_markw -->
<!-- https://github.com/SonicBSV/android_vendor_xiaomi_markw -->

<!-- https://customroms.net/faq/caf-vs-aosp-los-roms/ -->
<!-- https://www.codeaurora.org/ -->

<!-- https://www.xda-developers.com/project-treble-custom-rom-development/ -->

## Dependencies
<!-- g++-multilib gcc-multilib lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5 libncurses5-dev libsdl1.2-dev libssl-dev libxml2 libxml2-utils xsltproc zlib1g-dev -->
The dependencies in the [docs](https://wiki.lineageos.org/devices/mido/build#install-the-build-packages) were listed for Ubuntu, while I used Fedora.
The [Fedora Android development docs](https://fedoraproject.org/wiki/HOWTO_Setup_Android_Development#Compiling_Android_from_Source) suggested some appropriate dependencies.
I installed all the packages I could find from these lists:
```shell
sudo dnf install adb bc ccache curl gcc gcc-c++ git gnupg gperf fastboot flex bison glibc-devel.{x86_64,i686} zlib-devel.{x86_64,i686} ncurses-devel.i686 readline-devel.i686 perl-Switch java-latest-openjdk glibc.i686 libstdc++.i686 libX11-devel.i686 libXrender.i686 libXrandr.i686 ImageMagick lzop pngcrush rsync zip schedtool squashfs-tools
```
It's possible this list is incomplete (I may have had some necessary packages already installed) and contains redundant packages.

## Directories
I [set up directories](https://wiki.lineageos.org/devices/mido/build#create-the-directories) for the build.
```shell
mkdir -p ~/bin
mkdir -p ~/android/lineage
```

## repo
Instead of using `git` directly, AOSP have a tool [`repo`](https://source.android.com/setup/develop#repo).
I [installed](https://wiki.lineageos.org/devices/mido/build#install-the-repo-command) it:
```shell
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
```
and ensured that `~/bin` was in `PATH`.

## LOS source
I [downloaded](https://wiki.lineageos.org/devices/mido/build#initialize-the-lineageos-source-repository) LOS 17.1 using `repo`:
```shell
cd ~/android/lineage
repo init -u git://github.com/LineageOS/android.git -b lineage-17.1
repo sync
source build/envsetup.sh
```
This was a whopping 91 GB!
I might try to use the `--depth 1` option from `git` if I do that again.

## Device specific configuration
```shell
mkdir -p ~/android/lineage/device/xiaomi
cd ~/android/lineage/device/xiaomi
git clone -b lineage-17.1 https://github.com/ShihabZzz/android_device_xiaomi_markw markw
```

## Proprietary blobs
```shell
cd ~/android/lineage/vendor
git clone -b ten https://github.com/ShihabZzz/proprietary_vendor_xiaomi_markw xiaomi
```

## Kernel
```shell
mkdir -p ~/android/lineage/kernel/xiaomi
cd -p ~/android/lineage/kernel/xiaomi
git clone -b lineage-17.1 https://github.com/ShihabZzz/android_kernel_xiaomi_markw markw
```

## ccache
I installed and set up [`ccache`](https://ccache.dev/) {{< wiki Ccache >}} {{< ghub "ccache" "ccache" >}} to speed up recompilation:
```shell
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
ccache -M 100G
```

## Build
```shell
cd ~/android/lineage
breakfast markw
brunch markw
```
This took four hours and failed.

## Conclusion

Next time I'll get a phone with official LineageOS support. <i class="fas fa-grin-beam-sweat"></i>

<!-- ## Future -->
<!-- microg -->
<!-- https://microg.org/ -->
<!-- https://en.wikipedia.org/wiki/MicroG -->
<!-- https://github.com/microg -->
<!-- https://f-droid.org/en/packages/com.github.kiliakin.yalpstore/ -->

+++
title = "Linux on the XPS 15 9560"
date = 2020-01-13T19:13:15Z
edit = 2021-04-05
categories = ["maintenance"]
tags = ["linux","laptop","xps","fedora","install","configure","scp","nvidia","gpu","grub","gnome","xorg","tlp","fwupd","geforce","gtx-1050m","ssh","negativo17","rpmfusion"]
description = "Getting Fedora working on the Dell XPS 15 9560 laptop"
toc = true
cover = "img/covers/85.avif"
coveralt = "stable-diffusion-v1-4+RealESRGAN_x4plus+ImageMagick"
+++

## Introduction

Following in my [series of PhD laptops]({{< ref "install-fedora#hardware" >}}), I'm now using a Dell XPS 15 9560.
I installed and configured Fedora on it much as [before]({{< ref "install-fedora" >}}), with some extra model-specific steps.

## Identification

I received the laptop second-hand, so initially didn't know the model.
I identified the laptop by reading the serial number from the service tag on the bottom under a plate, then entering this number on the [Dell website](https://www.dell.com/support/home/uk/en/ukbsdt1/).

## First hurdle

On booting (`F12` [again]({{< ref "install-fedora#starting-the-install" >}}) for boot menu) the Fedora install medium, it hangs before starting GNOME, failing in [kernel modesetting](https://wiki.archlinux.org/index.php/Kernel_mode_setting#Late_KMS_start).
To get around this, add `nomodeset` to GRUB options to [disable modesetting](https://wiki.archlinux.org/index.php/Kernel_mode_setting#Disabling_modesetting).
This can be done the same way we added `3` to GRUB [previously]({{< ref "broken#changing-grub-settings-on-boot" >}}).

## Partitioning

This time, I used the default partitioning scheme.
This uses [Logical Volume Manager (LVM)](https://wiki.archlinux.org/index.php/LVM) with `ext4` partitions.
I usually use a single `ext4` partition containing `root` and `home` directories, and a [`swapfile`](https://wiki.archlinux.org/index.php/Swap#Swap_file).
Although it is more complicated and so goes against the [KISS](https://wiki.archlinux.org/index.php/Arch_terminology#KISS) principle of my usual setup, this LVM solution a worthy advantage:
the operating system, on `root`, can be reinstalled without affecting `home` since they're on different paritions.
I'd avoid this with normal, statically-sized partitions, but these logical partitions can be resized on the fly.
The logical `swap` partition can be resized just like the `swapfile`, so nothing lost there.

## Firmware update

I updated the firmware on the laptop with
```shell
sudo dnf install fwupd
fwupdmgr refresh
fwupdmgr get-updates
sudo fwupdmgr update
```
The laptop rebooted into a firmware-update mode, took some time to do its thing, then rebooted normally.

<!-- ## Notes -->
<!-- Middle click on trackpad with three fingers -->

<!-- https://wiki.archlinux.org/index.php/Dell_XPS_15_9560 -->
<!-- also  https://wiki.archlinux.org/index.php/Dell_XPS_15 -->

## Temperature monitoring

This laptop runs quite hot compared to my others.
I installed
```shell
sudo dnf install lm_sensors
```
to check the temperature with
```shell
sensors
```

## Graphics card

### Identification

I confirmed that the laptop has an Nvidia graphics card with
```shell
lspci | grep 3D
```
```vim
01:00.0 3D controller: NVIDIA Corporation GP107M [GeForce GTX 1050 Mobile] (rev a1)
```

### Installation

Having [installed Nvidia drivers before on Fedora]({{< ref "nvidia-drivers-on-fedora" >}}), I did this again.
The proprietary drivers were used to attain the best performance and power management.
Notice that I use `VGA` to [find]({{< ref "#identification" >}}) the card on my desktop and `3D` on the laptop.
The install removes `nomodeset` from the boot options since [that](https://askubuntu.com/a/207177/827359) will now work.
I leave the Nouveau drivers `xorg-x11-drv-nouveau` installed, but not as the kernel driver in use, so we can [switch](https://rpmfusion.org/Howto/NVIDIA?highlight=%28%5CbCategoryHowto%5Cb%29#Switching_between_nouveau.2Fnvidia) back to it in case of emergency.

> {{< cal "2022-01-29" >}}
>
> DEPRECATED: `negativo17`
>
> Nvidia drivers really are a mess on Linux.
> [On my desktop, I'm using the RPM Fusion package of the proprietary Nvidia drivers now]({{< ref "linux-video#update-on-drivers" >}}).
> The i/dGPU switching stopped working for the laptop and I can't figure out a nice solution without a fresh installation, so I'm not using the graphics card on Linux on my laptop just now.
> I use the Windows partition for anything graphics instead.

> {{< cal "2022-10-31" >}}
>
> [I've now installed the RPM Fusion Nvidia drivers on my laptop too so that I can do GPGPU compute]({{< ref "futhark#nvidia-drivers" >}}) with [CUDA](https://rpmfusion.org/Howto/NVIDIA?highlight=%28%5CbCategoryHowto%5Cb%29#CUDA) or OpenCL.
> I've left the iGPU permanently in charge of graphics rendering though.

<!-- https://docs.fedoraproject.org/en-US/quick-docs/bumblebee/ -->
<!-- https://negativo17.org/nvidia-driver/#Optimus_laptops -->
<!-- https://wiki.archlinux.org/index.php/Dell_XPS_15_9560#Proprietary_driver_with_PRIME_output_offloading -->
<!-- https://wiki.archlinux.org/index.php/NVIDIA_Optimus -->
<!-- https://wiki.archlinux.org/index.php/PRIME#PRIME_render_offload -->

### Confirmation

I checked that the new driver was in use with
```shell
lspci -v | grep 3D -A 10
```
```vim
01:00.0 3D controller: NVIDIA Corporation GP107M [GeForce GTX 1050 Mobile] (rev a1)
        Subsystem: Dell Device 07be
        Flags: bus master, fast devsel, latency 0, IRQ 143
        Memory at ec000000 (32-bit, non-prefetchable) [size=16M]
        Memory at c0000000 (64-bit, prefetchable) [size=256M]
        Memory at d0000000 (64-bit, prefetchable) [size=32M]
        I/O ports at e000 [size=128]
        [virtual] Expansion ROM at ed000000 [disabled] [size=512K]
        Capabilities: <access denied>
        Kernel driver in use: nvidia
        Kernel modules: nouveau, nvidia_drm, nvidia
```
Indeed, `nvidia` is the kernel driver in use.

### Xorg

The `nvidia` driver only supports [Xorg](https://wiki.archlinux.org/index.php/Xorg), not GNOME's default compositor [Wayland](https://wiki.archlinux.org/index.php/Wayland), so be sure to select GNOME on Xorg in the GNOME log in options.

I checked which display server was in use with
```shell
loginctl
```
```vim
SESSION  UID USER SEAT  TTY 
      2 1000 ryan seat0 tty2
     c1   42 gdm  seat0 tty1

2 sessions listed.
```
to get the `SESSION` id of `2` then
```shell
loginctl show-session 2 -p Type
```
```shell
Type=x11
```
which confirmed that Xorg was in use.

> {{< cal "2022-10-31" >}}
>
> [Since F35 with Nvidia driver 495 and Gnome 41, Wayland is supported.](https://rpmfusion.org/Howto/NVIDIA?highlight=%28%5CbCategoryHowto%5Cb%29#Wayland)

### Fixed bugs

Before installing the graphics driver, I noticed some buggy behaviour:

* I could not change the brightness of the screen. The keyboard buttons had no effect and GNOME showed no options to do this.
* After suspend, the screen would not come back on.

Installing the graphics driver fixed these issues.

### Using the discrete card for rendering

> Disclaimer: this is only tested on Fedora 32.

Having the software installed, actually using the discrete graphics processing unit (dGPU) still involved some hassle.

The built-in screen is connected to the integrated GPU (iGPU) only, so [the dGPU can be used for rendering but then it must pass the output to the iGPU, which displays it](https://en.wikipedia.org/wiki/Nvidia_Optimus).
Ideally, the iGPU would be used for light workloads and the dGPU would only be switched to for intensive workloads.
This would yield lower overall power consumption and quieter operation; I've found the fans to be permanently very audible when the dGPU is on, even if it is not in use. 
However, it seems that this setup has [performance and tearing issues on Linux](https://wiki.archlinux.org/index.php/Dell_XPS_15_9560#Proprietary_driver_with_bumblebee).
The only official viable way to use the dGPU without these issues is for it to [always be on](https://wiki.archlinux.org/index.php/Dell_XPS_15_9560#Proprietary_driver_with_PRIME_output_offloading), which of course is bad news for the battery life.

#### Enabling the dGPU

I followed [Nvidia's instructions for this configuration](http://us.download.nvidia.com/XFree86/Linux-x86_64/375.66/README/randr14.html).
To `/etc/X11/xorg.conf`, which was originally empty, I added
```vim
Section "ServerLayout"
    Identifier "layout"
    Screen 0 "nvidia"
    Inactive "intel"
EndSection

Section "Device"
    Identifier "nvidia"
    Driver "nvidia"
    BusID "PCI:1:0:0"
EndSection

Section "Screen"
    Identifier "nvidia"
    Device "nvidia"
    Option "AllowEmptyInitialConfiguration"
EndSection

Section "Device"
    Identifier "intel"
    Driver "modesetting"
EndSection

Section "Screen"
    Identifier "intel"
    Device "intel"
EndSection
```
finding the `BusID` with `nvidia-settings` under `Bus ID` on the `GPU 0 - (GeForce GTX 1050)` tab.
Restarting showed that the dGPU was now in use.

#### Enabling v-sync

To eliminate the tearing, append `nvidia_drm.modeset=1` to `GRUB_CMDLINE_LINUX` in `/etc/default/grub`, then regenerate the [grub configuration](https://wiki.archlinux.org/index.php/Kernel_parameters#GRUB) with
```shell
sudo grub2-mkconfig -o /boot/efi/EFI/fedora/grub.cfg
```
just like we did [before]({{< ref "install-fedora#boot-behaviour" >}}).
This enables [PRIME synchronisation](https://devtalk.nvidia.com/default/topic/957814/linux/prime-and-prime-synchronization/1) and is active on reboot.

#### Switching between iGPU and dGPU

Since I generally use the laptop without need of the dGPU, I opted for the simple, manual approach to switching.

To switch off the dGPU, I just
```shell
sudo mv /etc/X11/xorg.conf /etc/X11/xorg.conf.bkp 
sudo reboot
```
and to switch it on again,
```shell
sudo mv /etc/X11/xorg.conf.bkp /etc/X11/xorg.conf
sudo reboot
```
There exist [unofficial automated methods](https://wiki.archlinux.org/index.php/NVIDIA_Optimus#Available_methods) at do this, but I was too lazy to set them up since I'm happy to reboot on the odd occasion I require dGPU rendering.

### Monitoring graphics usage

I used the tool [`nvtop`](https://github.com/Syllo/nvtop) to monitor graphics usage in a similar manner to `htop`.
It requires `nvidia-driver-cuda` from the [negativo17 repo]({{< ref "nvidia-drivers-on-fedora#install" >}}).
I detail its installation [here]({{< ref "nvtop" >}}).
It is invoked with `nvtop`.

## DPI scaling

Since the laptop has a [HiDPI](https://wiki.archlinux.org/index.php/HiDPI) 3840x2160 15.6" screen, I changed `GNOME Settings > Devices > Displays > 1 Build-in display > Scale` to `200%`.

This messes up external displays which are not HiDPI and I have not yet found a solution for this.

<!-- Wayland: -->
<!-- GNOME Settings > Devices > Displays > 1 Build-in display > Scale > 200% -->
<!-- External monitors set to 100% -->

<!-- Xorg: -->
<!-- ```shell -->
<!-- gsettings set org.gnome.settings-daemon.plugins.xsettings overrides "[{'Gdk/WindowScalingFactor', <2>}]" -->
<!-- gsettings set org.gnome.desktop.interface scaling-factor 2 -->
<!-- ``` -->
<!-- https://github.com/ashwinvis/xrandr-extend -->
<!-- ```shell -->
<!-- pip install xrandr-extend --user -->
<!-- ``` -->
<!-- Generate default config file -->
<!-- ```shell -->
<!-- python -m xrandr_extend.config -->
<!-- ``` -->

<!-- Edit config file -->
<!-- ```shell -->
<!-- vi ~/.config/xrandr-extend.cfg -->
<!-- ``` -->
<!-- ```vim -->
<!-- [provider:modesetting] -->
<!-- primary = eDP-1 -->
<!-- middle = DP-1 -->

<!-- [provider:nvidia-g0] -->
<!-- primary = eDP-1 -->
<!-- middle = DP-1 -->

<!-- [resolutions] -->
<!-- primary = 3840, 2160 -->
<!-- middle = 1920, 1080 -->

<!-- [scaling] -->
<!-- primary = 0.9999 -->
<!-- middle = 2 -->
<!-- ``` -->
<!-- https://wiki.archlinux.org/index.php/HiDPI#Multiple_displays -->

<!-- Get providers -->
<!-- ```shell -->
<!-- xrandr --listproviders -->
<!-- ``` -->

<!-- Get monitor names -->
<!-- ```shell -->
<!-- xrandr --listmonitors -->
<!-- ``` -->

<!-- Get resolutions -->
<!-- ```shell -->
<!-- xrandr -->
<!-- ``` -->

<!-- Run the program to apply. -->
<!-- Note that I have `export PATH=$HOME/.local/bin:$PATH` in my `~/.zshrc` so can run `pip` installed executables with the usual prefixing of `python -m `. -->
<!-- ```shell -->
<!-- xrandr-extend middle -->
<!-- ``` -->

<!-- Still got mouse flickering. -->

## PCIe bus error in system logs

I was warned of PCIe bus errors on receiving the laptop.
Checking
```shell
journalctl | grep "PCIe Bus Error" | tail -n 1
```
```shell
Jan 14 08:52:23 localhost.localdomain kernel: nvme 0000:04:00.0: AER: PCIe Bus Error: severity=Corrected, type=Physical Layer, (Receive
r ID)
```
showed that there was indeed a problem.

I followed [this](https://wiki.archlinux.org/index.php/Dell_XPS_15_9560#PCIe_Bus_Error_in_system_logs) and [this](https://unix.stackexchange.com/questions/327730/what-causes-this-pcieport-00000003-0-pcie-bus-error-aer-bad-tlp) to solve the problem:
I added the kernel boot option `pci=nommconf` to the `GRUB_CMDLINE_LINUX` line in `/etc/default/grub` and updated `grub2`, just as in [this]({{< ref "install-fedora#boot-behaviour" >}}).

## TLP

This laptop doesn't have the best battery life, so I improved things by installing [TLP](https://wiki.archlinux.org/index.php/TLP):

```shell
sudo dnf install tlp smartmontools
sudo reboot
```

I used TLP to [autosuspend the touchscreen](https://wiki.archlinux.org/index.php/Dell_XPS_15_9560#Disable/autosuspend_of_touchscreen) when it is not in use.

First, I found the ID of the touchscreen:
```shell
lsusb | grep Touchscreen
```
```shell
Bus 001 Device 003: ID 04f3:24a0 Elan Microelectronics Corp. Touchscreen
```
then I added it to [TLP's `USB_WHITELIST` after ensuring that `USB_AUTOSUSPEND` was enabled](https://linrunner.de/en/tlp/docs/tlp-configuration.html#usb):
```shell
sudo vi /etc/default/tlp
```
```vim
...
USB_AUTOSUSPEND=1
...
USB_WHITELIST="04f3:24a0"
...
```

## Moving files over

To move files over from the old laptop, I connect them both via ethernet cable to a local network and used [`scp`](https://wiki.archlinux.org/index.php/SCP_and_SFTP#Secure_copy_protocol_(SCP)).
This requires that `openssh` is installed
```shell
sudo dnf install openssh
```

The laptops have the same username, so I didn't need to specify the user.

The transfer proceeded as:

* Old laptop:
    * Start the `ssh` daemon with
    ```shell
    sudo systemctl start sshd
    ```
    * and use `hostname` or `ip a` to get the `<old_laptop_address>`

* New laptop:
    * Add public key of new laptop to old laptop with
    ```shell
    ssh-copy-id <old_laptop_address>
    ```

* Old laptop:
    * For security,
    ```shell
    sudo vi /etc/ssh/sshd_config
    ```
    and [force public key authentication only](https://wiki.archlinux.org/index.php/OpenSSH#Force_public_key_authentication) by setting
    ```vim
    PasswordAuthentication no
    ```
    * then check with
    ```shell
    sudo sshd -t
    ```
    which gives no output if all is fine.

* New laptop:
    * To [copy a directory from the old laptop to the new laptop](https://wiki.archlinux.org/index.php/SCP_and_SFTP#Linux_to_Linux),
    ```shell
    scp -v -r <old_laptop_address>:/remote/directory /local/directory/
    ```
    * On completion, `scp` gives an [exit status](https://stackoverflow.com/a/921244). 
    `0` means it was successful.

    > Alternatively, use [`rsync`]({{< ref "utilities#incremental-file-transfer-over-network" >}})

## Disabling the GNOME on-screen keyboard

The GNOME on-screen keyboard, `caribou`, is not required on this laptop as the physical laptop keyboard is always available (and I usually use an [external keyboard]({{< ref "split-keyboard" >}}) anyway).
Moreover, it's annoying when touchscreen usage causes it to pop up and it's [not easily disabled](https://bugzilla.gnome.org/show_bug.cgi?id=742246#c39).
On Fedora 32 (GNOME 3.36.4), the only way I could find to deactivate this behaviour was an [extension]({{< ref "install-fedora#gnome-extensions" >}}): [Block Caribou 36](https://extensions.gnome.org/extension/3222/block-caribou-36/) {{< ghub "lxylxy123456" "cariboublocker" >}}.
